﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;

public class AdsManager : MonoBehaviour {

	private static AdsManager instance = null;

	public static AdsManager Instance {
		get {
			if (instance == null)
				instance = FindObjectOfType (typeof(AdsManager)) as AdsManager;
			
			return instance;
		}
	}

	void Awake ()
	{
		instance = this;
	}

	public void ShowAd(){
		if(Advertisement.IsReady()){
			Debug.Log("showing ad");
			Advertisement.Show();
		}
		else{
			Debug.Log("ad was not ready");
		}
	}
}
