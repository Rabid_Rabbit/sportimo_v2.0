﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

#if UNITY_IOS
public class DeviceUtilities : MonoBehaviour {
	
	//
	//private methods
	//

	[DllImport("__Internal")]
	private static extern string _GetCarrierName();
	
	// Returns the locale currently set on the device
	public static string GetCarrierName()
	{
		if( Application.platform == RuntimePlatform.IPhonePlayer )
			return _GetCarrierName();
		return "en";
	}

	[DllImport("__Internal")]
	private static extern string _GetCountryCode();
	
	// Returns the locale currently set on the device
	public static string GetCountryCode()
	{
		if( Application.platform == RuntimePlatform.IPhonePlayer )
			return _GetCountryCode();
		return "en";
	}

	[DllImport("__Internal")]
	private static extern string _GetCarrierCode();
	
	// Returns the locale currently set on the device
	public static string GetCarrierCode()
	{
		if( Application.platform == RuntimePlatform.IPhonePlayer )
			return _GetCarrierCode();
		return "en";
	}

}
#endif