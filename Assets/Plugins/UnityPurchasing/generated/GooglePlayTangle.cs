#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("vhTmA2zZbJPo54P4nLaNFh1OSJzVVlhXZ9VWXVXVVlZX5jvbotheN1zxizwCLYi/GTtClMVJSnh8ghhmMeErTiHCgE6hqkGevcmg9QkL/RXAGJIDoYg75rSDA191FZ0rEHiacBvXDQduFKz5GkdzqJNsfK403H78AaJJ5shj9zg3t+yOAjcJCZ2zvgs5gj9N6U4inFEJt8xojSC7U21Mbei7eZxotK2IVsyhvxDratQvPbgFZ9VWdWdaUV590R/RoFpWVlZSV1SRHe0mgktYnJb3pHo6Eo3gg1kd9A4HZporC4QWSf0w3G/5BB1klDmk/VdqQPL8nx9jPNq45BXT5TP08ar+UK6GGs6b3mlxYQzhGRwkxTnK/6UlumgWLC4A+FVUVldW");
        private static int[] order = new int[] { 8,1,4,4,13,9,10,11,13,13,10,12,13,13,14 };
        private static int key = 87;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
