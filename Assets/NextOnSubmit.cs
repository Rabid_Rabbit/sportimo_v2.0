﻿using UnityEngine;
using System.Collections;

public class NextOnSubmit : MonoBehaviour {

	public static string lastClickedInput;

	public void OnClick(){
		Debug.Log (name);
		lastClickedInput = name;                                                                                                                                                                      
	}

	public void onSubmit (UIInput go){

	
//		#if !UNITY_EDITOR                                
		if (lastClickedInput != name)
			return;
//		#endif

		go.isSelected = true;
		UICamera.hoveredObject = go.gameObject;
		lastClickedInput = go.name;
	}

}
