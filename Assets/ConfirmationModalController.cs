﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using I2.Loc;

public class ConfirmationModalController : MonoBehaviour {

	public UILabel main_text;
	public UILabel info_text;
	public UILabel option1text;
	public UILabel option2text;

    public GameObject Buttons;
    public GameObject AltButtons;

    public UILabel altoption1text;
    public UILabel altoption2text;

    public UIWidget confirmationWidget;

	private Action<string> ConfirmationCallback;

	private static ConfirmationModalController _instance;

	public static bool ConfirmationIsActive = false;


	void Awake(){
		_instance = this;
	}

	/// <summary>
	/// Confirm uses terms that will be localized. Useful for pre-defined confirmation popups.
	/// </summary>
	public static void Confirm(string textTerm, string infoTerm, string setOption1term = null, string setOption2term = null, Action<string> callback = null, bool useAltButtons = false){

        if (useAltButtons)
        {
            _instance.Buttons.SetActive(false);
            _instance.AltButtons.SetActive(true);
        }
        else
        {
            _instance.Buttons.SetActive(true);
            _instance.AltButtons.SetActive(false);
        }


		ConfirmationIsActive = true;

		Main.onViewChange += _instance.closeModalOnViewChange;

		_instance.main_text.text = textTerm;
		_instance.info_text.text = infoTerm;

        if (!string.IsNullOrEmpty(setOption1term))
        {
            _instance.option1text.text = setOption1term;
            _instance.altoption1text.text = setOption1term;
        }
        else
        {
            _instance.option1text.text = LocalizationManager.GetTermTranslation("OK", true, 0, true);
            _instance.altoption1text.text = LocalizationManager.GetTermTranslation("OK", true, 0, true);
        }
        if (!string.IsNullOrEmpty(setOption2term))
        {
            _instance.option2text.text = setOption2term;
            _instance.altoption2text.text = setOption2term;
        }
        else
        {
            _instance.option2text.text = LocalizationManager.GetTermTranslation("cancel", true, 0, true);
            _instance.altoption2text.text = LocalizationManager.GetTermTranslation("cancel", true, 0, true);
        }

            _instance.ConfirmationCallback = callback;

		_instance.confirmationWidget.alpha = 1;

	}

	/// <summary>
	/// Ask uses supplied localization objects. It populates the modal based on user's locale selection or fallback to default language.
	/// Useful for dynamic questions or quizes.
	/// </summary>
	public static void Ask(Dictionary<string,string> mainText, Dictionary<string,string> infoText, Dictionary<string,string> setOption1Text = null, Dictionary<string,string> setOption2Text = null, Action<string> callback = null){
        _instance.Buttons.SetActive(true);
        _instance.AltButtons.SetActive(false);
        ConfirmationIsActive = true;

		Main.onViewChange += _instance.closeModalOnViewChange;

		_instance.main_text.text = BedBugTools.GetStringByLocPrefs (mainText);
		_instance.info_text.text = BedBugTools.GetStringByLocPrefs (infoText);
		
		if (setOption1Text != null)
			_instance.option1text.text = BedBugTools.GetStringByLocPrefs (setOption1Text);
		
		if (setOption2Text != null)
			_instance.option2text.text = BedBugTools.GetStringByLocPrefs (setOption2Text);
		
		_instance.ConfirmationCallback = callback;
		
		_instance.confirmationWidget.alpha = 1;
		
	}

	public void optionSelected(string option){
		confirmationWidget.alpha = 0;
		Main.onViewChange -= _instance.closeModalOnViewChange;

		ConfirmationIsActive = false;

		if (ConfirmationCallback != null) {
			ConfirmationCallback(option);
		}
	}

	private void closeModalOnViewChange ()
	{
		confirmationWidget.alpha = 0;
		Main.onViewChange -= _instance.closeModalOnViewChange;

		ConfirmationIsActive = false;

		if (ConfirmationCallback != null) {
			ConfirmationCallback("Btn3");
		}
	}
}
