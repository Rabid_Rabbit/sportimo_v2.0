﻿using UnityEngine;
using System.Collections;

public class BlockInput : MonoBehaviour {

	public UIWidget utilWidget;

	public static BlockInput _instance;

	void Awake(){
		_instance = this;
	}

	public static void set(bool visible){
		if (visible)
			_instance.utilWidget.alpha = 1;
		else
			_instance.utilWidget.alpha = 0;
	}
}
