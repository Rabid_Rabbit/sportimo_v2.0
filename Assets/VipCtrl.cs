﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class VipCtrl : MonoBehaviour {

	public UICenterOnChild vipCenter;
	public Transform firstSlide;


	void Start() {
		StartCoroutine (centerDelay ());
	}

	public void centerOn(GameObject child) {
		vipCenter.CenterOn (child.transform);
	}

	public void centerOnFirst() {
		
		vipCenter.CenterOn (firstSlide);
	}

	IEnumerator centerDelay() {
		yield return new WaitForSeconds (0.1f);
		vipCenter.CenterOn (firstSlide);
	}

	public void closeVip() {
		NGUITools.SetActive (gameObject, false);
	}

	public void openSubscription() {
        closeVip();
        DummySubscribe.show();
//#if UNITY_ANDROID
//        ConfirmationModalController.Confirm(I2.Loc.LocalizationManager.GetTermTranslation("_subscribe_by_text", true, 23), null, I2.Loc.LocalizationManager.GetTermTranslation("_subscribe_mobile", true, 23), I2.Loc.LocalizationManager.GetTermTranslation("_subscribe_google", true, 23), (userSelection) =>
//        {
//            if (userSelection == "Btn1")
//            {
//                Debug.Log("mobile");
//                UnityEngine.SceneManagement.SceneManager.LoadScene("subscription");
//            }
//            else if (userSelection == "Btn2")
//            {
//                Debug.Log("GooglePlay Purchase");
//                StoreListener.Instance.InitiatePurchase("com.gup.basicweekly");
//            }
//        }, true);
//#elif UNITY_IPHONE
//                   StoreListener.Instance.InitiatePurchase("com.gup.basicweekly");
//#else
//                    confirm("purchase");
//#endif
    }

    //	void OnEnable() {
    //		vipCenter.CenterOn (firstSlide);
    //	}
}
