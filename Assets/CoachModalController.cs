﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using I2.Loc;

public class CoachModalController : MonoBehaviour {

	public UILabel main_text;
	public UILabel option1text;
	

	public UIWidget confirmationWidget;

	private Action<string> ConfirmationCallback;

	private static CoachModalController _instance;

	public static bool ConfirmationIsActive = false;

	public Animation coachAnim;

	void Awake(){
		_instance = this;
	}

	/// <summary>
	/// Confirm uses terms that will be localized. Useful for pre-defined confirmation popups.
	/// </summary>
	public static void Confirm(string textTerm,  string setOption1term = null, Action<string> callback = null){

		ConfirmationIsActive = true;

		Main.onViewChange += _instance.closeModalOnViewChange;

		_instance.main_text.text = textTerm.Replace("\\n", "\n");		

		if(!string.IsNullOrEmpty(setOption1term))
			_instance.option1text.text = setOption1term;


		_instance.ConfirmationCallback = callback;

		_instance.confirmationWidget.alpha = 1;

		_instance.StartCoroutine (_instance.playCoachAnim());

	}

	IEnumerator playCoachAnim() {
		coachAnim.Play ("CoachAppear");
		yield return new WaitForSeconds (1.1f);
		coachAnim.Play ("CoachTalk");
	}

	public void optionSelected(string option){
		confirmationWidget.alpha = 0;
		Main.onViewChange -= _instance.closeModalOnViewChange;

		ConfirmationIsActive = false;

		if (ConfirmationCallback != null) {
			ConfirmationCallback(option);
		}
	}

	private void closeModalOnViewChange ()
	{
		confirmationWidget.alpha = 0;
		Main.onViewChange -= _instance.closeModalOnViewChange;

		ConfirmationIsActive = false;

		if (ConfirmationCallback != null) {
			ConfirmationCallback("Btn2");
		}
	}
}
