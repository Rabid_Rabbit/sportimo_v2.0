using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class TutorialCtrl : MonoBehaviour {

	public Animation logo;

	public GameObject coachObj;
	public Transform bubblesTransform;
	public GameObject[] bubbles = new GameObject[6];
	public int OpenBubbleCount = 2;

	public GameObject matchBtn;
	public GameObject matchesBtn;
	public GameObject screenBtn;
	public GameObject playBtn;
	public GameObject CardRoller;
	public GameObject CardRollerR;
	RollerCardCtrl cardRollerCtrl;
	public GameObject ceneterCardBtn;
	public GameObject skipBtn;

	public GameObject multiBtn;
	public UILabel pointsLabel;

	public GameObject bgscreen;
	public GameObject menuScreen;
	public GameObject matchesScreen;
	public GameObject matchScreen;
	public GameObject cardsScreen;
	public GameObject playCardsScreen;

	public GameObject card;
	public UILabel cardTimer;
	public UISprite cardFill;
	public UILabel scoreLabel;
	public GameObject greenLabel;
	public UILabel scoreboardLAbel;

	public RollerCardCtrl rollerCardCtrl;
	public Collider rollerCol;
	private PlayCard centeredCard;

	public Animation bannerAnim;
	public bool isInMain = false;

	public Dictionary<string, string> languageStrings = new Dictionary<string, string>();
	public List<string> languages = new List<string> ();
	public UISprite flag;
	private int flagCount;
	Animation cardAnims;

	// Use this for initialization
	void Start () {

		languageStrings.Add("en", "englishFlag");
		languageStrings.Add("uae", "arabiaFlag");

		languages.Add ("en");
		languages.Add ("uae");

		//StartCoroutine( startTutorial () );
	}

	//
	// FLAGS
	//

	public void changeFlag() {
		flagCount++;
		print("Language Changed!");
		if (flagCount >= languages.Count)
			flagCount = 0;

		flag.spriteName = languageStrings [languages [flagCount]];
	}

	public void onTransitionInStart(viewData _viewdata = null){
		StartCoroutine(	startTutorial ());
	}

	IEnumerator startTutorial ()
	{
//		yield return new WaitForSeconds (0.5f);
//		logo.Play ("logoAnimation");
		yield return new WaitForSeconds (0.5f);
		openCoach ();
		yield return new WaitForSeconds (1.0f);
		//openBubble (0);
	}

	void openCoach ()
	{
		NGUITools.SetActive (coachObj, true);
	}

	public void closeCoach ()
	{
		NGUITools.SetActive (coachObj, false);
		startTutorial2 ();
	}

	public void startTutorial2 ()
	{
		// close bg screen
		NGUITools.SetActive(bgscreen, false);
		// open screen collider
		screenBtn.GetComponent<Collider>().enabled = true;
		// open menu screen
		NGUITools.SetActive(menuScreen, true);
		// open first tutorial bubble
		openBubble (2);
		//clearBubbles ();
		openBubbleAutoCout ();
	}

	public void clearBubbles() { // This is where secondary tutorials get started
//		print ("clear Bubbles");
		
		if (OpenBubbleCount == 14) { 
			// Check for Goal Card centered
			if (rollerCardCtrl.playCardCentered.int_id == 2) {
				foreach (Transform child in bubblesTransform) {
					//if(child.name.IndexOf() <= -1 )

					NGUITools.SetActive (child.gameObject, false);
				}
			}
		} else {
			foreach (Transform child in bubblesTransform) {
				//if(child.name.IndexOf() <= -1 )
				
				NGUITools.SetActive (child.gameObject, false);
			}
		}

		print (OpenBubbleCount);
		if (OpenBubbleCount == 3) { // Close Menu & open Matches Screen
			NGUITools.SetActive(matchesBtn, false);
			// close menu screen
			NGUITools.SetActive(menuScreen, false);
			// open matches screen
			NGUITools.SetActive(matchesScreen, true);
			// open screenBtn collider
			screenBtn.GetComponent<Collider>().enabled = true;
			// open next bubble
			openBubble(4);
			//OpenBubbleCount = 4;
			//openBubbleAutoCout();
		}

		if (OpenBubbleCount == 5) { // Close Matches & open Match Screen
			NGUITools.SetActive(matchBtn, false);
			// close matches screen
			NGUITools.SetActive(matchesScreen, false);
			// open match screen
			NGUITools.SetActive(matchScreen, true);
			// open screenBtn collider
			screenBtn.GetComponent<Collider>().enabled = true;
			// open next bubble
			openBubble(6);
		}

		if (OpenBubbleCount == 10) { // Close Match & open Play Screen
			NGUITools.SetActive(playBtn, false);
			// close matches screen
			NGUITools.SetActive(matchScreen, false);
			// open match screen
			NGUITools.SetActive(playCardsScreen, true);
			// open screenBtn collider
			screenBtn.GetComponent<Collider>().enabled = true;
			// open next bubble
			//openBubble(13);
			//OpenBubbleCount = 11;
			openBubbleAutoCout();
			return;
		}

		if (OpenBubbleCount == 14) { 
			// Check for Goal Card centered
			if(rollerCardCtrl.playCardCentered.int_id == 2) {

				NGUITools.SetActive(CardRoller, false);
				NGUITools.SetActive(CardRollerR, false);
				NGUITools.SetActive(ceneterCardBtn, true);
				// close matches screen
				//NGUITools.SetActive(matchesScreen, false);
				// open match screen
				//NGUITools.SetActive(playCardsScreen, true);
				// open screenBtn collider
				screenBtn.GetComponent<Collider>().enabled = false;
				// open next bubble
				openBubble(15);
				NGUITools.SetActive (skipBtn, true);
				StartCoroutine( openBubbleDelayed(16, 2.0f) );
			} else {
				//return;
			}
		}

		if (OpenBubbleCount == 19) { 
			cardAnims ["cardshow1"].time = 1;
			cardAnims ["cardshow1"].speed = -1;
			cardAnims.Play ("cardshow1");
		}

		if (OpenBubbleCount == 16) { 

				NGUITools.SetActive(ceneterCardBtn, false);
				// close cardsSceen screen
				NGUITools.SetActive(playCardsScreen, false);
				// open match screen
				NGUITools.SetActive(matchScreen, true);
				// Open card
				NGUITools.SetActive(card, true);
				cardAnims = card.GetComponent<Animation> ();
				cardAnims.Play ("cardClose");

				// Start the card animation
				StartCoroutine(cardSequence(cardAnims));
				// open screenBtn collider
				screenBtn.GetComponent<Collider>().enabled = true;
				// open next bubble
				openBubble(17);
		
		}

		if (OpenBubbleCount == 21) { 
				
				// open next bubble
				openBubble(21);
				
		}


	}

	IEnumerator cardSequence (Animation cardAnims)
	{
		//Animation cardAnims = card.GetComponent<Animation> ();

		cardAnims.Play ("cardCount");
		// Timer change
		cardTimer.text = "3'";
		yield return new WaitForSeconds (1.1f);
		cardAnims.Play ("cardCount");
		// Timer change
		cardTimer.text = "2'";
		yield return new WaitForSeconds (1.1f);
		cardAnims.Play ("cardCount");
		// Timer change
		cardTimer.text = "1'";
		yield return new WaitForSeconds (1.1f);
		cardAnims.Play ("cardCount");
		// Timer change
		cardTimer.text = "0'";
		yield return new WaitForSeconds (1.1f);
		cardAnims ["cardshow1"].time = 0;
		cardAnims ["cardshow1"].speed = 1;
		cardAnims.Play ("cardshow1");
		yield return new WaitForSeconds (1.0f);
		clearBubbles ();
		//openBubbleAutoCout ();
		openBubble(19);
	}

	public void openBubbleAutoCout() {
		OpenBubbleCount++;
		// Open Next Scene
		if (OpenBubbleCount == 22) { // Final Tap

			if (!isInMain) {
				int subscribed = PlayerPrefs.GetInt("subscribed");
				PlayerPrefs.SetInt("tutorial_complete",1);

                if (subscribed < 1)
#if UNITY_5
                   SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
#else
                   Application.LoadLevel(Application.loadedLevel+1);
#endif
                else
#if UNITY_5
                    SceneManager.LoadScene("subscription");
#else
                Application.LoadLevel("subscription");
#endif
			} else {
				Main.Instance.openView("menu");
				resetTut ();
			}
			//screenBtn.GetComponent<Collider>().enabled = false;
		}

		print (OpenBubbleCount);
		if (OpenBubbleCount == 22 || OpenBubbleCount == 2)
			return;

		NGUITools.SetActive (bubbles [OpenBubbleCount-1], false);
		bubbles [OpenBubbleCount].transform.localPosition = new Vector3 (-110, 242, 0);
		NGUITools.SetActive (bubbles [OpenBubbleCount], true);

		// Open Matches Tutorial
		if (OpenBubbleCount == 3) { // Open Match Button
			openMatchesBtn ();
			screenBtn.GetComponent<Collider>().enabled = false;
		}

		// Open Match Tutorial
		if (OpenBubbleCount == 5) { // 
			openMatchBtn ();
			screenBtn.GetComponent<Collider>().enabled = false;
		}
		if (OpenBubbleCount == 7) { // 
			OpenBubbleCount = 9;
			clearBubbles();
			openBubbleAutoCout();
		}

		// Open PlayCards Tutorial
		if (OpenBubbleCount == 10) { // 
			openPlayBtn ();
			screenBtn.GetComponent<Collider>().enabled = false;

		}

		if (OpenBubbleCount == 11) { // 
			NGUITools.SetActive (skipBtn, true);
			
		}

		if (OpenBubbleCount == 12) { // 

				clearBubbles();
				bubbles [14].transform.localPosition = new Vector3 (-110, 242, 0);
				openBubble(14);

		}
		if ( OpenBubbleCount == 11) { // 
			openPlayCardRoler();
			rollerCol.enabled = false;
			cardRollerCtrl = CardRoller.GetComponent<RollerCardCtrl> ();

			if(cardRollerCtrl.cardsOpen)
			cardRollerCtrl.closeRoller ();
			//screenBtn.GetComponent<Collider>().enabled = false;
		}
		// Open PlayCards Btn
		if ( OpenBubbleCount == 14) { // 
			//openPlayCardRoler();
			//if(rollerCardCtrl.playCardCentered.int_id == 2) {
			rollerCol.enabled = true;
			screenBtn.GetComponent<Collider>().enabled = false;
			//} else
			//OpenBubbleCount--;
		}

		// Open Multi Btn
		if (OpenBubbleCount == 18) { // 
			StartCoroutine(openMultiDelay());

			screenBtn.GetComponent<Collider>().enabled = false;
		}

		if (OpenBubbleCount == 20) { // 
			NGUITools.SetActive (bubbles [OpenBubbleCount-2], false);
			bubbles [OpenBubbleCount].transform.localPosition = new Vector3 (-110, 92, 0);
			bannerAnim.Play("bannerGoal");
			//Change Scoreboard
			scoreboardLAbel.text = "0-1"; 
			//Change cardLabelColor
			NGUITools.SetActive (greenLabel, true);
			//Change Score
			scoreLabel.text = "120";
		}

		// Open Last Screen
		if (OpenBubbleCount == 21) { // 
			clearBubbles();
			//screenBtn.GetComponent<Collider>().enabled = false;
		}

		// Open Next Scene
		if (OpenBubbleCount == 22) { // 
			if (!isInMain) {
				UnityEngine.SceneManagement.SceneManager.LoadScene (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().buildIndex + 1);
				//Application.LoadLevel(Application.loadedLevel+1);
			} else {
				Main.Instance.openView ("menu");
				resetTut ();
			}
			//screenBtn.GetComponent<Collider>().enabled = false;


		}
	}

	void resetTut() {
		//clearBubbles ();
		print("reseting Tutorial");

		Animation cardAnims = card.GetComponent<Animation> ();

		cardAnims.Play("cardshow1");
		cardAnims["cardshow1"].speed = -1;
		cardAnims["cardshow1"].time = 0;

//		cardAnims ["cardshow1"].time = 1;
//		cardAnims ["cardshow1"].speed = -1;
//		cardAnims.Play ("cardshow1");
		//NGUITools.SetActive(bgscreen, true);
		NGUITools.SetActive(matchScreen, false);
		NGUITools.SetActive(card, false);
		OpenBubbleCount = 2;
		foreach (Transform child in bubblesTransform) {
			//if(child.name.IndexOf() <= -1 )

			NGUITools.SetActive (child.gameObject, false);
		}
	}

	IEnumerator openMultiDelay ()
	{
		yield return new WaitForSeconds (1.0f);
		NGUITools.SetActive( multiBtn, true);
	}

	void openPlayCardRoler ()
	{
		NGUITools.SetActive (CardRoller, true);
		NGUITools.SetActive(CardRollerR, true);

		//cardRollerCtrl = CardRoller.GetComponent<RollerCardCtrl> ().closeRoller ();
	}

	void openPlayBtn ()
	{
		NGUITools.SetActive (playBtn, true);
		NGUITools.SetActive (skipBtn, false);
	}

	void openMatchesBtn ()
	{
		NGUITools.SetActive (matchesBtn, true);
	}

	void openMatchBtn ()
	{
		NGUITools.SetActive (matchBtn, true);

		//NGUITools.SetActive (screenBtn, false);
	}

	public void openBubble(int count) {
		// sync counts
		OpenBubbleCount = count;
		// open bubble
		NGUITools.SetActive (bubbles [count], true);
	}

	public IEnumerator openBubbleDelayed(int count, float delay) {
		yield return new WaitForSeconds (delay);

		// sync counts
		OpenBubbleCount = count;
		// open bubble
		NGUITools.SetActive (bubbles [count], true);
	}

	public void multiPressed() {
		//NGUITools.SetActive( multiBtn, false);
		multiBtn.GetComponent<UIButton> ().isEnabled = false;
		pointsLabel.text = "240";
		screenBtn.GetComponent<Collider>().enabled = true;
		bannerAnim.Play ("pointsBoost");
		clearBubbles ();
		openBubble(19);
	}
 
	public void skip() {

		if (!isInMain) {

			int subscribed = PlayerPrefs.GetInt("subscribed");
			if (subscribed < 1)
                UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
            //Application.LoadLevel (Application.loadedLevel + 1);
			else
                UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
            //Application.LoadLevel ("Main");

			PlayerPrefs.SetInt ("tutorial_complete", 1);
		} else {
			Main.Instance.openView("menu");
			resetTut ();
		}


	}

}
