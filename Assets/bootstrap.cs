﻿using UnityEngine;
using System.Collections;
using BedbugRest;
using LitJson;

public class bootstrap : MonoBehaviour
{
    public GameObject updateWarning;

    //	 Use this for initialization
    void Start()
    {
        Debug.Log("Getting Sportimo Settings:");
        API.GetAll("https://sportimo.mod.bz/settings", OnSettingsReceived);
    }

    private void OnSettingsReceived(BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
    {
        Debug.Log("Settings received: " + res.StatusCode);

        if (res.IsSuccess)
        {
            Main.Settings = JsonMapper.ToObject<AppSettings>(res.DataAsText);

#if UNITY_IOS
            if(Main.Settings.ios_current_build_version > Main.ios_local_build_version){
            Debug.Log("Needs update to "+ Main.Settings.ios_current_build_version);
            updateWarning.SetActive(true);
            return;
            }
#elif UNITY_ANDROID
            if (Main.Settings.android_current_build_version > Main.android_local_build_version)
            {
                Debug.Log("Needs update to build version: " + Main.Settings.ios_current_build_version);
                updateWarning.SetActive(true);
                return;
            }
#endif
            Continue();
        }
    }

    void Continue()
    {
//        int tutorial = PlayerPrefs.GetInt("tutorial_complete");
//        if (tutorial > 0)
            UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
//        //Application.LoadLevel ("Main");
//        else
//            UnityEngine.SceneManagement.SceneManager.LoadScene("tutorial");
//        //Application.LoadLevel ("tutorial");
    }

    public void gotoStore()
    {
#if UNITY_IOS
          
            Debug.Log("Goto ios store url");
          
          
#elif UNITY_ANDROID
        Debug.Log("Goto gplay store url");
#endif
        Continue();
    }

    //	void Start() {
    //		int tutorial = PlayerPrefs.GetInt("tutorial_complete");
    //				if (tutorial > 0)
    //					Application.LoadLevel ("Main");
    //				else
    //					Application.LoadLevel ("tutorial");
    //	}



}
