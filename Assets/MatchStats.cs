﻿using UnityEngine;
using System.Collections;

public class MatchStats : MonoBehaviour {

	public UITable statsTable;
	public GameObject possessionStat;
	public UILabel team1_possesion;
	public UILabel team2_possesion;
	
	public UISprite team1_possesion_fill;
	public UISprite team2_possesion_fill;
	public UILabel team1_target;
	public UILabel team2_target;
	
	public UISprite team1_target_fill;
	public UISprite team2_target_fill;
	public UILabel team1_corner;
	public UILabel team2_corner;
	public UISprite team1_corner_fill;
	public UISprite team2_corner_fill;
	public UILabel team1_offside;
	public UILabel team2_offside;
	public UISprite team1_offside_fill;
	public UISprite team2_offside_fill;
	public UILabel team1_free;
	public UILabel team2_free;
	public UISprite team1_free_fill;
	public UISprite team2_free_fill;
	
	
	public UILabel team1_yellow;
	public UILabel team2_yellow;
	public UISprite team1_yellow_fill;
	public UISprite team2_yellow_fill;
	public UILabel team1_red;
	public UILabel team2_red;
	public UISprite team1_red_fill;
	public UISprite team2_red_fill;
	
	public UISprite[] hthsprites;
	public UILabel[] hthlabels;
	
	public Color32 Win=new Color32(49,171,100,255);
	public Color32 Draw=new Color32(72,142,224,255);
	public Color32 Loss=new Color32(207,1,27,255);
}
