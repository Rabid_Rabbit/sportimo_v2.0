﻿using UnityEngine;
using System.Collections;

public  class BannerAnimator: MonoBehaviour {

	public Animation bannerAnims;
	public UITexture bannerTex;
	public UILabel stateTitle;

	public UILabel subIn;
	public UILabel subOut;

	public  void eventPlay(string eventName) {

		bannerAnims.PlayQueued (eventName);
	}

	public  void bannerPlay() {
		// Add texture to banner

		//Play animation when loaded
		bannerAnims.Play ("banner");
	}

	public  void eventStatePlay( string eventTitle) {
		// change stete title
		stateTitle.text = eventTitle;
		//Play animation when loaded
		bannerAnims.Play ("bannerState");
	}

	public void eventSubPlay (string subInName, string subOutName) {

		subIn.text = subInName;
		subOut.text = subOutName;

		bannerAnims.Play ("bannerSub");
	}
}
