﻿using UnityEngine;
using System.Collections;
using Prime31;
using I2.Loc;

public class NewsItemCtrl : MonoBehaviour
{

	// Use this for initialization[]
	public Publication publicationData;
	public UILabel title;
	public string photoURL;
	public UITexture photoTexture;
	public UILabel date;
	public GameObject Loader;
	public UIScrollView List;
	public UIPanel Panel;


	public UITweener[] ImageTweens;

	private bool imgLoaded = false;

	void Start ()
	{
//		List = NGUITools.FindInParents<UIScrollView> (gameObject);

		List.onStoppedMoving += CheckVisible;
		StartCoroutine (CheckVisibleInOne ());
	}
	
	void OnDisable ()
	{
		List.onStoppedMoving -= CheckVisible;
	}
	
	void OnDestroy ()
	{
		List.onStoppedMoving -= CheckVisible;
	}

	public void CheckVisible ()
	{
        Debug.Log(Panel.width);
		if (!imgLoaded && Panel.IsVisible (title) && !string.IsNullOrEmpty (photoURL)) {
            photoTexture.gameObject.SetActive(false);
//			BedBugTools.loadImageOnUITextureAndCrop (photoTexture, photoURL, (int)Panel.width, 200, imageLoaded);
            BedBugTools.loadImageOnUITextureAndResize (photoTexture, photoURL, (int)Panel.width, 200, imageLoaded);

		}
	}
	
	public IEnumerator CheckVisibleInOne ()
	{
		yield return new WaitForSeconds (.3f);
		CheckVisible ();
	}
	
	void imageLoaded (UITexture obj)
	{
        
        foreach (UITweener tween in ImageTweens) {
						if( tween != null )
						tween.PlayForward();
					}
        if(photoTexture != null)
        photoTexture.gameObject.SetActive(true);
        imgLoaded = true;

		if(Loader!=null)
		Loader.SetActive (false);
	}

	public void OpenArticle ()
	{
      
	}
    
#if  UNITY_ANDROID
	UniWebView _webView;

	bool OnWebViewShouldClose (UniWebView webView)
	{
		if (webView == _webView) {
			_webView.OnLoadComplete -= OnLoadComplete;
			_webView.OnWebViewShouldClose -= OnWebViewShouldClose;
			_webView = null;
			return true;
		}
		return false;
	}

	// The listening method of OnLoadComplete method.
	void OnLoadComplete (UniWebView webView, bool success, string errorMessage)
	{
		if (success) {
			// Great, everything goes well. Show the web view now.
			Debug.Log ("WEB VIEW LAODED!");
			webView.Show ();
		} else {
			// Oops, something wrong.
			Debug.LogError ("Something wrong in web view loading: " + errorMessage);
		}
	}
#endif

	public GameObject UniWebPrefab;

	public void newsItemClick ()
	{

		string url;
		url = "https://sportimo.mod.bz/v1/data/articles/"+ publicationData._id + "/render/"+LocalizationManager.CurrentLanguageCode;


		#if UNITY_EDITOR
			Application.OpenURL (url);
		#elif UNITY_ANDROID
			 _webView = (Instantiate (UniWebPrefab) as GameObject).GetComponent<UniWebView> ();//new GameObject().AddComponent<UniWebView>();
			 _webView.backButtonEnable=true;
			_webView.OnWebViewShouldClose += OnWebViewShouldClose;
			_webView.OnLoadComplete += OnLoadComplete;
			_webView.Show();
			_webView.url = url;
			_webView.Load ();
		#elif UNITY_IOS
			EtceteraBinding.showWebPage(url ,true);
		#endif



	}
}
