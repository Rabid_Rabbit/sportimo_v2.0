﻿using UnityEngine;
using System.Collections;
using LitJson;

public class PushwooshNotificator : MonoBehaviour
{
    // Use this for initialization

    //TODO decide payload handling/payload schema
    //TODO ios setup on pushwoosh control panel
    //use bedbug_keystore.keystore for device testing, otherwise supply other keystore for setup
    //configured for package name: com.bedbug.sportimo2

    void Awake()
    {
        Pushwoosh.ApplicationCode = "BF7AF-7F9B8";
        Pushwoosh.GcmProjectNumber = "381888113928";
        Pushwoosh.Instance.OnRegisteredForPushNotifications += OnRegisteredForPushNotifications;
        Pushwoosh.Instance.OnFailedToRegisteredForPushNotifications += OnFailedToRegisteredForPushNotifications;
        Pushwoosh.Instance.OnPushNotificationsReceived += OnPushNotificationsReceived;
        Pushwoosh.Instance.RegisterForPushNotifications();

    }

    void OnRegisteredForPushNotifications(string token)
    {
        //do handling here
        Debug.Log("Received token: \n" + token);
        Main.AppUser.tempPushToken = token;
        Main.AppUser.updatePushToken();
    }

    void OnFailedToRegisteredForPushNotifications(string error)
    {
        //do handling here
        Debug.Log("Error ocurred while registering to push notifications: \n" + error);
    }

    //payload: {"type":"view",{"data":{"view":"e.g."match", "view_id":"*id*","message":"some random text here"}}}
    void OnPushNotificationsReceived(string payload)
    {        
        JsonData data = JsonMapper.ToObject(payload);
        if (data["type"] != null && data["type"].ToString() == "view")
        {
            if (data["data"].Keys.Contains("view") && data["data"].Keys.Contains("view_id"))
                Main.Instance.openView(data["data"]["view"].ToString(), data["data"]["view_id"].ToString());

            if (data["data"].Keys.Contains("message"))            
                MessagePanel.Instance.animMessage(data["data"]["message"].ToString());
            
        }
        else if (data["type"].ToString() == "text")
        {
            if (data["data"].Keys.Contains("message"))
                MessagePanel.Instance.animMessage(data["data"]["message"].ToString());
        }
    }
}