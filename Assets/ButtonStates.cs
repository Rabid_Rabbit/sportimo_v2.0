﻿using UnityEngine;
using System.Collections;

public class ButtonStates : MonoBehaviour {

    public GameObject[] states;

    public void setState(int state)
    {
        foreach (GameObject go in states)
            go.SetActive(false);

        states[state].SetActive(true);
    }
}
