﻿using UnityEngine;
using System.Collections;

public class user_state_button : MonoBehaviour {

    public UILabel state_label;

    public void toggleState()
    {
        if (Main.AppUser.customerType == "free") {
            state_label.text = "Paid";
                Main.AppUser.customerType = "paid";
        }else
        {
            state_label.text = "Free";
            Main.AppUser.customerType = "free";
        }
    }

    public void onTransitionInStart()
    {
        toggleState();
        toggleState();
    }
}
