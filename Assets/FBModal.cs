﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FBModal : MonoBehaviour {

	// Use this for initialization
	private static FBModal instance = null;

	public UILabel label;

	public static FBModal Instance {
		get {
			if (instance == null)
				instance = FindObjectOfType (typeof(FBModal)) as FBModal;
			
			return instance;
		}
	}

	public Action<string> answer;
	void Awake ()
	{
		instance = this;

	}

	public void PopModal(Action<string> Callback,string text = null){
		if(!string.IsNullOrEmpty(text)){
			label.text =  text;
		}
		answer = Callback;
		this.GetComponent<UIPanel>().alpha=1.0f;
	}

	public void Answer(GameObject go){
		if( go.name == "Yes" ){
			answer( "yes" );
		}
		else{
			answer( "no" );
		}
		this.GetComponent<UIPanel>().alpha=0f;
		BlockInput.set (false);
	}
}
