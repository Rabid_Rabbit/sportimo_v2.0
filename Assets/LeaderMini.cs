﻿using UnityEngine;
using System.Collections.Generic;
using BedbugRest;
using System;
using LitJson;
using I2.Loc;
using System.Collections;
using System.Linq;

public class LeaderMini : MonoBehaviour
{
	public UILabel top1pos;
	public UILabel top1name;
	public UILabel top1points;
	[Space(10)]
	public UILabel top2pos;
	public UILabel top2name;
	public UILabel top2points;
	[Space(10)]
	public UILabel top3pos;
	public UILabel top3name;
	public UILabel top3points;
	[Space(10)]
	public UILabel top4pos;
	public UILabel top4name;
	public UILabel top4points;
	[Space(10)]
	public UILabel userpos;
	public UILabel username;
	public UILabel userpoints;
	[Space(10)]
	public Animation leaderMiniAnimations;
	private Leaderboard leadeMiniObj;
	[Space(10)]
	public GameObject name1;
	public GameObject name2;
	public GameObject name3;
	public GameObject name4;

	public int testInt = 0;

	public void leaderMiniCheck(string response, string matchId) {

		// Add the response to an object
		leadeMiniObj = JsonMapper.ToObject<Leaderboard> (response);

//		///	TEST  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//		Main.AppUser.leaderCheckObj.lastPos = testInt;
//		
//		leadeMiniObj = new Leaderboard ();
//		Score testMiniuser = new Score ();
//		leadeMiniObj.leaderboad = new List<Score> ();
//		//List<Score> scores = new List<Score>();
//
//		Score testname1 = new Score ();
//		Score testname2 = new Score ();
//		Score testname3 = new Score ();
//		Score testname4 = new Score ();
//
//		testMiniuser._id = "57fe38c39c10e200f2f57276";
//		testMiniuser.name = "Hk0000";
//		testMiniuser.level = 0;
//		testMiniuser.score = 71;
//		testMiniuser.rank = 15;
//		leadeMiniObj.user = testMiniuser;
//
//		testname1._id = "57fe38c39c10e200f2f57276";
//		testname1.name = "Themis Brink";
//		testname1.level = 0;
//		testname1.score = 400;
//		testname1.rank = 13;
//		leadeMiniObj.leaderboad.Add(testname1);
//
//		testname2._id = "5794791ac81985fd003a91d2";
//		testname2.name = "denise10";
//		testname2.level = 0;
//		testname2.score = 350;
//		testname2.rank = 14;
//		leadeMiniObj.leaderboad.Add(testname2);
//
//		leadeMiniObj.leaderboad.Add(testMiniuser);
//
//		testname3._id = "57f24466f93a8109ce93bbed";
//		testname3.name = "bug2";
//		testname3.level = 0;
//		testname3.score = 50;
//		testname3.rank = 16;
//		leadeMiniObj.leaderboad.Add(testname3);
//
//		testname4._id = "580895e740cac300fe5abea3";
//		testname4.name = "Test Dude";
//		testname4.level = 0;
//		testname4.score = 10;
//		testname4.rank = 17;
//		leadeMiniObj.leaderboad.Add(testname4);
//
//		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		print("Leader Mini Check!!!!!!!!!!");

		// Check if leadpos has changed and in what direcion
		if (Main.AppUser.leaderCheckObj.lastPos == 0) { // First time getting a rank position
			print ("Last Position was 0!");
			Main.AppUser.leaderCheckObj.lastPos = leadeMiniObj.user.rank; // Get the position and do nothing
		} else {
			if (Main.AppUser.leaderCheckObj.matchId == matchId) {
				print ("Same Match as before, doing the check!");
				if (Main.AppUser.leaderCheckObj.lastPos == leadeMiniObj.user.rank) {
					print ("Same Position, doing nothing!");
					} else {
					print ("New Position: "+ leadeMiniObj.user.rank+"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
					// Add all the elements to the animated parts
						if (leadeMiniObj.leaderboad [0].rank == 0) {
							NGUITools.SetActive (name1, false);
						} else {
							NGUITools.SetActive (name1, true);
							top1pos.text = leadeMiniObj.leaderboad [0].rank.ToString();
							top1name.text = leadeMiniObj.leaderboad [0].name;
						top1points.text = leadeMiniObj.leaderboad [0].score.ToString()+" pts";
						}
						if (leadeMiniObj.leaderboad [1].rank == 0) {
							NGUITools.SetActive (name2, false);
						} else {
							NGUITools.SetActive (name2, true);
							top2pos.text = leadeMiniObj.leaderboad [1].rank.ToString();
							top2name.text = leadeMiniObj.leaderboad [1].name;
						top2points.text = leadeMiniObj.leaderboad [1].score.ToString()+" pts";
						}
						if (leadeMiniObj.leaderboad [3].rank == 0) {
						print ("NAme 3 rank = 0!");
							NGUITools.SetActive (name3, false);
						} else {
							NGUITools.SetActive (name3, true);
							top3pos.text = leadeMiniObj.leaderboad [3].rank.ToString();
							top3name.text = leadeMiniObj.leaderboad [3].name;
						top3points.text = leadeMiniObj.leaderboad [3].score.ToString()+" pts";
						}
						if (leadeMiniObj.leaderboad [4].rank == 0) {
							print ("NAme 4 rank = 0!");
							NGUITools.SetActive (name4, false);
						} else {
							NGUITools.SetActive (name4, true);
							top4pos.text = leadeMiniObj.leaderboad [4].rank.ToString();
							top4name.text = leadeMiniObj.leaderboad [4].name;
							top4points.text = leadeMiniObj.leaderboad [4].score.ToString()+" pts";
						}

					int rankDef = Main.AppUser.leaderCheckObj.lastPos - leadeMiniObj.user.rank;

						// USER
						userpos.text = Main.AppUser.leaderCheckObj.lastPos.ToString();
						username.text = leadeMiniObj.user.name;
					userpoints.text = leadeMiniObj.user.score.ToString ()+" pts";

					print ("Rank Def: " + rankDef + " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
						

					if (Main.AppUser.leaderCheckObj.lastPos > leadeMiniObj.user.rank) { // Lost rank
						// Play the animation
						if (rankDef < 5 && rankDef > -5) {
							Main.AppUser.leaderCheckObj.lastPos = leadeMiniObj.user.rank;
							leaderMiniAnimations.Play ("LeaderCheckShort");
						} else {
							print ("Going Down  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
							Main.AppUser.leaderCheckObj.lastPos = leadeMiniObj.user.rank;
							leaderMiniAnimations.Play ("LeaderCheckUp");
						}
					} else { // Won Rank
						if (rankDef < 5 && rankDef > -5) {
							Main.AppUser.leaderCheckObj.lastPos = leadeMiniObj.user.rank;
							leaderMiniAnimations.Play ("LeaderCheckShort");
						} else {
							print ("Going Up  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
							Main.AppUser.leaderCheckObj.lastPos = leadeMiniObj.user.rank;
							leaderMiniAnimations.Play ("LeaderCheckDown");
						}
					}
						
						
						
					}

				} else {
					print ("Deferent Match, saving new!");
					Main.AppUser.leaderCheckObj.matchId = matchId;
					Main.AppUser.leaderCheckObj.lastPos = leadeMiniObj.user.rank;
				}

			}

		}

	public void changeToNew() {
		userpos.text = Main.AppUser.leaderCheckObj.lastPos.ToString();
	}
			
}
	
