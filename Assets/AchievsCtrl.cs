﻿using UnityEngine;
using System.Collections.Generic;
using BedbugRest;
using BestHTTP;
using LitJson;
using I2.Loc;
public class AchievsCtrl : View {

	public GameObject AchPrefab;
	public UIGrid ach_grid;

	public UISprite starsfill;

	// Use this for initialization
	//public UISprite avatarSprite;
	public UITexture avatarSprite;

	public void onTransitionInStart(viewData _viewData){
		// Get avatar
		avatarSprite.mainTexture = null;
		if (Main.AppUser.picture != null) {
			BedBugTools.loadImageOnUITextureAndResize (avatarSprite, Main.AppUser.picture, 128,128, FadeInOnLoad, 0);
		} else {
			string baseUrl = "https://s3-eu-west-1.amazonaws.com/sportimo-media/avatars/playerProfile.png";
			BedBugTools.loadImageOnUITextureAndResize (avatarSprite, baseUrl, 128,128, FadeInOnLoad, 0);
		}

		API.GetOneByID(Main.Settings.apis["users"]+"/",_viewData.data.ToString()+"/stats",OnAchievsCallback);
		Loader.Visible (true);
	}

	private void FadeInOnLoad(UITexture texture){
		
		TweenAlpha.Begin (texture.gameObject, 0.5f, 1.0f);
	}

	public void OnAchievsCallback(HTTPRequest req, HTTPResponse res){

		Debug.Log (res.DataAsText);

		ProfileStats profile = JsonMapper.ToObject<ProfileStats>(res.DataAsText);
		Debug.Log (profile.user.level);
		starsfill.fillAmount=(float)profile.user.level;
		foreach(Achievement ach in profile.user.achievements){
			GameObject achiev = NGUITools.AddChild(ach_grid.gameObject,AchPrefab);
			NGUITools.SetActive(achiev,true);
			achiev.transform.FindChild("Name&Fill/TitleLabel").GetComponent<UILabel>().text = LocalizationManager.FixRTL_IfNeeded( ach.title[ getUsedLang(ach.title)],0,true);
			achiev.transform.FindChild("Name&Fill/BarGreenFill").GetComponent<UISprite>().fillAmount = (float)ach.has/ach.total;
				achiev.transform.FindChild("Name&Fill/DescriptionLabel").GetComponent<UILabel>().text = LocalizationManager.FixRTL_IfNeeded( ach.text[ getUsedLang(ach.text) ],28, true);
			achiev.transform.FindChild("Count/CountLabel").GetComponent<UILabel>().text=ach.has.ToString()+"/"+ach.total.ToString();



		}
		ach_grid.Reposition();
		ach_grid.transform.parent.gameObject.GetComponent<UIScrollView> ().ResetPosition ();
		Loader.Visible(false);

		print ("laying Tip 11");
		Coach.coachMessage (LocalizationManager.GetTermTranslation( "tip11", true, 28, true) , 27f, 5f, 1.5f, true, "tip11") ;
	}

	public void onTransitionOutStart(){
		foreach(Transform trans in ach_grid.GetChildList()){
			if(trans.gameObject.activeSelf){
			NGUITools.Destroy(trans.gameObject);
			}
		}
	}

	string getUsedLang(Dictionary<string, string> dictionary)
	{
		string usedLang;

		if (dictionary.ContainsKey(LocalizationManager.CurrentLanguageCode))
			usedLang = LocalizationManager.CurrentLanguageCode;
		else
			usedLang = "en";

		return usedLang;
	}


}
