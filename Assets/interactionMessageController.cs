﻿using UnityEngine;
using System.Collections;


public class interactionMessageController : MonoBehaviour {

	public GameObject chatButton;
	public GameObject responseButton;

	[Space(10)]
	public GameObject response;
	public UILabel responseLabel;

	[Space(10)]
	public GameObject responses;
	public UIScrollView responsesScrollView;

	[Space(10)]
	public UITexture userImage;
	public UILabel userName;
	public UILabel userMessage;

	[Space(10)]
	public UITweener fadeTween;

	[Space(10)]
	public GameObject xButton;

	public Interaction thisInteraction;
	private bool responsesVisible = false;
	private bool hasResponded = false;

	public void init ()
	{
		if (thisInteraction != null) {
			Debug.Log(thisInteraction.sender);
			Debug.Log(thisInteraction.sender.picture);

			if(!string.IsNullOrEmpty(thisInteraction.sender.picture))
				BedBugTools.loadImageOnUITextureAndResize (userImage, thisInteraction.sender.picture, 50,50);

			userName.text = thisInteraction.sender.username;

			if(thisInteraction.taunt.type == "text")
			userMessage.text = BedBugTools.GetStringByLocPrefs(thisInteraction.taunt.text);

			if(thisInteraction.taunt.type == "term")
				userMessage.text = I2.Loc.LocalizationManager.GetTermTranslation(thisInteraction.taunt.term, true);
		}

		fadeTween.PlayForward ();
	}

	public void deleteInteraction(string tauntID){
		if (responsesVisible) {
			responsesVisible = false;
			responses.SetActive (false);
			transform.parent.gameObject.GetComponent<UITable> ().Reposition ();
			// Move X button back
			iTween.MoveTo(xButton, iTween.Hash("x", 0, "time", 0.6f, "islocal", true));
			return;
		}

		if (response.activeSelf) {
			response.SetActive(false);
			return;
		}

		Debug.Log ("Removing this user taunt with id: " + tauntID);
		InteractionsController.Reposition ();
		InteractionsController.SubstructTauntCount ();
		print ("Check if One "+transform.parent.childCount);
		if (transform.parent.childCount == 1) {
			InteractionsController.deleteAllTaunts ();
		} else {
			Destroy (gameObject);
		}

	}

	public void openResponseInteractions(){
		StartCoroutine (ResetPositition ());
	}

	public void openResponse(){
		response.SetActive(true);
	}

	IEnumerator ResetPositition ()
	{
		responses.SetActive (true);
		// Move X button Over
		iTween.MoveTo(xButton, iTween.Hash("x", -40, "time", 0.6f, "islocal", true));
		yield return new WaitForSeconds (0.1f);
		transform.parent.gameObject.GetComponent<UITable> ().Reposition ();
		responsesScrollView.ResetPosition ();

		responsesVisible = true;

	}

	public void responseSelected(string responseID){
		Debug.Log (responseID);

		if (responsesVisible) {
			responsesVisible = false;
			responses.SetActive (false);
			// Move X button back
			iTween.MoveTo(xButton, iTween.Hash("x", 0, "time", 0.3f, "islocal", true));
		}


		responseLabel.text = I2.Loc.LocalizationManager.GetTermTranslation (responseID, true);
		hasResponded = true;

		chatButton.SetActive (false);
		//responseButton.SetActive (true);

		TermTaunt (thisInteraction.recipient, thisInteraction.sender, responseID);

	}

	public static void TermTaunt(simpleUser sender, simpleUser recipient, string term){
		Interaction taunt = new Interaction ();
		taunt.sender = sender;
		taunt.recipient = recipient;
		taunt.taunt = new Taunt ();
		taunt.taunt.type = "term";
		taunt.taunt.term = term;

		Debug.Log (LitJson.JsonMapper.ToJson (taunt));

		BedbugRest.API.PostOne (Main.Settings.apis ["users"] + "/" + recipient._id + "/taunt", taunt, (BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)=>{
			if(res.IsSuccess)
				MessagePanel.Instance.animMessage (I2.Loc.LocalizationManager.GetTermTranslation("_taunt_success", true),I2.Loc.LocalizationManager.GetTermTranslation("_success", true).ToUpper());
			else
				MessagePanel.Instance.animMessage (res.DataAsText);
		});
	}

}
