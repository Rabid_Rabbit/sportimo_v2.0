﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using BedbugRest;
using BestHTTP;
using LitJson;
using I2.Loc;
using Prime31;
using Facebook.Unity;

public class loginCtrl : View
{


	public GameObject LoginView;
	public GameObject ResetView;
	[Space(10)]
	public GameObject facebookReg;
	public GameObject emailReg;
	[Space(10)]
	public UILabel username;
	public UIInput email;
	public UIInput password;
	public UIInput passwordconfirm;
	public GameObject username_panel;
	public UIInput username_input;
	public LogoAnimCtrl LogoCtrl;
	public GameObject loader;
	public bool isRegister = false;

	private readonly string MatchEmailPattern =
		@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{1,}))@"
		+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
		+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
		+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

	// Use this for initialization
	
	private static int Authenticating = 0;

	// Flags
	Dictionary<string, string> languageStrings = new Dictionary<string, string>();
	List<string> languages = new List<string> ();
	public UISprite flagSprite;
	private int flagCount;


	void Start() {
		if (flagSprite != null) {
			
			languageStrings.Add ("en", "englishFlag");
			languageStrings.Add ("ar", "arabiaFlag");

			languages.Add ("en");
			languages.Add ("ar");

			flagSprite.spriteName = languageStrings [LocalizationManager.CurrentLanguageCode];

			if (LocalizationManager.CurrentLanguageCode == "en")
				flagCount = 0;
			else
				flagCount = 1;
		}
		//LocalizationManager.CurrentLanguageCode = "en"; 
	}

	public void changeFlag() {
		flagCount++;
		print("Language Changed!");
		if (flagCount >= languages.Count)
			flagCount = 0;

		flagSprite.spriteName = languageStrings [languages [flagCount]];
		print(languages [flagCount]);
		LocalizationManager.CurrentLanguageCode = languages [flagCount];
		print("to "+ languages [flagCount]);
		print ("Current language is: " + LocalizationManager.CurrentLanguage);
		print ("Current languageCode is: " + LocalizationManager.CurrentLanguageCode);
	}

	public override void Init ()
	{
	
	}


	
	public void FBlogin ()
	{
		if (!FB.IsInitialized) {
			FB.Init (FBlogin);
			return;
		}
		if (!FB.IsLoggedIn)
			FB.LogInWithReadPermissions (new List<string>(){"email,user_friends"}, OnFBLogin);
	}

	public void onTransitionInStart (viewData _viewData = null)
	{
		if(!Main.forcedLogout)
		if (string.IsNullOrEmpty (PlayerPrefs.GetString ("social_id")) && !string.IsNullOrEmpty (PlayerPrefs.GetString ("username")) && !string.IsNullOrEmpty (PlayerPrefs.GetString ("password")) && Authenticating == 0) {
			Authenticating = 1;
			Main.AppUser.AutoAuthenticate ((result) => {
				if (result.success) {
					NGUITools.SetActive (loader, false);
					Main.AppUser = result.userData;
					FinishAuth();

					
				} else {
					Debug.Log (result.message);
					MessagePanel.Instance.animMessage ("There was a problem when trying to autologin!", "Warning");
					// Now let's change the view
					LogoCtrl.playAnimation ("logoAnimationEnd");
					NGUITools.SetActive (loader, false);
				}
				
				Authenticating = 0;
			});
		} else if (!string.IsNullOrEmpty (PlayerPrefs.GetString ("social_id")) && Authenticating == 0) {
			Authenticating = 1;
			Main.AppUser.SocialAutoAuthenticate ((result) => {
				if (result.success) {
					Main.AppUser = result.userData;
					FinishAuth();

				} else {
					Debug.Log("FB");
					FB.LogInWithReadPermissions ();
					Debug.Log (result.message);
					NGUITools.SetActive (loader, false);
				}
				
				Authenticating = 0;
			});
		}
	}

    private void FinishAuth()
    {
        //			Debug.Log ("We have an authenticated user. Now Let's do Ctrl stuff.");

        // First lets store the user's push token if any
        string tempPushToken = Main.AppUser.tempPushToken;
        PlayerPrefs.SetString("_id", Main.AppUser._id);

        // Lets's do something with the received user data.
        if (!string.IsNullOrEmpty(tempPushToken))
            Main.AppUser.tempPushToken = tempPushToken;

        Main.AppUser.updatePushToken();
        PlayerPrefs.SetString("token", Main.AppUser.token);
        PlayerPrefs.SetInt("tutorial_complete", 1);
        
		// Now let's change the view
		if(isRegister)
			Main.Instance.openView("tutorial");
		else
        	Main.Instance.openView("menu");
		
        // And register our ID to the socket server
        ConnectionManager.Instance.RegisterUser();
        //if (Main.AppUser.subscription != null && !string.IsNullOrEmpty(Main.AppUser.subscription.receiptid))
        //{

        //    if (StoreListener.Instance.ValidateReceipt())
        //    {
        //        Main.AppUser.UpdateSubInfo();
        //        return;
        //    }

        //Main.AppUser.ValidateTpay((result) =>
        //{
        //    if (result.success)
        //    {
        //        Main.AppUser.UpdateSubInfo();
        //        return;
        //    }
        //});
    //}
    //Main.AppUser.subscription.status = "free";
    //MessagePanel.Instance.animMessage("You are a free user");
    //prompt for pro subscription

}



    public void Register ()
	{

		NGUITools.SetActive (loader, true);

		if (username.text == "Username" || username.text == "Player Name" || string.IsNullOrEmpty (username.text)) {
			Debug.Log ("input error");
			MessagePanel.Instance.animMessage ("Not a valid Username", LocalizationManager.GetTermTranslation ("Warning", true), null);
            NGUITools.SetActive(loader, false);
            //userRegName.transform.parent.GetComponentInChildren<UISprite> ().spriteName = "wrong-input 1";
        } else if (email.value == "Email" || email.value == "email" || string.IsNullOrEmpty (email.value) || !IsEmail(email.value)){
			Debug.Log (IsEmail (email.value));
			Debug.Log ("input error");
            NGUITools.SetActive(loader, false);
            MessagePanel.Instance.animMessage (LocalizationManager.GetTermTranslation ("not_email", true), LocalizationManager.GetTermTranslation ("Warning", true), null);
			//		pasRegMail.transform.parent.GetComponentInChildren<UISprite>().spriteName="wrong-input 1";
		} else if (password.value == "Password" || passwordconfirm.value == "Re-type Password" || string.IsNullOrEmpty (password.value)) {
			Debug.Log ("input error");
            NGUITools.SetActive(loader, false);
            MessagePanel.Instance.animMessage ("Please enter a password", LocalizationManager.GetTermTranslation ("Warning", true), null);
			//pasRegName.transform.parent.GetComponentInChildren<UISprite> ().spriteName = "wrong-input 1";
		} else if (password.value != passwordconfirm.value) {
			Debug.Log ("input error");
            NGUITools.SetActive(loader, false);
            MessagePanel.Instance.animMessage (LocalizationManager.GetTermTranslation ("password_not_same", true), LocalizationManager.GetTermTranslation ("Warning", true), null);
			//	rePasRegNameInput.GetComponentInChildren<UISprite> ().spriteName = "wrong-input 1";
		} else {
			//User.name="orfeas panagou";
			Debug.Log ("starting register call");
			Main.AppUser.username = username.text;
			Main.AppUser.password = password.value;
			Main.AppUser.email = email.value;
			Main.AppUser.country = "UAE";

			Main.AppUser.Register ((result)=>{
				print("result Success: "+result.success);
				NGUITools.SetActive (loader, false);

				if(result.success)
					FinishAuth();
			});
		}
	}

	private bool IsEmail (string email)
	{
		if (email != null)
			return Regex.IsMatch (email, MatchEmailPattern);
		else
			return false;
	}

	public void openLogin ()
	{
		print ("Goto Login");
		NGUITools.SetActive (loader, false);
		Main.Instance.openView ("login2");
	}
	
	public void openRegister ()
	{
		NGUITools.SetActive (loader, false);
		Main.Instance.openView ("login");
	}
	
	public void Login ()
	{
		Debug.Log("LOGIN");
		Main.AppUser.username = username.text;
		Main.AppUser.password = password.value;

		// open Loader
		NGUITools.SetActive (loader, true);
//	if(StoreListener.Instance.ValidateReceipt()) // Subscription Check - no more relevant
		Main.AppUser.Authenticate ((result) => {
				Debug.Log("IF");
			if (result.success) {
				Debug.Log ("We have an authenticated user. Now Let's do Ctrl stuff.");

			
				Main.AppUser = result.userData;
			

				// And setup properties for safekeeping and autologin
				PlayerPrefs.SetString ("username", Main.AppUser.username);
				PlayerPrefs.SetString ("password", password.value);
			
				FinishAuth();
				NGUITools.SetActive (loader, false);
			} else {
				Debug.Log (result.message);
				NGUITools.SetActive (loader, false);
			}
		});
//else{
//			MessagePanel.Instance.animMessage("You don't have an active subscription");
//			Main.Instance.openSubmenu();
//
//		}
	}

#if !UNITY_WEBPLAYER
    UniWebView _webView;
	
	bool OnWebViewShouldClose (UniWebView webView)
	{
		if (webView == _webView) {
			_webView.OnLoadComplete -= OnLoadComplete;
			_webView.OnWebViewShouldClose -= OnWebViewShouldClose;
			_webView = null;
			return true;
		}
		return false;
	}
	
	// The listening method of OnLoadComplete method.
	void OnLoadComplete (UniWebView webView, bool success, string errorMessage)
	{
		if (success) {
			// Great, everything goes well. Show the web view now.
			Debug.Log ("WEB VIEW LOADED!");
			webView.Show ();
		} else {
			// Oops, something wrong.
			Debug.LogError ("Something wrong in web view loading: " + errorMessage);
        }
    }
#endif

    public void toggleReset ()
	{
		LoginView.SetActive (ResetView.activeSelf);
		ResetView.SetActive (!ResetView.activeSelf);
	}

	public GameObject UniWebPrefab;

	public void resetPassword ()
	{
		Debug.Log ("Reseting: " + email.value);

		Hashtable hash = new Hashtable ();
		hash ["email"] = email.value;
		API.PostOne (Main.Settings.apis ["users"] + "/reset", hash, (req, res) => {

			Debug.Log (res.DataAsText);

			if (res.StatusCode == 200) {

				JsonData resData = JsonMapper.ToObject<JsonData> (res.DataAsText);
				MessagePanel.Instance.animMessage (LocalizationManager.FixRTL_IfNeeded( resData["text"][LocalizationManager.CurrentLanguageCode].ToString()),LocalizationManager.GetTermTranslation("reset", true));


				if ((bool)resData ["redirect"] == true) {

					string url = "http://sportimo_reset_password.mod.bz/#/reset/" + resData ["token"];
					#if UNITY_EDITOR && !UNITY_ANDROID
				Application.OpenURL (url);
					#elif UNITY_ANDROID || UNITY_IOS
			_webView = (Instantiate (UniWebPrefab) as GameObject).GetComponent<UniWebView> ();//new GameObject().AddComponent<UniWebView>();
			_webView.backButtonEnable=true;
			_webView.OnWebViewShouldClose += OnWebViewShouldClose;
			_webView.OnLoadComplete += OnLoadComplete;
			_webView.Show();
			_webView.url = url;
			_webView.Load ();
//					#elif UNITY_IOS
//			EtceteraBinding.showWebPage(url ,true);
					#endif
				}
			}

			toggleReset();
		});
	}

	private int FBregistering = 0;
	public void FBRegister ()
	{
		if(FBregistering == 1) return;

		FB.LogOut();

		FBregistering = 1;

		fbtable.Clear ();
		Debug.Log("---- FB REGISTER -----: "+FB.IsInitialized);

		if(!FB.IsInitialized)
			FB.Init (onInitComplete);
		else
			onInitComplete();

	}
	
	public void onInitComplete ()
	{
		Debug.Log("---- onInitComplete -----: "+FB.IsInitialized);
		if (FB.IsInitialized) {
			FB.LogInWithReadPermissions (new List<string>(){"email,user_friends"}, OnFBLogin);
		} else {
			Debug.Log ("facebook couldnt initialize");
		}

		FBregistering = 0;
	}
	
	public void OnFBLogin (ILoginResult fbresult)
	{
		Debug.Log("---- OnFBLogin -----: "+ fbresult);

		if (fbresult.Error != null) {
			Debug.Log (fbresult.Error);
            MessagePanel.Instance.animMessage( fbresult.Error, "Error Message", null);
			NGUITools.SetActive (loader, false);
        } else {

			JsonData data = JsonMapper.ToObject (fbresult.RawResult);

            if (!data.Keys.Contains("user_id")) return;

			Main.AppUser.social_id = data ["user_id"].ToString ();

			Main.AppUser.SocialAuthenticate ((result) => {
				if (result.success) {
					
					// First lets store the user's push token if any
					
					// Lets's do something with the received user data.
					Main.AppUser = result.userData;
					

					PlayerPrefs.SetString ("username", Main.AppUser.username);
					PlayerPrefs.SetString ("password", AccessToken.CurrentAccessToken.UserId);
					PlayerPrefs.SetInt ("tutorial_complete", 1);
					PlayerPrefs.SetString ("social_id", AccessToken.CurrentAccessToken.UserId);
					// Now let's change the view
					FinishAuth();
				} else {
					FB.API ("me?fields=id,name,email", HttpMethod.GET, OnGetEmail);
					NGUITools.SetActive (loader, false);
				}
			});
		}
		
	}

	Hashtable fbtable = new Hashtable ();

	public void OnGetEmail (IGraphResult result)
	{
		Debug.Log (result.RawResult);
		JsonData data = JsonMapper.ToObject (result.RawResult);
		if (!data.Keys.Contains ("email")) {
			MessagePanel.Instance.animMessage ("Email permission is needed for the registration process to complete");
		} else {
			fbtable.Add ("name", data ["name"].ToString ());
			fbtable.Add ("email", WWW.UnEscapeURL (data ["email"].ToString ()));
			fbtable.Add ("social_id", data ["id"].ToString ());
			fbtable.Add ("password", data ["id"].ToString ());
			PopModal ();
		}
	}

	public void PopModal ()
	{
		NGUITools.SetActive (username_panel, true);
	}
	
	public void OnUsernameOK ()
	{
		
		if (username_input.value == "Username" || username_input.value == "Player Name" || string.IsNullOrEmpty (username_input.value)) {
			Debug.Log ("input error");
			MessagePanel.Instance.animMessage ("Not a valid Username");
		} else {
			fbtable.Add ("username", username_input.value);
			NGUITools.SetActive (username_panel, false);
			Loader.Visible (true);
			API.PostOne (Main.Settings.apis ["users"], fbtable, Main.AppUser.RegCallback);

		}
	}

	IEnumerator OpenMain ()
	{
		yield return new WaitForSeconds (1.5f);
		Main.Instance.openView ("menu");
	}

	public void openEmailReg() {
		NGUITools.SetActive(facebookReg, false);
		NGUITools.SetActive(emailReg, true);
	}

}
