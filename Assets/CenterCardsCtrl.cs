﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using BestHTTP;
using LitJson;
using BedbugRest;
using I2.Loc;

public class CenterCardsCtrl : MonoBehaviour
{

	
	public MatchCtrl matchCtrl;
	public List<PlayCard> centerPlayCards = new List<PlayCard> ();
	public PlayCard centerCardInfo;
	public PlayCard animCardInfo;
	int cardIndex;
	public GameObject cardInfo;
	public GameObject cardStats;
	public GameObject cardStory;
	public GameObject centerCard;
	public GameObject CenterAnimHolder;
	private Vector2 dragPoint;
	public UILabel centercardName;
	public UILabel centercardDesc;
	public UILabel centercardCh1;
	public UILabel centercardCh2;
	public UILabel centercardCh3;
	public UILabel centercardCh4;

	public UILabel centercard_points_choice1;
	public UILabel centercard_points_choice2;
	public UILabel centercard_points_choice3;
	public UILabel centercard_points_choice4;

	public UISprite centercardIcon;
	public UISprite centercardIconTop;
	public UIPanel animCardPanel;
	public UILabel animcardName;
	public UILabel animcardDesc;
	public UILabel animcardCh1;
	public UILabel animcardCh2;
	public UILabel animcardCh3;
	public UILabel animcardCh4;
	public UISprite animcardIcon;
	public UILabel animCardFrontName;
	public UILabel animCardFrontDesc;
	public UILabel animCardFrontCh1;
	public UILabel animCardFrontCh2;
	public UILabel animCardFrontCh3;
	public UILabel animCardFrontCh4;
	public UISprite animCardFrontIcon;
	Animator CenterCardsAnimator; 
	public bool isActive = false;
	[Space(10)]
	public GameObject SportimoGuru; //oh yeah
	public Animation guruPulseAnim;
	public GameObject SportimGuruText;
	public UILabel SportimoGuruLabel; //for infinite wisdom
	public int guruCloseCounter = 0;
	int currentHigh;
	int currentBiggestSpan;
	List<string> minSpans = new List<string>();
	[Space(10)]
	//Card stat references
	public UITexture home_team;
	public UITexture away_team;
	public UILabel home_team_stat;
	public UILabel away_team_stat;
	[Space(10)]
	public GameObject presetTimeSelect;
	public GameObject presetTimeBtn;
	public UIScrollBar presetBar;
	public UILabel presetLabel;
	public int presetValue;
	public GameObject centerChoices;
	public Animation ClockAnim;
	private bool dontDrag = false;
    private bool timeSet = false;
    public GameObject timesetObj;
    public UILabel timeSetLabel;
	[Space(10)]
	public bool optionPressed;

	// Info on Cards
	public GameObject overallInfo;
	public GameObject liveInfo;
	public GameObject presetInfo;
	// Swipe animaton
	public GameObject swipeAnim;

	void Start ()
	{
		CenterCardsAnimator = gameObject.GetComponent<Animator> ();
		minSpans = new List<string> (new String[] {
			"1' - 10'",
			"11' - 20'",
			"21' - 30'",
			"31' - 40'",
			"41' - 50'",
			"51' - 60'",
			"61' - 70'",
			"71' - 80'",
			"81' - 90'"
		});
	}

	public void createCardList (List<PlayCard> playcards, int choiceIndex)
	{
		ConnectionManager.OnStatsEvent += OnStatsUpdateReceived;
		//centerPlayCards.Clear ();
//		print ("playcards: " + playcards[choiceIndex].Icon);
		centerPlayCards = playcards;

		cardIndex = choiceIndex;
// 		print ("choiceIndex sent by CreateCardList: "+choiceIndex);
		updateCenterCard (choiceIndex);

		optionPressed = false;

        // First time in, reset timeset
        timeSet = false;
        NGUITools.SetActive(timesetObj, false);
    }

	public void closeCenterCards(){
//		print ("REMOVING ONSTATSEVENT HOOK!");
		ConnectionManager.OnStatsEvent -= OnStatsUpdateReceived;
		GuruClose ();
	}

	void OnStatsUpdateReceived (string json)
	{
		updateCenterCard (cardIndex);
	}


	public void updateCenterCard (int id)
	{
		GuruClose();
//		print ("id sent by CreateCardList: "+id);
//		print ("centerPlayCards [id]: "+centerPlayCards [id]);
		centerCardInfo = centerPlayCards [id] as PlayCard;
		GuruCheck();
		print ("cardType: "+centerCardInfo.cardType);
		if (centerCardInfo.cardType != null) {
			if (centerCardInfo.cardType == "PresetInstant")
				presetOpen ();
			else
				presetClose ();
		}
		centercardName.text = LocalizationManager.FixRTL_IfNeeded( centerCardInfo.title [ getUsedLang(centerCardInfo.title) ], 0, true);
		centercardDesc.text = LocalizationManager.FixRTL_IfNeeded( centerCardInfo.text [ getUsedLang(centerCardInfo.text) ], 27, true);
		print ("Options Length: " + centerCardInfo.options.Length);
		print ("cardType: "+centerCardInfo.cardType);
//		if(centerCardInfo.cardType == "Overall")
//			centerCardInfo.options[0].startPoints

		int match_time = matchCtrl.liveMatch.matchData.GetStat (matchCtrl.match_id, "Minute");

		// Open the right info
		openCardInfo ();
		if (centerCardInfo.cardType == "Instant") {
			NGUITools.SetActive (liveInfo, true);
			NGUITools.SetActive (overallInfo, false);
			NGUITools.SetActive (presetInfo, false);

		} else if (centerCardInfo.cardType == "PresetInstant") {
			NGUITools.SetActive (liveInfo, false);
			NGUITools.SetActive (overallInfo, false);
			NGUITools.SetActive (presetInfo, true);
		} else {
			NGUITools.SetActive (liveInfo, false);
			NGUITools.SetActive (overallInfo, true);
			NGUITools.SetActive (presetInfo, false);
		}


		for (int i = 0; i < 4; i++) {
			switch (i) {
			case 0:
				int points = centerCardInfo.options[0].startPoints;
				Debug.Log(points);
				if(centerCardInfo.cardType == "Overall"){
					points += Mathf.RoundToInt (match_time * (float)centerCardInfo.options[0].pointsPerMinute);
				}
				centercardCh1.text = LocalizationManager.FixRTL_IfNeeded( centerCardInfo.options [0].text [ getUsedLang(centerCardInfo.options [0].text) ], 0, true);
				centercard_points_choice1.text = points+"pts";
				break;
			case 1:
				points = centerCardInfo.options[1].startPoints;
				if(centerCardInfo.cardType == "Overall"){
					points += Mathf.RoundToInt(match_time * (float)centerCardInfo.options[1].pointsPerMinute);
				}
				centercardCh2.text =  LocalizationManager.FixRTL_IfNeeded( centerCardInfo.options [1].text [ getUsedLang(centerCardInfo.options [1].text) ], 0, true);

				centercard_points_choice2.text = points+"pts";
				break;
			case 2:
				if (2 < centerCardInfo.options.Length) {
					points = centerCardInfo.options[2].startPoints;
					if(centerCardInfo.cardType == "Overall"){
						points += Mathf.RoundToInt(match_time * (float)centerCardInfo.options[2].pointsPerMinute);
					}
					NGUITools.SetActive (centercardCh3.transform.parent.gameObject, true);
					centercardCh3.text =  LocalizationManager.FixRTL_IfNeeded( centerCardInfo.options [2].text [ getUsedLang(centerCardInfo.options [2].text) ], 0, true);
					centercard_points_choice3.text = points+"pts";
				} else {
					NGUITools.SetActive (centercardCh3.transform.parent.gameObject, false);
				}
				break;
			case 3:
				if (3 < centerCardInfo.options.Length) {
					points = centerCardInfo.options[3].startPoints;
					if(centerCardInfo.cardType == "Overall"){
						points += Mathf.RoundToInt(match_time * (float)centerCardInfo.options[3].pointsPerMinute);
					}
					NGUITools.SetActive (centercardCh4.transform.parent.gameObject, true);
					centercardCh4.text =  LocalizationManager.FixRTL_IfNeeded( centerCardInfo.options [3].text [ getUsedLang(centerCardInfo.options [3].text) ], 0, true);
					centercard_points_choice4.text = points+"pts";
				} else {
					NGUITools.SetActive (centercardCh4.transform.parent.gameObject, false);
				}
				break;
			}
		}

		centercardIcon.spriteName = centerCardInfo.Icon;
		centercardIconTop.spriteName = centerCardInfo.Icon;
	}

	public void updateAnimCard (int id)
	{
//		print (id);
		animCardInfo = centerPlayCards [id];
		TweenAlpha.Begin (animcardName.gameObject, 0.5f, 1);
		TweenAlpha.Begin (animcardIcon.gameObject, 0.5f, 1);

		animcardName.text =  LocalizationManager.FixRTL_IfNeeded( animCardInfo.title [getUsedLang(animCardInfo.title)], 0, true);
		animcardDesc.text =  LocalizationManager.FixRTL_IfNeeded( animCardInfo.text [getUsedLang(animCardInfo.text)], 27, true);
		animcardIcon.spriteName = animCardInfo.Icon;
		print ("Options Length: " + centerCardInfo.options.Length);

		for (int i = 0; i < 4; i++) {
			switch (i) {
			case 0:
				animcardCh1.text =  LocalizationManager.FixRTL_IfNeeded( animCardInfo.options [0].text [getUsedLang(animCardInfo.options [0].text)]+"\n"+centerCardInfo.options[0].startPoints.ToString()+"pts");
				break;
			case 1:
				animcardCh2.text =  LocalizationManager.FixRTL_IfNeeded( animCardInfo.options [1].text [getUsedLang(animCardInfo.options [1].text)]+"\n"+centerCardInfo.options[1].startPoints.ToString()+"pts");
				break;
			case 2:
//				print (2 < animCardInfo.options.Length);
				if (2 < animCardInfo.options.Length) {
					NGUITools.SetActive (animcardCh3.transform.parent.gameObject, true);

				} else {
					NGUITools.SetActive (animcardCh3.transform.parent.gameObject, false);
				}
				break;
			case 3:
				if (3 < animCardInfo.options.Length) {
					NGUITools.SetActive (animcardCh4.transform.parent.gameObject, true);

				} else {
					NGUITools.SetActive (animcardCh4.transform.parent.gameObject, false);
				}
				break;
			}
		}

				
	}

	public void updateCardFront (int id)
	{
		//		print (id);
		animCardInfo = centerPlayCards [id];
		
		animCardFrontName.text =  LocalizationManager.FixRTL_IfNeeded( animCardInfo.title [getUsedLang(animCardInfo.title)], 0, true);
		animCardFrontDesc.text =  LocalizationManager.FixRTL_IfNeeded( animCardInfo.text [getUsedLang(animCardInfo.text)], 27, true);
		animCardFrontIcon.spriteName = animCardInfo.Icon;

		print ("Options Length: " + centerCardInfo.options.Length);
		for (int i = 0; i < 4; i++) {
			switch (i) {
			case 0:
				animCardFrontCh1.text =  LocalizationManager.FixRTL_IfNeeded( animCardInfo.options [0].text [getUsedLang( animCardInfo.options [0].text )]+"\n"+centerCardInfo.options[0].startPoints.ToString()+"pts");
				break;
			case 1:
				animCardFrontCh2.text =  LocalizationManager.FixRTL_IfNeeded( animCardInfo.options [1].text [getUsedLang( animCardInfo.options [1].text )]+"\n"+centerCardInfo.options[1].startPoints.ToString()+"pts");
				break;
			case 2:
				if (2 < animCardInfo.options.Length) {
					NGUITools.SetActive (animCardFrontCh3.transform.parent.gameObject, true);

				} else {
					NGUITools.SetActive (animCardFrontCh3.transform.parent.gameObject, false);
				}
				break;
			case 3:
				if (3 < animCardInfo.options.Length) {
					NGUITools.SetActive (animCardFrontCh4.transform.parent.gameObject, true);

				} else {
					NGUITools.SetActive (animCardFrontCh4.transform.parent.gameObject, false);
				}
				break;
			}
		}


	}

	public void resetAnimHolder ()
	{
		//iTween.Stop();
		CenterAnimHolder.transform.localPosition = new Vector3 (0, 0, 0);
		CenterAnimHolder.transform.localRotation = Quaternion.Euler (0, 0, 0);
		CenterAnimHolder.transform.localScale = new Vector3 (1, 1, 1);
		centerCard.transform.localPosition = new Vector3 (0, -40, 0);
	}

	public void resetCards ()
	{	
		GuruClose();
        closeTimeX();

        animCardPanel.depth = 8;
		CenterAnimHolder.transform.localRotation = Quaternion.Euler (0, 0, 0);
		centerCardInfo = animCardInfo;
		GuruCheck();
		//print ("cardType: "+centerCardInfo.cardType);
		if (centerCardInfo != null) {
			if (centerCardInfo.cardType == "PresetInstant")
				presetOpen ();
			else
				presetClose ();
		}
		int match_time;

		if ( matchCtrl.liveMatch  != null )
			match_time = matchCtrl.liveMatch.matchData.GetStat (matchCtrl.match_id, "Minute");	
		else 
			return;
		
		centercardName.text =  LocalizationManager.FixRTL_IfNeeded( centerCardInfo.title [getUsedLang(centerCardInfo.title)]);
		centercardDesc.text =  LocalizationManager.FixRTL_IfNeeded( (centerCardInfo.text [getUsedLang(centerCardInfo.text)]) ,27 ,true);

		for (int i = 0; i < 4; i++) {
			switch (i) {
			case 0:
				int points = centerCardInfo.options[0].startPoints;
//				Debug.Log(points);
				if(centerCardInfo.cardType == "Overall"){
					points += Mathf.RoundToInt (match_time * (float)centerCardInfo.options[0].pointsPerMinute);
				}
				centercard_points_choice1.text = points+"pts";
				Debug.Log(points);
				centercardCh1.text =  LocalizationManager.FixRTL_IfNeeded( centerCardInfo.options [0].text [getUsedLang(centerCardInfo.options [0].text)], 0, true);

				break;
			case 1:
				points = centerCardInfo.options[1].startPoints;

				if(centerCardInfo.cardType == "Overall"){
					points += Mathf.RoundToInt (match_time * (float)centerCardInfo.options[1].pointsPerMinute);
				}
				centercard_points_choice2.text = points+"pts";
				centercardCh2.text =  LocalizationManager.FixRTL_IfNeeded( centerCardInfo.options [1].text [getUsedLang(centerCardInfo.options [1].text)], 0, true);
				break;
			case 2:
				if (2 < centerCardInfo.options.Length) {
					NGUITools.SetActive (centercardCh3.transform.parent.gameObject, true);
					points = centerCardInfo.options[2].startPoints;
					if(centerCardInfo.cardType == "Overall"){
						points += Mathf.RoundToInt (match_time * (float)centerCardInfo.options[2].pointsPerMinute);
					}
					centercard_points_choice3.text = points+"pts";
					centercardCh3.text =  LocalizationManager.FixRTL_IfNeeded( centerCardInfo.options [2].text [getUsedLang(centerCardInfo.options [2].text)], 0, true);
				} else {
					NGUITools.SetActive (centercardCh3.transform.parent.gameObject, false);
				}
				break;
			case 3:
				if (3 < centerCardInfo.options.Length) {
					NGUITools.SetActive (centercardCh4.transform.parent.gameObject, true);
					points = centerCardInfo.options[3].startPoints;
					if(centerCardInfo.cardType == "Overall"){
						points += Mathf.RoundToInt (match_time * (float)centerCardInfo.options[3].pointsPerMinute);
					}
					centercard_points_choice4.text = points+"pts";
					centercardCh4.text =  LocalizationManager.FixRTL_IfNeeded( centerCardInfo.options [3].text [getUsedLang(centerCardInfo.options [3].text)], 0, true);
				} else {
					NGUITools.SetActive (centercardCh4.transform.parent.gameObject, false);
				}
				break;
			}
		}

		centercardIcon.spriteName = centerCardInfo.Icon;
		centercardIconTop.spriteName = centerCardInfo.Icon;
	}
	
	private int option_selected;

	public void OptionClick (GameObject opt)
	{
        print("Preset Time Set is " + timeSet);
        // if(centerCardInfo.cardType=="Instant" && !match.timecounting){
        // 	Debug.Log("warning message");
        // 	return;
        // }
        // Flag to keep the user from pressing to many times
        if (optionPressed)
			return;
		else
			optionPressed = true;

		// Close the Swipe Animation
		NGUITools.SetActive (swipeAnim, false);
      
        // Fix for Arabix
        var infoDesc = LocalizationManager.GetTermTranslation("_playCardConfirmInfo", false);
        infoDesc = string.Format(infoDesc, MatchCtrl.cardsLeft);


        
        // Check if the card is Preset, if yes check if a time is set, if no then pop the setTime popup
        if (centerCardInfo.cardType == "PresetInstant" && !timeSet)
        {
            //pop the time module
            openTimeSelect();
            optionPressed = false;
        }
        else
        {
            // else do what you did till now
            ConfirmationModalController.Confirm(LocalizationManager.GetTermTranslation("_playCardConfirm", true, 23), LocalizationManager.FixRTL_IfNeeded(infoDesc, 0, true), null, null, (option) =>
            {
                if (option == "Btn1")
                {
                    Loader.Visible(true);

                    Hashtable table = new Hashtable();
                    table.Add("matchid", matchCtrl.match_id);
                    table.Add("userid", Main.AppUser._id);
                    table.Add("gamecardDefinitionId", centerCardInfo._id);
                    table.Add("creationTime", DateTime.UtcNow.ToString(("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'")));
                    table.Add("segment", matchCtrl.liveMatch.matchData.state);

                    if (centerCardInfo.cardType == "PresetInstant")
                        table.Add("minute", presetValue);
                    else
                        table.Add("minute", (matchCtrl.minutes + 1));

                    if (opt.name.Contains("1"))
                    {
                        table.Add("optionId", 1);
                        option_selected = 1;
                    }
                    else if (opt.name.Contains("2"))
                    {
                        table.Add("optionId", 2);
                        option_selected = 2;
                    }
                    else if (opt.name.Contains("3"))
                    {
                        table.Add("optionId", 3);
                        option_selected = 3;
                    }
                    else
                    {
                        table.Add("optionId", 4);
                        option_selected = 4;
                    }
                    Debug.Log(JsonMapper.ToJson(table));
                    API.PostOneCard(Main.Settings.apis["gamecards"] + "/" + matchCtrl.match_id + "/users", table, OnPlayCardPost);
                    ConnectionManager.OnStatsEvent -= OnStatsUpdateReceived;
                    matchCtrl.closeCardPlay();

                    if (centerCardInfo.cardType == "PresetInstant")
                        matchCtrl.playedCardsAction();
                }
                else
                    optionPressed = false;
            });
        }
	}

	private void presetOpen () {
		// Dont allow draging

		presetBar.value = 0.5f;
		StartCoroutine (delayTimeOpen ( 0.5f ));
		TweenAlpha.Begin (presetTimeBtn, .5f, 1);
		iTween.MoveTo (centerChoices, iTween.Hash ("y", 7, "time", .5f, "islocal", true, "easetype", iTween.EaseType.easeOutCirc));
	}

	void presetClose() {
		TweenAlpha.Begin (presetTimeBtn, 0.0f, 0);
		ClockAnim.Play("clockStop");
		NGUITools.SetActive (presetTimeBtn, false);
		//ClockAnim.Stop("clockPulse");

		iTween.MoveTo (centerChoices, iTween.Hash ("y", 33, "time", 0.0f, "islocal", true, "easetype", iTween.EaseType.easeOutCirc));
	}

	public void openTimeSelect() {
		print ("OpenTimeSelect!");
		dontDrag = true;
		TweenAlpha.Begin (presetTimeSelect, 0.5f, 1);
		//ClockAnim.Stop("clockPulse");
		ClockAnim.Play("clockStop");
		iTween.ScaleTo (presetTimeSelect, iTween.Hash ("x" , 1.0f, "y", 1.0f, "time", 1.0f, "islocal", true, "easetype", iTween.EaseType.easeOutElastic));
	}

	public void closeTimeX() {
		//print ("close  X TimeSelect!");
		dontDrag = false;
        timeSet = false;
        NGUITools.SetActive(timesetObj, false);
        //iTween.ScaleTo (presetTimeSelect, iTween.Hash ("x" , 0f, "y", 0f, "time", 1.0f, "islocal", true, "easetype", iTween.EaseType.easeOutCirc));
        TweenAlpha.Begin (presetTimeSelect, 0.5f, 0);
		presetBar.value = 0.5f;
		ClockAnim["clockPulse"].speed = 1;
		ClockAnim.Play("clockPulse");
	}

	public void closeTimeYes() {
		dontDrag = false;
        timeSet = true;
        NGUITools.SetActive(timesetObj, true);
        timeSetLabel.text = presetLabel.text;
        //iTween.ScaleTo (presetTimeSelect, iTween.Hash ("x" , 0f, "y", 0f, "time", 1.0f, "islocal", true, "easetype", iTween.EaseType.easeOutCirc));
        TweenAlpha.Begin (presetTimeSelect, 0.5f, 0);
		//presetBar.value = 0.5f;
		// Close Pulse;
	}

	IEnumerator delayTimeOpen( float delay ) {
		yield return new WaitForSeconds (delay);
		ClockAnim["clockPulse"].speed = 1;
		ClockAnim.Play("clockPulse");
		NGUITools.SetActive (presetTimeBtn, true);
	}

	public void getvalue() {
//		print ((presetBar.value*100)*0.9f);
		presetLabel.text = Math.Round((presetBar.value*100)*0.9f) + "'";
		presetValue = (int) Math.Round ((presetBar.value*100)*0.9f);
//		print (presetBar.value);
		if (presetBar.value < 0.01f) {
			//Coach.coachMessage(LocalizationManager.GetTermTranslation("_under_presettime"), 27f, 0, 2.5f, true);
			presetBar.value = 0.01f;
			presetLabel.text = Math.Round(( presetBar.value * 100 ) *0.9f ) + "'";
		}
	}
	//the mighty sportimo guru code is here!
	//1- wait for matching gurustats/card
	//2-pulsate so the user knows guru is available
	//3-on click show text(click again to close?)
	//4-on other card reset to 1

	//This is called everytime a card opens in the center(on roller click and on swipe)
	private void GuruCheck(){
        //print("Checking Guru!");
        //		print ("match.match :"+ matchCtrl.match);
		//		print ("match.match.matchData.guruStats :"+ matchCtrl.liveMatch.matchData.guruStats);
        //		print ("MatchCtrl.match_minute :"+ MatchCtrl.match_minute);
        if ( matchCtrl.liveMatch != null && matchCtrl.liveMatch.matchData.guruStats != null && MatchCtrl.match_minute < 90){

			int total;
			int home;
			int away;
			int index;

			GuruStats.GuruStat guruStat;

			if(centerCardInfo.guruAction != null)
			print ( "CenterCard guruAction: " + centerCardInfo.guruAction);

			if(MatchCtrl.match_minute < 10){
				index = 0;
			}
			else{
				index = MatchCtrl.match_minute/10%10;
			}

			print ("Index: " + index);
			print ("centerCardInfo.primaryStatistic: " + centerCardInfo.primaryStatistic);
			print ( "CenterCard guruAction: " + centerCardInfo.guruAction);

			if(centerCardInfo.primaryStatistic == "Yellow" && matchCtrl.liveMatch.matchData.guruStats.Yellow != null){
				guruStat = matchCtrl.liveMatch.matchData.guruStats.Yellow;
			}
			else if(centerCardInfo.primaryStatistic == "Goal" && matchCtrl.liveMatch.matchData.guruStats.Goal != null){
				guruStat = matchCtrl.liveMatch.matchData.guruStats.Goal;
			}
			else if(centerCardInfo.primaryStatistic == "Corner" && matchCtrl.liveMatch.matchData.guruStats.Corner != null){
				guruStat = matchCtrl.liveMatch.matchData.guruStats.Corner;
			}
			else if(centerCardInfo.primaryStatistic == "Shot_On_Goal" && matchCtrl.liveMatch.matchData.guruStats.Shot_On_Goal != null){
				guruStat = matchCtrl.liveMatch.matchData.guruStats.Shot_On_Goal;
			}
			else{
				GuruClose();
				return;
			}

			// Get The Highest value and game 10min span

			for (int i = index; i < guruStat.total.Length; i++) {
				if (currentHigh <= guruStat.total [i]) {
					currentHigh = guruStat.total [i];
					currentBiggestSpan = i;
				}
//				print (i + ": " + guruStat.total[i]);
//				print ("Biggest 10min: " + currentBiggestSpan);
			}

			if(guruStat.total != null && guruStat.total.Length > 0){
				total = Array.IndexOf(guruStat.total, guruStat.total.Skip(index).Max());

			}
			else{
				GuruClose();
				return;
			}
			if(guruStat.awayTeam != null && guruStat.awayTeam.Length > 0){
				away = Array.IndexOf(guruStat.awayTeam, guruStat.awayTeam.Skip(index).Max());
			}
			else{
				GuruClose();
				return;
			}
			if(guruStat.homeTeam != null && guruStat.homeTeam.Length > 0){
				home = Array.IndexOf(guruStat.homeTeam, guruStat.homeTeam.Skip(index).Max());
			}
			else{
				GuruClose();
				return;
			}

			// get the best 10min span for thi kind of card
//			SportimoGuruLabel.text= "After deliberation, the best time to play this card for both teams is "
//			+total.ToString()+"0 to "+total+1.ToString()+"0,for "
//			+match.match.matchData.home_team.name["en"]+" "+home.ToString()+"0 to "+home+1.ToString()+"0 and for "
//			+match.match.matchData.away_team.name["en"]+" "+away.ToString()+"0 to "+away+1.ToString()+"0.";
			//My scientific models suggest that the best time to play your [goal] card is between [51-60]
//			print(centerCardInfo.primaryStatistic);

			if (LocalizationManager.CurrentLanguageCode == "ar") {
				SportimoGuruLabel.alignment = NGUIText.Alignment.Right;
				print ("Language is ar changing to Right");
			} else {
				SportimoGuruLabel.alignment = NGUIText.Alignment.Left;
				print ("Language is en changing to Left");
			}

			int sumHomeTeam = guruStat.homeTeam.Sum ();
			int sumAwayTeam = guruStat.awayTeam.Sum ();


			if (centerCardInfo.guruAction == "Compare") {
				if (sumHomeTeam > sumAwayTeam)
                    SportimoGuruLabel.text = LocalizationManager.GetTermTranslation("guru3", true, 25, true) + LocalizationManager.FixRTL_IfNeeded(BedBugTools.GetStringByLocPrefs(matchCtrl.liveMatch.matchData.home_team.name), 0, true) + LocalizationManager.GetTermTranslation("guru4", true, 25, true) + LocalizationManager.GetTermTranslation(centerCardInfo.primaryStatistic + "_guru", true, 25, true); //matchCtrl.liveMatch.matchData.home_team.name [LocalizationManager.CurrentLanguageCode]                    
				else if (sumHomeTeam < sumAwayTeam)
					SportimoGuruLabel.text = LocalizationManager.GetTermTranslation ("guru3", true, 25, true) + LocalizationManager.FixRTL_IfNeeded(BedBugTools.GetStringByLocPrefs(matchCtrl.liveMatch.matchData.away_team.name), 0, true) + LocalizationManager.GetTermTranslation ("guru4", true, 25, true) + LocalizationManager.GetTermTranslation (centerCardInfo.primaryStatistic+"_guru", true, 25, true);
				else if (sumHomeTeam == sumAwayTeam)
					SportimoGuruLabel.text = LocalizationManager.GetTermTranslation ("guru5", true, 25, true) + LocalizationManager.GetTermTranslation (centerCardInfo.primaryStatistic, true, 25, true);
			} else {
				SportimoGuruLabel.text = LocalizationManager.GetTermTranslation ("guru1", true, 25, true) + " " + LocalizationManager.GetTermTranslation (centerCardInfo.primaryStatistic, true, 25, true) + " " + LocalizationManager.GetTermTranslation ("guru2", true, 25, true) + " " + minSpans [currentBiggestSpan];
			}

			//SportimoGuruLabel.text = LocalizationManager.FixRTL_IfNeeded( (LocalizationManager.GetTermTranslation( "guru1")),0,true) +" "+ LocalizationManager.FixRTL_IfNeeded( (LocalizationManager.GetTermTranslation("Shot_on_Goal")),0,true)+" "+ LocalizationManager.FixRTL_IfNeeded( (LocalizationManager.GetTermTranslation( "guru2")),0,true) +" "+ minSpans[currentBiggestSpan];
			//SportimoGuru.GetComponent<UIWidget>().alpha = 1f;//simple turn on for now
			TweenAlpha.Begin (SportimoGuru, 0.2f, 1.0f);
			guruPulseAnim.Play("guruPulse");
			InvokeRepeating("GuruPulsate",1.0f,1.0f); //Using this as a counter
		}
		else{
			//Debug.Log("this doesnt have gurustats");
		}
		
	}
	bool gurutextopen = false;

	private void GuruPulsate(){
			guruCloseCounter++;
		if (guruCloseCounter == 30) // Seconds to wait before auto-closing the Guru
			GuruClose ();
	}

	// the click method, animation for text expansion should go here
	public void GuruClick(){
		if(!gurutextopen){
			TweenAlpha.Begin (SportimGuruText, 0.2f, 1.0f);

			guruPulseAnim["guruPulse"].time = 0;
			guruPulseAnim.Sample ();
			guruPulseAnim.Stop("guruPulse");
			CancelInvoke("GuruPulsate");
			//SportimGuruText.GetComponent<UIWidget>().alpha = 1.0f;
			gurutextopen = true;
			print ("Open the Guru");
			guruPulseAnim.Play("guruTalks");
			StartCoroutine ( playGuruIdle () );
		}
		else{
			GuruClose ();
//			TweenAlpha.Begin (SportimGuruText, 0.2f, 0.0f);
//			//SportimGuruText.GetComponent<UIWidget>().alpha = 0.0f;
//			print ("Open the Guru");
//			gurutextopen = false;
		}
		
	}

	IEnumerator playGuruIdle() {
		yield return new WaitForSeconds (2.5f);
		guruPulseAnim.Play("guruEyes");
	}


	//this method is to close all guru things(reset) so it can wait to be opened again from the checkguru method
	private void GuruClose(){
		TweenAlpha.Begin (SportimoGuru, 0.2f, 0.0f);
		TweenAlpha.Begin (SportimGuruText, 0.2f, 0.0f);
		guruPulseAnim.Stop("guruPulse");
		CancelInvoke("GuruPulsate");
		guruCloseCounter = 0;
		gurutextopen = false;
		currentHigh = 0;
		currentBiggestSpan = 0;
		//SportimoGuru.GetComponent<UIWidget>().alpha=0f;
		//SportimGuruText.GetComponent<UIWidget>().alpha=0.0f;

	}
	
	private void OnPlayCardPost (HTTPRequest req, HTTPResponse res)
	{
		if(res.StatusCode!=200 && res.StatusCode!=304){
			MessagePanel.Instance.animMessage(LocalizationManager.GetTermTranslation("preset_played", true));
			//Debug.Log ("Card Post Responce: "+ res.DataAsText);
			Loader.Visible(false);
			return;
		}
		Debug.Log ("Card Post Responce: "+ res.DataAsText);

		//TODO change match method for response card
		string json = res.DataAsText.Replace ("\"pointsAwarded\":null", "\"pointsAwarded\":0");
		json = json.Replace ("\"duration\":null", "\"duration\":0");
		Debug.Log (json);
		JsonData data = JsonMapper.ToObject (json);
		Loader.Visible (false);

		if (data ["error"] == null) {
			PlayCard clickedCard = JsonMapper.ToObject<PlayCard> (data ["userGamecard"].ToJson ());

			matchCtrl.OnCardPlay (clickedCard);
		} else {
			MessagePanel.Instance.animMessage (data ["error"].ToString ());
		}

		
	}

	void OnSwipe (SwipeGesture gesture)
	{
		if (ConfirmationModalController.ConfirmationIsActive)
			return;
		
		if (!isActive || dontDrag) {
			//print ("IsActive inside Swipe is: "+isActive);
			return;
		}

		//Check if animation is playing
		if (CenterCardsAnimator.GetCurrentAnimatorStateInfo (0).IsName ("CenterCardDown") || 
			CenterCardsAnimator.GetCurrentAnimatorStateInfo (0).IsName ("CenterCardUp") ||
			!isActive) {
//			print ("Animation Playing" + gesture.Direction);
			return;
		}

		closeCardInfoFast ();

//		print ("Gesture Velocity: " + gesture.Velocity);
		if (!PlayerPrefs.HasKey ("hasswiped"))
			PlayerPrefs.SetString ("hasswiped", "true");
		
			

		CenterCardsAnimator.speed = gesture.Velocity / 1000;

		if (CenterCardsAnimator.speed < 0.5f)
			CenterCardsAnimator.speed = 0.5f;
		if (CenterCardsAnimator.speed > 0.7f)
			CenterCardsAnimator.speed = 0.7f;

		if (gesture.Direction == FingerGestures.SwipeDirection.Down || gesture.Direction == FingerGestures.SwipeDirection.LowerLeftDiagonal
			|| gesture.Direction == FingerGestures.SwipeDirection.LowerRightDiagonal || gesture.Direction == FingerGestures.SwipeDirection.LowerDiagonals 
		    ) {

			cardIndex--;
			if (cardIndex < 0)
				cardIndex = centerPlayCards.Count - 1;

//			print (cardIndex);

			//animCardPanel.depth = 7;
			updateAnimCard (cardIndex);
			CenterCardsAnimator.SetTrigger ("swipedown");

//			float tempX = centerCard.transform.localPosition.x;
//			float tempY = centerCard.transform.localPosition.y + 40;
//			CenterAnimHolder.transform.localPosition = new Vector3( tempX, tempY, 0 );
//
//			iTween.RotateTo(CenterAnimHolder, iTween.Hash("z", 0, "time", 0.5f, "islocal",true, "easetype", iTween.EaseType.easeInCirc));
//			iTween.MoveTo(CenterAnimHolder, iTween.Hash("y", -430, "time", 0.5f, "islocal",true, "easetype", iTween.EaseType.easeInCirc, "onCompleteTarget",gameObject, "onComplete", "resetAnimHolder"));
		}

		if (gesture.Direction == FingerGestures.SwipeDirection.Up || gesture.Direction == FingerGestures.SwipeDirection.UpperLeftDiagonal
			|| gesture.Direction == FingerGestures.SwipeDirection.UpperRightDiagonal) {

			cardIndex++;
			if (cardIndex > centerPlayCards.Count - 1)
				cardIndex = 0;
			
			//iTween.RotateTo(CenterAnimHolder, iTween.Hash("z", 0, "time", 0.3f, "islocal",true, "easetype", iTween.EaseType.easeInCirc));
//			iTween.Stop();
//			iTween.MoveTo(CenterAnimHolder, iTween.Hash("y", 0, "time", 0.3f, "islocal",true, "easetype", iTween.EaseType.easeInCirc, "onCompleteTarget",gameObject, "onComplete", "resetAnimHolder"));
//			iTween.RotateTo(CenterAnimHolder, iTween.Hash("z", 0, "time", 0.3f, "islocal",true, "easetype", iTween.EaseType.easeInCirc));

			updateCardFront (cardIndex);

			CenterCardsAnimator.SetTrigger ("swipeup");
			resetAnimHolder ();
			//animCardPanel.depth = 9;
		}
		
	}

	void OnDrag (DragGesture gesture)
	{
		if (!isActive || dontDrag) {
			//print ("IsActive inside Drag is: "+isActive);
			return;
		}

		//Check if animation is playing
		if (CenterCardsAnimator.GetCurrentAnimatorStateInfo (0).IsName ("CenterCardDown") || 
			CenterCardsAnimator.GetCurrentAnimatorStateInfo (0).IsName ("CenterCardUp") ||
			!isActive) {
			//			print ("Animation Playing" + gesture.Direction);
			return;
		}

		Vector2 deltaMove = gesture.DeltaMove;
		//print (deltaMove);
		//print ("Dragging in centerCards");
		if (gesture.Phase == ContinuousGesturePhase.Started) {

			//close back card Alpha
			TweenAlpha.Begin (animcardName.gameObject, 0.01f, 0);
			TweenAlpha.Begin (animcardIcon.gameObject, 0.01f, 0);

			// change  Back animacards faster
			int tempCardIndex = cardIndex;
			tempCardIndex--;
			if (tempCardIndex < 0)
				tempCardIndex = centerPlayCards.Count - 1;
			updateAnimCard (tempCardIndex);
			
//			print ("Dragging in centerCards");
			CenterCardsAnimator.enabled = false;

//			print("Gesture Position "+ (gesture.Position.x -(Screen.width/2)));
			dragPoint = gesture.Position;
			float tempRotZ = (dragPoint.x - (Screen.width / 2)) / 10;
			iTween.RotateTo (CenterAnimHolder, iTween.Hash ("z", tempRotZ, "time", 1.5f, "islocal", true, "easetype", iTween.EaseType.easeOutElastic));
		}

		if (gesture.Phase == ContinuousGesturePhase.Ended) {
//			print ("Dragging ended in centerCards");
			CenterCardsAnimator.enabled = true;


			//iTween.RotateTo(CenterAnimHolder, iTween.Hash("z", 0, "time", 0.5f, "islocal",true, "easetype", iTween.EaseType.easeInCirc));

			int tempCardIndex = cardIndex;

			if (deltaMove.y > 0) {

				// change Front animacards faster
				tempCardIndex++;
				if (tempCardIndex > centerPlayCards.Count - 1)
					tempCardIndex = 0;
				updateCardFront (tempCardIndex);

//				print (deltaMove.y);
				iTween.MoveTo (CenterAnimHolder, iTween.Hash ("y", 13, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInCirc, "onCompleteTarget", gameObject, "onComplete", "resetAnimHolder"));
				iTween.ScaleTo (CenterAnimHolder, iTween.Hash ("x", 0.9f, "y", 0.9f, "time", 0.3f, "islocal", true, "easetype", iTween.EaseType.easeInCirc));
				iTween.ScaleTo (CenterAnimHolder, iTween.Hash ("x", 1f, "y", 1f, "time", 0.1f, "delay", 0.3f, "islocal", true));
				iTween.RotateTo (CenterAnimHolder, iTween.Hash ("z", 0, "time", 0.3f, "islocal", true, "easetype", iTween.EaseType.easeInCirc));

			} else  {



//				print (deltaMove.y);
				float tempX = centerCard.transform.localPosition.x;
				float tempY = centerCard.transform.localPosition.y + 40;
				CenterAnimHolder.transform.localPosition = new Vector3 (tempX, tempY, 0);
				
				iTween.RotateTo (CenterAnimHolder, iTween.Hash ("z", 0, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInCirc));
				iTween.MoveTo (CenterAnimHolder, iTween.Hash ("y", -430, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInCirc, "onCompleteTarget", gameObject, "onComplete", "resetAnimHolder"));
			}

			//resetAnimHolder();
			//centerCard.transform.localPosition = new Vector3(0,-40,0);
			//Tween CenterCard
			//iTween.MoveTo(centerCard, iTween.Hash("y", 470, "time", 1.0f, "islocal", true));
			//StartCoroutine(centerTweenDrop());
		}

		//print ("Gesture Position: "+gesture.Position);
	}

//	IEnumerator centerTweenDrop(){
//		yield return new WaitForSeconds (1.0f);
//		iTween.MoveTo(centerCard, iTween.Hash("y", -470, "time", 1.0f, "islocal", true));
//	}

	public void openCardInfo ()
	{
		//NGUITools.SetActive (cardStats, false);
		//NGUITools.SetActive (cardInfo, true);
//		print ("Open Choices");
		iTween.MoveTo (cardStory, iTween.Hash ("y", 0, "time", 0.3f, "easetype", iTween.EaseType.easeOutCirc, "islocal", true));
        // Open timeset with delay
        if(centerCardInfo.cardType == "PresetInstant")
            presetOpen();
	}

	void closeCardInfoFast ()
	{
//		print ("Close Choices");
		iTween.MoveTo (cardStory, iTween.Hash ("y", 0, "time", 0.0f, "islocal", true));
	}
	
	public void openCardStats ()
	{
		//NGUITools.SetActive (cardStats, true);
		//NGUITools.SetActive (cardInfo, false);
//		print ("Open Stats");
		iTween.MoveTo (cardStory, iTween.Hash ("y", -260, "time", 0.3f, "easetype", iTween.EaseType.easeOutCirc, "islocal", true));
        // close timeset
        presetClose();

    }

	public void playSwipeAnim() {
		if (!PlayerPrefs.HasKey ("hasswiped"))
		StartCoroutine (playSwipeAnimation ());
	}
	
	IEnumerator playSwipeAnimation ()
	{
		yield return new WaitForSeconds (1.0f);
		NGUITools.SetActive (swipeAnim, true);
		yield return new WaitForSeconds (4.0f);
		NGUITools.SetActive (swipeAnim, false);
	}

	string getUsedLang(Dictionary<string,string> dictionary) {
		string usedLang;

		if (dictionary.ContainsKey (LocalizationManager.CurrentLanguageCode))
			usedLang = LocalizationManager.CurrentLanguageCode;
		else
			usedLang = "en";

		return usedLang;
	}
}
