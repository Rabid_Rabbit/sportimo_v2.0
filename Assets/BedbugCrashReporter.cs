﻿using UnityEngine;

// This example shows a list of crash reports (if available),
// and allows you to output crash data to console, or
// delete them.
public class BedbugCrashReporter: MonoBehaviour {
	void OnGUI() {
		
		var reports = CrashReport.reports;
		if(reports.Length > 0)
		GUILayout.Label("Crash reports:");
		foreach (var r in reports) {
			GUILayout.BeginHorizontal();
			GUILayout.Label("Crash: " + r.time);
			if (GUILayout.Button("Log")) {
				Debug.Log(r.text);
			}
			if (GUILayout.Button("Remove")) {
				r.Remove();
			}
			GUILayout.EndHorizontal();
		}
	}
}