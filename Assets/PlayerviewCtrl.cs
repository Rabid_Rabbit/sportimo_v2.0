﻿
using UnityEngine;
using System.Collections;
using BedbugRest;
using I2.Loc;

public class PlayerviewCtrl : View {

	// Use this for initialization
	public new UILabel name;
	public UILabel weight;
	public UILabel height;
	public UILabel dob;
	public UILabel uniform;
	public UILabel foulsS;
	public UILabel foulsC;
	
	public UILabel yellow;
	
	public UILabel red;
	
	public UILabel pass;
	
	public UILabel assist;
	
	public UILabel intercept;
	
	public UILabel games;
	
	public UILabel starting;
	
	public UILabel goals_total;
	
	public UILabel kicked;
	public UILabel head;
	public UILabel winning;
	
	public UIGrid grid;
	public UIScrollView infoScroll;
	public UIPopupList infoPopList;
	public UILabel  infoLabel; 

	// UNI Select
	[Space(10)]
	public GameObject uniPopSelect;
	public UILabel selectLabel;
	public bool useTwoColors = false;
	private bool useDarc = false;
	private bool uniPopOpen = false;
	public UICenterOnChild center;
	public UIGrid selectTable;
	public UIScrollView selectScroll;
	public UIWrapContent wrap;
	public GameObject lightPrefab;
	public GameObject darkPrefab;
	public GameObject topPrefab;
	public GameObject bottomPrefab;
	GameObject uniTeam;
	[Space(10)]

	private string endpoint = "/players";

	public void onTransitionInStart(viewData _viewData){

		foreach(Transform trans in grid.GetChildList()){
			
			//if(trans.gameObject.activeSelf && trans.name != "nostat"){
			NGUITools.SetActive(trans.gameObject,false);
			//}
			//if(trans.gameObject.name == "nostat"){
			//	NGUITools.SetActive(trans.gameObject,false);
			//}
		}

		//Close uniPopSelect
		TweenAlpha.Begin (uniPopSelect, 0.0f, 0);
		//		NGUITools.SetActive (uniPopSelect, false);
		uniPopOpen = false;

		Loader.Visible (true);
		API.GetOneByID (Main.Settings.apis ["data"] + endpoint, _viewData.data.ToString (), OnGetPlayerCallback);
	}
	
	Player player;
	public void OnGetPlayerCallback(BestHTTP.HTTPRequest req,BestHTTP.HTTPResponse res){
		
		if(res.StatusCode != 200 && res.StatusCode != 304){
			MessagePanel.Instance.animMessage(res.DataAsText);
			return;
		}
		
		LitJson.JsonData data=LitJson.JsonMapper.ToObject(res.DataAsText);
		BedbugRest.API.Log(res.DataAsText);
		
		player = LitJson.JsonMapper.ToObject<Player>(data["player"].ToJson());

		name.text = LocalizationManager.FixRTL_IfNeeded( player.name[LocalizationManager.CurrentLanguageCode]);
		if(player.position == "Defender"){
			name.text+=" ("+"DF)";
		}
		else if(player.position=="Forward"){
			name.text+=" ("+"FW)";
		}
		else if(player.position=="Goalkeeper"){
			name.text+=" ("+"GK)";
		}
		else if(player.position=="Midfielder"){
			name.text+=" ("+"MF)";
		}
		

		
		//if(player.personalData != null){
		//	if(player.personalData.weight != null)
		//		weight.text=player.personalData.weight.kilograms.ToString();
		//	if(player.personalData.height!=null)
		//		height.text=player.personalData.height.centimeters.ToString();
		//}
		
		if(player.personalData.birth != null)
			dob.text = player.personalData.birth.birthDate["full"].ToString();
		
		infoLabel.text = "Season Statistics";
		
		if(player.stats != null && player.stats.ContainsKey("season")){

//			print(infoPopList.value);
//			infoPopList.value = "Season Statistics";
//			infoLabel.text = infoPopList.value;
//			print(infoPopList.value);

			foulsC.text = player.stats["season"].foulsCommitted.ToString();
			foulsS.text = player.stats["season"].foulsSuffered.ToString();
			yellow.text = player.stats["season"].yellowCards.ToString();
			red.text = player.stats["season"].redCards.ToString();
			pass.text = player.stats["season"].touchesPasses.ToString();
			assist.text = player.stats["season"].assistsTotal.ToString();
			intercept.text = player.stats["season"].touchesInterceptions.ToString();
			games.text = player.stats["season"].gamesPlayed.ToString();
			starting.text = player.stats["season"].gamesStarted.ToString();
			goals_total.text = player.stats["season"].goalsTotal.ToString();
			kicked.text = player.stats["season"].goalsKicked.ToString();
			head.text = player.stats["season"].goalsHeaded.ToString();
			winning.text = player.stats["season"].goalsGameWinning.ToString();
			
			float delayMove = 0.0f;
			infoScroll.ResetPosition();
			foreach(Transform trans in grid.GetChildList()){
				
				if(!trans.gameObject.activeSelf && trans.name != "nostat"){
					NGUITools.SetActive(trans.gameObject,true);
					
					
					float topY = trans.localPosition.y;
					topY -= 30;
					iTween.MoveFrom(trans.gameObject, iTween.Hash("y", topY, "time",0.3f, "delay", delayMove,"easetype", iTween.EaseType.easeOutCirc,  "islocal", true)); 
					delayMove += 0.1f;
				}
				//				if(trans.gameObject.name == "nostat"){
				//					NGUITools.SetActive(trans.gameObject,false);
				//				}
			}
		}
		else{
			foreach(Transform trans in grid.GetChildList()){
				
				//			if(trans.gameObject.activeSelf){
				//				NGUITools.SetActive(trans.gameObject,false);
				//			}
				if(trans.name == "nostat"){
					NGUITools.SetActive(trans.gameObject,true);
				}
				
			}
			
			//grid.Reposition();
		}
		
		
		Loader.Visible (false);

	}

	public void toggleUniSelect() {
		if (uniPopOpen) {
			uniPopOpen = false;
			TweenAlpha.Begin (uniPopSelect, 0.2f, 0);
		} else {
			uniPopOpen = true;
			TweenAlpha.Begin (uniPopSelect, 0.2f, 1);
		}
	}

	public  void uniSelect(GameObject GO) {

		switch (GO.name) {

		case "career":
			if(player.stats != null && player.stats.ContainsKey("career")){
				foulsC.text = player.stats["career"].foulsCommitted.ToString();
				foulsS.text = player.stats["career"].foulsSuffered.ToString();
				yellow.text = player.stats["career"].yellowCards.ToString();
				red.text = player.stats["career"].redCards.ToString();
				pass.text = player.stats["career"].touchesPasses.ToString();
				assist.text = player.stats["career"].assistsTotal.ToString();
				intercept.text = player.stats["career"].touchesInterceptions.ToString();
				games.text = player.stats["career"].gamesPlayed.ToString();
				starting.text = player.stats["career"].gamesStarted.ToString();
				goals_total.text = player.stats["career"].goalsTotal.ToString();
				kicked.text = player.stats["career"].goalsKicked.ToString();
				head.text = player.stats["career"].goalsHeaded.ToString();
				winning.text = player.stats["career"].goalsGameWinning.ToString();
				selectLabel.text = I2.Loc.LocalizationManager.GetTermTranslation ("career_stats", true);
			}
			break;

		case "season":
			if(player.stats != null && player.stats.ContainsKey("season")){
				foulsC.text = player.stats["season"].foulsCommitted.ToString();
				foulsS.text = player.stats["season"].foulsSuffered.ToString();
				yellow.text = player.stats["season"].yellowCards.ToString();
				red.text = player.stats["season"].redCards.ToString();
				pass.text = player.stats["season"].touchesPasses.ToString();
				assist.text = player.stats["season"].assistsTotal.ToString();
				intercept.text = player.stats["season"].touchesInterceptions.ToString();
				games.text = player.stats["season"].gamesPlayed.ToString();
				starting.text = player.stats["season"].gamesStarted.ToString();
				goals_total.text = player.stats["season"].goalsTotal.ToString();
				kicked.text = player.stats["season"].goalsKicked.ToString();
				head.text = player.stats["season"].goalsHeaded.ToString();
				winning.text = player.stats["season"].goalsGameWinning.ToString();
				selectLabel.text = I2.Loc.LocalizationManager.GetTermTranslation ("season_stats", true);
			}
			break;

		case "team":
			if(player.stats != null && player.stats.ContainsKey("team")){
				foulsC.text = player.stats["team"].foulsCommitted.ToString();
				foulsS.text = player.stats["team"].foulsSuffered.ToString();
				yellow.text = player.stats["team"].yellowCards.ToString();
				red.text = player.stats["team"].redCards.ToString();
				pass.text = player.stats["team"].touchesPasses.ToString();
				assist.text = player.stats["team"].assistsTotal.ToString();
				intercept.text = player.stats["team"].touchesInterceptions.ToString();
				games.text = player.stats["team"].gamesPlayed.ToString();
				starting.text = player.stats["team"].gamesStarted.ToString();
				goals_total.text = player.stats["team"].goalsTotal.ToString();
				kicked.text = player.stats["team"].goalsKicked.ToString();
				head.text = player.stats["team"].goalsHeaded.ToString();
				winning.text = player.stats["team"].goalsGameWinning.ToString();
				selectLabel.text = I2.Loc.LocalizationManager.GetTermTranslation ("team_stats", true);
			}
			break;

		}

		// Close UniSelect
		toggleUniSelect ();

	}
	
//	public override void selectionChanged(string selection) {
//		base.selectionChanged (selection);
//		
//		int itemsIndex = UIPopupList.current.items.IndexOf (UIPopupList.current.value);
//		//		print (selection +" This was fired from the Standings view");
//		//		print(UIPopupList.current.value); 
//		//		print(UIPopupList.current.items.IndexOf(UIPopupList.current.value));
//		//StartCoroutine( PopulateLeague(selection));
//		
//		switch (itemsIndex) {
//			
//		case 0:
//			if(player.stats != null && player.stats.ContainsKey("career")){
//				foulsC.text = player.stats["career"].foulsCommitted.ToString();
//				foulsS.text = player.stats["career"].foulsSuffered.ToString();
//				yellow.text = player.stats["career"].yellowCards.ToString();
//				red.text = player.stats["career"].redCards.ToString();
//				pass.text = player.stats["career"].touchesPasses.ToString();
//				assist.text = player.stats["career"].assistsTotal.ToString();
//				intercept.text = player.stats["career"].touchesInterceptions.ToString();
//				games.text = player.stats["career"].gamesPlayed.ToString();
//				starting.text = player.stats["career"].gamesStarted.ToString();
//				goals_total.text = player.stats["career"].goalsTotal.ToString();
//				kicked.text = player.stats["career"].goalsKicked.ToString();
//				head.text = player.stats["career"].goalsHeaded.ToString();
//				winning.text = player.stats["career"].goalsGameWinning.ToString();
//			}
//			break;
//			
//		case 1:
//			if(player.stats != null && player.stats.ContainsKey("season")){
//				foulsC.text = player.stats["season"].foulsCommitted.ToString();
//				foulsS.text = player.stats["season"].foulsSuffered.ToString();
//				yellow.text = player.stats["season"].yellowCards.ToString();
//				red.text = player.stats["season"].redCards.ToString();
//				pass.text = player.stats["season"].touchesPasses.ToString();
//				assist.text = player.stats["season"].assistsTotal.ToString();
//				intercept.text = player.stats["season"].touchesInterceptions.ToString();
//				games.text = player.stats["season"].gamesPlayed.ToString();
//				starting.text = player.stats["season"].gamesStarted.ToString();
//				goals_total.text = player.stats["season"].goalsTotal.ToString();
//				kicked.text = player.stats["season"].goalsKicked.ToString();
//				head.text = player.stats["season"].goalsHeaded.ToString();
//				winning.text = player.stats["season"].goalsGameWinning.ToString();
//			}
//			break;
//			
//		case 2:
//			if(player.stats != null && player.stats.ContainsKey("team")){
//				foulsC.text = player.stats["team"].foulsCommitted.ToString();
//				foulsS.text = player.stats["team"].foulsSuffered.ToString();
//				yellow.text = player.stats["team"].yellowCards.ToString();
//				red.text = player.stats["team"].redCards.ToString();
//				pass.text = player.stats["team"].touchesPasses.ToString();
//				assist.text = player.stats["team"].assistsTotal.ToString();
//				intercept.text = player.stats["team"].touchesInterceptions.ToString();
//				games.text = player.stats["team"].gamesPlayed.ToString();
//				starting.text = player.stats["team"].gamesStarted.ToString();
//				goals_total.text = player.stats["team"].goalsTotal.ToString();
//				kicked.text = player.stats["team"].goalsKicked.ToString();
//				head.text = player.stats["team"].goalsHeaded.ToString();
//				winning.text = player.stats["team"].goalsGameWinning.ToString();
//			}
//			break;
//			
//		}
//		
//	}
	
}
