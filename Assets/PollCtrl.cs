﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;
using LitJson;
using BedbugRest;

public class PollCtrl : MonoBehaviour {

	private Poll thisPoll;

	// Poll Question
	public UILabel question;
	public List<GameObject> answerObjs = new List<GameObject>();
	public List<UILabel> answers = new List<UILabel>();
	public List<UITexture> answerimgs = new List<UITexture>();

	// Poll Answers
	public List<GameObject> bars = new List<GameObject>();
	
	public List<UILabel> persantages = new List<UILabel>();
	public List<UILabel> names = new List<UILabel>();
	public List<UITexture> imgs = new List<UITexture>();
	public List<UIProgressBar> progresses = new List<UIProgressBar> ();

	public UITexture bannerPic;
	public Animation bannerAnim;

	public UITable parentTable;
	public UITable choicesTable;
	public UITable barsTable;

	public UILabel yourAnswer;

	public void updatePoll(Poll poll) {
		thisPoll = poll;
        Debug.Log(thisPoll._id);
		// get Used Language
		//string usedlang = getUsedLang (poll);

		// Add Question
		question.text = LocalizationManager.FixRTL_IfNeeded( poll.text [getUsedLang(poll.text)]);

		for (int k = 0; k < 4; k++) { // Close Everything
			NGUITools.SetActive(answerObjs[k], false);
			NGUITools.SetActive(bars[k], false);
			progresses[k].value = 0;
			//imgs[k]= null;
			//imgs[k].mainTexture = "";
		}


		for (int i = 0; i < poll.answers.Count; i++) { // Answer Number depended
			// Add Text and answerid to the buttons
			answers[i].text = LocalizationManager.FixRTL_IfNeeded( poll.answers[i].text[ getUsedLang( poll.answers[i].text ) ] );
			answerObjs[i].name = poll.answers[i]._id;

			if (poll.hasAnswered != null) {
				if( poll.hasAnswered == poll.answers [i]._id) //BedBugTools.GetStringByLocPrefs (questionText)
                    yourAnswer.text = LocalizationManager.FixRTL_IfNeeded ( LocalizationManager.GetTermTranslation("You_answered", false) +" "+ BedBugTools.GetStringByLocPrefs(poll.answers[i].text), 0, true) ;
			}

			if(poll.answers[i].img != null) {
				BedBugTools.loadImageOnUITextureAndResize (answerimgs[i], poll.answers[i].img,64,64, FadeInOnLoad, 0 );
			}

			NGUITools.SetActive(answerObjs[i], true);

			// Add info to the poll bars
			float value = (float)poll.answers[i].percent / 100;
			print (value);
			fillToAmount(value, 3.5f, progresses[i]);
			persantages[i].text = poll.answers[i].percent+"%";
			names[i].text = LocalizationManager.FixRTL_IfNeeded( poll.answers[i].text[ getUsedLang( poll.answers[i].text ) ]);

			if(poll.answers[i].img != null) {
				BedBugTools.loadImageOnUITextureAndResize (imgs[i], poll.answers[i].img,64,64, FadeInOnLoad, 0 );
			}
			NGUITools.SetActive(bars[i], true);
		}

		choicesTable.Reposition ();
		barsTable.Reposition ();

		barsTable.transform.localPosition = Vector3.zero;

		if (poll.hasAlreadyVoted == 1) {
			NGUITools.SetActive(choicesTable.transform.parent.gameObject, false);
			NGUITools.SetActive(barsTable.transform.parent.gameObject, true);

		} else {
			NGUITools.SetActive(choicesTable.transform.parent.gameObject, true);
			NGUITools.SetActive(barsTable.transform.parent.gameObject, false);
		}

		if (poll.sponsor != null) {
			BedBugTools.loadImageOnUITextureAndResize (bannerPic, poll.sponsor.banner,320,75, openBanner, 1);
		}

		StartCoroutine( delayRepoTable (barsTable, 0.1f));
		StartCoroutine( delayRepoTable (choicesTable, 0.1f));
        StartCoroutine(repositionDelay(parentTable, 0.2f));
	}

    IEnumerator repositionDelay(UITable table, float delay)
    {
        yield return new WaitForSeconds(0.1f);
        table.repositionNow = true;
        table.Reposition();
    }

	IEnumerator delayRepoTable (UITable table, float delay) {
		yield return new WaitForSeconds (delay);
		print ("Repositioning!!!!");

		if (table == choicesTable) {
			if (thisPoll.answers.Count > 2)
				table.transform.localPosition = new Vector3 (0, 40, 0);
			else
				table.transform.localPosition = new Vector3 (0, 20, 0);
		} else {
			table.transform.localPosition = new Vector3 (0, 0, 0);
		}

	}

	//establish parameter hash:
	Hashtable ht;
	void fillToAmount(float fillTo, float speed, UIProgressBar fillSprite)
	{
	
		if(fillSprite == progresses[0])
			ht = iTween.Hash("from",0,"to",fillTo,"time",1.0f,"onupdate","changeFillValue0", "easetype", iTween.EaseType.easeOutCubic);
		else if(fillSprite == progresses[1])
			ht = iTween.Hash("from",0,"to",fillTo,"time",1.0f,"delay", 0.5f,"onupdate","changeFillValue1", "easetype", iTween.EaseType.easeOutCubic);
		else if(fillSprite == progresses[2])
			ht = iTween.Hash("from",0,"to",fillTo,"time",1.0f,"delay", 1.5f,"onupdate","changeFillValue2", "easetype", iTween.EaseType.easeOutCubic);
		else if(fillSprite == progresses[3])
			ht = iTween.Hash("from",0,"to",fillTo,"time",1.0f,"delay", 2.0f,"onupdate","changeFillValue3", "easetype", iTween.EaseType.easeOutCubic);
		
		
		//make iTween call:
		iTween.ValueTo(gameObject, ht);
	}
	void changeFillValue0(float newValue){
		progresses[0].value = newValue;
		persantages[0].text = (int)( newValue*100)+"%";
	}
	void changeFillValue1(float newValue){
		progresses[1].value = newValue;
		persantages[1].text = (int)( newValue*100)+"%";
	}
	void changeFillValue2(float newValue){
		progresses[2].value = newValue;
		persantages[2].text = (int)( newValue*100)+"%";
	}
	void changeFillValue3(float newValue){
		progresses[3].value = newValue;
		persantages[3].text = (int)( newValue*100)+"%";
	}

	private void FadeInOnLoad(UITexture texture){
		if(texture){
			UITweener tween = texture.GetComponent<UITweener> ();
			
			if (tween)
				tween.PlayForward ();
			else
				TweenAlpha.Begin (texture.gameObject, 0.3f, 1.0f);
		}
	}

	void openBanner (UITexture obj)
	{
		bannerAnim.Play ("bannerAnimOpen");
		StartCoroutine (repoPollsGrid ());
	}

	IEnumerator repoPollsGrid ()
	{
		yield return new WaitForSeconds (0.5f);
		parentTable.Reposition ();
	}

	// ANSWERS

	public void updateAnswer(GameObject answerObj) {

		string answerId = answerObj.name;

		Hashtable table = new Hashtable ();
		table.Add ("userid", Main.AppUser._id);
		table.Add ("answerid", answerId);


		Debug.Log (JsonMapper.ToJson (table));


		Loader.Visible(true);
		API.PostOneCard (Main.Settings.apis ["polls"] +"/"+thisPoll._id+"/vote", table, OnPollPost);
	}

	void OnPollPost (BestHTTP.HTTPRequest originalRequest, BestHTTP.HTTPResponse response)
	{
		Loader.Visible(false);
		Debug.Log (response.DataAsText);

		Poll poll = LitJson.JsonMapper.ToObject<Poll> (response.DataAsText);
		poll.hasAlreadyVoted = 1;
		updatePoll (poll);
	}

	string getUsedLang(Dictionary<string,string> dictionary) {
		string usedLang;

		if (dictionary.ContainsKey (LocalizationManager.CurrentLanguageCode))
			usedLang = LocalizationManager.CurrentLanguageCode;
		else
			usedLang = "en";

		return usedLang;
	}

//	string getUsedLang(Poll poll) {
//		string usedLang;
//
//		if (poll.text.ContainsKey (LocalizationManager.CurrentLanguageCode))
//			usedLang = LocalizationManager.CurrentLanguageCode;
//		else
//			usedLang = "en";
//
//		return usedLang;
//	}
}
