﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BedbugRest;
using LitJson;
using System.Linq;
using I2.Loc;
public class profilectrl : View {
	
	
	public UILabel matches_num;
	
	public UILabel points_num;
	
	public UILabel cards_num;
	
	public UILabel cards_won_num;
	
	public UILabel cards_percent;
	
	public UILabel cards_label;
	
	public UISprite fill;
	
	public UILabel ppm;
	
	public UILabel all_percent;
	public UISprite all_fill;
	public UISprite[] last_games;
	
	public UILabel top_label;
	public UILabel cardsUserLabel;
	public UILabel overUserLabel;
	public UILabel liveUserLabel;

	public UIScrollView achievsScroll;
	public UIScrollView statsScroll;
	public UIGrid ach_grid;

	public UISprite starsfill;
	public GameObject AchPrefab;

	// overall stats
	public UISprite overRed;
	public UISprite overGreen;
	public UILabel overTotal;
	public UILabel overYouCor;
	public UILabel overAllCor;

	// live stats
	public UISprite liveRed;
	public UISprite liveGreen;
	public UILabel liveTotal;
	public UILabel liveYouCor;
	public UILabel liveAllCor;

	//public UISprite avatarSprite;
	public UITexture avatarSprite;

	public GameObject noData;

	public UITable statsTable;

	// Best Rank
	public GameObject rankObj;
	public UILabel bestRank;
	public UILabel rank_dates;
	public UILabel rank_homename;
	public UILabel rank_awayname;
	public UILabel scoreRankLabel;
	public UITexture rank_home;
	public UITexture rank_away;

	// Best score
	public GameObject scoreObj;
	public UILabel bestscore;
	public UILabel score_dates;
	public UILabel score_homename;
	public UILabel score_awayname;
	public UILabel scoreLabel;
	public UITexture score_home;
	public UITexture score_away;


    public GameObject locked_stats;

    //establish parameter hash:
    Hashtable ht;
	// Use this for initialization
	public void onTransitionInStart(viewData _viewData){
       

        if (isUserView) {
			avatarSprite.mainTexture = null;
			//openAchievs ();
			openStats();
			//Destroy Previous 
			foreach (Transform transform in ach_grid.GetChildList()) {
				if (transform.gameObject.activeSelf)
					NGUITools.Destroy (transform);
			}
		} else {
			Coach.coachMessage ( LocalizationManager.GetTermTranslation( "tip9", true, 30, true), 27f, 5f, 1.5f, true, "tip9");
		}
		clearAllData ();
        //		print (Main.Settings.apis ["users"] + "/");
        //		print (arg.ToString()+"/stats");
        if (Main.isPaidCustomer("", true))
        {
			if(locked_stats)
            locked_stats.SetActive(false);
            API.GetOneByID(Main.Settings.apis["users"] + "/", _viewData.data.ToString() + "/stats", OnGetStatsCallback);
            Loader.Visible(true);
        }
        else {
            Debug.Log("this");
			if(locked_stats)
            locked_stats.SetActive(true);
            top_label.text = LocalizationManager.GetTermTranslation("myprofile", true);
        }
       
	}
		
	public void OnGetStatsCallback(BestHTTP.HTTPRequest req,BestHTTP.HTTPResponse res){
       Debug.Log(res.DataAsText);

//		print (res.StatusCode);
		if (res.StatusCode == 500) {
			Loader.Visible (false);
			MessagePanel.Instance.animMessage (LocalizationManager.GetTermTranslation("message26", true), LocalizationManager.GetTermTranslation ("Warning", true), "sportimo-Logo", 3.0f);
			GameObject noDataObj = NGUITools.AddChild (ach_grid.gameObject, noData);
			NGUITools.SetActive(noDataObj, true);
			openAchievs();
			achievsScroll.ResetPosition();
			return;
		}



        ProfileStats profile = JsonMapper.ToObject<ProfileStats>(res.DataAsText);
       
		if (profile.user == null || res.StatusCode == 500 && isUserView) {

			Loader.Visible (false);
			MessagePanel.Instance.animMessage (LocalizationManager.GetTermTranslation("message26", true), LocalizationManager.GetTermTranslation ("Warning", true), "sportimo-Logo", 3.0f);
			GameObject noDataObj = NGUITools.AddChild (ach_grid.gameObject, noData);
			NGUITools.SetActive(noDataObj, true);
			openAchievs();
			achievsScroll.ResetPosition();
			return;
		}

		if (profile.user._id != Main.AppUser._id) {
			top_label.text = profile.user.username + "'s Profile";
			if(isUserView) {
				cardsUserLabel.text = profile.user.username+":";
				overUserLabel.text = profile.user.username+":";
				liveUserLabel.text = profile.user.username+":";
			}
		} else {
			top_label.text = LocalizationManager.GetTermTranslation ("myprofile", true);
			if(isUserView) {
				cardsUserLabel.text = LocalizationManager.GetTermTranslation ("you", true);
				overUserLabel.text = LocalizationManager.GetTermTranslation ("you", true);
				liveUserLabel.text = LocalizationManager.GetTermTranslation ("you", true);
			}
		}

		if (isUserView) {
			//openAchievs ();
			openStats();
			achievsScroll.ResetPosition();

			avatarSprite.mainTexture = null;
			if (profile.user.picture != null) {
				BedBugTools.loadImageOnUITextureAndResize (avatarSprite, profile.user.picture, 128,128, FadeInOnLoad, 0);
			} else {
				string baseUrl = "https://s3-eu-west-1.amazonaws.com/sportimo-media/avatars/playerProfile.png";
				BedBugTools.loadImageOnUITextureAndResize (avatarSprite, baseUrl, 128,128, FadeInOnLoad, 0);
			}
		}

		if (starsfill != null) {
			starsfill.fillAmount = (float) profile.user.level;

			foreach (Achievement ach in profile.user.achievements) {
				GameObject achiev = NGUITools.AddChild (ach_grid.gameObject, AchPrefab);
				NGUITools.SetActive (achiev, true);

				achiev.transform.FindChild ("Name&Fill/TitleLabel").GetComponent<UILabel> ().text = LocalizationManager.FixRTL_IfNeeded( ach.title [ getUsedLang(ach.title) ], 0, true);
				achiev.transform.FindChild ("Name&Fill/BarGreenFill").GetComponent<UISprite> ().fillAmount = (float)ach.has / ach.total;
				achiev.transform.FindChild ("Name&Fill/DescriptionLabel").GetComponent<UILabel> ().text = LocalizationManager.FixRTL_IfNeeded( ach.text [ getUsedLang(ach.text) ], 0, true);
				achiev.transform.FindChild ("Count/CountLabel").GetComponent<UILabel> ().text = ach.has.ToString () + "/" + ach.total.ToString ();
			}
		}

		if (ach_grid != null) {
			ach_grid.Reposition ();
			ach_grid.transform.parent.gameObject.GetComponent<UIScrollView> ().ResetPosition ();
		}

		print ("Profile Stats: " + profile.user.stats);
		if (profile.user.stats != null) {
			matches_num.text = profile.user.stats.matchesPlayed.ToString ();
			points_num.text = profile.pointsPerGame.ToString ();
			cards_num.text = profile.user.stats.cardsPlayed.ToString ();
			cards_won_num.text = profile.user.stats.cardsWon.ToString ();
			cards_label.text = profile.user.stats.cardsPlayed.ToString ();
		
	//CARDS PLAYED -------------------------------------------------------------------------------------------------------------------------------------
			if (profile.user.stats.cardsPlayed != 0) {
				cards_percent.text = (profile.user.stats.cardsWon * 100.0 / profile.user.stats.cardsPlayed).ToString ("#") + "% " + LocalizationManager.GetTermTranslation ("correct"); 
				float fillAmount = (float)(profile.user.stats.cardsWon * 100.0 / profile.user.stats.cardsPlayed) / 100;
//				print ("fillAmount " + fillAmount);
			
				fillToAmount (fillAmount, 3.0f, fill);
			} else {
				fill.fillAmount = 0;
				cards_percent.text = "0%";
			}

			//all_fill.fillAmount=(float)(profile.all.successPercent/100);
			profile.all.successPercent = 32;
			fillToAmount ((float)(profile.all.successPercent / 100), 3.0f, all_fill);
			all_percent.text = (profile.all.successPercent.ToString ("#") + "% " + LocalizationManager.GetTermTranslation ("correct"));

	//OVERALL CARDS PLAYED -------------------------------------------------------------------------------------------------------------------------------------
			print ("Over Played: "+profile.user.stats.overallCardsPlayed);
			print ("Over Won: "+profile.user.stats.overallCardsWon);

			if (profile.user.stats.overallCardsPlayed != 0) {
//				print ("Overall cards Pl;ayed: "+profile.user.stats.overallCardsPlayed);
				if(profile.user.stats.overallCardsWon!=0)
					overYouCor.text = ( profile.user.stats.overallCardsWon * 100.0 / profile.user.stats.overallCardsPlayed).ToString ("#") + "% " + LocalizationManager.GetTermTranslation ("correct") ;
				else
					overYouCor.text = "0% " + LocalizationManager.GetTermTranslation ("correct");

				float fillAmount = (float)(profile.user.stats.overallCardsWon * 100.0 / profile.user.stats.overallCardsPlayed) / 100;

//				print ("fillAmount " + fillAmount);

				overTotal.text = profile.user.stats.overallCardsPlayed.ToString();

				fillToAmount (fillAmount, 3.0f, overGreen);
			} else {
				overGreen.fillAmount = 0;
				overTotal.text = "0";
				overYouCor.text = "0%";
			}
			print ("Over ALl: "+(float)profile.all.overallSuccessPercent);
			//profile.all.overallSuccessPercent = 17;
			//all_fill.fillAmount=(float)(profile.all.successPercent/100);
			profile.all.overallSuccessPercent = 18;
			fillToAmount ((float)(profile.all.overallSuccessPercent / 100), 3.0f, overRed);
			overAllCor.text = (profile.all.overallSuccessPercent.ToString ("#") + "% " + LocalizationManager.GetTermTranslation ("correct"));

	//LIVE CARDS PLAYED -------------------------------------------------------------------------------------------------------------------------------------
			if (profile.user.stats.instantCardsPlayed != 0) {
				if(profile.user.stats.instantCardsWon!=0)
					liveYouCor.text = (profile.user.stats.instantCardsWon * 100.0 / profile.user.stats.instantCardsPlayed).ToString ("#") + "% " + LocalizationManager.GetTermTranslation ("correct");
				else
					liveYouCor.text = "0% " + LocalizationManager.GetTermTranslation ("correct");

				float fillAmount = (float)(profile.user.stats.instantCardsWon * 100.0 / profile.user.stats.instantCardsPlayed) / 100;
//				print ("fillAmount " + fillAmount);

				liveTotal.text = profile.user.stats.instantCardsPlayed.ToString();

				fillToAmount (fillAmount, 3.0f, liveGreen);
			} else {
				liveGreen.fillAmount = 0;
				liveTotal.text = "0";
				liveYouCor.text = "0%";
			}
			
			//all_fill.fillAmount=(float)(profile.all.successPercent/100);
			profile.all.instantSuccessPercent = 32;
			fillToAmount ((float)(profile.all.instantSuccessPercent / 100), 3.0f, liveRed);

			liveAllCor.text = (profile.all.instantSuccessPercent.ToString ("#") + "% " + LocalizationManager.GetTermTranslation ("correct"));

		} else {
			matches_num.text = "0";
			points_num.text = "0";

			cards_num.text = "0";
			cards_won_num.text = "0";
			cards_label.text = "0";
			liveTotal.text = "0";
			overTotal.text = "0";


			fill.fillAmount = 0;
			liveGreen.fillAmount = 0;
			overGreen.fillAmount = 0;

			cards_percent.text = "0%";
			overYouCor.text = "0%";
			liveYouCor.text = "0%";

			fillToAmount ((float)(profile.all.overallSuccessPercent / 100), 3.0f, overRed);
			overAllCor.text = (profile.all.overallSuccessPercent.ToString ("#") + "% " + LocalizationManager.GetTermTranslation ("correct"));

			fillToAmount ((float)(profile.all.instantSuccessPercent / 100), 3.0f, liveRed);
			liveAllCor.text = (profile.all.instantSuccessPercent.ToString ("#") + "% " + LocalizationManager.GetTermTranslation ("correct"));

			fillToAmount ((float)(profile.all.successPercent / 100), 3.0f, all_fill);
			all_percent.text = (profile.all.successPercent.ToString ("#") + "% " + LocalizationManager.GetTermTranslation ("correct"));
		}




		if(profile.lastmatches != null && profile.lastmatches.Count() != 0){
				int max=profile.lastmatches.Max();
				for(int i=0; i< profile.lastmatches.Length;i++){
					//last_games[i].fillAmount = (float)(profile.lastmatches[i]*100.0/max)/100;

					fillToAmount((float)(profile.lastmatches[i]*100.0/max)/100, 3.0f, last_games[i]);
				}
				if(profile.lastmatches.Length<5){
					for(int j = profile.lastmatches.Length; j<5; j++){
						last_games[j].fillAmount=0;
					}
					
				}
			}
			else{
				for(int j=0;j<5;j++){
						last_games[j].fillAmount=0;
					}
			}

		if (profile.user.rankingStats != null) {
//			print ("Best rank: " + profile.user.rankingStats.bestRank);

			if(profile.user.rankingStats.bestRank != 9999 && profile.user.rankingStats.bestRank != 0){
				NGUITools.SetActive (rankObj, true);

				bestRank.text = "#"+profile.user.rankingStats.bestRank.ToString();

				// last match
				if( profile.user.rankingStats.bestRankMatch != null ) {
					rank_dates.text = profile.user.rankingStats.bestRankMatch.start.ToString("dd/MM HH:mm"); // team.lastmatch.eventdate.ToString ("dd/MM HH:mm");
					rank_homename.text = LocalizationManager.FixRTL_IfNeeded( profile.user.rankingStats.bestRankMatch.home_team.name[LocalizationManager.CurrentLanguageCode]);
					BedBugTools.loadImageOnUITextureAndResize (rank_home, profile.user.rankingStats.bestRankMatch.home_team.logo, 64, 64);
					
					rank_awayname.text = LocalizationManager.FixRTL_IfNeeded( profile.user.rankingStats.bestRankMatch.away_team.name[LocalizationManager.CurrentLanguageCode]);
					BedBugTools.loadImageOnUITextureAndResize (rank_away, profile.user.rankingStats.bestRankMatch.away_team.logo, 64, 64);
					
					scoreRankLabel.text = profile.user.rankingStats.bestRankMatch.home_score+"-"+profile.user.rankingStats.bestRankMatch.away_score;
				} else {
					rank_dates.text ="";
					rank_homename.text ="";
					rank_home.mainTexture = null;
					rank_awayname.text ="";
					rank_away.mainTexture = null;
					scoreRankLabel.text ="";
				}
			} else {
				NGUITools.SetActive (rankObj, false);
				//bestRank.text = "#0";
			}

			// Most Points
			if(profile.user.rankingStats.bestScore != 0){
				NGUITools.SetActive (scoreObj, true);
				
//				print ("Best score: " + profile.user.rankingStats.bestScore);
				bestscore.text = profile.user.rankingStats.bestScore.ToString();
				
				// last match
				if( profile.user.rankingStats.bestScoreMatch != null ) {

					score_dates.text = profile.user.rankingStats.bestScoreMatch.start.ToString("dd/MM HH:mm"); // team.lastmatch.eventdate.ToString ("dd/MM HH:mm");
					score_homename.text = LocalizationManager.FixRTL_IfNeeded( profile.user.rankingStats.bestScoreMatch.home_team.name[LocalizationManager.CurrentLanguageCode]);
					BedBugTools.loadImageOnUITextureAndResize (score_home, profile.user.rankingStats.bestScoreMatch.home_team.logo, 64, 64);
					
					score_awayname.text = LocalizationManager.FixRTL_IfNeeded( profile.user.rankingStats.bestScoreMatch.away_team.name[LocalizationManager.CurrentLanguageCode]);
					BedBugTools.loadImageOnUITextureAndResize (score_away, profile.user.rankingStats.bestScoreMatch.away_team.logo, 64, 64);
					
					scoreLabel.text = profile.user.rankingStats.bestScoreMatch.home_score+"-"+profile.user.rankingStats.bestScoreMatch.away_score;
				} else {

					score_dates.text ="";
					score_homename.text ="";
					score_home.mainTexture = null;
					score_awayname.text ="";
					score_away.mainTexture = null;
					scoreLabel.text ="";

				}

				       
			}else{
				NGUITools.SetActive (scoreObj, false);
				//bestRank.text = "#0";
			}

		} else {

			print ("No ranking stats!");
		}

		if(statsTable != null)
		statsTable.Reposition ();

		Loader.Visible (false);
	}

	private void FadeInOnLoad(UITexture texture){
		
		TweenAlpha.Begin (texture.gameObject, 0.5f, 1.0f);
	}

	void fillToAmount(float fillTo, float speed, UISprite fillSprite)
	{


		if(fillSprite == fill)
			 ht = iTween.Hash("from",0,"to",fillTo,"time",3.5f,"onupdate","changeFillValue", "easetype", iTween.EaseType.easeOutElastic);
		else if(fillSprite == all_fill)
			ht = iTween.Hash("from",0,"to",fillTo,"time",3.5f,"delay", 0.5f,"onupdate","changeAllFillValue", "easetype", iTween.EaseType.easeOutElastic);
		else if(fillSprite == last_games[0])
			ht = iTween.Hash("from",0,"to",fillTo,"time",3.5f,"onupdate","last0Game", "easetype", iTween.EaseType.easeOutElastic);
		else if(fillSprite == last_games[1])
			ht = iTween.Hash("from",0,"to",fillTo,"time",3.5f,"delay", 0.3f,"onupdate","last1Game", "easetype", iTween.EaseType.easeOutElastic);
		else if(fillSprite == last_games[2])
			ht = iTween.Hash("from",0,"to",fillTo,"time",3.5f,"delay", 0.6f,"onupdate","last2Game", "easetype", iTween.EaseType.easeOutElastic);
		else if(fillSprite == last_games[3])
			ht = iTween.Hash("from",0,"to",fillTo,"time",3.5f,"delay", 0.9f,"onupdate","last3Game", "easetype", iTween.EaseType.easeOutElastic);
		else if(fillSprite == last_games[4])
			ht = iTween.Hash("from",0,"to",fillTo,"time",3.5f,"delay", 1.2f,"onupdate","last4Game", "easetype", iTween.EaseType.easeOutElastic);
		else if(fillSprite == overRed)
			ht = iTween.Hash("from",0,"to",fillTo,"time",3.5f,"delay", 0.5f,"onupdate","changeOverRedFillValue", "easetype", iTween.EaseType.easeOutElastic);
		else if(fillSprite == overGreen)
			ht = iTween.Hash("from",0,"to",fillTo,"time",3.5f,"delay", 0.5f,"onupdate","changeOverGreenFillValue", "easetype", iTween.EaseType.easeOutElastic);
		else if(fillSprite == liveRed)
			ht = iTween.Hash("from",0,"to",fillTo,"time",3.5f,"delay", 0.5f,"onupdate","changeLiveRedFillValue", "easetype", iTween.EaseType.easeOutElastic);
		else if(fillSprite == liveGreen)
			ht = iTween.Hash("from",0,"to",fillTo,"time",3.5f,"delay", 0.5f,"onupdate","changeLiveGreenFillValue", "easetype", iTween.EaseType.easeOutElastic);
		//make iTween call:
		iTween.ValueTo(gameObject, ht);
	}
	
	//since our ValueTo() iscalculating floats the "onupdate" callback will expect a float as well:
	void changeFillValue(float newValue){
		fill.fillAmount = newValue;
	}
	void changeAllFillValue(float newValue){
		all_fill.fillAmount = newValue;
	}
	void last0Game(float newValue){
		last_games[0].fillAmount = newValue;
	}
	void last1Game(float newValue){
		last_games[1].fillAmount = newValue;
	}
	void last2Game(float newValue){
		last_games[2].fillAmount = newValue;
	}
	void last3Game(float newValue){
		last_games[3].fillAmount = newValue;
	}
	void last4Game(float newValue){
		last_games[4].fillAmount = newValue;
	}
	void changeOverRedFillValue(float newValue){
		overRed.fillAmount = newValue;
	}
	void changeOverGreenFillValue(float newValue){
		overGreen.fillAmount = newValue;
	}
	void changeLiveRedFillValue(float newValue){
		liveRed.fillAmount = newValue;
	}
	void changeLiveGreenFillValue(float newValue){
		liveGreen.fillAmount = newValue;
	}


		
	
	IEnumerator lerpFill(UISprite fiilSprite , float fillTo = 0.5f, float fillSpeed = 5.0f   ) {
		
		float t = 0f;
		
		for(t = 0.0f; t < fillSpeed; t += Time.deltaTime){
			fiilSprite.fillAmount = Mathf.Lerp(0f, fillTo, fillSpeed);
			t += Time.deltaTime;
			//print(t);
		}
		return null;
	}
	
	
	
	void clearAllData ()
	{
		top_label.text = "";
		matches_num.text = "";
		points_num.text = "";
		cards_num.text = "";
		cards_won_num.text = "";
		cards_label.text = "";
		
		cards_percent.text = "";
		fill.fillAmount = 0;
		
		all_fill.fillAmount = 0;
		all_percent.text = "";

		for(int j=0; j<5; j++){
			last_games[j].fillAmount = 0.1f;
		}
	}

	// Achivements/Stats
	public bool isUserView = false;
	public GameObject[] lines = new GameObject[2];
	public bool achievsOpen = true;

	public void toggleAchievs() {
		if (achievsOpen) {
			// Tween Out achievs scroll
			TweenAlpha.Begin (achievsScroll.gameObject, 0.5f, 0.0f);
			// Tween in stats Scroll
			TweenAlpha.Begin (statsScroll.gameObject, 0.5f, 1.0f);

			achievsOpen = false;
			openLine (1);
		} else {
			// Tween In achievs scroll
			TweenAlpha.Begin (achievsScroll.gameObject, 0.5f, 1.0f);
			// Tween Out stats Scroll
			TweenAlpha.Begin (statsScroll.gameObject, 0.5f, 0.0f);

			achievsOpen = true;
			openLine (0);
		}


	}

	private void openAchievs() {
		// Tween In achievs scroll
		TweenAlpha.Begin (achievsScroll.gameObject, 0.5f, 1.0f);
		// Tween Out stats Scroll
		TweenAlpha.Begin (statsScroll.gameObject, 0.5f, 0.0f);
		
		achievsOpen = true;
		openLine (0);
	}

	private void openStats() {
		// Tween Out achievs scroll
		TweenAlpha.Begin (achievsScroll.gameObject, 0.5f, 0.0f);
		// Tween in stats Scroll
		TweenAlpha.Begin (statsScroll.gameObject, 0.5f, 1.0f);
		
		achievsOpen = false;
		openLine (1);
	}

	void openLine (int lineNum)
	{
		for (int i = 0; i < lines.Length; i++) {
			NGUITools.SetActive( lines[i], false);
		}
		NGUITools.SetActive (lines [lineNum], true);
	}

	string getUsedLang(Dictionary<string,string> dictionary) {
		string usedLang;

		if (dictionary.ContainsKey (LocalizationManager.CurrentLanguageCode))
			usedLang = LocalizationManager.CurrentLanguageCode;
		else
			usedLang = "en";

		return usedLang;
	}
}

