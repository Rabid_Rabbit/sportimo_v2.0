﻿using UnityEngine;
using System.Collections;

public class openlnkInText : MonoBehaviour {

	public UILabel theLinkLabel;

	void OnClick ()
	{
		string url = theLinkLabel.GetUrlAtPosition(UICamera.lastWorldPosition);
		if (!string.IsNullOrEmpty (url))
			Application.OpenURL (url);
	}
}
