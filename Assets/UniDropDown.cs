﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using I2.Loc;


/**
**  UniDropdown
    ** UpdateOptions() with a list of strings to create the options.
    ** SetSelected() to set the default selection.
    ** Hook on OnSelect event to be inform of the user selection.
*/

public class UniDropDown : MonoBehaviour
{

    private string selected = "";

    private List<string> cachedList;

    public GameObject dropup_go;
    public GameObject dropdown_go;
    public GameObject dropdown_options_go;
    public UIWidget dropArrow;

    public UILabel up_label;
    public UILabel down_label;

    public UISprite dropdown_background;
    public UITable options_table;
    public GameObject option_prototype;

    public int option_height = 50;

    public event Action<string> OnSelect;



    public void Start()
    {
    }

    private bool dropped = false;

    public void toggleDropDown()
    {
        dropped = !dropped;

        if (dropped)
        {
            dropup_go.SetActive(false);
            dropdown_go.SetActive(true);

            dropdown_options_go.GetComponent<UIWidget>().alpha = 0;         
            TweenAlpha.Begin(dropdown_options_go, .1f, 1);
            TweenPosition tween = dropdown_options_go.GetComponent<TweenPosition>();
            tween.ResetToBeginning();
            tween.PlayForward();
        }
        else
        {
            dropup_go.SetActive(true);
            dropdown_go.SetActive(false);
        }

        setLabel();
    }

    public void setSelected(string selection)
    {
        selected = selection;
        setLabel();
    }

    public void select(string selection)
    {
        selected = selection;

        if (OnSelect != null)
            OnSelect(selected);

        toggleDropDown();
        UpdateOptions(cachedList);

    }

    public void setLabel()
    {
        up_label.text = I2.Loc.LocalizationManager.FixRTL_IfNeeded(selected, 0, true);
        down_label.text = I2.Loc.LocalizationManager.FixRTL_IfNeeded(selected, 0, true);
    }

    public void onTransitionOutStart()
    {
        selected = "";
        if (cachedList != null)
            cachedList.Clear();
        up_label.text = "";
        down_label.text = "";

        if (OnSelect != null)
            OnSelect = null;

        dropup_go.SetActive(true);
        dropdown_go.SetActive(false);

    }

    public void Clear()
    {
        selected = "";
        setLabel();
        UpdateOptions(new List<string>());

    }

    public void UpdateOptions(List<string> options_strings)
    {
        cachedList = options_strings;

        if (string.IsNullOrEmpty(selected) && cachedList.Count > 0)
        {
            selected = cachedList[0];
            setLabel();
        }

        foreach (Transform tr in options_table.GetChildList())
            Destroy(tr.gameObject);

        options_table.Reposition();

        if (cachedList.Count > 1)
            showArrow();
        else
            hideArrow();


        foreach (string str in cachedList)
        {
            if (str != selected)
            {
                UniDropOption _option = NGUITools.AddChild(options_table.gameObject, option_prototype).GetComponent<UniDropOption>();
                _option.setOption(str);
            }
        }

        options_table.repositionNow = true;
        dropdown_background.height = (cachedList.Count - 1) * (option_height) + 5;
    }

    private void hideArrow()
    {
        if (dropArrow == null)
            dropArrow = dropup_go.transform.FindChild("arrow").GetComponent<UIWidget>();

        dropArrow.alpha = 0.3f;
        dropup_go.GetComponent<Collider>().enabled = false;
    }

    private void showArrow()
    {
        if (dropArrow == null)
            dropArrow = dropup_go.transform.FindChild("arrow").GetComponent<UIWidget>();

        dropArrow.alpha = 1f;
        dropup_go.GetComponent<Collider>().enabled = true;
    }
}
