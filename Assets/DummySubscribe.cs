﻿using UnityEngine;
using System.Collections;

public class DummySubscribe : MonoBehaviour {

    public UIPanel thisPanel;

    private static DummySubscribe _this;
    public static DummySubscribe instance
    {
        get { if (_this != null) return _this;
            else
                _this = FindObjectOfType<DummySubscribe>();
            return _this;
                }
    }

	public static void show()
    {
        instance.thisPanel.alpha = 1;
    }

    public void hide()
    {
        thisPanel.alpha = 0;
    }
}
