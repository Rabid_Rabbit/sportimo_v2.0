﻿using UnityEngine;
using System.Collections.Generic;
using BedbugRest;
using System;
using LitJson;
using I2.Loc;
using System.Collections;
using System.Linq;

public class EndMatchCtrl : MonoBehaviour
{
	public UILabel top1pos;
	public UILabel top1name;
	public UILabel top1points;
	[Space(10)]
	public UILabel top2pos;
	public UILabel top2name;
	public UILabel top2points;
	[Space(10)]
	public UILabel top3pos;
	public UILabel top3name;
	public UILabel top3points;
	[Space(10)]
	public UILabel top4pos;
	public UILabel top4name;
	public UILabel top4points;
	[Space(10)]
	public UILabel userpos;
	public UILabel username;
	public UILabel userpoints;
	[Space(10)]
	public Animation MatchEndAnimation;
	private Leaderboard endMatchLeader;
	[Space(10)]
	public GameObject name1;
	public GameObject name2;
	public GameObject name3;
	public GameObject name4;

	public int testInt = 0;
	public UILabel scoreLabel;
	private int userScore;

	void Start() {
		
		//StartCoroutine (startCount(200));
	}

	public void endMatchHandler(string response) {
		// Add the response to an object
		endMatchLeader = JsonMapper.ToObject<Leaderboard> (response);

		// Add all the elements to the animated parts
		if (endMatchLeader.leaderboad [0].rank == 0) {
			NGUITools.SetActive (name1, false);
		} else {
			NGUITools.SetActive (name1, true);
			top1pos.text = endMatchLeader.leaderboad [0].rank.ToString();
			top1name.text = endMatchLeader.leaderboad [0].name;
			top1points.text = endMatchLeader.leaderboad [0].score.ToString()+" pts";
		}
		if (endMatchLeader.leaderboad [1].rank == 0) {
			NGUITools.SetActive (name2, false);
		} else {
			NGUITools.SetActive (name2, true);
			top2pos.text = endMatchLeader.leaderboad [1].rank.ToString();
			top2name.text = endMatchLeader.leaderboad [1].name;
			top2points.text = endMatchLeader.leaderboad [1].score.ToString()+" pts";
		}
		if (endMatchLeader.leaderboad [3].rank == 0) {
			print ("NAme 3 rank = 0!");
			NGUITools.SetActive (name3, false);
		} else {
			NGUITools.SetActive (name3, true);
			top3pos.text = endMatchLeader.leaderboad [3].rank.ToString();
			top3name.text = endMatchLeader.leaderboad [3].name;
			top3points.text = endMatchLeader.leaderboad [3].score.ToString()+" pts";
		}
		if (endMatchLeader.leaderboad [4].rank == 0) {
			print ("NAme 4 rank = 0!");
			NGUITools.SetActive (name4, false);
		} else {
			NGUITools.SetActive (name4, true);
			top4pos.text = endMatchLeader.leaderboad [4].rank.ToString();
			top4name.text = endMatchLeader.leaderboad [4].name;
			top4points.text = endMatchLeader.leaderboad [4].score.ToString()+" pts";
		}

		// USER
		userpos.text = endMatchLeader.user.rank.ToString();
		username.text = endMatchLeader.user.name;
		userpoints.text = endMatchLeader.user.score.ToString ()+" pts";

		userScore = endMatchLeader.user.score;

		MatchEndAnimation.Play ("EndMatch");
	}		

	public void startScoreCount() {
		StartCoroutine (startCount( userScore ));
	}

	IEnumerator startCount(int count) {
		int startCount = 0;
		do
		{
			yield return new WaitForSeconds(0.005f);
			startCount += 1;
			scoreLabel.text = startCount+"pts";

		} while (startCount < count);
		startPosAnim ();
	}

	public void startPosAnim() {
		MatchEndAnimation.Play ("EndMatchPos");
	}

	public void closeEndMatch() {
		MatchEndAnimation.Play ("EndMatchClose");
	}
}
	
