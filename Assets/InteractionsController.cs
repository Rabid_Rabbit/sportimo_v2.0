﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InteractionsController : MonoBehaviour {

	public GameObject prototypeMessage;
//	public Taunt thisTaunt;

	public UIGrid interactionsGrid;
	public UITable interactionsTable;
	public UIScrollView interactionsScroll;
	public GameObject tauntCount_obj;
	public UILabel tauntCount_label;
	[Space(10)]
	// Content
	public  GameObject Content;
	public GameObject chatArrow;
	[Space(10)]
	private bool chatOpen = false;
	private int tauntCount;

	private bool enabledMessages = true;
	public UILabel enableLabel;
	public GameObject noMessages;
	[Space(10)]
	public static InteractionsController instance;

	public static void Reposition(){
		Debug.Log("Asked to reposition");
		instance.repositionWithDelay ();
	}

	public static void SubstructTauntCount(){
		instance.substructTaunt();
	}

	public static void deleteAllTaunts(){
		instance.deleteAll();
	}

	public void Awake(){

		instance = this;
		ConnectionManager.OnTauntReceived += HandleOnTauntReceived;
	}
	
	public void OnDisable(){
		ConnectionManager.OnTauntReceived -= HandleOnTauntReceived;
	}

	void HandleOnTauntReceived (string json)
	{
		Debug.Log ("We received a taunt and now we have to display it:\n"+json);

		if (!enabledMessages)
			return;

		//GameObject newInteraction = NGUITools.AddChild (interactionsGrid.gameObject, prototypeMessage); 
		GameObject newInteraction = NGUITools.AddChild (interactionsTable.gameObject, prototypeMessage); 
		interactionMessageController newInteractionController = newInteraction.GetComponent<interactionMessageController> ();

		newInteractionController.thisInteraction = LitJson.JsonMapper.ToObject<Interaction> (json);

		newInteractionController.init ();

		//interactionsGrid.repositionNow = true;
		interactionsTable.Reposition();
		interactionsTable.repositionNow = true;
		interactionsScroll.ResetPosition ();

		// Make Content visible
		TweenAlpha.Begin(Content.transform.parent.gameObject, 0.5f, 1);
		//NGUITools.SetActive(Content, true);
		// Add Count to count holder
		tauntCount ++;
		TweenAlpha.Begin (tauntCount_label.transform.parent.gameObject, 0,1);
		tauntCount_label.text = tauntCount.ToString();
	}

	public  void repositionWithDelay(){
		StartCoroutine (ResetPositition ());
	}

	public  void substructTaunt(){
		tauntCount--;
		tauntCount_label.text = tauntCount.ToString();
		if (tauntCount == 0)
			TweenAlpha.Begin (tauntCount_label.transform.parent.gameObject, 0.5f, 0);
	}

	IEnumerator ResetPositition ()
	{
		Debug.Log("Applying delay");
		yield return new WaitForSeconds (.1f);
		Debug.Log("Reposition");
		//interactionsGrid.repositionNow = true;
		interactionsTable.Reposition();
		interactionsTable.repositionNow = true;
		yield return new WaitForSeconds (.5f);
		interactionsScroll.ResetPosition ();
	}

	public void toogleChat() {
		if (!chatOpen) {
			chatOpen = true;
			iTween.MoveTo ( Content, iTween.Hash("x", 270, "time", 1f, "islocal", true));
			iTween.RotateTo (chatArrow, iTween.Hash ("z", 0, "time", .5f, "islocal", true));
		}else {
			chatOpen = false;
			iTween.MoveTo ( Content, iTween.Hash("x", 0, "time", .5f, "islocal", true));
			iTween.RotateTo (chatArrow, iTween.Hash ("z", 180, "time", .5f, "islocal", true));
		}
	}

	public void deleteAll () {
		NGUITools.DestroyChildren (interactionsTable.transform);
		toogleChat ();
		StartCoroutine (closeContentDelayed ());
	}

	public void toggleEnable () {

		if (enabledMessages) {
			enableLabel.text = I2.Loc.LocalizationManager.GetTermTranslation ("enable_mes", true);
			NGUITools.DestroyChildren (interactionsTable.transform);
			tauntCount = 0;
			tauntCount_label.text = tauntCount.ToString();
			NGUITools.SetActive (noMessages, true);
			enabledMessages = false;
			TweenAlpha.Begin (tauntCount_label.transform.parent.gameObject, 0.5f, 0);
		} else {
			enableLabel.text = I2.Loc.LocalizationManager.GetTermTranslation ("disable_mes", true);
			NGUITools.SetActive (noMessages, false);
			enabledMessages = true;
			TweenAlpha.Begin (tauntCount_label.transform.parent.gameObject, 0.3f, 1);
		}
	}
		
	IEnumerator closeContentDelayed() {
		// Add Count to count holder
		tauntCount = 0;
		tauntCount_label.text = tauntCount.ToString();
		yield return new WaitForSeconds (1.5f);
		// Hide Content 
		if(enabledMessages)
		TweenAlpha.Begin(Content.transform.parent.gameObject, 0.3f, 0);
			//NGUITools.SetActive(Content, false);

	}
}


public class Interaction{
	public simpleUser sender;
	public simpleUser recipient;
	public Taunt taunt;
}

public class simpleUser{
	public string _id;
	public string username;
	public string picture;
}

public class Taunt{
	public string type;
	public string imgurl;
	public string sprite;
	public Dictionary<string,string> text;
	public string term;
	public string animation;
}

