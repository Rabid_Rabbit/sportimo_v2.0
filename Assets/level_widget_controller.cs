﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class level_widget_controller : MonoBehaviour {

	public UISprite[] StarFills  = new UISprite[5];

	private float _level;
	public float level {
		get{
			return _level;
		}
		set{            
			_level = value;
			setStarFill(_level);
		}
	}



	public void setStarFill(float value){

        Debug.Log("Value:" + value);

        // 1st Filling
        if (value < 0.2) {
			StarFills [0].fillAmount = value / 2 * 10;
		}

        // 1st Filled - Second Filling
        if (value < 0.4 && value > 0.2){
            Debug.Log("1 star filled");
            StarFills [0].fillAmount = 1;
			StarFills [1].fillAmount = (value - 0.2f) / 2 * 10;
		}

        // 2st Filled - 3d Filling
        if (value < 0.6 && value > 0.4){
            Debug.Log("2 star filled");
            StarFills [0].fillAmount = 1;
			StarFills [1].fillAmount = 1;
			StarFills [2].fillAmount = (value - 0.4f) / 2 * 10;
		}

        // 3d Filled - 4th Filling
        if (value < 0.8 && value > 0.6){
            Debug.Log("3 star filled");
            StarFills [0].fillAmount = 1;
			StarFills [1].fillAmount = 1;
			StarFills [2].fillAmount = 1;
			StarFills [3].fillAmount = (value - 0.6f) / 2 * 10;
		}

      
        if (value > 0.8){
            Debug.Log("4 star filled");
            StarFills [0].fillAmount = 1;
			StarFills [1].fillAmount = 1;
			StarFills [2].fillAmount = 1;
			StarFills [3].fillAmount = 1;
			StarFills [4].fillAmount = (value - 0.8f) / 2 * 10;
		}

	}
}
