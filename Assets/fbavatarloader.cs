﻿using UnityEngine;
using System.Collections;
using LitJson;
using Facebook.Unity;

public class fbavatarloader : MonoBehaviour {

	/// <summary>
	/// The texture that will hold the fb image
	/// </summary>
	public UITexture texture;

	/// <summary>
	/// Loads the supplied fb uri and assigns image on texture
	/// </summary>
	public void SetTextureToFBPic(string baseUrl){
		BedBugTools.loadImageOnUITextureAndResize (texture, baseUrl, 256, 256, FadeInOnLoad, 0);
	}

	/// <summary>
	/// Helper method to fade in loaded image
	/// </summary>
	private void FadeInOnLoad(UITexture texture){
		
		TweenAlpha.Begin (texture.gameObject, 0.5f, 1.0f);
	}

	/// <summary>
	/// Ons the transition in start load the image on texture of logged in.
	/// </summary>
	public void onTransitionInStart(viewData _viewData = null){
		Debug.Log ("Widget Loaded:"+ FB.IsLoggedIn);
		if (FB.IsLoggedIn) {
			FB.API ("me?fields=picture.height(250)", HttpMethod.GET, OnGetPicture);
		}
	}

	public void OnGetPicture(IGraphResult result){
		JsonData data = JsonMapper.ToObject(result.RawResult);
		SetTextureToFBPic(data["picture"]["data"]["url"].ToString());
	}

}
