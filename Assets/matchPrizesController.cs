﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using BedbugRest;
using System.Linq;
using I2.Loc;

public class matchPrizesController : MonoBehaviour
{
    Pool _pool = new Pool();

    public GameObject info;
    public UITable prizes_grid;
    public GameObject prizePrefab;

    public void open()
    {
        foreach (Transform trans in prizes_grid.GetChildList())
        {
            if (trans.gameObject.activeSelf)
            {
                NGUITools.Destroy(trans.gameObject);
            }
        }

    //localhost: 3030 / leaderpay / v1 / pools / forgame / 58173c1d8f4fa300f709684b / GR /

        API.GetAll(Main.Settings.apis["leaderpay"] + "/pools/forgame/" + MatchCtrl.matchid + "/" + Main.AppUser.country, OnResponse);
        Loader.Visible(true);

        //}

    }


    public void OnResponse(BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
    {


        if (!res.IsSuccess )
        {
            MessagePanel.Instance.animMessage(res.DataAsText);
            return;
        }

        Loader.Visible(false);
        Debug.Log(res.DataAsText);

        List<Pool> pools = LitJson.JsonMapper.ToObject<List<Pool>>(res.DataAsText);

        if (pools.Count > 0)
        {
            info.SetActive(false);
            _pool = pools[0];
        }
        else {
            info.SetActive(true);
            return;
        }
        if (_pool.prizes.Count > 0)
        {
            for (int i = 0; i < _pool.prizes.Count; i++)
            {
                GameObject _prize = NGUITools.AddChild(prizes_grid.gameObject, prizePrefab);
                NGUITools.SetActive(_prize, true);
                BedBugTools.loadImageOnUITextureAndResize(_prize.transform.FindChild("image").GetComponent<UITexture>(), _pool.prizes[i].picture, 256, 256);
                _prize.name = i.ToString();
                Dictionary<string, int> pos = _pool.prizes[i].positions;

                if (pos["from"] != pos["to"])
                {
                    _prize.transform.FindChild("BgTop").GetComponentInChildren<UILabel>().text = pos["from"].ToString() + Pool.Prize.returnEnding(pos["from"]) + " - " + pos["to"].ToString() + Pool.Prize.returnEnding(pos["to"]) + " place";
                }
                else {
                    _prize.transform.FindChild("BgTop").GetComponentInChildren<UILabel>().text = pos["from"].ToString() + Pool.Prize.returnEnding(pos["from"]) + " place";
                }
                _prize.transform.FindChild("descriptionLabel").GetComponent<UILabel>().text = LocalizationManager.FixRTL_IfNeeded(BedBugTools.GetStringByLocPrefs(_pool.prizes[i].text),30, true);
            }
        }
        else {
            print("There are no Polls yet");
        }

        prizes_grid.Reposition();
        prizes_grid.transform.parent.gameObject.GetComponent<UIScrollView>().ResetPosition();
        Loader.Visible(false);
    }


}
