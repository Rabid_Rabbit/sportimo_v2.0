﻿using UnityEngine;
using System.Collections;
using Prime31;
using I2.Loc;

public class MessageCtrl : MonoBehaviour
{

	public UILabel title;
	public UILabel text;
	public UILabel date;
	public UISprite unreadDot;

	public GameObject expandBtn;
	public GameObject linkBtn;
	public UILabel linkText;
	public UITexture imgHolder;

	public UITable mailTable;
	public UIScrollView mailScroll;

	public Message thisMessage;

	public void expandMessage() {

		if (text.overflowMethod == UILabel.Overflow.ClampContent)
			text.overflowMethod = UILabel.Overflow.ResizeHeight;
		else {
			text.overflowMethod = UILabel.Overflow.ClampContent;
			text.height = 45;
		}

		mailTable.repositionNow = true;

//		StartCoroutine (resetDelay ());
		TweenAlpha.Begin (expandBtn, 0.1f, 0);
		//NGUITools.SetActive (expandBtn, false);
	}

	public void remove(){
		ConfirmationModalController.Confirm (LocalizationManager.GetTermTranslation("_mailDelete", true), "", null, null, (option) => {
			if(option == "Btn1"){
				NGUITools.FindInParents<MailCtrl>(gameObject).removeMessageById(thisMessage._id);
				gameObject.SetActive(false);
			}
		});

	}

	public void showLink(){
		if (!string.IsNullOrEmpty (linkText.text)) 
			TweenAlpha.Begin (linkBtn, 0.3f, 1.0f);
	}

	public void hideLink(){
		linkBtn.GetComponent<Collider>().enabled = false;
//			linkBtn.GetComponent<UIWidget> ().alpha = 0;
	}

	IEnumerator resetDelay() {


		yield return new WaitForSeconds (0.1f);
		mailScroll.ResetPosition ();
	}

#if !UNITY_WEBPLAYER
	UniWebView _webView;
	
	bool OnWebViewShouldClose (UniWebView webView)
	{
		if (webView == _webView) {
			_webView.OnLoadComplete -= OnLoadComplete;
			_webView.OnWebViewShouldClose -= OnWebViewShouldClose;
			_webView = null;
			return true;
		}
		return false;
	}
	
	// The listening method of OnLoadComplete method.
	void OnLoadComplete (UniWebView webView, bool success, string errorMessage)
	{
		if (success) {
			// Great, everything goes well. Show the web view now.
			Debug.Log ("WEB VIEW LAODED!");
			webView.Show ();
		} else {
			// Oops, something wrong.
			Debug.LogError ("Something wrong in web view loading: " + errorMessage);
		}
	}
#endif
    public GameObject UniWebPrefab;
	public void linkClick() {
		Debug.Log (thisMessage.link);

        //		#if UNITY_EDITOR
        //      	Application.OpenURL (thisMessage.link);
        //		#elif UNITY_ANDROID
        //		_webView = (Instantiate (UniWebPrefab) as GameObject).GetComponent<UniWebView> ();//new GameObject().AddComponent<UniWebView>();
        //		_webView.backButtonEnable=true;
        //		_webView.OnWebViewShouldClose += OnWebViewShouldClose;
        //		_webView.OnLoadComplete += OnLoadComplete;
        //		_webView.Show();
        //		_webView.url = thisMessage.link;
        //		_webView.Load ();
        //		#elif UNITY_IOS
        //		EtceteraBinding.showWebPage(thisMessage.link ,true);
        //		#endif
    }
}
