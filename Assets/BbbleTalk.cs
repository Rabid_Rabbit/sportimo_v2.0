﻿using UnityEngine;
using System.Collections;

public class BbbleTalk : MonoBehaviour {
	public Animation coachTalkAnim;

	public void coachTalk() {
		coachTalkAnim.Play ("CoachTalk");
	}
}
