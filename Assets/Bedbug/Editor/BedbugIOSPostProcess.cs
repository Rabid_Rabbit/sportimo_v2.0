﻿#if UNITY_IOS
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using UnityEditor.iOS.Xcode;
using System.IO;
 
public class BedbugIOSPostProcess
{
    internal static void CopyAndReplaceDirectory(string srcPath, string dstPath)
    {
        if (Directory.Exists(dstPath))
            Directory.Delete(dstPath);
        if (File.Exists(dstPath))
            File.Delete(dstPath);

        Directory.CreateDirectory(dstPath);

        foreach (var file in Directory.GetFiles(srcPath))
            File.Copy(file, Path.Combine(dstPath, Path.GetFileName(file)));

        foreach (var dir in Directory.GetDirectories(srcPath))
            CopyAndReplaceDirectory(dir, Path.Combine(dstPath, Path.GetFileName(dir)));
    }

    [PostProcessBuild]
	public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
    {

        if (buildTarget == BuildTarget.iOS)
        {
            /*------------------------------------------------------*/
            // for frameworks
            Debug.Log("Bedbug Utilities post-process");

			string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

			PBXProject proj = new PBXProject();
			proj.ReadFromFile(projPath);

			string target = proj.TargetGuidByName("Unity-iPhone");

            proj.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

            proj.WriteToFile(projPath);

            // Get plist
            string plistPath = path + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            // Change value of CFBundleVersion in Xcode plist
            var buildKey = "CFBundleVersion";
            rootDict.SetString(buildKey, "22");

            PlistElementDict NSAppTransportSecurityDict = rootDict.CreateDict("NSAppTransportSecurity");
            NSAppTransportSecurityDict.SetBoolean("NSAllowsArbitraryLoads", true);

            rootDict.SetBoolean("ITSAppUsesNonExemptEncryption", false);

            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());


        }
    }
}
#endif