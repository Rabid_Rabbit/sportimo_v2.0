//
//  BedbugUtilities.m
//
//
//  Created by Aris Brink on 29/1/15.
//  Copyright 2015 Bedbug. All rights reserved.
//

#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>




//#define INCLUDE_ADDRESS_BOOK_FEATURE 1


#if UNITY_VERSION < 420
	#import "AppController.h"
	#define APPCONTROLLER_CLASS AppController
#else
	#import "UnityAppController.h"
	#define APPCONTROLLER_CLASS UnityAppController
#endif


// Converts NSString to C style string by way of copy (Mono will free it)
#define MakeStringCopy( _x_ ) ( _x_ != NULL && [_x_ isKindOfClass:[NSString class]] ) ? strdup( [_x_ UTF8String] ) : NULL

// Converts C style string to NSString
#define GetStringParam( _x_ ) ( _x_ != NULL ) ? [NSString stringWithUTF8String:_x_] : [NSString stringWithUTF8String:""]

// Converts C style string to NSString as long as it isnt empty
#define GetStringParamOrNil( _x_ ) ( _x_ != NULL && strlen( _x_ ) ) ? [NSString stringWithUTF8String:_x_] : nil

//const char * _etceteraGetCurrentLanguage()
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
//
//    return MakeStringCopy( [languages objectAtIndex:0] );
//}

// Get carrier name
const char * _GetCarrierName()
{
    // Setup the Network Info and create a CTCarrier object
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    NSString *carrierName = [carrier carrierName];
    if (carrierName != nil)
        NSLog(@"Carrier: %@", carrierName);
    
    return MakeStringCopy( carrierName );
}


// Get mobile country code
const char * _GetCountryCode()
{
    // Setup the Network Info and create a CTCarrier object
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    NSString *mcc = [carrier mobileCountryCode];
    if (mcc != nil)
        NSLog(@"Mobile Country Code (MCC): %@", mcc);
    
    return MakeStringCopy( mcc );
}



// Get mobile network code
const char * _GetCarrierCode()
{
    // Setup the Network Info and create a CTCarrier object
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    NSString *mnc = [carrier mobileNetworkCode];
    if (mnc != nil)
        NSLog(@"Mobile Network Code (MNC): %@", mnc);
    
    return MakeStringCopy( mnc);
}


