﻿using UnityEngine;
using System.Collections;
using I2.Loc;

public class UniDropOption : MonoBehaviour {


    public UILabel option_text;

	public void setOption(string text)
    {
		option_text.text = LocalizationManager.FixRTL_IfNeeded( text, 0,  true );
        gameObject.name = text;
    }
}
