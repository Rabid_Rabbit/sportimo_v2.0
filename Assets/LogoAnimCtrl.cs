﻿using UnityEngine;
using System.Collections;

public class LogoAnimCtrl : MonoBehaviour {

	public Animation animation;


	void Start() {
		if(PlayerPrefs.HasKey("token"))
			playAnimation ("logoAnimationStart");
		else
			playAnimation ("logoAnimation");
	}

	public void playAnimation(string animName)
	{
		animation.Play (animName);



	}

	void OnClick() {
		print ("Changing Speed!!!!!!!!");
		animation ["logoAnimationStart"].speed = 3;
		playAnimation ("logoAnimationStart");
	}

}
