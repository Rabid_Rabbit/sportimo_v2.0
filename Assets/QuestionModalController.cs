﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using I2.Loc;

public class QuestionModalController : MonoBehaviour {

	public UILabel info_header;
	public UILabel info_text;

	public UILabel question_text;
	public UILabel answer1text;
	public UILabel answer2text;

	public UIWidget confirmationWidget;

	private Action<string> AnswerCallback;

	private static QuestionModalController _instance;



	void Awake(){
		_instance = this;
	}


	/// <summary>
	/// Ask uses supplied localization objects. It populates the modal based on user's locale selection or fallback to default language.
	/// Useful for dynamic questions or quizes.
	/// </summary>
	public static void Ask(

		string infoHeaderTerm, 
		string infoTextTerm,
		Dictionary<string,string> questionText,
		string answer1ID,
		Dictionary<string,string> answer1Text,
		string answer2ID,
		Dictionary<string,string> answer2Text,
		Action<string> callback = null

		){

		Main.onViewChange += _instance.closeModalOnViewChange;

		_instance.info_header.text = LocalizationManager.GetTermTranslation (infoHeaderTerm, true);


        _instance.info_text.text = LocalizationManager.FixRTL_IfNeeded (LocalizationManager.GetTermTranslation (infoTextTerm, false), 35, true );


		_instance.question_text.text = LocalizationManager.FixRTL_IfNeeded ( BedBugTools.GetStringByLocPrefs (questionText), 35 , true);

		
		_instance.answer1text.name = answer1ID;
		_instance.answer1text.text = LocalizationManager.FixRTL_IfNeeded(BedBugTools.GetStringByLocPrefs (answer1Text),0, true);
		
		_instance.answer2text.name = answer2ID;
		_instance.answer2text.text = LocalizationManager.FixRTL_IfNeeded(BedBugTools.GetStringByLocPrefs (answer2Text),0, true);
		
		_instance.AnswerCallback = callback;
		
		_instance.confirmationWidget.alpha = 1;
		
	}

	public void optionSelected(string option){

		confirmationWidget.alpha = 0;
		Main.onViewChange -= _instance.closeModalOnViewChange;

		if (AnswerCallback != null) {
			AnswerCallback(option);
		}
	}

	private void closeModalOnViewChange ()
	{
		confirmationWidget.alpha = 0;
		Main.onViewChange -= _instance.closeModalOnViewChange;

		if (AnswerCallback != null) {
			AnswerCallback("cancel");
		}
	}
}
