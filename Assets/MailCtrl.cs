﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BedbugRest;
using LitJson;
using I2.Loc;
using System;

public class MailCtrl : View {

	// Use this for initialization
	public UITable grid;
	
	public GameObject messagePrefab;
	List<Message> messages;


	public UIScrollView MailScrollview;
	private DateTime last_mail_check;

	public GameObject noMailObg;
	private int mailCount;

	public void onTransitionInStart(viewData _viewData = null){

		string checkTimeString = PlayerPrefs.GetString ("last_mail_check");
        if (!string.IsNullOrEmpty(checkTimeString))
        {

            last_mail_check = Convert.ToDateTime(checkTimeString);
        }
        else
            last_mail_check = DateTime.UtcNow;

		Loader.Visible (true);
		API.GetOneByID(Main.Settings.apis["users"], Main.AppUser._id+"/messages",OnGetMessagesCallback);
	}
	
	public void OnGetMessagesCallback(BestHTTP.HTTPRequest req,BestHTTP.HTTPResponse res){
		Debug.Log(res.DataAsText);
		if(res.StatusCode!=200 && res.StatusCode!=304){
			MessagePanel.Instance.animMessage(res.DataAsText);
			return;
		}
		messages = JsonMapper.ToObject<List<Message>>(res.DataAsText);

		mailCount = 0;

		foreach(Message mess in messages){
			mailCount++;
			GameObject message  = NGUITools.AddChild(grid.gameObject,messagePrefab);
			NGUITools.SetActive(message, true);

			message.name = mess._id;

			MessageCtrl ctrl = message.GetComponent<MessageCtrl>();
			ctrl.thisMessage = mess;
            Debug.Log(BedBugTools.GetStringByLocPrefs(mess.msg));

            if (mess.title != null)
				ctrl.title.text = LocalizationManager.FixRTL_IfNeeded( BedBugTools.GetStringByLocPrefs(mess.title));

			if(mess.msg != null)
				ctrl.text.text = LocalizationManager.FixRTL_IfNeeded(BedBugTools.GetStringByLocPrefs(mess.msg), 40 , false);

            

			// Check the text length if it's more than the default size an open the expand button
			if(ctrl.text.text.Length > 70) {
				TweenAlpha.Begin (ctrl.expandBtn, 0.3f, 1.0f);
				//NGUITools.SetActive( ctrl.expandBtn, true);
			}

			if(!string.IsNullOrEmpty( mess.link)){
//				ctrl.linkText.text = mess.link;
			}else{
				ctrl.hideLink();
			}

			if(!string.IsNullOrEmpty( mess.img))
				BedBugTools.loadImageOnUITextureAndResize (ctrl.imgHolder, mess.img, 128, 128,null,1);

			if(mess.created != null)
			ctrl.date.text=mess.created.ToString("dd/MM/yyyy HH:mm");

			Debug.Log(last_mail_check < mess.created.ToUniversalTime());
			if(last_mail_check < mess.created.ToUniversalTime())
				ctrl.unreadDot.alpha = 1;
			else
				ctrl.unreadDot.alpha = 0;


			StartCoroutine (delayOpen (message));
		}

		Loader.Visible (false);

		print ("You have exactly "+mailCount+" mails.");
		if (mailCount == 0) {
			TweenAlpha.Begin (noMailObg, 0.5f, 1.0f);
		} else {
			TweenAlpha.Begin (noMailObg, 0.1f, 0.0f);
		}
			

		StartCoroutine (reposition ());
		PlayerPrefs.SetString ("last_mail_check",DateTime.UtcNow.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'"));
	}

	IEnumerator delayOpen(GameObject message) {
		yield return new WaitForSeconds (.5f);
		TweenAlpha.Begin (message, 0.5f, 1);
	}

	IEnumerator reposition(){
		yield return new WaitForSeconds (0.1f);
		grid.Reposition();
		MailScrollview.ResetPosition ();
	}

	public void removeMessageById(string id){
		BedbugRest.API.Delete (Main.Settings.apis ["users"] +"/"+ Main.AppUser._id + "/messages/" + id, (BestHTTP.HTTPRequest req,BestHTTP.HTTPResponse res) => {
			Debug.Log (res.DataAsText);
			
			StartCoroutine (reposition ());
			
		});
	}

	public void onTransitionOutStart(){
		foreach(Transform trans in grid.GetChildList()){
			if(trans.gameObject.activeSelf){
				NGUITools.Destroy(trans.gameObject);
			}
		}
	}
}
