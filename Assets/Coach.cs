﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;

public class Coach : MonoBehaviour {

	public  UILabel coachTalk;
	public  GameObject CoachObj;
	public  GameObject noShowV;
	public static Coach Instance;
	public GameObject closeCol;
	public GameObject noShowCheckBox;
	public GameObject BG;

	string currentTip;

	void Awake(){
		Instance = this;
	}

	/// <summary>
	/// Basic method to make coach speak in tongues.
	/// The method will handle default language shown based on user preferences.
	/// </summary>
	/// <param name="words">Words.</param>
	/// <param name="CloseByClicking">If set to <c>true</c> close by clicking.</param>
	public static void Says(Dictionary<string,string> headline, Dictionary<string,string> words, bool CloseByClicking = true){
		Coach.coachMessage (BedBugTools.GetStringByLocPrefs(words), 27f, 8f, 0, CloseByClicking, null, BedBugTools.GetStringByLocPrefs(headline));
	}

	public static void coachMessage(string coachSaid, float height = 0, float time = 8, float delay = 0, bool closeClick = true, string tipName = null, string headline = "") {


		Instance.noShowCheckBox.SetActive(false);

		// Hide or show "Show checkbox: based on tip existance
		//Instance.noShowCheckBox.SetActive(!string.IsNullOrEmpty(tipName));
		Instance.StartCoroutine( Instance.delayShowCheckBox(delay, !string.IsNullOrEmpty(tipName) ));

		// Return the alpha to 1
		TweenAlpha.Begin (Instance.CoachObj, 0.1f, 1);
		//Change the closing method
		if (!closeClick) {
			Instance.Invoke("timedClose", delay+time);
			NGUITools.SetActive(Instance.closeCol, false);
		} else if(closeClick && time == 0){
			NGUITools.SetActive(Instance.closeCol, true);
		}
		else
		{
			Instance.Invoke("timedClose", delay+time);
			NGUITools.SetActive(Instance.closeCol, true);
		}

		Instance.StartCoroutine (Instance.delayStart (delay , tipName, coachSaid, height, headline));
		//Instance.Invoke ("delayStart", delay);

	}

	IEnumerator delayShowCheckBox (float delay , bool openTrue) {

		yield return new WaitForSeconds (delay+1.5f);
		Instance.noShowCheckBox.SetActive(openTrue);
	}

	IEnumerator  delayStart(float delay , string tipName, string coachSaid, float height, string headline = "") {

		yield return new WaitForSeconds (delay );
		//print (tipName);
		TweenAlpha.Begin (Instance.CoachObj, 0.1f, 1);
		if (!string.IsNullOrEmpty (tipName)) {
			if (PlayerPrefs.HasKey (tipName))
				yield break;
			currentTip = tipName;
		} else {
			
		}

		if (LocalizationManager.CurrentLanguageCode == "ar") {
			coachTalk.alignment = NGUIText.Alignment.Right;
			print ("Language is ar changing to Right");
		} else {
			coachTalk.alignment = NGUIText.Alignment.Left;
			print ("Language is en changing to Left");
		}

		coachTalk.text = "";
		if(!string.IsNullOrEmpty(headline))
			coachTalk.text = "[b]"+headline+"[/b]\n";
		//UILabel coachTalk = game
		coachTalk.text += coachSaid;
		// Close the dont show toggle
		NGUITools.SetActive (noShowV, false);

		// Open The Transparent BG
		//NGUITools.SetActive (Instance.BG, true);
		TweenAlpha.Begin (BG, 0.5f, 0.7f);

		yield return new WaitForSeconds (0.5f);
		// Move the object to a specific height
		CoachObj.transform.localPosition = new Vector3 (0, height, 0);



		NGUITools.SetActive (CoachObj, true);
	}

	void timedClose() {
		TweenAlpha.Begin (CoachObj, 0.5f, 0);
		StartCoroutine (closeObj ());

	}

	IEnumerator closeObj ()
	{
		yield return new WaitForSeconds (0.5f);
		// Open The Transparent BG
		//NGUITools.SetActive (BG, false);
		TweenAlpha.Begin (BG, 0.3f, 0.0f);
		NGUITools.SetActive (CoachObj, false);
	}

	public void dontShowAgain () { 
		print (currentTip);
		PlayerPrefs.SetString (currentTip,currentTip);
		NGUITools.SetActive (noShowV, true);
	}

	public void closeCoach() {
		timedClose ();
	}
}
