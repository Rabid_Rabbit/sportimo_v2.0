﻿using UnityEngine;
using System.Collections;

public class btmMenuCtrl : MonoBehaviour {
	
	public GameObject[] Lines = new GameObject[5];
	public int lineToOpen;
	
	void Start() {
//		for (int i = 0; i< Lines.Length; i++) {
//			NGUITools.SetActive(Lines[i], false);
//		}
		
//		if(lineToOpen != 0)
//			NGUITools.SetActive(Lines[lineToOpen-1], true);
	}
	
	public void openMatches() {
		Main.Instance.openView ("matches");
		openLine (0);
	}
	
	public void openLeaders() {
		Main.Instance.openView ("leaders");
		openLine (1);
	}
	
	public void openPrizes() {
		Main.Instance.openView ("prizes");
		openLine (2);
	}

	public void openfavs() {
		Main.Instance.openView ("favteam", null);
		openLine (2);
	}
	
	public void openStandings() {
		Main.Instance.openView ("standings");
		openLine (3);
	}
	
	public void openNews() {
		publicationQuerry querry = new publicationQuerry ();
		querry.limit = 30;
		querry.type = "News";
		Main.Instance.openView ("news",querry);
		//Main.Instance.openView ("news");
		openLine (4);
	}

	public void openLine(int lineNum) {
//		print ("Line to open: " + lineNum);
		for (int i = 0; i< Lines.Length; i++) {
			NGUITools.SetActive(Lines[i], false);
		}
		if(lineNum != -1)
		NGUITools.SetActive(Lines[lineNum], true);
	}
	
	// When the view starts its Transitions this will do its own transition
	void onTransitionInStart(viewData _viewdata = null) {
		print ("Do something when the transition In Starts");
		//iTween.FadeFrom (this.gameObject, 0.0f, 5);
		//TweenAlpha.Begin (this.gameObject,0.5f,1);
//		transform.localPosition = new Vector3 (0, -272, 0);
//		TweenPosition.Begin (this.gameObject, 0.1f, new Vector3 (0, -272, 0));
		//TweenPosition.Begin (this.gameObject, 1.0f, new Vector3 (0, -272, 0));
		StartCoroutine( delayStartAlpha (0.1f) );
	}

	IEnumerator delayStartAlpha (float delay)
	{
		yield return new WaitForSeconds (delay);
		TweenAlpha.Begin (this.gameObject,0.0f,1);
	}
	
	void onTransitionOutStart() {
		print ("Do something when the transition Out Starts");
		TweenAlpha.Begin (this.gameObject,0.6f,0);
		//TweenPosition.Begin (this.gameObject, 0.3f, new Vector3 (0, -322, 0));
	}
}


