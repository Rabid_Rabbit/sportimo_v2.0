﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class scrollMatchCtrl : MonoBehaviour {
    public string match_id;
	public Animation logoAnim;
	public UITexture competition_banner;

	void OnEnable()
	{
		// Fix weird ngui position bug
		Vector3 localPos = transform.localPosition;
		localPos.x = 0;
		transform.localPosition = localPos;
	}

	public void openMatch() {
		StartCoroutine (delayOpenMatch ());
	}

	IEnumerator delayOpenMatch() {
		logoAnim.Play ("logoAnimation");
		yield return new WaitForSeconds (0.1f);
		Main.Instance.openView ("match",match_id);
	}


}
