
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;
public class RollerCardCtrl :  MonoBehaviour {

	public PlayCardCtrl playCardCtrl;

	public List<PlayCard> playcards = new List<PlayCard>();
	private Dictionary<string, int> playCardsRot = new Dictionary<string, int>();
	public PlayCard playCardCentered;
	public List<PlayCard> PlayCardsHolds = new List<PlayCard>(); 
	private int rotateTo = 0;
	public bool cardsOpen = true;
	float momRotTo;

	public GameObject RollerBtn;
	public GameObject RollerTextObj;
	public GameObject RollerCardBtn;
	public Animation rollerBtnAnim;
	private UILabel CardDesc;

	// Attributes
	public bool dockLeft = true;
	public bool isPreset;
	public RollerCardCtrl otherRoller;
	public RollerCardCtrl presetRoller;

	public bool RollerClosedAtStart;
	public bool useMomentum;
	public float MomentumStartLimit = 500;
	public int MomentumSpeedDiv = 2000;
	public int CenterSpeed = 5;
	private float rotateSpeed;

	public bool isTutorial = false;
	private bool inited = false;
	public bool noCards = false;
	public bool isRotating = false;

	public MatchCtrl matchControl;
	// CenterCards Connection
	private CenterCardsCtrl CenterCards;

	// Less Than 6 logic
	public bool lessThanSix = false;

	// Double check
	public bool doDoubleCheck;

	// if roller is locked
	public bool locked = false;



	void Start() {
		if (isTutorial) {
			print ("Is Tutorial is TRUE!");
			 
		
			playcards.Add (new PlayCard("0", 0, new Dictionary<string, string>{{"en","Corner in the next 10'"},{"ar", LocalizationManager.GetTermTranslation("tut_card1",false,25,true)}}, new Dictionary<string, string>{ {"ar", LocalizationManager.GetTermTranslation("Corner", false)}, {"en", "Corner" }}, "corner"));
			playcards.Add (new PlayCard("1", 1, new Dictionary<string, string>{{"en","Yellow in the next 10'"},{"ar", LocalizationManager.GetTermTranslation("tut_card2", false, 25, true) }}, new Dictionary<string, string>{{"ar", LocalizationManager.GetTermTranslation("Yellow", false)}, {"en", "Yellow" }}, "yellow"));
			playcards.Add (new PlayCard("2", 2, new Dictionary<string, string>{{"en","Goal in the next 10'"},{"ar", LocalizationManager.GetTermTranslation("tut_card3", false, 25, true) }}, new Dictionary<string, string>{{"ar", LocalizationManager.GetTermTranslation("Goal",false)}, {"en", "Goal" }}, "goal"));
			playcards.Add (new PlayCard("3", 3, new Dictionary<string, string>{{"en","Foul in the next 10'"},{"ar", LocalizationManager.GetTermTranslation("tut_card4", false, 25, true) }}, new Dictionary<string, string>{{"ar", LocalizationManager.GetTermTranslation("Foul", false)}, {"en", "Foul" }}, "foul"));
			playcards.Add (new PlayCard("4", 4, new Dictionary<string, string>{{"en","Offside in the next 10'"},{"ar", LocalizationManager.GetTermTranslation("tut_card5", false, 25, true) }}, new Dictionary<string, string>{{"ar", LocalizationManager.GetTermTranslation("Offside",false)}, {"en", "Offside" }}, "offside"));
			playcards.Add (new PlayCard("5", 5, new Dictionary<string, string>{{"en","Red in the next 10'"},{"ar", LocalizationManager.GetTermTranslation("tut_card6", false, 25, true) }}, new Dictionary<string, string>{{"ar", LocalizationManager.GetTermTranslation("Red",false)}, {"en", "Red" }}, "red"));
			//playcards.Add (new PlayCard("6", 6, new Dictionary<string, string>{{"en","Penalty in the next 5'"},{"ar", LocalizationManager.FixRTL_IfNeeded(LocalizationManager.GetTermTranslation("tut_card7"), 9)}}, new Dictionary<string, string>{{"ar",LocalizationManager.FixRTL_IfNeeded(LocalizationManager.GetTermTranslation("Penalty"))}, {"en", "Penalty" }}, "penalty"));

//			playcards.Add (new PlayCard("0", 0, new Dictionary<string, string>{{"en","Corner in the next 10'"},{"ar","tut_card1"}}, new Dictionary<string, string>{{"ar", "Corner"}}, "corner"));
//			playcards.Add (new PlayCard("1", 1, new Dictionary<string, string>{{"en","Yellow in the next 10'"},{"ar","tut_card2"}}, new Dictionary<string, string>{{"ar","Yellow"}}, "yellow"));
//			playcards.Add (new PlayCard("2", 2, new Dictionary<string, string>{{"en","Goal in the next 10'"},{"ar","tut_card3"}}, new Dictionary<string, string>{{"ar","Goal"}}, "goal"));
//			playcards.Add (new PlayCard("3", 3, new Dictionary<string, string>{{"en","Foul in the next 10'"},{"ar","tut_card4"}}, new Dictionary<string, string>{{"ar","Foul"}}, "foul"));
//			playcards.Add (new PlayCard("4", 4, new Dictionary<string, string>{{"en","Offside in the next 10'"},{"ar","tut_card5"}}, new Dictionary<string, string>{{"ar","Offside"}}, "offside"));
//			playcards.Add (new PlayCard("5", 5, new Dictionary<string, string>{{"en","Red in the next 10'"},{"ar","tut_card6"}}, new Dictionary<string, string>{{"ar","Red"}}, "red"));
//			playcards.Add (new PlayCard("6", 6, new Dictionary<string, string>{{"en","Penalty in the next 5'"},{"ar","tut_card7"}}, new Dictionary<string, string>{{"ar","Penalty"},}, "penalty"));

			// CardRotations
			if (dockLeft ) {
				playCardsRot.Clear();
				print ("Filling playCardRot Dic");
				playCardsRot.Add ("Card1", -60);
				playCardsRot.Add ("Card2", 0);
				playCardsRot.Add ("Card3", 60);
				playCardsRot.Add ("Card4", 120);
				playCardsRot.Add ("Card5", -180);
				playCardsRot.Add ("Card6", -120);
				
			} else {
				playCardsRot.Clear();
				playCardsRot.Add ("Card1", 120);
				playCardsRot.Add ("Card2", 180);
				playCardsRot.Add ("Card3", -120);
				playCardsRot.Add ("Card4", -60);
				playCardsRot.Add ("Card5", 0);
				playCardsRot.Add ("Card6", 60);
				
			}
		
			for (int i = 0; i < PlayCardsHolds.Count; i++) {
				PlayCardsHolds[i].int_id = playcards[i].int_id;
				PlayCardsHolds[i].Icon = playcards[i].Icon;
				PlayCardsHolds[i].text = playcards[i].text;
				PlayCardsHolds[i].title = playcards[i].title;
				// Refresh info on card
				PlayCardsHolds[i].refreshFast();
			}

			inited = true;
			playCardCtrl.CardsOpenCheck = true;

			// Roller Card Button
			rollerBtnAnim = RollerCardBtn.GetComponent<Animation> ();
			CardDesc = RollerCardBtn.transform.FindChild ("CardDesc").GetComponent<UILabel> ();
			
			// Closed At the beginning
			if (RollerClosedAtStart)
				checkRollerOpenState ();
			
			// Do the CentercardsConnection
//			if (CenterCards == null)
//				CenterCards = GameObject.FindGameObjectWithTag ("CenterCards").GetComponent<CenterCardsCtrl>();
		}

		if (isTutorial) {
			print ("Rotating!!!");
			rotateTo = 180;
		}
	}

	//void Start() {
		public void init(){
		// //PlayCard(int id,string icon, string text, string desc, string choice1, string choice2, string choice3, string choice4)
		// playcards.Add(new PlayCard(0, "corner","Corner", "Corner in the next 10'", "choise 50pts", "choise 100pts", "choise 250pts", "choise 250pts"));
		// playcards.Add(new PlayCard(1, "yellow","Yellow", "Yellow card in the next 10'", "choise 50pts", "choise 100pts", "choise 250pts", "choise 250pts"));
		// playcards.Add(new PlayCard(2, "goal","Goal", "Goal in the next 10'", "choise 50pts", "choise 100pts", "choise 250pts", "choise 250pts"));
		// playcards.Add(new PlayCard(3, "foul","Foul", "Foul in the next 10'", "choise 50pts", "choise 100pts", "choise 250pts", "choise 250pts"));
		// playcards.Add(new PlayCard(4, "offside","Offside", "Offside in the next 10'", "choise 50pts", "choise 100pts", "choise 250pts", "choise 250pts"));
		// playcards.Add(new PlayCard(5, "red","Red", "Red card in the next 10'", "choise 50pts", "choise 100pts", "choise 250pts", "choise 250pts"));
		// playcards.Add(new PlayCard(6, "red","Test card1", "test card 1", "choise 50pts", "choise 100pts", "choise 250pts", "choise 250pts"));
		// playcards.Add(new PlayCard(7, "red","Test card2", "test card 2", "choise 50pts", "choise 100pts", "choise 250pts", "choise 250pts"));
//
//		foreach (PlayCard card in playcards) {
//			if(card.cardType == "PresetInstant")
////			print (card.cardType+" "+card.primaryStatistic);
//		}

		// CardRotations
		if (dockLeft ) {
			playCardsRot.Clear();
//			print ("Filling playCardRot Dic");
			playCardsRot.Add ("Card1", -60);
			playCardsRot.Add ("Card2", 0);
			playCardsRot.Add ("Card3", 60);
			playCardsRot.Add ("Card4", 120);
			playCardsRot.Add ("Card5", -180);
			playCardsRot.Add ("Card6", -120);

		} else {
			playCardsRot.Clear();
			playCardsRot.Add ("Card1", 120);
			playCardsRot.Add ("Card2", 180);
			playCardsRot.Add ("Card3", -120);
			playCardsRot.Add ("Card4", -60);
			playCardsRot.Add ("Card5", 0);
			playCardsRot.Add ("Card6", 60);

		}
//		print ("PlayCardsHolds.Count" + PlayCardsHolds.Count);

		// Fill In the content for the Roller
		for (int i = 0; i < PlayCardsHolds.Count; i++) {
//			print ("i.Count " + i);
//			if(playcards[i].cardType == "PresetInstant")
//			print ("playcards.Count " + playcards.Count + "   current i" + i);

			if(playcards.Count > i) { // if the number of the playcards is higher than the i
//				if(playcards[i].cardType == "PresetInstant")
//					print ("playcards[i].primaryStatistic " + playcards[i].primaryStatistic );
				
				PlayCardsHolds[i].int_id = playcards[i].int_id;
				//PlayCardsHolds[i].Icon = playcards[i].Icon;
				PlayCardsHolds[i].text = playcards[i].text;
				//LocalizationManager.FixRTL_IfNeeded (match.matchData.home_team.name[ getUsedLang( match.matchData.home_team.name )])
//				PlayCardsHolds[i].refreshCard (playcards [i].Icon, LocalizationManager.FixRTL_IfNeeded ( playcards [i].title[ getUsedLang(playcards [i].title)] ), 
//				                               playcards[i].text, 
//				                               playcards[i].newCard);
				PlayCardsHolds [i].refreshFull (playcards [i]);

			} else { // if the holders are more
				//print("card Type: "+playcards[i].cardType);
//				PlayCardsHolds[i].int_id = i;
//				//PlayCardsHolds[i].Icon = playcards[i].Icon;
//				PlayCardsHolds[i].text = new Dictionary<string, string>();
//				PlayCardsHolds[i].text.Add("en", "No Card");
				PlayCardsHolds[i].emptyCard();

			}



		
		}

		inited = true;

		// Roller Card Button
		rollerBtnAnim = RollerCardBtn.GetComponent<Animation> ();
		CardDesc = RollerCardBtn.transform.FindChild ("CardDesc").GetComponent<UILabel> ();

		if (dockLeft) {
			if(playcards.Count > 1) {
//				playCardCentered = playcards [1];
				CardDesc.text = LocalizationManager.FixRTL_IfNeeded(playcards [1].text [ getUsedLang(playcards [1].text) ],9,true );
				//CardDesc.text = LocalizationManager.FixRTL_IfNeeded( playCardCentered.text [ getUsedLang(playCardCentered.text) ], 10, true);
				print (CardDesc.text);
				playCardCentered = playcards [1] as PlayCard;
				rotateTo = 0;
			} else if( playcards.Count == 1) {
				CardDesc.text = LocalizationManager.FixRTL_IfNeeded( playcards [0].text [ getUsedLang(playcards [0].text) ], 9, true);
				playCardCentered = playcards [0] as PlayCard;
				rotateTo = -60;
			}



		} else {
			//print ("Overall Playcards Count: "+playcards.Count);
			if(playcards.Count > 0) {
				CardDesc.text = LocalizationManager.FixRTL_IfNeeded( playcards [0].text [ getUsedLang(playcards [0].text) ], 9, true);
				playCardCentered = playcards [0] as PlayCard;
				rotateTo = 120;
//				print (playCardCentered.text["en"]);
			}
		}

		// Closed At the beginning
		if (RollerClosedAtStart)
			checkRollerOpenState ();

		// Do the CentercardsConnection
		if (CenterCards == null)
			CenterCards = GameObject.FindGameObjectWithTag ("CenterCards").GetComponent<CenterCardsCtrl>();

//		print ("playcards :" + playcards.Count);
	}

	void OnTriggerEnter(Collider other) {

		//print ("playcards :" + other.name);
		playCardCentered  = other.gameObject.GetComponent<PlayCard>();
		//print ("playcards.Count: " + playcards.Count);
		//print ("isPreaset: " + isPreset);

		if (playcards.Count < 6)
			return;
		


		if (!inited || noCards || lessThanSix || isRotating)
			return;
	
			int leftCount;

			if (playCardCentered.int_id - 2 < 0){
				
				leftCount = ((playcards.Count) + (playCardCentered.int_id - 2));

			}else{

				leftCount = playCardCentered.int_id - 2;
				//print ("LEFT COUNT: "+leftCount);
			}

//		print ("refreshing TOP card as: "+playcards[leftCount].title["en"] );
			playCardCentered.cardLeft.refreshFull(playcards[leftCount]);

			//refresh right card
			int rightCount;
//			print ("playCardCentered.int_id+2: " + playCardCentered.int_id + 2);
			if (playCardCentered.int_id + 2 > playcards.Count - 1) {
				rightCount = ((playCardCentered.int_id + 2) - (playcards.Count)); // -1 Gets in account the 0 of the list
//				print ("Right COUNT: "+rightCount);
			} else {
				rightCount = playCardCentered.int_id + 2;
				//print ("Right COUNT: "+rightCount);
			}

		//print ("refreshing BOTTOM card as: "+playcards[rightCount].title["en"] );
		playCardCentered.cardRight.refreshFull(playcards[rightCount]);

		if (doDoubleCheck ) {
			//refresh extra right card
			int rightCloseCount;
			//			print ("playCardCentered.int_id+2: " + playCardCentered.int_id + 2);
			if (playCardCentered.int_id + 1 > playcards.Count - 1) {
				rightCloseCount = ((playCardCentered.int_id + 1) - (playcards.Count)); // -1 Gets in account the 0 of the list
								//print ("Right COUNT: "+rightCount);
			} else {
				rightCloseCount = playCardCentered.int_id + 1;

			}
			//print ("Right COUNT: "+rightCloseCount);
			if(!isTutorial)
			playCardCentered.cardCloseRight.refreshFull(playcards[rightCloseCount]);
		
			int leftCloseCount;
			
			if (playCardCentered.int_id - 1 < 0){
				
				leftCloseCount = ((playcards.Count) + (playCardCentered.int_id - 1));
				
			}else{
				
				leftCloseCount = playCardCentered.int_id - 1;

			}
			//print ("LEFT COUNT: "+leftCloseCount);
			playCardCentered.cardCloseLeft.refreshFull(playcards[leftCloseCount]);
		}
	}

	public void openCenterCards() {//TODO check if there is actually a card to see;
		// Open CenterCards Object
		iTween.ScaleTo(CenterCards.gameObject, iTween.Hash("scale", new Vector3(1.0f,1.0f,1.0f), "time", 0.8f, "delay", 0.5f, "easetype", iTween.EaseType.easeOutElastic));
		// Close both Rollers
		closeRoller ();
		// Call the CenterCard create the Card list
		CenterCards.isActive = true;
		CenterCards.resetAnimHolder ();
//		print (playCardCentered.int_id);
//		print (playcards[playCardCentered.int_id].title["en"]);
		CenterCards.createCardList (playcards, playCardCentered.int_id);
		CenterCards.playSwipeAnim ();

		//Coach.coachMessage (LocalizationManager.GetTermTranslation( "tip4", true, 30, true), 27f, 4f, 2.5f, true, "tip4");
	}

	public GameObject rollerDex;
	private bool fingerDown = false;


	void Update() {


	
		if (!cardsOpen || !playCardCtrl.CardsOpenCheck)
			return;

		// center to Object
		if(!fingerDown && !useMomentum)
			rollerDex.transform.rotation = Quaternion.Lerp(rollerDex.transform.rotation, Quaternion.Euler(0, 0, rotateTo), Time.deltaTime*CenterSpeed);

//		if(!fingerDown && useMomentum)
//			rollerDex.transform.rotation = Quaternion.Lerp(rollerDex.transform.rotation, Quaternion.Euler(0, 0, rotateTo), Time.deltaTime*10);
	}

	void OnSwipe( SwipeGesture gesture ) 
	{
		if (!cardsOpen || !playCardCtrl.CardsOpenCheck || noCards)
			return;
		//print ("Gesture Move: " + gesture.Move);
//		print ("Gesture Velocity: " + gesture.Velocity);
		//print ("Gesture Direction: " + gesture.Direction);
		if(gesture.Velocity > MomentumStartLimit) {
//			print ("Do Roller Lerp Before Centering Object");
			rotateSpeed = gesture.Velocity;

				if(gesture.Direction == FingerGestures.SwipeDirection.Down){
					if(dockLeft)
					momRotTo = -gesture.Velocity/MomentumSpeedDiv;		
					else
					momRotTo = gesture.Velocity/MomentumSpeedDiv;
			}else if(gesture.Direction == FingerGestures.SwipeDirection.Up){
				if(dockLeft)
					momRotTo = gesture.Velocity/MomentumSpeedDiv;		
				else
					momRotTo = -gesture.Velocity/MomentumSpeedDiv;	
			}				
			useMomentum = true;

			// can not trigger cards
			isRotating = true;
//			print ("momRotTo"+ momRotTo);
			iTween.RotateBy(rollerDex, iTween.Hash("z", momRotTo, "time", gesture.Velocity/(MomentumSpeedDiv/2), "easetype"
			                                       , iTween.EaseType.easeOutCirc, "islocal", true
			                                       ,"onCompleteTarget",gameObject, "onComplete", "swipeAnimEnded"));
		}

	}

	void swipeAnimEnded() {

		// Can trigger cards
		isRotating = false;
		if (!cardsOpen || !playCardCtrl.CardsOpenCheck || noCards)
			return;

		string animToPlay;
		if (dockLeft)
			animToPlay = "RollerBtnPop";
		else
			animToPlay = "RollerBtnPopRight";

		// Tell the update that the dragging is finished so it can Lerp Rotate to the center of the card
		fingerDown = false;
//		print (playCardCentered);

		if(inited)
		rotateTo = playCardsRot[playCardCentered.gameObject.name];

		// Open The Btn
		CardDesc.text = LocalizationManager.FixRTL_IfNeeded( playCardCentered.text[ getUsedLang(playCardCentered.text) ],9, true);
		
		rollerBtnAnim.GetComponent<Animation>()[animToPlay].speed = 1;
		rollerBtnAnim.GetComponent<Animation>()[animToPlay].time = 0;
		rollerBtnAnim.GetComponent<Animation>().Play(animToPlay);

		useMomentum = false;
	}

	void OnDrag( DragGesture gesture ) 
	{
		if (!cardsOpen || useMomentum || !playCardCtrl.CardsOpenCheck)
			return;

		string animToPlay;
		if (dockLeft)
			animToPlay = "RollerBtnPop";
		else
			animToPlay = "RollerBtnPopRight";
		

		if (gesture.Phase == ContinuousGesturePhase.Started) {
			fingerDown = true;

			// Close The Btn
			rollerBtnAnim.GetComponent<Animation>()[animToPlay].speed = -3;
			rollerBtnAnim.GetComponent<Animation>()[animToPlay].time = 1;
			rollerBtnAnim.GetComponent<Animation>().Play(animToPlay);

//			CardDesc.text = playCardCentered.text[LocalizationManager.CurrentLanguageCode];
		}
//			print (	Input.mousePosition.y);
			if(dockLeft)
			rollerDex.transform.Rotate(0f, 0f, gesture.DeltaMove.y/3);//(Input.mousePosition.y- screenMiddle)/80);
			else
			rollerDex.transform.Rotate(0f, 0f, -gesture.DeltaMove.y/3);//(Input.mousePosition.y- screenMiddle)/80);

		if (gesture.Phase == ContinuousGesturePhase.Ended) {
			// Tell the update that the dragging is finished so it can Lerp Rotate to the center of the card
			fingerDown = false;

			if(inited){
			rotateTo = playCardsRot[playCardCentered.gameObject.name];

				CardDesc.text = LocalizationManager.FixRTL_IfNeeded( playCardCentered.text[ getUsedLang(playCardCentered.text) ],9,true);

				//CardDesc.text = playcards[playCardCentered.int_id].text[LocalizationManager.CurrentLanguageCode];
				rollerBtnAnim.GetComponent<Animation>()[animToPlay].speed = 1;
				rollerBtnAnim.GetComponent<Animation>()[animToPlay].time = 0;
				rollerBtnAnim.GetComponent<Animation>().Play(animToPlay);
			}

		}

	}

	public void checkRollerOpenState() {
			if (locked)
			return;

		if (cardsOpen) { //CLOSE THE ROLLER
//			print ("Roller Open: "+ cardsOpen);
			iTween.ScaleTo( rollerDex, iTween.Hash("scale", new Vector3(0.2f,0.2f,0.2f), "time", 0.5f, "easetype", iTween.EaseType.easeInOutCirc));
			NGUITools.SetActive(RollerCardBtn, false);
			cardsOpen = !cardsOpen;
			// change the center button attributes
			iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(0.8f,0.8f,0.8f), "time", 1, 
			                                       "easetype", iTween.EaseType.easeInOutElastic,
			                                       "onCompleteTarget",gameObject, "onComplete", "openRollerTweenEnd"));

			TweenColor.Begin(RollerTextObj, 0.5f, new Color32(253,196,48,255));
			TweenColor.Begin(RollerBtn, 0.5f, new Color32(36,47,41,255));
		} else { // OPEN THE ROLLER
			if (noCards) {
				MessagePanel.Instance.animMessage ( LocalizationManager.GetTermTranslation("mes_nocards", true), LocalizationManager.GetTermTranslation("mes_warning", true), "sportimo-Logo", 4.0f);
				return;
			}
			else if(!MatchCtrl.timecounting && dockLeft){
				if(!isTutorial && !isPreset) { // Check if this Roller is used in a tutorial OR is our new Roller with Preset Cards
					MessagePanel.Instance.animMessage("Live cards are only available during timed segments");
					return;
				}
			}
			else if(MatchCtrl.instants == 0 && dockLeft){
				MessagePanel.Instance.animMessage("You don't have any more instant cards available to play");
				return;
			}
			else if(MatchCtrl.overalls == 0 && !dockLeft){
				MessagePanel.Instance.animMessage("You don't have any more overall cards available to play");
				return;
			} 
//				else if (matchControl.match.matchData.completed) {
//				MessagePanel.Instance.animMessage ("You can't play cards on a match that has ended");
//				return;
//			}

//			print ("CenterCard id" + playCardCentered.int_id);
//			print ("Roller Open: "+ cardsOpen);
			iTween.ScaleTo( rollerDex, iTween.Hash("scale", new Vector3(1f,1f,1f), "time", 1, "easetype", iTween.EaseType.easeInOutElastic));
			StartCoroutine(openRollerBtnDelay());

			//close other rollers
			print("Other ROller Name: "+ otherRoller.name);
			print("otherRoller.cardsOpen: "+ otherRoller.cardsOpen);
			if(otherRoller.cardsOpen)
			otherRoller.checkRollerOpenState();
			
			if (presetRoller != null) {
				if(presetRoller.cardsOpen)
					presetRoller.checkRollerOpenState();
			}

			cardsOpen = !cardsOpen;

			// change the center button attributes
			iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(1.0f,1.0f,1.0f), "time", 0.5f, "easetype", iTween.EaseType.easeInOutElastic));
			TweenColor.Begin(RollerTextObj, 0.5f, new Color32(36,47,41,255));
			TweenColor.Begin(RollerBtn, 0.5f, new Color32(253,196,48,255));
		}
		if (CenterCards != null) {
			// TODO CHANGE THIS TO CENTERCARDS METHOD FOR CLOSE
			iTween.ScaleTo (CenterCards.gameObject, iTween.Hash ("scale", new Vector3 (0.001f, 0.001f, 0.001f), "time", 0.5f));

			CenterCards.closeCenterCards ();
			// Deactivate Center Cards
			CenterCards.isActive = false;
		}

	}

	void openRollerTweenEnd() {
		// Do th first rotate to spice it up
		//rotateTo = 120;
	}

	public void closeRoller() {
		iTween.ScaleTo( rollerDex, iTween.Hash("scale", new Vector3(0.2f,0.2f,0.2f), "time", 0.5f, "easetype", iTween.EaseType.easeInOutCirc));
		NGUITools.SetActive(RollerCardBtn, false);
		cardsOpen = false;

		// change the center button attributes
		iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(0.8f,0.8f,0.8f), "time", 1, "easetype", iTween.EaseType.easeInOutElastic));
		TweenColor.Begin(RollerTextObj, 0.5f, new Color32(253,196,48,255));
		TweenColor.Begin(RollerBtn, 0.5f, new Color32(36,47,41,255));

//		if (otherRoller.cardsOpen)  // CHANGE THE OTHER ROLLERS STATE
//			otherRoller.checkRollerOpenState ();

	}

	public void openRoller() {
		if (locked)
			return;
	
		iTween.ScaleTo( rollerDex, iTween.Hash("scale", new Vector3(1f,1f,1f), "time", 0.5f, "easetype", iTween.EaseType.easeInOutCirc));
		NGUITools.SetActive(RollerCardBtn, true);
		cardsOpen = true;

		// change the center button attributes
		iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(1.0f,1.0f,1.0f), "time", 0.5f, "easetype", iTween.EaseType.easeInOutElastic));
		TweenColor.Begin(RollerTextObj, 0.5f, new Color32(36,47,41,255));
		TweenColor.Begin(RollerBtn, 0.5f, new Color32(253,196,48,255));



		if(otherRoller.cardsOpen) 
			otherRoller.closeRoller();
		
		if (presetRoller != null) {
			if(presetRoller.cardsOpen)
				presetRoller.checkRollerOpenState();
		}
		// Deactivate Center Cards
//		if(CenterCards != null)
//		CenterCards.isActive = false;
	}


	IEnumerator openRollerBtnDelay ()
	{
		yield return new WaitForSeconds (.3f);
		//Test nocards 

//		if (noCards) {
//			MessagePanel.Instance.animMessage ( LocalizationManager.GetTermTranslation("mes_nocards"), LocalizationManager.GetTermTranslation("mes_warning"), "sportimo-Logo", 3.0f);
//			StopCoroutine (openRollerBtnDelay ());
//		}

		NGUITools.SetActive(RollerCardBtn, true);
	}

	string getUsedLang(Dictionary<string,string> dictionary) {
		string usedLang;

		if (dictionary.ContainsKey (LocalizationManager.CurrentLanguageCode))
			usedLang = LocalizationManager.CurrentLanguageCode;
		else
			usedLang = "en";

		return usedLang;
	}


}
