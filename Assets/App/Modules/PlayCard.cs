﻿using UnityEngine;
using System.Collections.Generic;
using LitJson;
using System;
using I2.Loc;

public class PlayCard : MonoBehaviour {

	public int int_id;
	public string _id;
	
	public string id;
	public string gamecardTemplateId;
	
	public string gamecardDefenitionId;
	public string primaryStatistic;
	public Dictionary<string,string> title;
	public Dictionary<string,string> text;
	
	public Dictionary<string,string> image;

	public string cardType;
	
	public int status;
	public int segment;
	
	public int minute;
	
	public int startPoints;
	
	public string optionId;
	public int endPoints;
	
	public int activationLatency;
	public string activationTime;
    
    public string terminationTime;
	public string creationTime;

	public string pauseTime;

	public string resumeTime;
	public string specialCreationTime;
	public string specialActivationTime;

	public string wonTime;
	
	public int pointsAwarded;
	public int duration;
	
	public bool isDoublePoints;
	public bool isDoubleTime;
	public string Icon;
	public Option[] options = new Option[4];

	public bool newCard = false;
	public GameObject newCardTag;

	public Specials specials;

	public string guruAction = "Sum";

	public GameObject emptyCardObj;
	// public PlayCard(JsonData data) {
	// 	this.id = data["_id"].ToString();
	// 	this.cardType = data["cardType"].ToString();
	// 	this.defenition_id = data["gamecardTemplateId"].ToString();
	// 	this.text = JsonMapper.ToObject<Dictionary<string,string>>(data["text"].ToJson());
	// 	if(this.cardType == "Instant")
	// 	this.duration = (int)data["duration"]/1000;

	// 	for(int i = 0; i < data["options"].Count; i++){
	// 		Option option = new Option(data["options"][i],this.cardType);
	// 		this.options[i] = option;
	// 	}
	// }
	public PlayCard(){}
	public PlayCard(string _id, int int_id, Dictionary<string,string> text, Dictionary<string,string> title, string icon) {
		this._id = _id;
		this.int_id = int_id;
		this.title = title;
		this.text = text;
		this.Icon = icon;
	}

	public PlayCard(int int_id) {
		this._id = "locked";
		this.int_id = int_id;

		Dictionary<string,string> titles = new Dictionary<string, string> ();
		titles.Add("en","Locked");
		this.title = titles;

		Dictionary<string,string> discriptions = new Dictionary<string, string> ();
		discriptions.Add("en","No Card!");
		this.text = discriptions;
		this.Icon = "Fill 3";
		//this.cardType = data["cardType"].ToString();
		//this.defenition_id = data["gamecardTemplateId"].ToString();
//		this.text = JsonMapper.ToObject<Dictionary<string,string>>(data["text"].ToJson());
//		if(this.cardType == "Instant")
//			this.duration = (int)data["duration"];
//		
//		for(int i = 0; i < data["options"].Count; i++){
//			Option option = new Option(data["options"][i],this.cardType);
//			this.options[i] = option;
//		}
	}
	
	// public static List<PlayCard> playcards(string json){
	// 	List<PlayCard> playcards = new List<PlayCard>();
	// 	JsonData data = JsonMapper.ToObject(json);
	// 	for(int i = 0; i < data["data"].Count; i++){

	// 		PlayCard card = new PlayCard(data["data"][i]);
	// 		// TODO change this when the data are comming
	// 		//card.text.Add("en", "Test name");
	// 		if(card.text.Count == 0){
	// 			card.text.Add("en","test");
	// 		}
	// 		card.int_id = i;
	// 		playcards.Add(card);
	// 	}
		
	// 	return playcards;
		
	// }

	public UILabel textLabel;
	public UISprite iconSprite;

	public PlayCard cardCloseLeft;
	public PlayCard cardLeft;
	public PlayCard cardCloseRight;
	public PlayCard cardRight;
	public PlayCard cardExtraRight;

	private Collider cardCollider;

	void Start () { 
		textLabel = transform.FindChild ("CardText").GetComponent<UILabel> ();
		iconSprite = transform.FindChild ("CardIcon").GetComponent<UISprite> ();

//		if( text[LocalizationManager.CurrentLanguageCode] != null )
// 		textLabel.text = text[LocalizationManager.CurrentLanguageCode];
//		else
		textLabel.text = "";
		iconSprite.spriteName = Icon;

		cardCollider = GetComponent<Collider> ();
	}

	public void refreshCard(string Icon, string title, Dictionary<string,string> text = null, bool newCard = false) {
	
		textLabel.text = title;
		iconSprite.spriteName = Icon;
		this.text = text;


		if (newCard) {
			print ("New Card :" + title);
			//print ("New Card Creation:" + creationTime);

			NGUITools.SetActive (newCardTag, true);
		} else {
			//print ("Old Card");
			NGUITools.SetActive (newCardTag, false);
		}

		cardCollider.enabled = true;
		if(emptyCardObj != null)
			NGUITools.SetActive (emptyCardObj, false);
	}

	public void refreshFull (PlayCard playCard ) {
		this._id = playCard.id;
		this.int_id = playCard.int_id;
		this.title = playCard.title;
		this.text = playCard.text;
		this.Icon = playCard.Icon;
		this.newCard = playCard.newCard;

		textLabel.text = LocalizationManager.FixRTL_IfNeeded( title [getUsedLang (title) ]);
		iconSprite.spriteName = Icon;
		this.text = text;
		cardCollider.enabled = true;
		if(emptyCardObj != null)
			NGUITools.SetActive (emptyCardObj, false);

		if (playCard.newCard) {
			print (textLabel.text+" is NEW!!!");
			NGUITools.SetActive (newCardTag, true);
		} else {
			NGUITools.SetActive (newCardTag, false);
		}
	}

	public void refreshFast() {
		if (textLabel == null) {
			textLabel = transform.FindChild ("CardText").GetComponent<UILabel> ();
			iconSprite = transform.FindChild ("CardIcon").GetComponent<UISprite> ();
		}

		 //textLabel.text = text[LocalizationManager.CurrentLanguageCode];
		textLabel.text = title["en"];
		iconSprite.spriteName = Icon;
		//iconSprite.spriteName = "Icon_Leaders";
	}

	public void emptyCard() {
		cardCollider.enabled = false;
		if(emptyCardObj != null)
			NGUITools.SetActive (emptyCardObj, true);
	}
	
	public class Option {
		public string optionId;
		public int startPoints;

		public int endPoints;
		
		public double pointsPerMinute;
		public int remaining;
		
		public Dictionary<string,string> text;
	}

	public class Specials {
		public Special DoubleTime;
		public Special DoublePoints;
	}

	public class Special {
		public string creationTime;
		public string activationTime;
		public int activationLatency;
		
		public string _id;

		public int status;

	}	

		
	// 	public Option(JsonData data,string cardtype){
	// 		this.option_id = int.Parse((string)data["optionId"]);
	// 		if(cardtype == "Instant"){
//
	// 		if(data.Keys.Contains("startPoints")){
	// 			this.startPoints = (int)data["startPoints"];
	// 		}
	// 		else if(data["winConditions"][0].Keys.Contains("startPoints")){
	// 			this.startPoints=(int)data["winConditions"][0]["startPoints"];
	// 		}
	// 		if(data.Keys.Contains("endPoints")){
	// 			this.endPoints = (int)data["endPoints"];
	// 		}
	// 		else if(data["winConditions"][0].Keys.Contains("endPoints")){
	// 			this.endPoints=(int)data["winConditions"][0]["endPoints"];
	// 		}
	// 		}
	// 		this.remaining = (int)data["winConditions"][0]["remaining"];
	// 		if(data.Keys.Contains("text")){
	// 		this.text = JsonMapper.ToObject<Dictionary<string,string>>(data["text"].ToJson());
	// 		}
	// 		else if(data["winConditions"][0].Keys.Contains("text")){
	// 		this.text = JsonMapper.ToObject<Dictionary<string,string>>(data["winConditions"][0]["text"].ToJson());
	// 		}
	// 		if(this.text.Count==0){
	// 			this.text.Add("en","test");
	// 		}
			
	// 	}
	// }
//}

	string getUsedLang(Dictionary<string,string> dictionary) {
		string usedLang;

		if (dictionary.ContainsKey (LocalizationManager.CurrentLanguageCode))
			usedLang = LocalizationManager.CurrentLanguageCode;
		else
			usedLang = "en";

		return usedLang;
	}
}
