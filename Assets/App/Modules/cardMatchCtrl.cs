﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using I2.Loc;
using BedbugRest;

public class cardMatchCtrl : MonoBehaviour
{

    public PlayCard card;
    public string _id;
    public int option_selected;
    private int point_spread;
    private float points;
    private float points_step;
    private float fill_step;

    //UI references
    public Transform matchTable;
    public Transform cardsTable;
    public UISprite fill;
    public UISprite flag;
    public UISprite icon;
    public UILabel event_label;
    public UILabel desc_label;
    public UILabel pointslabel;
    public UILabel specials_count;
    public UIButton points_power;
    public UILabel double_label;
    public UISprite double_fill;

    //public bool doubleCounting = false;

    public UIButton time_power;
    private GameObject scoremask;
    private BannerAnimator bannerAnimator;
    public GameObject cardFiilObj;
    public GameObject activationObj;
    public UILabel activationlabel;
    public UISprite activationFill;
    public Animation cardAnimations;
    public UITexture home_team;
    public UITexture away_team;
    public UILabel home_team_stat;
    public UILabel away_team_stat;
    public string home_logo;
    public string away_logo;
    public string home_stat;
    public string away_stat;

    public string cardType;
    public GameObject cardStats;
    private bool statsOpen = false;
    public MatchCtrl matchInfo;

    void Start()
    {
        scoremask = GameObject.FindGameObjectWithTag("scoremask");
        if (scoremask)
            bannerAnimator = scoremask.GetComponent<BannerAnimator>();


    }

    public void Init()
    {
        // Fix weird ngui position bug
        //TODO populate correctly all fields
        // if(MatchCtrl.specials==0){
        //     points_power.isEnabled = false;
        //     if(card.cardType=="Instant")
        //     time_power.isEnabled = false;
        // }
        CancelInvoke("cardTimer");
        BedBugTools.loadImageOnUITextureAndResize(home_team, home_logo, 64, 64);
        BedBugTools.loadImageOnUITextureAndResize(away_team, away_logo, 64, 64);
        home_team_stat.text = home_stat;
        away_team_stat.text = away_stat;
        Vector3 localPos = transform.localPosition;
        localPos.x = 0;

        if (card.minute != 0)
        {
            //print(card.minute);
            //print(card.segment);
            //print(card.cardType == "Overall" && card.segment != 0 && card.segment != 2);

            if (card.cardType == "Overall" && card.segment != 0 && card.segment != 2) // OverAll Not in PreGame Not in HaklfTime
            {
                if (MatchCtrl.timecounting)
                    event_label.text = card.minute.ToString() + "' " + LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
                else
                    event_label.text = LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
            }
            else if (card.cardType == "Overall" && (card.segment == 0 || card.segment == 2)) // OverAll  in PreGame OR in HaklfTime
                event_label.text = LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);

            else if (card.cardType != "PresetInstant" || card.status != 0) // Instant
            {
                event_label.text = LocalizationManager.FixRTL_IfNeeded((card.minute.ToString() + "' " + card.title[getUsedLang(card.title)]), 0, true);
            }
            else { // PRESETINSTANT
                //event_label.text = LocalizationManager.FixRTL_IfNeeded( (card.title[getUsedLang(card.title)] + " " + LocalizationManager.GetTermTranslation("will_activate", false) + " " + card.minute.ToString() + "' "),0,true);
                event_label.text = LocalizationManager.FixRTL_IfNeeded((LocalizationManager.GetTermTranslation("will_activate", false) + " " + card.minute.ToString() + "' "), 0, true);
                pointslabel.text = card.startPoints.ToString();
            }
        }
        else {
            event_label.text = LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
        }


        //		Debug.Log (card.text[LocalizationManager.CurrentLanguageCode]);
        //		Debug.Log (desc_label);
        //		Debug.Log (MatchCtrl.stReturnName ("[[home_team_name]]"));

        //desc_label.text = LocalizationManager.FixRTL_IfNeeded( card.text [ getUsedLang(card.text) ].Replace ("[[home_team_name]]", MatchCtrl.stReturnName ("[[home_team_name]]")));

        desc_label.text = BedBugTools.GetStringByLocPrefs(card.text).Replace("[[home_team_name]]", MatchCtrl.stReturnName("[[home_team_name]]"));

        desc_label.text = LocalizationManager.FixRTL_IfNeeded(desc_label.text.Replace("[[away_team_name]]", MatchCtrl.stReturnName("[[away_team_name]]")));
        //Debug.Log(card.duration);

        // GET THE NUMBER OF SPECIALS LEFT TO PLAY
        specials_count.text = MatchCtrl.specials.ToString();

        // IF CARD ALREADY WON or LOST
        //		if (card.status == 2) {
        //			//NGUITools.SetActive (specials_count.parent.gameObject, false);
        //		}

        transform.localPosition = localPos;

        if (card.id != null)
            _id = card.id;
        else if (card._id != null)
            _id = card._id;

        option_selected = int.Parse(card.optionId);
        cardType = card.cardType;

        if (card.image.ContainsKey("sprite"))
        {
            icon.spriteName = card.image["sprite"];
        }
        else {
            icon.spriteName = "sportimoicon";
        }

        //		print ("Double points Status: "+card.specials.DoublePoints.status);

        // Special Activated
        if (card.specials.DoublePoints != null)
            if (card.specials.DoublePoints.status == 2)
            {
                if (card.cardType == "Overall")
                {
                    time_power.GetComponent<UISprite>().color = points_power.disabledColor;
                }
                points_power.isEnabled = false;
            }

        // Still in activation mode
        if (card.specials.DoublePoints != null)
            if (card.specials.DoublePoints.status == 1)
            {
                InvokeRepeating("countPointsActiv", 0f, 1f);
            }



        // card still active
        if (card.cardType == "Instant" || card.cardType == "PresetInstant")
        {


            if (card.specials.DoubleTime.status == 2)
            {
                time_power.isEnabled = false;
                StartCoroutine(ExtendBar());
            }

            if (card.status == 3 && card.terminationTime != null)
            { //card is paused


                flag.color = new Color32(74, 142, 225, 255);

                point_spread = card.startPoints - card.endPoints;
                points_step = (float)point_spread / (card.duration / 1000);
                fill_step = (float)1 / (card.duration / 1000);
                points = (float)card.startPoints;

                NGUITools.SetActive(fill.transform.parent.gameObject, true);

                Debug.Log(card.activationTime);
                Debug.Log(card.pauseTime);

                if (card.activationTime != null)
                {
                    if (DateTime.Parse(card.activationTime) > DateTime.Parse(card.pauseTime))
                    {
                        fill.fillAmount = 1f;
                        points = (float)card.startPoints;
                        pointslabel.text = Mathf.RoundToInt(points).ToString();
                        return;

                    }
                }



                DateTime terminationTime = DateTime.Parse(card.terminationTime).ToUniversalTime();
                int minutes_remaining = (terminationTime - DateTime.Parse(card.pauseTime).ToUniversalTime()).Minutes;
                int seconds_remaining = (terminationTime - DateTime.Parse(card.pauseTime).ToUniversalTime()).Seconds;
                seconds_remaining = seconds_remaining + minutes_remaining * 60;

                points = (points_step * seconds_remaining) + card.endPoints;

                Debug.Log("Third:" + ((float)(seconds_remaining * fill_step)));

                fill.fillAmount = (float)(seconds_remaining * fill_step);
                pointslabel.text = Mathf.RoundToInt(points).ToString();



            }
            else if (card.status == 3 && card.terminationTime == null)
            {
                cardAnimations.Play("cardshow1");
                flag.color = new Color32(74, 142, 225, 255);
                this.transform.FindChild("Match_Main").gameObject.GetComponent<UISprite>().color = new Color32(253, 196, 48, 255);
                this.transform.FindChild("Card_main_EndSide").gameObject.GetComponent<UISprite>().color = new Color32(253, 196, 48, 255);
            }
            else if (card.status != 2)
            {

                flag.color = new Color32(74, 142, 225, 255);


                point_spread = card.startPoints - card.endPoints;
                points_step = (float)point_spread / (card.duration / 1000);
                fill_step = (float)1 / (card.duration / 1000);
                points = (float)card.startPoints;

                if (card.activationTime != null)
                {
                    if (DateTime.Parse(card.creationTime).AddSeconds(card.activationLatency / 1000).ToUniversalTime() < DateTime.UtcNow.ToUniversalTime())
                    {
                        DateTime terminationTime = DateTime.Parse(card.terminationTime).ToUniversalTime();
                        DateTime now = DateTime.UtcNow;


                        int minutes_remaining = (terminationTime - DateTime.UtcNow.ToUniversalTime()).Minutes;
                        int seconds_remaining = (terminationTime - DateTime.UtcNow.ToUniversalTime()).Seconds;




                        Debug.Log(card.title["en"] + " " + card.startPoints + " " + terminationTime + " " + seconds_remaining);

                        seconds_remaining = seconds_remaining + minutes_remaining * 60;

                        if (MatchCtrl.timecounting)
                        { // If match time is not changing don't calculate
                            ContinueInstantCountDown(seconds_remaining);
                        }

                    }
                    else {//card is active but not activated

                        if (MatchCtrl.timecounting)
                        {
                            Debug.Log(card.startPoints);

                            pointslabel.text = card.startPoints.ToString();

                            if (card.activationLatency == 0)
                            {
                                StartInstantCountDown();
                            }
                            else {
                                StartActivationCountDown();
                            }
                        }
                    }
                }

                if (card.cardType == "PresetInstant")
                {

                    cardAnimations.Play("cardshow1");
                    if (card.status == 0)
                    {
                        this.transform.FindChild("Match_Main").gameObject.GetComponent<UISprite>().color = new Color32(253, 196, 48, 255);
                        this.transform.FindChild("Card_main_EndSide").gameObject.GetComponent<UISprite>().color = new Color32(253, 196, 48, 255);
                        //flag.color = new Color32 (74, 142, 225, 255);
                    }
                }

            }
            else {
                //				print ("PresetInstant else :" + card.status);
                pointslabel.text = card.pointsAwarded.ToString(); // Draw the points on the card

                if (card.pointsAwarded == 0)
                {
                    flag.color = new Color32(208, 2, 27, 255);
                }
                else {
                    flag.color = new Color32(71, 211, 32, 255);

                }
            }

            //print(MatchCtrl.timecounting);
            if (!MatchCtrl.timecounting && card.cardType != "Overall")
            {

                cancelTimer();
                //if(card.status == 1)

                //pointslabel.text = LocalizationManager.FixRTL_IfNeeded (LocalizationManager.GetTermTranslation ("paused"));
            }

        }
        else { // AND ITS AN OVERALL CARD
            if (card.status == 2)
            {
                pointslabel.text = card.pointsAwarded.ToString();

                if (card.pointsAwarded == 0)
                {
                    //					print ("Overall card is Lost");
                    flag.color = new Color32(208, 2, 27, 255);
                }
                else {
                    //					print ("Overall card is Won");
                    flag.color = new Color32(71, 211, 32, 255);

                }
            }
            else {
                pointslabel.text = card.startPoints.ToString();
                //				print ("Overall card is Pending");
                flag.color = new Color32(74, 142, 225, 255);

                if (DateTime.Parse(card.activationTime).ToUniversalTime() > DateTime.UtcNow)
                {
                    StartOverallActivationCountDown();
                }


            }
        }
        //print ("matchInfo.liveMatch.matchData.state: " + matchInfo.liveMatch.matchData.state);

        // Check if card is paused

        if (!MatchCtrl.timecounting && card.cardType != "Overall")
        {

            cancelTimer();
            if (matchInfo.liveMatch.matchData.state == 2 && card.status == 1)
            {
                print("Changing to Paused");
                pointslabel.text = LocalizationManager.FixRTL_IfNeeded(LocalizationManager.GetTermTranslation("paused"));
            }
        }
    }

    IEnumerator ExtendBar()
    {
        if (cardFiilObj.transform.localScale.x <= 1)
        {
            yield return new WaitForSeconds(1.5f);
            iTween.ScaleTo(cardFiilObj, iTween.Hash("x", 1.37f, "time", .5f, "easetype", iTween.EaseType.easeInOutQuad, "islocal", true));

        }
    }
    //card is not active
    public void UpdateSpecials()
    {
        specials_count.text = MatchCtrl.specials.ToString();
    }


    public void StartOverallActivationCountDown()
    {
        NGUITools.SetActive(activationObj, true);
        startNum = (DateTime.Parse(card.creationTime).ToUniversalTime().AddSeconds(card.activationLatency / 1000).ToUniversalTime() - DateTime.UtcNow.ToUniversalTime()).Seconds;

        //activationFill.fillAmount = 1;
        if (matchInfo.liveMatch.matchData.state == 0 || matchInfo.liveMatch.matchData.state == 2 || matchInfo.liveMatch.matchData.state == 4 || matchInfo.liveMatch.matchData.state == 6)
        {
            activationlabel.text = "";
            cardAnimations.Play("cardshow1");

            card.status = 1;

            if (card.cardType == "Overall" && card.segment != 0 && card.segment != 2)
            {
                event_label.text = card.minute.ToString() + "' " + LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
            }
            else if (card.cardType == "Overall" && (card.segment == 0 || card.segment == 2))
                event_label.text = LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
            else
                event_label.text = LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
            NGUITools.SetActive(activationObj, false);

        }
        else {
            InvokeRepeating("overallActivationTimer", 1f, 1f);
        }


        //activationlabel.text = startNum + "'";
    }

    private void overallActivationTimer()
    {
        int timeNum = ((DateTime.Parse(card.creationTime).ToUniversalTime().AddSeconds(card.activationLatency / 1000).ToUniversalTime() - DateTime.UtcNow.ToUniversalTime()).Seconds);
        if (matchInfo.liveMatch.matchData.state == 0 || matchInfo.liveMatch.matchData.state == 2 || matchInfo.liveMatch.matchData.state == 4 || matchInfo.liveMatch.matchData.state == 6)
        {
            activationlabel.text = "";
        }
        else {
            activationlabel.text = timeNum + "'";
        }

        cardAnimations.Play("cardCount");
        //activationFill.fillAmount = (float)timeNum/(float)startNum;
        //		print ("Activation Time: " + card.activationTime);
        if ((DateTime.Parse(card.activationTime).ToUniversalTime() <= DateTime.UtcNow))
        {
            CancelInvoke("overallActivationTimer");
            activationlabel.text = "";
            cardAnimations.Play("cardshow1");

            card.status = 1;

            if (card.cardType == "Overall" && card.segment != 0 && card.segment != 2)
            {
                event_label.text = card.minute.ToString() + "' " + LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
            }
            else if (card.cardType == "Overall" && (card.segment == 0 || card.segment == 2))
                event_label.text = LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
            else
                event_label.text = LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
            NGUITools.SetActive(activationObj, false);
        }


    }

    int startNum;

    public void StartActivationCountDown()
    {
        Debug.Log("Start timer");
        //NGUITools.SetActive (activationObj, true); 
        startNum = (DateTime.Parse(card.creationTime).ToUniversalTime().AddSeconds(card.activationLatency / 1000).ToUniversalTime() - DateTime.UtcNow.ToUniversalTime()).Seconds;
        activationlabel.text = startNum + "'";
        //activationFill.fillAmount = 1;
        InvokeRepeating("activationTimer", 1f, 1f);
    }

    int activation_remaining;

    public void activationTimer()
    {
        //		print ("Card creation: " + card.creationTime);
        int timeNum = ((DateTime.Parse(card.creationTime).ToUniversalTime().AddSeconds(card.activationLatency / 1000).ToUniversalTime() - DateTime.UtcNow.ToUniversalTime()).Seconds);
        //print (((float)timeNum/1000)*(float)startNum);
        activationlabel.text = timeNum + "'";
        //activationFill.fillAmount = (float)timeNum/(float)startNum;
        cardAnimations.Play("cardCount");

        if ((DateTime.Parse(card.activationTime).ToUniversalTime() <= DateTime.UtcNow))
        {
            cardAnimations.Play("cardshow1");
            CancelInvoke("activationTimer");
            card.status = 1;
            if (card.minute != 0)
            {

                print(card.cardType == "Overall" && card.segment != 0 && card.segment != 2);

                if (card.cardType == "Overall" && card.segment != 0 && card.segment != 2)
                    event_label.text = card.minute.ToString() + "' " + LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
                else if (card.cardType == "Overall" && (card.segment == 0 || card.segment == 2))
                    event_label.text = LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
                else if (card.cardType != "PresetInstant" || card.status != 0)
                    event_label.text = card.minute.ToString() + "' " + LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
                else
                    event_label.text = LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]) + " " + LocalizationManager.GetTermTranslation("activated_at", true) + " " + card.minute.ToString() + "' ";
            }
            else {
                event_label.text = LocalizationManager.FixRTL_IfNeeded(card.title[getUsedLang(card.title)]);
            }
            //NGUITools.SetActive (activationObj, false); 
            if (MatchCtrl.timecounting)
                StartInstantCountDown();

        }


    }

    public void StartInstantCountDown()
    {
        NGUITools.SetActive(fill.transform.parent.gameObject, true);
        fill.fillAmount = 1;
        pointslabel.text = points.ToString();
        InvokeRepeating("cardTimer", 1f, 1f);
    }

    public void ContinueInstantCountDown(int sec_diff)
    {
        points = (float)(points_step * sec_diff) + card.endPoints;

        NGUITools.SetActive(fill.transform.parent.gameObject, true);

        fill.fillAmount = ((float)fill_step * sec_diff);

        pointslabel.text = Mathf.FloorToInt(points).ToString();

        InvokeRepeating("cardTimer", 1f, 1f);



        //		print ("TERMINATION TIME: "+card.terminationTime);
        //print ("TERMINATION TIME: "+card.terminationTime);
    }

    void cardTimer()
    {
        if (!MatchCtrl.timecounting && card.cardType != "Overall")
        {
            print("CARD TYPE " + card.cardType);
            // pointslabel.text = LocalizationManager.FixRTL_IfNeeded (LocalizationManager.GetTermTranslation ("paused"));
            fill.fillAmount = 0;
            return;
        }

        DateTime terminationTime = DateTime.Parse(card.terminationTime).ToUniversalTime();
        int minutes_remaining = (terminationTime - DateTime.UtcNow.ToUniversalTime()).Minutes;
        int seconds_remaining = (terminationTime - DateTime.UtcNow.ToUniversalTime()).Seconds;
        seconds_remaining = seconds_remaining + minutes_remaining * 60;


        points = (float)(points_step * seconds_remaining) + card.endPoints;
        fill.fillAmount = fill.fillAmount - (float)fill_step;
        // 
        pointslabel.text = Math.Round((decimal)points, MidpointRounding.AwayFromZero).ToString();
        //print ("From card timer: " + points);

        if (fill.fillAmount == 0)
        {
            CancelInvoke("cardTimer");
        }
    }

    public void cancelTimer()
    {
        if (cardType == "Instant" || cardType == "PresetInstant")
        {
            CancelInvoke("cardTimer");
        }
    }



    public void FinalizeCard(int awardedPoints)
    {
        CancelInvoke("cardTimer");
        NGUITools.SetActive(fill.transform.parent.gameObject, false);
        pointslabel.text = awardedPoints.ToString();
        card.status = 2;

        if (awardedPoints > 0)
        {
            flag.color = new Color32(71, 211, 32, 255);
        }
        else {
            flag.color = new Color32(208, 2, 27, 255);
        }
        //NGUITools.SetActive (specials_count.parent.gameObject, false);
        //specials_count.parent.gameObject.GetComponent<AnimatedAlpha>().alpha = 0;
        //TweenAlpha.Begin(specials_count.parent.gameObject,0.5f,0.0f);


    }

    public void DoubleTime()
    {
        Hashtable table = new Hashtable();
        if (card.cardType == "overall")
        {
            Debug.Log("double time on overall???");
            return;
        }

        print("Card Status: " + card.status);
        if (MatchCtrl.specials > 0)
        {
            int matchState = matchInfo.liveMatch.matchData.state;
            print("MAtch State: " + matchState);
            //if (card.status == 1  && MatchCtrl.timecounting || card.status == 1 && card.cardType == "Overall" && matchState != 2 || card.cardType == "PresetInstant" && matchState != 2) {
            if (card.status == 1 && matchState != 2 || card.cardType == "PresetInstant" && matchState != 2)
            {

                var temptext = LocalizationManager.GetTermTranslation("_powerUpsLeft", false);
                temptext = string.Format(temptext, MatchCtrl.specials);

                ConfirmationModalController.Confirm(LocalizationManager.GetTermTranslation("_extendTimeConfirm", true), LocalizationManager.FixRTL_IfNeeded(temptext, 0, true), null, null, (option) =>
                {
                    if (option == "Btn1")
                    {
                        // Play the Time Boost animation
                        bannerAnimator.eventPlay("timeBoost");

                        table.Add("doubleTime", true);
                        print(Main.Settings.apis["gamecards"] + "/" + _id + "/users");
                        API.PutOne(Main.Settings.apis["gamecards"] + "/" + _id + "/users", table, OnSpecialTimeUsed);
                    }
                });
            }
            else {
                MessagePanel.Instance.animMessage(LocalizationManager.FixRTL_IfNeeded(LocalizationManager.GetTermTranslation("not_powerup_time")));
            }
        }
        else {

            MessagePanel.Instance.animMessage(LocalizationManager.FixRTL_IfNeeded(LocalizationManager.GetTermTranslation("no_more_powerups")));
        }

    }

    public void OnSpecialTimeUsed(BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
    {
        if (res.StatusCode != 200 && res.StatusCode != 304)
        {
            Debug.Log(res.StatusCode);
            MessagePanel.Instance.animMessage(res.DataAsText);
            return;
        }

        string json = res.DataAsText.Replace("\"pointsAwarded\":null", "\"pointsAwarded\":0");
        json = json.Replace("\"duration\":null", "\"duration\":0");
        json = json.Replace("\"activationLatency\":null", "\"activationLatency\":0");
        Debug.Log(json);
        LitJson.JsonData data = LitJson.JsonMapper.ToObject(json);

        if (data["error"] == null)
        {
            PlayCard clickedCard = LitJson.JsonMapper.ToObject<PlayCard>(data["userGamecard"].ToJson());

            //	Preset Temporary Fix
            if (clickedCard.cardType == "PresetInstant")
                clickedCard.specials.DoublePoints.status = card.specials.DoublePoints.status; // Incase a DoubleTime PowerUp has already been played

            card = clickedCard;
            CancelInvoke("cardTimer");

            // Change the color of the button
            time_power.isEnabled = false;
            // Make the Fill bar Longer

            MatchCtrl.specials--;
            specials_count.text = MatchCtrl.specials.ToString();

            //MatchCtrl.FirePowerEvent (clickedCard);

            card.specials.DoubleTime.status = 2;

            //print ("Card _id: " + card._id);

            StartCoroutine(delayUpdateCard(card)); // Update card with new stats for playcards presentation before actual servise callup

            //print ("test Print");
            //Reset Cards Grid
            //transform.parent.gameObject.GetComponent<UITable>().Reposition();

            iTween.ScaleTo(cardFiilObj, iTween.Hash("x", 1.37f, "time", 1.0f, "easetype", iTween.EaseType.easeInOutQuad, "islocal", true));
            Init();

        }
        else {
            MessagePanel.Instance.animMessage(data["error"].ToString());
        }
    }

    IEnumerator delayUpdateCard(PlayCard card)
    {
        //print ("CARD TO SEARCH ID: "+ card._id);
        yield return new WaitForSeconds(1.0f);
        //print ("playCards cOUND "+ matchInfo.liveMatch.playedCards.Count);
        for (int i = 0; i < matchInfo.liveMatch.playedCards.Count; i++)
        {

            //print ("playCards "+i+" " + matchInfo.liveMatch.playedCards [i].id);
            if (matchInfo.liveMatch.playedCards[i].id == card._id || matchInfo.liveMatch.playedCards[i]._id == card._id)
            {
                matchInfo.liveMatch.playedCards[i] = card;
            }
        }
    }

    // DOUBLE POINTS SETUP
    public void DoublePoints()
    {
        //print ("Card Status: " + card.status);
        //print ("card.cardType: " + card.cardType);
        int matchState = matchInfo.liveMatch.matchData.state;
        if (MatchCtrl.specials > 0)
        {
            if (card.status == 1 && matchState != 2 || card.cardType == "PresetInstant" && matchState != 2)
            {
                var temptext = LocalizationManager.GetTermTranslation("_powerUpsLeft", false);
                temptext = string.Format(temptext, MatchCtrl.specials);
                ConfirmationModalController.Confirm(LocalizationManager.GetTermTranslation("_doublePointsConfirm", true, 45), LocalizationManager.FixRTL_IfNeeded(temptext, 0, true), null, null, (option) =>
                {
                    if (option == "Btn1")
                    {
                        Hashtable doublePoints = new Hashtable();
                        doublePoints.Add("doublePoints", true);


                        API.PutOne(Main.Settings.apis["gamecards"] + "/" + _id + "/users", doublePoints, OnSpecialPointsUsed);

                    }
                });
            }
            else {
                MessagePanel.Instance.animMessage(LocalizationManager.FixRTL_IfNeeded(LocalizationManager.GetTermTranslation("not_powerup_time")));
            }

        }
        else {
            MessagePanel.Instance.animMessage(LocalizationManager.FixRTL_IfNeeded(LocalizationManager.GetTermTranslation("no_more_powerups")));
        }

    }

    public void OnSpecialPointsUsed(BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
    {
        if (res.StatusCode != 200 && res.StatusCode != 304)
        {
            MessagePanel.Instance.animMessage(res.DataAsText);
            return;
        }

        //print ("Double Points responce");
        //print (res.DataAsText);



        string json = res.DataAsText.Replace("\"pointsAwarded\":null", "\"pointsAwarded\":0");
        json = json.Replace("\"duration\":null", "\"duration\":0");
        json = json.Replace("\"activationLatency\":null", "\"activationLatency\":0");
        Debug.Log(json);
        LitJson.JsonData data = LitJson.JsonMapper.ToObject(json);

        if (data["error"] == null)
        {
            PlayCard clickedCard = LitJson.JsonMapper.ToObject<PlayCard>(data["userGamecard"].ToJson());

            //	Preset Temporary Fix
            if (clickedCard.cardType == "PresetInstant")
                clickedCard.specials.DoubleTime.status = card.specials.DoubleTime.status; // Incase a DoubleTime PowerUp has already been played

            card = clickedCard;

            MatchCtrl.specials--;
            specials_count.text = MatchCtrl.specials.ToString();
            print("Specials Count :" + MatchCtrl.specials);
            print("Specials Text:" + specials_count.text);

            //Start Double Points Count Down
            if (card.cardType != "PresetInstant")
            {
                InvokeRepeating("countPointsActiv", 0f, 1f);
            }
            else {
                stopDPCounter = true;
                //card.startPoints = card.startPoints * 2;
                //pointslabel.text = card.startPoints.ToString();
                card.specials.DoublePoints.status = 2;
                point_spread = card.startPoints - card.endPoints;
                points_step = (float)point_spread / (card.duration / 1000);
                //print (double_label.text);
                points_power.isEnabled = false;
                bannerAnimator.eventPlay("pointsBoost");

            }

            StartCoroutine(delayUpdateCard(card));
            //MatchCtrl.FirePowerEvent (clickedCard);
            //Reset Cards Grid
            //transform.parent.gameObject.GetComponent<UITable>().Reposition();
            //Init ();
        }
        else {
            MessagePanel.Instance.animMessage(data["error"].ToString());
        }
    }

    bool stopDPCounter = false;
    public void cancelDoublePointsCounter()
    {
        stopDPCounter = true;
        CancelInvoke("countPointsActiv");
        double_label.text = "x2";
        double_fill.fillAmount = 0;

        //	print ("Cancel countPointsActiv");
    }

    public void countPointsActiv()
    {
        //	print ("Special Activation: " + card.specials.DoublePoints.activationTime);
        //		print ("Special creation: " + card.specials.DoublePoints.creationTime);
        int fullTime = (int)(DateTime.Parse(card.specials.DoublePoints.activationTime).ToUniversalTime() - DateTime.Parse(card.specials.DoublePoints.creationTime).ToUniversalTime()).TotalSeconds;
        //print ("FullTime: " + fullTime);
        int timeNum = ((DateTime.Parse(card.specials.DoublePoints.creationTime).ToUniversalTime().AddSeconds(card.specials.DoublePoints.activationLatency / 1000).ToUniversalTime() - DateTime.UtcNow.ToUniversalTime()).Seconds);
        //print (((float)timeNum/1000)*(float)startNum);
        double_label.text = timeNum + "'";
        //print ("timeNum: " + timeNum);
        double_fill.fillAmount = (float)timeNum / (float)fullTime;
        //doubleCounting = true;
        if (stopDPCounter)
        {
            //	print ("HARD CLOSE");
            CancelInvoke("countPointsActiv");
            card.specials.DoublePoints.status = 0;
            double_label.text = "x2";
            double_fill.fillAmount = 0;
            stopDPCounter = false;
        }

        //When timer ends do this
        if (timeNum <= 0)
        {
            //			print ("STOP COUNTER IS " + stopDPCounter);
            card.specials.DoublePoints.status = 2;
            CancelInvoke("countPointsActiv");
            //			print ("Double Points Counter Finished");
            FinalizeDoublePointsOperation();
        }


    }

    private void FinalizeDoublePointsOperation()
    {
        print("Double Points Counter Finilized");
        if (transform.parent.name != "Cards_table")
        { // Do this only for the one

            bannerAnimator.eventPlay("pointsBoost");

            card.startPoints = card.startPoints * 2;
            card.endPoints = card.endPoints * 2;
            point_spread = card.startPoints - card.endPoints;
            points_step = (float)point_spread / (card.duration / 1000);
            //points = points * 2;
            //print()
            //pointslabel.text = card.startPoints.ToString();

            for (int i = 0; i < matchInfo.liveMatch.playedCards.Count; i++)
            { //Update the card

                //print ("playCards "+i+" " + matchInfo.liveMatch.playedCards [i].id);
                if (matchInfo.liveMatch.playedCards[i].id == card._id || matchInfo.liveMatch.playedCards[i]._id == card._id)
                {
                    matchInfo.liveMatch.playedCards[i] = card;
                }
            }

        }

        matchInfo.updateCardInTables(card);

        //Init ();

        double_label.text = "x2";

        //MatchCtrl.FireDoublePointsEvent(card);

        if (card.cardType == "Overall")
        {
            points_power.isEnabled = false;
            time_power.GetComponent<UISprite>().color = points_power.disabledColor;
        }
        else {
            points_power.isEnabled = false;

        }


        //pointslabel.text = card.startPoints.ToString();
        //card.endPoints = card.endPoints * 2;

        //		Debug.Log(card._id);


        //		for (int i = 0; i <  liveMatch.playedCards.Count(); i++)
        //		{
        //			if (liveMatch.playedCards[i].id == card._id)
        //			{
        //				liveMatch.playedCards[i] = card;
        //			}
        //
        //		}

        if (!MatchCtrl.timecounting && card.cardType != "Overall")
        {
            print("CARD TYPE " + card.cardType);
            // pointslabel.text = LocalizationManager.FixRTL_IfNeeded (LocalizationManager.GetTermTranslation ("paused"));
        }
    }





    //	void OnEnable()
    //	{
    //		// Fix weird ngui position bug
    //		Vector3 localPos = transform.localPosition;
    //		localPos.x = 0;
    //		transform.localPosition = localPos;
    //	}
    //
    //	void OnDisable()
    //	{
    //		// Fix weird ngui position bug
    //		Vector3 localPos = transform.localPosition;
    //		localPos.x = 0;
    //		transform.localPosition = localPos;
    //	}

    public void toggleInfo(UILabel cardBName)
    {

        if (!statsOpen)
        {
            print("You opened a " + cardBName.text + " card!");
            NGUITools.SetActive(cardStats, true);
            statsOpen = true;
        }
        else {
            NGUITools.SetActive(cardStats, false);
            statsOpen = false;
        }

        transform.parent.GetComponent<UITable>().Reposition();
    }

    public void closeCardStats()
    {
        if (statsOpen)
        {
            NGUITools.SetActive(cardStats, false);
            statsOpen = false;
        }
    }

    string getUsedLang(Dictionary<string, string> dictionary)
    {
        string usedLang;

        if (dictionary.ContainsKey(LocalizationManager.CurrentLanguageCode))
            usedLang = LocalizationManager.CurrentLanguageCode;
        else
            usedLang = "en";

        return usedLang;
    }

    public void DoNothing()
    {

    }
}
