﻿using UnityEngine;
using System.Collections;
using I2.Loc;

public class MessagePanel : MonoBehaviour {

	private static MessagePanel _instance;
	public static MessagePanel Instance{
		get{
			if(_instance == null)
				_instance = GameObject.FindObjectOfType(typeof(MessagePanel)) as MessagePanel;
			return _instance;
		}
	}

	public UILabel messTitle;
	public UILabel messText;
	public UISprite messIcon;
	public Collider backCol;
	public Animation messageAnim;

//	void Start() {
//		print ("From message Pane: "+Screen.width);
//		//transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0,206,0));
////		Vector3 v3T = transform.localPosition; 
////		float offset = Screen.width + 100;
////		v3T.x = offset;
////		transform.localPosition = v3T;
//	}

	public void Dismiss(){
		if(reloadOnDismiss == 1){
			reloadOnDismiss = 1;
		}

		backCol.enabled = false;

		playAnim (1, -1.3f, "messageAnim");
		CancelInvoke ("timedClose");
	}

	public void animMessage (string mes, string title = "", string icon = "sportimo-Logo", float time = 5, float delay = 0) {



		messTitle.text = title;
		messText.text = mes;

		if(string.IsNullOrEmpty(icon)){
			messIcon.spriteName = "sportimo-Logo";
		}
		else{
			messIcon.spriteName = icon;
		}


		Invoke ("delayStart", delay);
		Invoke("timedClose", delay+time);

	}

	void delayStart() {

		playAnim (0, 1, "messageAnim");
		backCol.enabled = true;
	}

	void playAnim (float time, float speed, string animationName)
	{
		messageAnim [animationName].time = time;
		messageAnim [animationName].speed = speed;
		messageAnim.Play(animationName);
	}

	void timedClose() {
		playAnim (1, -1.3f, "messageAnim");
		backCol.enabled = false;
	}

	private int reloadOnDismiss = 0;

//	public void animMessageReload (string mes,string title ="warning",string icon = "notice") {
//		messTitle.text=title;
//		messText.text=mes;
//		if(string.IsNullOrEmpty(icon)){
//			messIcon.spriteName="notice";
//		}
//		else{
//			messIcon.spriteName=icon;
//		}
//		iTween.MoveTo(this.gameObject ,iTween.Hash("y", 250,"time",0.5, "easetype","easeOutBack", "delay", 0.0f, "islocal",true));
//		reloadOnDismiss = 1;
//	}



}
