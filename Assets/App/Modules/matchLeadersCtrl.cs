﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using I2.Loc;
using BedbugRest;
using LitJson;
using Facebook.Unity;
using System;

public class matchLeadersCtrl : MonoBehaviour
{

    public GameObject[] Lines = new GameObject[5];
    public int lineToOpen;
    public Pool pool;



    public UIGrid leaders_grid;

    public GameObject leader_user;
    public GameObject user_dark;
    public GameObject user_light;
    public GameObject user;

    public GameObject InvButton;
    private string leaderboard_endpoint = "/leaderboards/";

    public Animation cardplayAnim;
    public bool cardPlayOpen = false;
    public bool friends = false;

    void Start()
    {
        for (int i = 0; i < Lines.Length; i++)
        {
            NGUITools.SetActive(Lines[i], false);
        }

        //if(lineToOpen !=0)
        NGUITools.SetActive(Lines[lineToOpen], true);
    }

    public void openline(int num)
    {
        for (int i = 0; i < Lines.Length; i++)
        {
            NGUITools.SetActive(Lines[i], false);
        }
        //s		print ("Opening Line " + num);
        NGUITools.SetActive(Lines[num], true);
    }

    public void tabSelect(GameObject lineParent)
    {

        // CLOSE ALL LINE AND OPEN THE PRESSED ONE
        for (int i = 0; i < Lines.Length; i++)
        {
            NGUITools.SetActive(Lines[i], false);
        }

        NGUITools.SetActive(lineParent.transform.FindChild("line").gameObject, true);

        // REDRAW THE SCROLLVIEW ACCORDING TO LINEPARENT
    }

    public void openMatchLeaders()
    {
        friends = false;
        foreach (Transform trans in leaders_grid.GetChildList())
        {
            if (trans.gameObject.activeSelf)
            {
                NGUITools.Destroy(trans.gameObject);
            }
        }
		openline (0);
        API.GetAll(Main.Settings.apis["leaderpay"] + leaderboard_endpoint + Main.AppUser._id + "/" + "GR" + "/match/" + MatchCtrl.matchid, OnGetMatchLeaders);
        Loader.Visible(true);

        //}

    }

    public void OpenFriendsMatchLeaders()
    {
        friends = true;
		openline (1);
        foreach (Transform trans in leaders_grid.GetChildList())
        {
            if (trans.gameObject.activeSelf)
            {
                NGUITools.Destroy(trans.gameObject);
            }
        }
        if (FB.IsLoggedIn)
        {
            Loader.Visible(true);
            FB.API("me?fields=friends", HttpMethod.GET, OnGetFriends);
        }
        else if (string.IsNullOrEmpty(PlayerPrefs.GetString("social_id")) && string.IsNullOrEmpty(Main.AppUser.social_id))
        {
            FBModal.Instance.PopModal(PopFBModal);
        }
        else {
            FacebookConnect();
        }
    }

    public void PopFBModal(string answer)
    {
        if (answer == "yes")
        {
            Loader.Visible(true);
            FacebookConnect();
        }
        else {
			openline (0);
            openMatchLeaders();
            return;
        }
    }

    public void FacebookConnect()
    {
        if (!FB.IsInitialized)
            FB.Init(FBLogin);
        else {
            FBLogin();
        }
    }
    public void FBLogin()
    {
        if (FB.IsLoggedIn)
        {
            if (!string.IsNullOrEmpty(Main.AppUser.social_id))
            {
                MessagePanel.Instance.animMessage("Facebook account already connected.");
                return;
            }

            FB.API("me?fields=id,name,email", HttpMethod.GET, OnGetData);
        }
        else {
            FB.LogInWithReadPermissions(new List<string>() { "email,user_friends" }, OnFBLogin);
        }
    }

    public void OnFBLogin(ILoginResult fbresult)
    {
        if (fbresult.Error != null)
        {
            Debug.Log(fbresult.Error);
            Loader.Visible(false);
        }
        else {
            Debug.Log(fbresult.RawResult);
            JsonData data = JsonMapper.ToObject(fbresult.RawResult);
            if (data.Keys.Contains("user_id"))
            {
                Debug.Log(data["user_id"].ToString());
                PlayerPrefs.SetString("social_id", AccessToken.CurrentAccessToken.UserId);
                Hashtable fbtable = new Hashtable();
                fbtable.Add("social_id", data["user_id"].ToString());
                API.PutOne(Main.Settings.apis["users"] + "/" + Main.AppUser._id, fbtable, OnConnectCallback, Main.AppUser.token);
            }
        }
    }

    public void OnGetData(IGraphResult result)
    {
        if (result.Error != null)
        {
            Debug.Log(result.Error);
            Loader.Visible(false);
        }
        else {
            JsonData data = JsonMapper.ToObject(result.RawResult);
            Hashtable fbtable = new Hashtable();
            fbtable.Add("social_id", data["id"].ToString());
            API.PutOne(Main.Settings.apis["users"] + "/" + Main.AppUser._id, fbtable, OnConnectCallback, Main.AppUser.token);
        }
    }

    public void OnConnectCallback(BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
    {
        Debug.Log(res.DataAsText);
        if (res.StatusCode == 200)
        {
            MessagePanel.Instance.animMessage("Facebook connected successfully");
            PlayerPrefs.SetString("social_id", AccessToken.CurrentAccessToken.UserId);
            FB.API("me?fields=friends", HttpMethod.GET, OnGetFriends);
            Main.AppUser.social_id = AccessToken.CurrentAccessToken.UserId;

        }
        else {
            FB.LogOut();
            MessagePanel.Instance.animMessage("Facebook id already connected to other account");
        }

    }

    private void OnGetFriends(IGraphResult result)
    {
        List<string> friends = new List<string>();
        Debug.Log(result.RawResult);
        JsonData data = JsonMapper.ToObject(result.RawResult);

        if (data.Keys.Contains("friends"))
            if ((int)data["friends"]["summary"]["total_count"] > 0)
            {
                if (data["friends"]["data"].Count > 0)
                {
                    if (data["friends"]["data"][0].Count > 0)
                    {
                        Debug.Log(data["friends"]["data"].ToJson());
                        List<Friend> fbdata = JsonMapper.ToObject<List<Friend>>(data["friends"]["data"].ToJson());
                        foreach (Friend friend in fbdata)
                        {
                            friends.Add(friend.id);
                        }
                    }
                    friends.Add(Main.AppUser.social_id);
                    Hashtable table = new Hashtable();
                    table.Add("friends", friends);
                    API.PostOne(Main.Settings.apis["leaderpay"] + "/leaderboards/" + Main.AppUser._id + "/friends/match/" + MatchCtrl.matchid, table, OnGetMatchLeaders);
                    return;
                }
            }

        Loader.Visible(false);
    }

    public void InviteFriends()
    {
        FB.Mobile.AppInvite(new Uri("https://fb.me/1762114710698244"), null, OnInviteFinished);
    }

    public void OnInviteFinished(IAppInviteResult result)
    {
        Debug.Log(result.RawResult);
    }
    public void OnGetMatchLeaders(BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
    {
        Loader.Visible(false);
        Debug.Log(res.DataAsText);
        if (res.StatusCode != 200 && res.StatusCode != 304)
        {
            MessagePanel.Instance.animMessage(res.DataAsText);
            return;
        }
        Leaderboard Leaders = LitJson.JsonMapper.ToObject<Leaderboard>(res.DataAsText);

        BedBugTools.loadImageOnUITextureAndResize(user.transform.GetComponentInChildren<UITexture>(), Leaders.user.pic, 64, 64);
        //		Debug.Log (Leaders.user.level);
        user.transform.FindChild("Level_widget").GetComponentInChildren<level_widget_controller>().level = (float)Leaders.user.level;

        if ((int)Leaders.user.rank > 100)
            user.transform.FindChild("index").GetComponentInChildren<UILabel>().text = LocalizationManager.GetTermTranslation("_leaderboard_you", true);
        else
            user.transform.FindChild("index").GetComponentInChildren<UILabel>().text = Leaders.user.rank.ToString();

        user.transform.FindChild("points").GetComponentInChildren<UILabel>().text = Leaders.user.score.ToString();
        user.transform.FindChild("name").GetComponentInChildren<UILabel>().text = Leaders.user.name;
        for (int i = 0; i < Leaders.leaderboad.Count; i++)
        {

            if (i > 99) continue;

            GameObject leader;
            if (i == 0)
            {
                leader = NGUITools.AddChild(leaders_grid.gameObject, leader_user);
            }
            else if (i % 2 == 0)
            {
                leader = NGUITools.AddChild(leaders_grid.gameObject, user_light);
            }
            else {
                leader = NGUITools.AddChild(leaders_grid.gameObject, user_dark);
            }
            leader.name = Leaders.leaderboad[i]._id;
            NGUITools.SetActive(leader, true);
            if (Leaders.leaderboad[i].pic != null)
                BedBugTools.loadImageOnUITextureAndResize(leader.transform.GetComponentInChildren<UITexture>(), Leaders.leaderboad[i].pic, 64, 64);

            leader.transform.FindChild("block").GetComponentInChildren<UISprite>().alpha = Main.AppUser.blockedusers.Contains(Leaders.leaderboad[i]._id) ? 1 : 0;

            leader.transform.FindChild("index").GetComponentInChildren<UILabel>().text = (i + 1).ToString();
            leader.transform.FindChild("points").GetComponentInChildren<UILabel>().text = Leaders.leaderboad[i].score.ToString();
            leader.transform.FindChild("name").GetComponentInChildren<UILabel>().text = Leaders.leaderboad[i].name;
        }

        leaders_grid.Reposition();
        leaders_grid.transform.parent.gameObject.GetComponent<UIScrollView>().ResetPosition();
        Loader.Visible(false);
        if (friends)
            NGUITools.SetActive(InvButton, true);
        else {
            NGUITools.SetActive(InvButton, false);
        }
    }



    public void closeLeaders()
    {
        foreach (Transform trans in leaders_grid.GetChildList())
        {
            if (trans.gameObject.activeSelf)
            {
                NGUITools.Destroy(trans.gameObject);
            }
        }

    }

    private class Friend
    {
        public string id;
        public string name;
    }

}
