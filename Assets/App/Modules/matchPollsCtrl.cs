﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using BedbugRest;
using System.Linq;
using I2.Loc;

public class matchPollsCtrl: MonoBehaviour
{ 
	List<Poll> Polls = new List<Poll> ();

	private string polls_endpoint = "/polls/";
	public UITable polls_grid;
	public GameObject pollPrefab;

	public void openMatchPolls ()
	{
			foreach (Transform trans in polls_grid.GetChildList()) {
			if (trans.gameObject.activeSelf) {
				NGUITools.Destroy (trans.gameObject);
			}
		}
		//openline (0);
//		string call = Main.Settings.apis ["polls"]+"/" + MatchCtrl.matchid + "/tag/" + Main.AppUser._id + "/user/";
//		print (call);
		API.GetAll (Main.Settings.apis ["polls"]+ "/" + MatchCtrl.matchid+"/tag/"+Main.AppUser._id + "/user/", OnGetMatchPolls);
		Loader.Visible (true);

		//}
		
	}

	
	public void OnGetMatchPolls (BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
	{


		if(res.StatusCode != 200 && res.StatusCode!=304){
			MessagePanel.Instance.animMessage(res.DataAsText);
			return;
		}

		Loader.Visible(false);
		Debug.Log (res.DataAsText);

		Polls = LitJson.JsonMapper.ToObject<List<Poll>> (res.DataAsText);

		if (Polls.Count > 0) {
			foreach(Poll poll in Polls) {
				GameObject pollObj = NGUITools.AddChild (polls_grid.gameObject, pollPrefab);
				NGUITools.SetActive (pollObj, true);

				// Get pollCtl component
				PollCtrl pollCtrl = pollObj.GetComponent<PollCtrl>();
				// Run setUpPoll in pollCtrl
				pollCtrl.updatePoll(poll);
			}
		} else {
			print("There are no Polls yet");
		}
		polls_grid.Reposition ();
		polls_grid.transform.parent.gameObject.GetComponent<UIScrollView> ().ResetPosition ();
		Loader.Visible (false);
	}


}
