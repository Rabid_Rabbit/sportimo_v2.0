﻿using UnityEngine;
using System.Collections;

public class eventMatchCtrl : MonoBehaviour {

	// Use this for initialization
	public string id;
	public UILabel descLabel;

	void Start() {
		// Fix weird ngui position bug
				Vector3 localPos = transform.localPosition;
				localPos.x = 0;
				transform.localPosition = localPos;
	}

	public void labelCorrection() {
		if(descLabel.text.Length > 0)
			GetComponent<Animation>().Stop("eventAppear");
	}
}
