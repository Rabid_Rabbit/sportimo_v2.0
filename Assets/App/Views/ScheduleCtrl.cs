﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using BedbugRest;
using System.Linq;
using I2.Loc;

public class ScheduleCtrl : View
{
    
 
	List<ScheduledMatch> Schedule = new List<ScheduledMatch> ();


	List<DateTime> Dates = new List<DateTime> ();
	int todayDatesCount;
	private string ScheduleEndpoint = "/schedule/country/";
	public UITable scheduleGrid;
	public bool populated;
    
	//Prefabs
	public GameObject Match;
	public GameObject divider;
	GameObject matchGO;
	GameObject div;
	public UILabel dateCenter;
	public UILabel datelabelL1;
	public UILabel datelabelL2;
	public UILabel datelabelL3;
	public UILabel datelabelR1;
	public UILabel datelabelR2;
	public UILabel datelabelR3;
	public UICenterOnChild matchesCenter;
	public UIPanel matchesScrollPanel;
	public Transform matchesGrid;

	public UIScrollView matchesScroll;

	private bool isPopulating;

	bool firstDivCentered = false;
	bool todayCentered = false;
	Transform toCenter;
	UIPopupList competitions;

	// New Additions
	public UIToggle liveToggle;
	public UIToggle upcomingToggle;
	public GameObject liveTabBtn;
	public UIGrid tabsGrid;
	List<ScheduledMatch> subGroupSchedule = new List<ScheduledMatch> ();

    [Space(10)]
    // UniDropDown Widget
    public UniDropDown _UniDropDown;


    enum TimeGroup {
		old,
		live,
		upcoming
	}
	private TimeGroup selectedTimeGroup = TimeGroup.live;

	public void selectTimeGroup(string selectedGo){
		selectedTimeGroup = (TimeGroup)int.Parse(selectedGo);

//		ClearGrid ();

		// Now just populate the scrollview with data
		StartCoroutine (PopulateScheduleList ());
	}

    //void OnTransitionFinishedMethod(){ GetStandings()}

    public void onTransitionInStart(viewData _viewdata = null)
    {
        _UniDropDown.OnSelect += selectionChanged;

        //        if(!populated){
        //			ScheduleEndpoint += Main.AppUser.country;
        //		GetSchedule();
        //		}
        //		else{
        //		print ("Standings On TRansition STarts!!");
        //Coach.coachMessage ("Drag upwards for avatar selection", 27f, 5f, 1.5f, false, "tipAvatar");

        //Close uniPopSelect
        //TweenAlpha.Begin(uniPopSelect, 0.0f, 0);



        GetSchedule();
        //		}
        //InvokeRepeating("GetSchedule",60,60);
    }

    public override void selectionChanged (string selection)
	{
		base.selectionChanged (selection);

        //		ClearGrid ();

        print(selection + " " + LocalizationManager.GetTermTranslation("all_games",false));

		if (selection != LocalizationManager.GetTermTranslation("all_games", false))
			subGroupSchedule = Schedule.Where (x => x.competition.name [LocalizationManager.CurrentLanguageCode] == selection).ToList () as List<ScheduledMatch>;
		else
			subGroupSchedule = Schedule;

		// Sorted by scheduled time descending
		subGroupSchedule = subGroupSchedule.OrderByDescending (match => match.start).ToList (); 

		// Set Live Tab visibility based on live matches existance
		bool hasLive = subGroupSchedule.Count(p => p.state > 0 && p.completed == false)>0;
		liveTabBtn.GetComponent<Collider>().enabled = hasLive;
		if (hasLive) {
			selectedTimeGroup = TimeGroup.live;
			liveToggle.Set (true, false);
			liveTabBtn.SetActive(true);
			liveTabBtn.GetComponent<UISprite>().width = 96;
//			liveTabBtn.GetComponent<UIButton>().ResetDefaultColor();
		} else {
			selectedTimeGroup = TimeGroup.upcoming;
			upcomingToggle.Set (true, false);
			liveTabBtn.GetComponent<UISprite>().width = 0;
			liveTabBtn.SetActive(false);
//			liveTabBtn.GetComponent<UIButton> ().defaultColor = liveTabBtn.GetComponent<UIButton> ().disabledColor;
		}
		liveTabBtn.GetComponent<UIButton> ().UpdateColor (true);

		// Now just populate the scrollview with data
		StartCoroutine (PopulateScheduleList ());

	}

	bool ClearGrid ()
	{
		// Reset scrollview to top left corner
		matchesScroll.ResetPosition ();

		foreach (Transform transform in scheduleGrid.GetChildList()) {	
			NGUITools.Destroy (transform);
		}
		return true;
	}

	IEnumerator PopulateScheduleList ()
	{
//		if (isPopulating) {
//			MessagePanel.Instance.animMessage ( "List is still populating", "Be patient", "sportimo-Logo", 3);
//			yield break;
//		}
//		
//		isPopulating = true;

		TimeGroup ThisTimeGroup = selectedTimeGroup;

		matchesScroll.ResetPosition ();
		yield return ClearGrid ();

		// Create the filtered group
		List<ScheduledMatch> filteredScheduleBasedOnTimeGroup = new List<ScheduledMatch>();

		switch (selectedTimeGroup) {
		case TimeGroup.live:
			filteredScheduleBasedOnTimeGroup = subGroupSchedule.Where(p => (p.state > 0 && p.completed == false)).ToList();
			break;
		case TimeGroup.old:
			filteredScheduleBasedOnTimeGroup = subGroupSchedule.Where(p => (p.completed == true)).ToList();
			break;
		case TimeGroup.upcoming:
			filteredScheduleBasedOnTimeGroup = subGroupSchedule.Where(p => (p.state == 0)).ToList();
			break;
		}

		int names = 0;
		float placeHeight = 0;
		float topY = 0;
		
		//Clear old dates
		Dates.Clear ();
		todayDatesCount = 0;
		bool hasToday = false;
		string lastDate = "";
		todayCentered = false;
		firstDivCentered = false;

		DateTime current = new DateTime (1999, 1, 1);

		// This is for stoping the animation for every prefab over 10 count
		int matchesCount = 0;

		foreach (ScheduledMatch _scheduledMatch in filteredScheduleBasedOnTimeGroup) {

			// We create a divider where necessary
			if(ThisTimeGroup != selectedTimeGroup){
				Debug.Log("Time Group Changed");
				yield break;
			}

			if (_scheduledMatch.start.Day != current.Day || matchesCount == 0)
			{			
				GameObject div = NGUITools.AddChild (scheduleGrid.gameObject, divider);
				NGUITools.SetActive (div, true);
					div.name = _scheduledMatch.start.ToString ("dd/MM");
					div.GetComponentInChildren<UILabel> ().text = _scheduledMatch.start.ToString ("dd/MM");
				current = _scheduledMatch.start;
				names++;
				
				// Placement Animation
				div.transform.localPosition = new Vector3 (0, placeHeight, 0);
				topY = div.transform.localPosition.y;
				topY -= 0;
				placeHeight -= 45;
				
				if(matchesCount < 5)
					iTween.MoveFrom (div, iTween.Hash ("y", topY, "x", 350, "time", 0.4f, "easetype", iTween.EaseType.easeOutCirc, "islocal", true)); 

				matchesScroll.ResetPosition ();
			}
			
			// We create the scheduled match info object
			{
				GameObject matchGO = NGUITools.AddChild (scheduleGrid.gameObject, Match);
				matchGO.transform.localPosition = new Vector2 (0, matchGO.transform.localPosition.y);
				matchGO.name = (matchesCount + names).ToString ();
				
				NGUITools.SetActive (matchGO, true);
				
				scrollMatchCtrl match_controller =  matchGO.GetComponent<scrollMatchCtrl> ();
					match_controller.match_id = _scheduledMatch._id;
				
					if(_scheduledMatch.competition.graphics != null){
						if(_scheduledMatch.competition.graphics.ContainsKey("banner_strip")){
						BedBugTools.loadImageOnUITextureAndResize (match_controller.competition_banner, _scheduledMatch.competition.graphics["banner_strip"], 450,38, FadeInOnLoad, 0 );
					}}
				if(_scheduledMatch.home_team == null)
					Debug.Log(_scheduledMatch._id);
				// BedbugTools create the coroutine no need to start a coroutine here and risk this gameobject not being active 
					BedBugTools.loadImageOnUITextureAndResize (matchGO.transform.FindChild ("Team1").GetComponentInChildren<UITexture>(), _scheduledMatch.home_team.logo, 64,64, FadeInOnLoad, 0 );
					BedBugTools.loadImageOnUITextureAndResize (matchGO.transform.FindChild ("Team2").GetComponentInChildren<UITexture>(), _scheduledMatch.away_team.logo, 64,64, FadeInOnLoad, 0 );
				
				UILabel[] matchinfo = matchGO.transform.FindChild ("centerMatchScore").GetComponentsInChildren<UILabel> ();
					matchinfo [0].text = _scheduledMatch.home_score + "-" + _scheduledMatch.away_score;
				if (_scheduledMatch.state==0) {
					NGUITools.SetActive(matchinfo[0].gameObject,false);
					NGUITools.SetActive(matchinfo[1].gameObject,false);
						matchinfo [2].text = _scheduledMatch.start.ToString ("HH:mm");
					} else if(_scheduledMatch.state == 2) {
					//NGUITools.SetActive(matchinfo[0].gameObject,false);
					//NGUITools.SetActive(matchinfo[1].gameObject,false);
					matchinfo [1].text = "HT";
				}
				else if(_scheduledMatch.state == 4){
					//NGUITools.SetActive(matchinfo[0].gameObject,false);
					//NGUITools.SetActive(matchinfo[1].gameObject,false);
					matchinfo [1].text = "FT";
				}
				else if(_scheduledMatch.completed){
					matchinfo [1].text = "FT";
					
				}
				else{
						matchinfo [1].text = _scheduledMatch.time.ToString () + "'";
				}
				
				matchGO.transform.FindChild ("nameteam1").GetComponentInChildren<UILabel> ().text = LocalizationManager.FixRTL_IfNeeded(BedBugTools.GetStringByOrder(new List<string>(){"short",LocalizationManager.CurrentLanguageCode,"en"},_scheduledMatch.home_team.name)); 
				matchGO.transform.FindChild ("nameteam2").GetComponentInChildren<UILabel> ().text = LocalizationManager.FixRTL_IfNeeded(BedBugTools.GetStringByOrder(new List<string>(){"short",LocalizationManager.CurrentLanguageCode,"en"},_scheduledMatch.away_team.name)); 
				
				// Match Placement Animation
				matchGO.transform.localPosition = new Vector3 (0, placeHeight, 0);
				topY = matchGO.transform.localPosition.y;
				topY -= 90;
				placeHeight -= 65;
				
				if(matchesCount < 5)
					iTween.MoveFrom (matchGO, iTween.Hash ("y", topY, "time", 0.5f, "easetype", iTween.EaseType.easeOutCirc, "islocal", true)); 
				//iTween.RotateFrom(matchGO, iTween.Hash("z", 15, "time",0.3f, "easetype", "delay", 0.2, iTween.EaseType.easeOutCirc,  "islocal", true)); 
			}

			if(matchesCount < 5)
				yield return new WaitForSeconds (0.1f);
			else
				yield return new WaitForSeconds (0.0f);

			matchesCount++;
		}

		isPopulating = false;
		matchesScroll.ResetPosition ();
	}

	
	private void GetSchedule ()
	{
		Loader.Visible (true);
		API.GetAll (Main.Settings.apis["data"]+ ScheduleEndpoint + Main.AppUser.country, GetScheduleCallback);
	}
	
	public void GetScheduleCallback (BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
	{

		if(res.StatusCode != 200 && res.StatusCode != 304){
			Debug.Log(res.StatusCode);
			MessagePanel.Instance.animMessage(res.DataAsText);
			return;
		}

		Schedule = LitJson.JsonMapper.ToObject<List<ScheduledMatch>> (res.DataAsText);

        var all_string = LocalizationManager.GetTermTranslation("all_games",false);
        List<string> Competitions = Schedule.Select(x => BedBugTools.GetStringByLocPrefs(x.competition.name)).OfType<string>().Distinct().ToList() as List<string>;
        
        Competitions.Insert(0, all_string);

        _UniDropDown.UpdateOptions(Competitions);
        //_UniDropDown.setSelected(all_string);
        selectionChanged(Competitions[0]);
        Loader.Visible (false);
	}


	public int OffsetScrollPosition = 11;
	public IEnumerator ScrollToDateGO (GameObject go, float wait = 0.1f)
	{
		yield return new WaitForSeconds (wait);
	
		UICenterOnChild center = NGUITools.FindInParents<UICenterOnChild> (go);
		UIPanel panel = NGUITools.FindInParents<UIPanel> (go);
		
		if (center != null) {
			if (center.enabled)
				center.CenterOn (go.transform);
			Debug.Log ("Scrolling to center :" + go.name);
		} else 
		if (panel != null && panel.clipping != UIDrawCall.Clipping.None) {
			Debug.Log ("Scrolling to:" + go.name);
			UIScrollView sv = panel.GetComponent<UIScrollView> ();
			Vector3 offset = -panel.cachedTransform.InverseTransformPoint (go.transform.position);
			if (!sv.canMoveHorizontally)
				offset.x = panel.cachedTransform.localPosition.x;
			if (!sv.canMoveVertically){
				offset.y = panel.cachedTransform.localPosition.y + 100L;
			}
			offset.y += OffsetScrollPosition;
			SpringPanel.Begin (panel.cachedGameObject, offset, 6f);
		}
	}

	private IEnumerator CenterTo(Transform target){
			matchesCenter.enabled = true;
			Debug.Log("scrolling to today");
			matchesCenter.CenterOn (target);
			yield return new WaitForSeconds(3.5f);
			matchesCenter.enabled = false;
	}

	private void FadeInOnLoad(UITexture texture){
		if(texture){
		UITweener tween = texture.GetComponent<UITweener> ();

		if (tween)
			tween.PlayForward ();
		else
			texture.alpha = 1;
		}
	}
    
	

	public void onTransitionOutStart ()
	{
		foreach (Transform transform in scheduleGrid.GetChildList()) {
			if (transform.gameObject.activeSelf)
				NGUITools.Destroy (transform);
		}
	}

	void populateDateBtns ()
	{
	
		dateCenter.text = Dates [todayDatesCount].ToString ("dd/MM");

		if (todayDatesCount - 1 >= 0)
			datelabelR1.text = Dates [todayDatesCount - 1].ToString ("dd/MM");
		else
			datelabelR1.text = "-";

		if (todayDatesCount - 2 >= 0)
			datelabelR2.text = Dates [todayDatesCount - 2].ToString ("dd/MM");
		else
			datelabelR2.text = "-";

		if (todayDatesCount - 3 >= 0)
			datelabelR3.text = Dates [todayDatesCount - 3].ToString ("dd/MM");
		else
			datelabelR3.text = "-";

		if (todayDatesCount + 1 < Dates.Count)
			datelabelL1.text = Dates [todayDatesCount + 1].ToString ("dd/MM");
		else
			datelabelL1.text = "-";

		if (todayDatesCount + 2 < Dates.Count)
			datelabelL2.text = Dates [todayDatesCount + 2].ToString ("dd/MM");
		else
			datelabelL2.text = "-";

		if (todayDatesCount + 3 < Dates.Count)
			datelabelL3.text = Dates [todayDatesCount + 3].ToString ("dd/MM");
		else
			datelabelL3.text = "-";

	}

	public void selectDate (GameObject go)
	{

		string dateString = go.GetComponent<UILabel> ().text;
//		print ("Center on: "+dateString);
		//Transform childWithName = matchesGrid.Find (dateString);


		foreach (Transform t in matchesGrid) {
			//print ("Center on: "+ t.name);
//			matchesCenter.enabled = true;

			if (t.name == dateString) {// Do something to child one

//				if (matchesCenter != null) {
//					if (matchesCenter.enabled) {
						StartCoroutine(ScrollToDateGO(t.gameObject,.1f));
				return;
//						matchesCenter.CenterOn (t);
//					}
//				}
			}

//			StartCoroutine(resetCenterScript());

		}

//		if (matchesCenter != null)
//		{
//			if (matchesCenter.enabled){
//				print ("Center on: "+childWithName);
//				matchesCenter.CenterOn(childWithName);
//			}
//		}
	
	}

//	IEnumerator resetCenterScript ()
//	{
//		yield return new WaitForSeconds(1);
//		matchesCenter.enabled = false;
//	}

//	// UniPop Select
//	public GameObject uniPopSelect;
//	public bool useTwoColors = false;
//	private bool useDarc = false;
//	private bool uniPopOpen = false;
//	public UICenterOnChild center;
//	public UIGrid selectTable;
//	public UIScrollView selectScroll;
//	public UIWrapContent wrap; 
//	public GameObject lightPrefab;
//	public GameObject darkPrefab;
//	public GameObject topPrefab;
//	public GameObject bottomPrefab;
//	public UILabel topLabel;
//	GameObject uniTeam;
//	int dbCount = 1; // Count to find the item in double count competition items

//	public void selectThis(UILabel compName){
//		//Close uniPopSelect
//		TweenAlpha.Begin (uniPopSelect, 0.0f, 0);
//		//		NGUITools.SetActive (uniPopSelect, false);
//		uniPopOpen = false;
//		// Update Top Label
//		topLabel.text = compName.text;
//		// Populate with selection
//		selectionChanged (compName.text);
//		//PopulateFavoriteBySelection (teamName.text);
//	}
	
//	public void openUniPop() {
//		// Check if Count = 0 or 1
//		if (competitions.items.Count+1 < 2)
//			return;
		
//		if (!uniPopOpen) {
//			//Open uniPopSelect
//			TweenAlpha.Begin (uniPopSelect, 0.3f, 1);
//			//NGUITools.SetActive (uniPopSelect, true);
//			uniPopOpen = true;
//			selectTable.Reposition ();
//			wrap.SortBasedOnScrollMovement ();
			
////			if(selectedTeam != null)
//				center.CenterOn(selectTable.transform.FindChild(topLabel.text).transform);
//		} else {
//			//Open uniPopSelect
//			TweenAlpha.Begin (uniPopSelect, 0.2f, 0);
//			//			NGUITools.SetActive (uniPopSelect, false);
//			uniPopOpen = false;
//			selectTable.Reposition ();
//			wrap.SortBasedOnScrollMovement ();
//		}
//	}
}
