﻿using UnityEngine;
using System.Collections;
using BedbugRest;
using LitJson;
public class MenuCtrl: View {

	public bool introPlayed = false;
	public Animation animation;
	
	public UILabel messages;
	//public UISprite avatarSprite;
	public UITexture avatarSprite;
	public VipCtrl vipCtrl;
	public GameObject Vip;
	[Space(10)]
	public GameObject newsBtn;
	public GameObject vipBtn;
    public UILabel versionLabel;
	public MenuCtrl() {
//		Debug.Log("Create a Menu View");
	}

	void Start() {
		animation = gameObject.GetComponent<Animation>();

		Init ();
	}

	//These methods are overrides and therefore
	//can override any virtual methods in the parent
	//class.


	public void onTransitionInStart(viewData _viewdata = null){
        versionLabel.text = Main.app_version;

//		avatarSprite.mainTexture = null;
//		Debug.Log (Main.AppUser.picture);
		if (Main.AppUser.picture != null) {
			BedBugTools.loadImageOnUITextureAndResize (avatarSprite, Main.AppUser.picture, 256, 256, FadeInOnLoad, 0);
		} else {
			string baseUrl = "https://s3-eu-west-1.amazonaws.com/sportimo-media/avatars/playerProfile.png";
			BedBugTools.loadImageOnUITextureAndResize (avatarSprite, baseUrl, 256, 256, FadeInOnLoad, 0);
		}

		API.GetAll(Main.Settings.apis["users"]+"/"+Main.AppUser._id+"/unread",OnGetUnread);
		if (!introPlayed) {
			animation.Play ("Intro");
			introPlayed = true;
		}

		// Open VIP if uesr is free
		if (Main.isPaidCustomer ("", true)) {
			NGUITools.SetActive (vipBtn, false);
			NGUITools.SetActive (newsBtn, true);
		} else {
			NGUITools.SetActive (vipBtn, true);
			NGUITools.SetActive (newsBtn, false);
		}
	}

	private void FadeInOnLoad(UITexture texture){

		TweenAlpha.Begin (texture.gameObject, 0.5f, 1.0f);
	}
	
	public void OnGetUnread(BestHTTP.HTTPRequest req,BestHTTP.HTTPResponse res){
		if(res.StatusCode!=200 && res.StatusCode!=304){
			MessagePanel.Instance.animMessage(res.DataAsText);
			return;
		}
		JsonData data = JsonMapper.ToObject(res.DataAsText);
		if(data.Keys.Contains("unread") && data["unread"].ToString()!="0"){
			NGUITools.SetActive(messages.transform.parent.gameObject,true);
			messages.text=data["unread"].ToString();
		}
		else{
			NGUITools.SetActive(messages.transform.parent.gameObject,false);
		}
		
	}
	public override void Init ()
	{
		base.Init();
//		Debug.Log("The function called by the Menu View.");     
	}

	public override void selectionChanged(string selection) {
		base.selectionChanged (selection);
		print (selection +" This was fired from the menu view");

	}

	public void openProfile() {
		Main.Instance.openView ("profile",Main.AppUser._id);
	}

	public void openMatches() {
		Main.Instance.openView ("matches");
	}
	
	public void openLeaders() {
		Main.Instance.openView ("leaders");
	}
	
	public void openPrizes() {
        //if(Main.isPaidCustomer())
		Main.Instance.openView ("prizes");
	}
	
	public void openStandings() {
		Main.Instance.openView ("standings");
	}

	public void openFavs() {
		Main.Instance.openView ("favteam");
	}
	
	public void openNews() {
		publicationQuerry querry = new publicationQuerry ();
		querry.limit = 30;
		querry.type = "News";
		Main.Instance.openView ("news",querry);
	}

	public void openVip() {
		NGUITools.SetActive (Vip, true);
		vipCtrl.centerOnFirst();
	}

	public void openAchievs() {
		Main.Instance.openView ("achievs",Main.AppUser._id);
	}
	
	public void openMail(){
		Main.Instance.openView("mail");
	}

	void OnClick() {
//		print ("PLAYING FASST!!!");
		animation ["Intro"].speed = 3;
	}
}
