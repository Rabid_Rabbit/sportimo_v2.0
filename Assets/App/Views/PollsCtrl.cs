﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using BedbugRest;
using System.Linq;
using I2.Loc;
using System.Text.RegularExpressions;




public class PollsCtrl: View
{
	
	List<Poll> Polls = new List<Poll> ();

	private string polls_endpoint = "/polls/";
	public UITable polls_grid;
	public GameObject pollPrefab;

	// I don't know what this is for. Left it for consistency.
	public PollsCtrl ()
	{
//		Debug.Log("Create a news View");
	}

	//public void openMatchPolls ()
	public  void onTransitionInStart()
	{
		foreach (Transform trans in polls_grid.GetChildList()) {
			if (trans.gameObject.activeSelf) {
				NGUITools.Destroy (trans.gameObject);
			}
		}
		//openline (0);
		//		string call = Main.Settings.apis ["polls"]+"/" + MatchCtrl.matchid + "/tag/" + Main.AppUser._id + "/user/";
		//		print (call);
		// https://sportimo.mod.bz/v1/polls/general/:uid
		// https://sportimo.mod.bz/v1/polls/general/:577a6ada16fca2f8009a3299
		API.GetAll (Main.Settings.apis ["polls"]+ "/general/"+Main.AppUser._id, OnGetMatchPolls);
		Loader.Visible (true);

		//}

	}


	public void OnGetMatchPolls (BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
	{


		if(res.StatusCode != 200 && res.StatusCode!=304){
			MessagePanel.Instance.animMessage(res.DataAsText);
			return;
		}

		Loader.Visible(false);
		Debug.Log (res.DataAsText);

		Polls = LitJson.JsonMapper.ToObject<List<Poll>> (res.DataAsText);

		if (Polls.Count > 0) {
			foreach(Poll poll in Polls) {
				GameObject pollObj = NGUITools.AddChild (polls_grid.gameObject, pollPrefab);
				NGUITools.SetActive (pollObj, true);

				// Get pollCtl component
				PollCtrl pollCtrl = pollObj.GetComponent<PollCtrl>();
				// Run setUpPoll in pollCtrl
				pollCtrl.updatePoll(poll);
			}
		} else {
			print("There are no Polls yet");
		}
		polls_grid.Reposition ();
		polls_grid.transform.parent.gameObject.GetComponent<UIScrollView> ().ResetPosition ();
		Loader.Visible (false);
	}

	public void openPolls() {
		Main.Instance.openView ("polls");
	}

	public void openNews() {
		publicationQuerry querry = new publicationQuerry ();
		querry.limit = 30;
		querry.type = "News";
		Main.Instance.openView ("news",querry);
		//Main.Instance.openView ("news");
	}
}
	
// TAB METHODS
	

