﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using BedbugRest;
using System.Linq;
using I2.Loc;
using System.Text.RegularExpressions;



public class publicationQuerry
{
    
	public DateTime minDate;
	public DateTime maxdate;
	public string type;
	public string related;
	public int limit;
}

public class NewsCtrl: View
{

    public GameObject locked_screen;
    public string tags;
    List<Publication> Publications = new List<Publication> ();
	bool populated = false;
	private string articles_endpoint = "/articles/search";
	public UITable newsGrid;
	public UIGrid Grid;
	public GameObject newsItem;
	public UIScrollView ScrollView;

	// I don't know what this is for. Left it for consistency.
	public NewsCtrl ()
	{
//		Debug.Log("Create a news View");
	}

	// I don't know what this is for. Left it for consistency. We shouldn't leave methods on classes
	// that are not used. Avoid it.
	public override void Init ()
	{	
		//Get All news with limit, order by date, populate list 
	}

	// When the view is called we receive the view querry object
	public  void onTransitionInStart (viewData _viewData)
	{
        if (Main.isPaidCustomer("", true))
        {
            if (locked_screen)
                locked_screen.SetActive(false);
            publicationQuerry querry = null;

            if (_viewData.data != null)
                querry = _viewData.data as publicationQuerry;
            else {
                querry.limit = 30;
                querry.type = "News";
            }
            Loader.Visible(true);
            API.PostOne(Main.Settings.apis["data"] + articles_endpoint, querry, GetNewsCallback);
        }
        else {
           
            if (locked_screen)
                locked_screen.SetActive(true);
        }

      
	}

	// We consume the response to a List of publications
	public void GetNewsCallback (BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
	{
		Debug.Log(res.DataAsText);
		if(res.StatusCode!=200 && res.StatusCode!=304){
			MessagePanel.Instance.animMessage(res.DataAsText);
			return;
		}
		Publications = LitJson.JsonMapper.ToObject<List<Publication>> (res.DataAsText);
		Publications = Publications.OrderByDescending (x => x.publishDate).ToList ();
		PopulateNews ();
	}
     
	// We create a list of newsItem prefabs and assign properties according
	// to each publication.
	public void PopulateNews ()
	{
		foreach (Transform transform in Grid.GetChildList()) {
			if (transform.gameObject.activeSelf)
				NGUITools.Destroy (transform);
		}

		foreach(Publication _publication in Publications){

			string usedlang = getUsedLang (_publication);

			GameObject newsitem = NGUITools.AddChild (Grid.gameObject, newsItem);

			newsitem.name = _publication._id;
			NewsItemCtrl publicationItem = newsitem.GetComponent<NewsItemCtrl> ();
			publicationItem.publicationData = _publication;
			string noHTML = Regex.Replace(_publication.publication.title[usedlang],  "<(.|\n)*?>|&#?[a-zA-Z0-9]*?;", " ").Trim();
			publicationItem.title.text =noHTML;
			publicationItem.date.text = _publication.publishDate.ToString ("dd/MM/yyyy");
			publicationItem.photoURL = _publication.photo;
			NGUITools.SetActive (newsitem, true);		

		}


		populated = true;
		Loader.Visible (false);
		StartCoroutine(Reposition());

	}

	string getUsedLang(Publication _publication) {
		string usedLang;

		if (_publication.publication.title.ContainsKey (LocalizationManager.CurrentLanguageCode))
			usedLang = LocalizationManager.CurrentLanguageCode;
		else
			usedLang = "en";

		return usedLang;
	}

	IEnumerator Reposition(){
		yield return new WaitForSeconds(0.1f);
		Grid.Reposition ();
		ScrollView.ResetPosition ();
	}

	public void onTransitionOutStart ()
	{
		foreach (Transform transform in Grid.GetChildList()) {
			if (transform.gameObject.activeSelf)
				NGUITools.Destroy (transform);
		}
	}

	public void openPolls() {
        //if (Main.isPaidCustomer("", true))
            Main.Instance.openView ("polls");
	}

	public void openNews() {
        //if (Main.isPaidCustomer("", true))
            Main.Instance.openView ("news");
	}
}
	
// TAB METHODS
	

