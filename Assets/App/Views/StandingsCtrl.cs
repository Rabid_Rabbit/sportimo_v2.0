﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BedbugRest;
using LitJson;
using System.Linq;
using I2.Loc;

public class StandingsCtrl: View
{

	List<League> Leagues = new List<League> ();
	bool populated = false;
	private string StandingsEndpoint = "/standings/";

    public UILabel TopLabel;

	public UIGrid standingsGrid;
	public GameObject teamTop;
	public GameObject teamDark;
	public GameObject teamLight;
	public UIScrollView standingsScroll;
	public GameObject scrollPanel;
	public GameObject selectPanel;

	public GameObject flags;

	public GameObject backToHistory;
	public GameObject backToFlags;
	//[Space(10)]
	public bool isFlags = true; 


	public StandingsCtrl ()
	{
//		Debug.Log("Create a Prizes View");
	}


	//These methods are overrides and therefore
	//can override any virtual methods in the parent
	//class.
	public override void Init ()
	{
		//GetStandings();
	}
	
	//void override OnTransitionFinishedMethod(){ GetStandings()}

	public override void selectionChanged (string selection)
	{
		base.selectionChanged (selection);
		
		
//		print (selection +" This was fired from the Standings view");
		
		//StartCoroutine( PopulateLeague(selection));

	}

	public void openStandingsB() {
		Main.Instance.openView ("standingsb");
		Loader.Visible (true);
	}

	public void openSelect ()
	{
		// Open select panel
		TweenAlpha.Begin (selectPanel, 0.3f, 1.0f);
		// Close Teams scroll
		TweenAlpha.Begin (scrollPanel, 0.3f, 0.0f);
		// Close backToFlags
		NGUITools.SetActive(backToFlags, false);
		// Open backToHistory
		NGUITools.SetActive(backToHistory, true);

        TopLabel.text = LocalizationManager.GetTermTranslation("_select_league", true);
	}

	string currentSelection;
	public void selectionMade (GameObject selectObj)
	{
		print ("Selection Name: "+selectObj.name);
		Loader.Visible (true);
		currentSelection = selectObj.name;
		string selection = selectObj.name;
		// Close select panel
		TweenAlpha.Begin (selectPanel, 0.3f, 0.0f);
		// Open Teams scroll
		TweenAlpha.Begin (scrollPanel, 0.3f, 1.0f);
//		// Open backToFlags
//		NGUITools.SetActive(backToFlags, true);
//		// Close backToHistory
//		NGUITools.SetActive(backToHistory, false);

		GetStandings ();
		//StartCoroutine (PopulateLeague (selection));
	}
	
	//Refresh/Get News
	private void GetStandings ()
	{
		Hashtable table = new Hashtable ();
		//table.Add("visiblein","GR"); //PlayerPrefs.GetString("country_code") or user.country_code
		//API.PostOne(StandingsEndpoint,table,GetStandingsCallback);
		Loader.Visible (true);
		print ("STANDINGS CALL: ");
		API.GetAll (Main.Settings.apis ["data"] + StandingsEndpoint, GetStandingsCallback);
		
	}
    
	public void GetStandingsCallback (BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
	{


		print ("Standing Call back: ");
		BedbugRest.API.Log (res.DataAsText);
		if (res.StatusCode != 200 && res.StatusCode != 304) {
			MessagePanel.Instance.animMessage (res.DataAsText);
			return;
		}

		UIPopupList leagues = this.gameObject.GetComponentInChildren<UIPopupList> ();
		Leagues = JsonMapper.ToObject<List<League>> (res.DataAsText);

		//if (!populated) {
		//	leagues.Clear ();
		//	//leagues.AddItem(league.name[LocalizationManager.CurrentLanguageCode]);
		//	leagues.items = Leagues.Select (x => x.name [LocalizationManager.CurrentLanguageCode]).Distinct ().ToList () as List<string>;
		//	leagues.value = Leagues[0].name [LocalizationManager.CurrentLanguageCode]; //[LocalizationManager.CurrentLanguageCode]	
		//}
		NGUITools.SetActive (flags, true);
		populated = true;
		Loader.Visible (false);

		StartCoroutine (PopulateLeague (currentSelection));
	}
	
	//League population by selection
	IEnumerator PopulateLeague (string selection)
	{
		print ("Selection Name: "+selection);
		
		foreach (Transform transform in standingsGrid.GetChildList()) {
			if (transform.gameObject.activeSelf)
				NGUITools.Destroy (transform);
		}
		
		foreach (League league in Leagues) {

			string usedLang = getUsedLang (league);

			Debug.Log((selection == league.competitionid) + " " + selection + " : [" + league.competitionid + " - " + LocalizationManager.FixRTL_IfNeeded( league.name[usedLang]));

            if (selection == league.competitionid && league.season == 2016) {

				TopLabel.text = LocalizationManager.FixRTL_IfNeeded( league.name[usedLang]);

                float placeHeight = -105;
				if (league.teams != null)
				for (int i = 0; i<league.teams.Count; i++) {
						GameObject top;
						standingsScroll.ResetPosition ();

						if (i == 0) {
							top = NGUITools.AddChild (standingsGrid.gameObject, teamTop);
						top.name = league.teams [i].teamId;
							NGUITools.SetActive (top, true);
							top.transform.FindChild ("name").GetComponentInChildren<UILabel> ().text = LocalizationManager.FixRTL_IfNeeded( league.teams [i].teamName [LocalizationManager.CurrentLanguageCode]); //name[LocalizationManager.CurrentLanguageCode];
						top.transform.FindChild ("index").GetComponentInChildren<UILabel> ().text = league.teams [i].rank.ToString ();
						top.transform.FindChild ("played").GetComponentInChildren<UILabel> ().text = league.teams [i].gamesPlayed.ToString ();
						top.transform.FindChild ("won").GetComponentInChildren<UILabel> ().text = league.teams [i].wins.ToString ();
						top.transform.FindChild ("drawn").GetComponentInChildren<UILabel> ().text = league.teams [i].ties.ToString ();
						top.transform.FindChild ("lost").GetComponentInChildren<UILabel> ().text = league.teams [i].losses.ToString ();
						top.transform.FindChild ("goals").GetComponentInChildren<UILabel> ().text = (league.teams [i].goalsFor - league.teams [i].goalsAgainst).ToString ();
						top.transform.FindChild ("points").GetComponentInChildren<UILabel> ().text = league.teams [i].points.ToString ();
						} else if (i % 2 == 0) {
							top = NGUITools.AddChild (standingsGrid.gameObject, teamDark);
						top.name = league.teams [i].teamId;
							NGUITools.SetActive (top, true);
							top.transform.FindChild ("name").GetComponentInChildren<UILabel> ().text = LocalizationManager.FixRTL_IfNeeded( league.teams [i].teamName [LocalizationManager.CurrentLanguageCode]); //name[LocalizationManager.CurrentLanguageCode];
						top.transform.FindChild ("index").GetComponentInChildren<UILabel> ().text = league.teams [i].rank.ToString ();
						top.transform.FindChild ("played").GetComponentInChildren<UILabel> ().text = league.teams [i].gamesPlayed.ToString ();
						top.transform.FindChild ("won").GetComponentInChildren<UILabel> ().text = league.teams [i].wins.ToString ();
						top.transform.FindChild ("drawn").GetComponentInChildren<UILabel> ().text = league.teams [i].ties.ToString ();
						top.transform.FindChild ("lost").GetComponentInChildren<UILabel> ().text = league.teams [i].losses.ToString ();
						top.transform.FindChild ("goals").GetComponentInChildren<UILabel> ().text = (league.teams [i].goalsFor - league.teams [i].goalsAgainst).ToString ();
						top.transform.FindChild ("points").GetComponentInChildren<UILabel> ().text = league.teams [i].points.ToString ();						
						} else {
							top = NGUITools.AddChild (standingsGrid.gameObject, teamLight);
						top.name = league.teams [i].teamId;
							NGUITools.SetActive (top, true);
						
							top.transform.FindChild ("name").GetComponentInChildren<UILabel> ().text = LocalizationManager.FixRTL_IfNeeded( league.teams [i].teamName [LocalizationManager.CurrentLanguageCode]); //name[LocalizationManager.CurrentLanguageCode];
						top.transform.FindChild ("index").GetComponentInChildren<UILabel> ().text = league.teams [i].rank.ToString ();
						top.transform.FindChild ("played").GetComponentInChildren<UILabel> ().text = league.teams [i].gamesPlayed.ToString ();
						top.transform.FindChild ("won").GetComponentInChildren<UILabel> ().text = league.teams [i].wins.ToString ();
						top.transform.FindChild ("drawn").GetComponentInChildren<UILabel> ().text = league.teams [i].ties.ToString ();
						top.transform.FindChild ("lost").GetComponentInChildren<UILabel> ().text = league.teams [i].losses.ToString ();
						top.transform.FindChild ("goals").GetComponentInChildren<UILabel> ().text = (league.teams [i].goalsFor - league.teams [i].goalsAgainst).ToString ();
						top.transform.FindChild ("points").GetComponentInChildren<UILabel> ().text = league.teams [i].points.ToString ();		
						}

						// Placement Animation
						top.transform.localPosition = new Vector3 (0, placeHeight, 0);
						float topY = top.transform.localPosition.y;
						topY -= 30;
						placeHeight -= 35;
						iTween.MoveFrom (top, iTween.Hash ("y", topY, "time", 0.3f, "easetype", iTween.EaseType.easeOutCirc, "islocal", true)); 
						yield return new WaitForSeconds (0.1f);

					}
				//standingsGrid.Reposition();

			}
		}
		Loader.Visible (false);

		Coach.coachMessage ( LocalizationManager.GetTermTranslation( "tip1", true, 30, true), 27f, 4f, 1.5f, true, "tip1");
	}
	
	public void OpenTeamInfo (GameObject go)
	{
		Loader.Visible (true);
		Main.Instance.openView ("teaminfo", go.name, true);
	}
    
	public void onTransitionInStart (viewData _viewData = null)
	{

//		if (!_viewData.useCache) {
//			// Close select panel
//			TweenAlpha.Begin (selectPanel, 0.3f, 1.0f);
//			// Open Teams scroll
//			TweenAlpha.Begin (scrollPanel, 0.3f, 0.0f);
//			// Close backToFlags
//			NGUITools.SetActive(backToFlags, false);
//			// Open backToHistory
//			NGUITools.SetActive(backToHistory, true);
//
//
//			foreach (Transform transform in standingsGrid.GetChildList()) {
//				if (transform.gameObject.activeSelf)
//					NGUITools.Destroy (transform);
//			}
//
//			NGUITools.SetActive (flags, false);
//			GetStandings ();
//		}
	}

	public void onTransitionOutStart ()
	{
		if (isFlags) {
			TopLabel.text = LocalizationManager.GetTermTranslation ("_select_league", true);
		
			populated = false;
        
			foreach (Transform transform in standingsGrid.GetChildList()) {
				if (transform.gameObject.activeSelf)
					NGUITools.Destroy (transform);
			}
		}
   	}

	string getUsedLang(League league) {
		string usedLang;

		if (league.name.ContainsKey (LocalizationManager.CurrentLanguageCode))
			usedLang = LocalizationManager.CurrentLanguageCode;
		else
			usedLang = "en";

		return usedLang;
	}


}
