﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BedbugRest;
using System;
using LitJson;
using I2.Loc;
using System.Linq;
using Facebook.Unity;

public class MatchCtrl2 : View
{   

	//variables for UI operations
	public GameObject matchStats;
	public GameObject livegame;
	public GameObject playedCards;
	public GameObject[] optionGOs = new GameObject[4];
	public Dictionary <string,bool> optionOpened = new Dictionary <string, bool> ();
	public GameObject cardPlay; // These are the Roller cards
    
	public Animation BannerAnim;
	public UIScrollView Timeline_scrollview;

	//variables for UI Data Setting
	public UILabel user_score;
	public GameObject home_team;
	public GameObject away_team;
	public UILabel match_score;
	public UILabel timelabel;

	public GameObject[] cardcounters;
	public GameObject clock;
	public UILabel segment_label;
	public GameObject lastGoalObj;
	public UILabel last_goal_label;
	public UILabel stats_team1;
	public UILabel stats_team2;
	public UITable match_table;
	public UIScrollView cardsScroll;
	public UITable cards_table;
	public GameObject EventPrefab;
	public GameObject cardPrefab;
	public GameObject overallcardPrefab;
	public GameObject cardviewPrefab;
	public GameObject overallcardviewPrefab;
	private int obj_count = 0;
	//variables for match data
	private string match_endpoint = "/match/";
	private string leaders_endpoint = "/pools/forgame/";
	public string match_id;
	public static string matchid;
	public UITexture banner;
	public btmMatchMenuCtrl botctrl;
	private string last_goal;
	public static bool timecounting;
	private static string home_name;
	private static string away_name;
	public  LiveMatch match;
	public static int specials;
	public static int instants;
	public static int overalls;
	public static int cardsLeft;
	private string hashtag;
	public static Pool matchPool;
	public MatchStats matchstats;
	//public List<StatsMod> Stats;
	private bool played_populated;
	public Dictionary<string,string> SpriteNames = new Dictionary<string, string> ();
	public Animation cardInfoAnim;
	public btmMatchMenuCtrl bottomMenu;
	[Space(10)]
	public UILabel specialsLeftLabel;
	public UILabel cards_count;
	[Space(10)]
	public LeaderMini leaderMini;
	public EndMatchCtrl endMatchCtrl;



	public delegate void PowerUp (PlayCard card);

	public static event PowerUp OnPowerUp;

	public delegate void DoublePointsUp (PlayCard card);

	public static event DoublePointsUp onDoublePointsActivated;

	// Match Stats RecentForm
	public Color32 WinColor = new Color32 (49, 171, 100, 255);
	public Color32 DrawColor = new Color32 (72, 142, 224, 255);
	public Color32 LoseColor = new Color32 (207, 1, 27, 255);
	public Dictionary<string,Color32> colors = new Dictionary<string, Color32> ();
	public UISprite[] hthsprites;
	public UILabel[] hthlabels;
	public GameObject headToheadObj;


	/// <summary>
	/// Socket animations are ignored while this property is set to true
	/// </summary>
	bool pauseAnimations {
		get;
		set;
	}

	/// <summary>
	/// Plays the event banner animation. 
	/// bannerAnimator.eventPlay (eventName); ex. "goal", "yellow"
	/// 
	/// Plays the change game state event animation. 
	/// eventStatePlay(string eventName, string eventTitle); ex. "halftime"
	/// 
	/// Plays the substitution animation
	/// eventSubPlay (string subInName, string subOutName) ex. "Orfeas", "Themis"
	/// </summary>
	public BannerAnimator bannerAnimator;


	// The list of the question for prize eligibility
	List<PrizeEligibilityQuestion> PrizeEligibilityQuestionsList {
		get;
		set;
	}

	/// <summary>
	/// The user step of the current prize eligibility test
	/// </summary>
	int eligibilityQuestionStep {
		get;
		set;
	}
	
	void Start ()
	{
		SpriteNames.Add ("Yellow", "yellow");
		SpriteNames.Add ("Corner", "corner");
		SpriteNames.Add ("Red", "red");
		SpriteNames.Add ("Foul", "foul");
		SpriteNames.Add ("Goal", "goal");
		SpriteNames.Add ("Injury", "sportimo");
		SpriteNames.Add ("Offside", "offside");
		SpriteNames.Add ("Penalty", "penalty");
		SpriteNames.Add ("Shot_on_Goal", "shoton");
		SpriteNames.Add ("Substitution", "sub2");
		SpriteNames.Add ("Own_Goal", "sportimoicon");
		SpriteNames.Add ("ref", "kickoff");

		// Add starting setting of optionGo
		foreach (GameObject Go in optionGOs) {
			optionOpened.Add (Go.name, false);
		}

		// Create the Color Dictionary
		colors.Add ("W", WinColor);
		colors.Add ("D", DrawColor);
		colors.Add ("T", DrawColor);
		colors.Add ("L", LoseColor);
	}

	public  void onTransitionInStart (viewData _viewData = null)
	{


        //		Debug.Log("Reseting the timeline");
        Timeline_scrollview.ResetPosition ();

		// Close Playcards if Open
		TweenAlpha.Begin (cardPlay, 1, 0);

		match_id = _viewData.data.ToString ();
		matchid = match_id;

		if (reconnecting == 0)
			ConnectionManager.Instance.Subscribe (match_id);

		ConnectionManager.OnGameEvent += OnGameEventReceived;
		ConnectionManager.OnSegmentEvent += OnSegmentEventReceived;
		ConnectionManager.OnUpdatedEvent += OnUpdatedEventReceived;
		ConnectionManager.OnMessageReceived += OnMessageEventReceived;
		ConnectionManager.OnCardLostEvent += OnCardLostReceived;
		ConnectionManager.OnCardWonEvent += OnCardWonReceived;
		ConnectionManager.OnStatsEvent += OnStatsUpdateReceived;
		ConnectionManager.OnFinalized += OnFinalizeReceived;
		ConnectionManager.OnCardResumedEvent += OnCardResumedReceived;
		ConnectionManager.OnPresetActivated += OnPresetActivatedReceived;
		OnPowerUp += PowerUpPlayed;
		onDoublePointsActivated += DoublePointsActive;
		
		ConnectionManager.OnReconnected += OnReconnect;
		GetmatchData ();
     
		Loader.Visible (true);
		bottomMenu.openline (2);

		// start invoke for banner animation
		openOption (optionGOs [1]);

		if (reconnecting == 1)
			livegame.transform.localPosition = Vector3.zero;

		reconnecting = 0;

		// recent Form Clear
		NGUITools.SetActive (headToheadObj, true);

		for (int h = 0; h < 5; h++) {
			// Change the color depending the result
			hthsprites [h].color = new Color32 (255, 255, 255, 1);
			// Change the label
			hthlabels [h].text = "";
		}

		// Check The last Pos in the leaderboard
		if (Main.AppUser.leaderCheckObj != null) {
			//print ("MatchId: " + matchid + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			//print ("Last MatchId: " + Main.AppUser.leaderCheckObj.matchId + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			if (matchid != Main.AppUser.leaderCheckObj.matchId) {
				//print ("New Match!!!!!");
				Main.AppUser.leaderCheckObj.matchId = matchid;
				Main.AppUser.leaderCheckObj.lastPos = 0; // Reset the last pos
			} else {
				print("Same Match "+ Main.AppUser.leaderCheckObj.lastPos);
			}
		} else {
			
			print("Main.AppUser.leaderCheckObj is NULL!!!!!");

		}


		// Close the play if opened
		if (botctrl.cardPlayOpen)
			botctrl.cardPlayToggle ();



	}
    
	public void onTransitionOutStart ()
	{
		foreach (Transform transform in match_table.GetChildList()) {
			if (transform.gameObject.activeSelf)
				NGUITools.Destroy (transform);
		}
		foreach (Transform transform in cards_table.GetChildList()) {
			if (transform.gameObject.activeSelf)
				NGUITools.Destroy (transform);
		}
		played_populated = false;

		CancelInvoke ("TimeCount");
		CancelInvoke ("TimeCountFromSegment");
		obj_count = 0;
		timelabel.text = "";
		CancelInvoke ("playBannerAnim");
		//check
		ConnectionManager.Instance.Unsubscribe (match_id);
		ConnectionManager.OnGameEvent -= OnGameEventReceived;
		ConnectionManager.OnSegmentEvent -= OnSegmentEventReceived;
		ConnectionManager.OnUpdatedEvent -= OnUpdatedEventReceived;
		ConnectionManager.OnMessageReceived -= OnMessageEventReceived;
		ConnectionManager.OnCardLostEvent -= OnCardLostReceived;
		ConnectionManager.OnCardWonEvent -= OnCardWonReceived;
		ConnectionManager.OnStatsEvent -= OnStatsUpdateReceived;
		ConnectionManager.OnReconnected -= OnReconnect;
		ConnectionManager.OnFinalized -= OnFinalizeReceived;
		ConnectionManager.OnCardResumedEvent-= OnCardResumedReceived;
		ConnectionManager.OnPresetActivated -= OnPresetActivatedReceived;
		onDoublePointsActivated -= DoublePointsActive;








	}

	int reconnecting = 0;

	public void OnReconnect (string lastsub)
	{
		JsonData data = JsonMapper.ToObject (lastsub);
		if (Main.Instance.viewSelected == this && reconnecting == 0) {		
			reconnecting = 1;
			CancelInvoke ("TimeCount");
			CancelInvoke ("TimeCountFromSegment");
			ConnectionManager.OnGameEvent -= OnGameEventReceived;
			ConnectionManager.OnSegmentEvent -= OnSegmentEventReceived;
			ConnectionManager.OnUpdatedEvent -= OnUpdatedEventReceived;
			ConnectionManager.OnMessageReceived -= OnMessageEventReceived;
			ConnectionManager.OnCardLostEvent -= OnCardLostReceived;
			ConnectionManager.OnCardWonEvent -= OnCardWonReceived;
			ConnectionManager.OnStatsEvent -= OnStatsUpdateReceived;
			ConnectionManager.OnReconnected -= OnReconnect;
			ConnectionManager.OnFinalized-=OnFinalizeReceived;
			ConnectionManager.OnCardResumedEvent -= OnCardResumedReceived;
			ConnectionManager.OnPresetActivated -= OnPresetActivatedReceived;

			// CancelInvoke("TimeCountFromSegment");
			// foreach (Transform transform in match_table.GetChildList()) {
			// 	if (transform.gameObject.activeSelf)
			// 		NGUITools.Destroy (transform);
			// }
			// foreach (Transform transform in cards_table.GetChildList()) {
			// 	if (transform.gameObject.activeSelf)
			// 		NGUITools.Destroy (transform);
			// }
			onTransitionInStart (new viewData (data ["subscribe"] ["room"]));
		}
	}
    
	void OnApplicationPause (bool inBackground)
	{

		if (Main.Instance.viewSelected == this) {		
			if (!inBackground) {
				//Debug.Log("We need to unpause the socket animations");
				StartCoroutine(unpauseAnimations());
//				obj_count = 0;
//				GetmatchData ();

			} else {
				//Debug.Log("We need to pause socket animations");
				pauseAnimations = true;

//				CancelInvoke ("TimeCount");
//
//				foreach (Transform transform in match_table.GetChildList()) {
//					if (transform.gameObject.activeSelf)
//						NGUITools.Destroy (transform);
//				}
			}
		}
	}

	IEnumerator unpauseAnimations ()
	{
		yield return new WaitForSeconds (2);
		//Debug.Log("- Animations unpaused");
		pauseAnimations = false;
	}
	
	public void OpenPlayCards ()
	{
        Debug.Log(match.matchData.state == 0);
        if (match.matchData.state == 0 && !Main.isPaidCustomer(LocalizationManager.GetTermTranslation("_conversion_pregame",true, 20)))
            return;

        if (!match.matchData.completed && cardsLeft > 0) {
			cardPlay.GetComponent<PlayCardCtrl> ().Initialize (match_id);

		} else if (match.matchData.completed) {
			MessagePanel.Instance.animMessage ("You can't play cards on a match that has ended");
		} else {
			MessagePanel.Instance.animMessage ("You can't play any more cards");
		}
	}
	//Data & Population Methods
	private void GetmatchData ()
	{
		
//		print (Main.Settings.apis ["data"] + match_endpoint + match_id + "/user/" + Main.AppUser._id);

		API.GetAll (Main.Settings.apis ["data"] + match_endpoint + match_id + "/user/" + Main.AppUser._id, OnGetmatchData);

		API.GetAll (Main.Settings.apis ["leaderpay"] + leaders_endpoint + match_id + "/" + Main.AppUser.country, OnGetPoolsData);
	}
	
	private void OnGetPoolsData (BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
	{
		if (res.StatusCode != 200 && res.StatusCode != 304) {
			MessagePanel.Instance.animMessage (res.DataAsText);
			return;
		}
		if (res.DataAsText != "[]") {
			string json = res.DataAsText.Remove (0, 1);
			json = json.Remove (json.LastIndexOf ("]"), 1);
			matchPool = JsonMapper.ToObject<Pool> (json);
			botctrl.pool = matchPool;
		}
	}

	void setUserEligible (string b)
	{
		BedbugRest.API.GetOne (Main.Settings.apis ["users"] + "/" + Main.AppUser._id + "/match/" + match_id + "/prizeseligible/" + b, (req, res) => {
			BedbugRest.API.Log (res.DataAsText);
		});
	}

	void askEligibilityQuestion ()
	{
		Debug.Log ("Ask eligibility question: " + eligibilityQuestionStep);

		QuestionModalController.Ask (
			"_prizesQuestionHeader" + eligibilityQuestionStep,
			"_prizesQuestionInfo" + eligibilityQuestionStep,
				PrizeEligibilityQuestionsList [eligibilityQuestionStep].text,
				PrizeEligibilityQuestionsList [eligibilityQuestionStep].answers [0]._id,
			    PrizeEligibilityQuestionsList [eligibilityQuestionStep].answers [0].text,
				PrizeEligibilityQuestionsList [eligibilityQuestionStep].answers [1]._id,
				PrizeEligibilityQuestionsList [eligibilityQuestionStep].answers [1].text, 
				(answer) => {
			Debug.Log (answer);
			if (PrizeEligibilityQuestionsList [eligibilityQuestionStep].correct == answer) {						
				Debug.Log ("Make coach say congrats, user succeded.");
					Coach.coachMessage(  LocalizationManager.GetTermTranslation("_prize_eligible", true, 30, true), 27f, 0, 2.5f, true);
                        setUserEligible ("true");
			} else if (answer != "cancel") {
				if (eligibilityQuestionStep < 2) {
					eligibilityQuestionStep++;
					askEligibilityQuestion ();
				} else {
						Debug.Log("Coach says: bad luck");
						Coach.coachMessage(  LocalizationManager.GetTermTranslation("_prize_not_eligible", true, 30, true), 27f, 0, 2.5f, true);
                            setUserEligible ("false");
				}
			}
					
		}
		);

	}

	void StartPrizeEligibilityQuestions ()
	{
		//Debug.Log ("Start Eligibility Process");
		// Step 1: Request the match questions
		BedbugRest.API.GetAll (Main.Settings.apis ["data"] + "/inquestions/" + match_id + "/match", (req, res) => {

			BedbugRest.API.Log (res.DataAsText);

			if (res.IsSuccess) {
				PrizeEligibilityQuestionsList = JsonMapper.ToObject<List<PrizeEligibilityQuestion>> (res.DataAsText);
				if (PrizeEligibilityQuestionsList.Count > 0) {
					eligibilityQuestionStep = 0;
					askEligibilityQuestion ();
				}
			}
		});
	}
   
	private void OnGetmatchData (BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
	{
		//print ("MATCH DATA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		BedbugRest.API.Log (res.DataAsText);

		pauseAnimations = true;
		StartCoroutine(unpauseAnimations());

		if (res.StatusCode != 200 && res.StatusCode != 304) {
			MessagePanel.Instance.animMessage (res.DataAsText);
			return;
		}

//		string json = res.DataAsText.Replace ("\"pointsAwarded\":null", "\"pointsAwarded\":0");
//		json = json.Replace ("\"duration\":null", "\"duration\":0");
		NGUITools.SetActive (lastGoalObj, false);
		last_goal_label.text = "";

		match = JsonMapper.ToObject<LiveMatch> (res.DataAsText);


		// Check if prizes are enabled in platform settings
		if (Main.Settings.prizes) {
			//Debug.Log ("Prizes are on: " + match.prize_eligible);
			// Check if user has previously completed the prized eligibility
			if (match.prize_eligible == null) {
				//Debug.Log ("Eligibility is not concluded");
				StartPrizeEligibilityQuestions ();
			}
			
		}

//		Stats = StatsMod.getStats(match.matchData.stats.ToJson());

		home_name = BedBugTools.GetStringByOrder (new List<string> (){"abbr","short",LocalizationManager.CurrentLanguageCode,"en"}, match.matchData.home_team.name);
		away_name = BedBugTools.GetStringByOrder (new List<string> (){"abbr","short",LocalizationManager.CurrentLanguageCode,"en"}, match.matchData.away_team.name);
		stats_team1.text = BedBugTools.GetStringByOrder (new List<string> (){"abbr","short",LocalizationManager.CurrentLanguageCode,"en"}, match.matchData.home_team.name);
		stats_team2.text = BedBugTools.GetStringByOrder (new List<string> (){"abbr","short",LocalizationManager.CurrentLanguageCode,"en"}, match.matchData.away_team.name);

		PopulateStats (match.matchData.stats);
		BedBugTools.loadImageOnUITextureAndResize (home_team.GetComponentInChildren<UITexture> (), match.matchData.home_team.logo, 128, 128, FadeInOnLoad, 0);
		BedBugTools.loadImageOnUITextureAndResize (away_team.GetComponentInChildren<UITexture> (), match.matchData.away_team.logo, 128, 128, FadeInOnLoad, 0);
		CalculateTime (match.matchData.state);

		if (match.matchData.settings.gameCards.Count > 0) {
			specials = match.matchData.settings.gameCards ["specials"];


			instants = match.matchData.settings.gameCards ["instant"];
//			instants = 100;
			overalls = match.matchData.settings.gameCards ["overall"];
			hashtag = match.matchData.settings.hashtag;
			foreach (PlayCard card in match.playedCards) {
				if (card.cardType == "Overall") {
					overalls--;
				} else if (card.cardType == "Instant") {
					instants--;
				}
				if (card.isDoublePoints) {
					specials--;
				}
				if (card.isDoubleTime) {
					specials--;
				}
			}
			// Add Specials left to the main counter
			specialsLeftLabel.text = specials.ToString();

			if (match.matchData.settings.gameCards.ContainsKey ("totalcards"))
				cardsLeft = match.matchData.settings.gameCards ["totalcards"] - match.playedCards.Count;
			else 
				cardsLeft = 0;

			if (cardsLeft > 0)
				cards_count.text = cardsLeft.ToString ();
			else
				cards_count.text = "0";
		}
		foreach (Transform transform in match_table.GetChildList()) {
			if (transform.gameObject.activeSelf)
				NGUITools.Destroy (transform);
		}
        
		//Team & Match info population
      
		user_score.text = match.userScore.ToString ();
		home_team.GetComponentInChildren<UILabel> ().text = BedBugTools.GetStringByOrder (new List<string> (){"abbr","short",LocalizationManager.CurrentLanguageCode,"en"}, match.matchData.home_team.name);
		away_team.GetComponentInChildren<UILabel> ().text = BedBugTools.GetStringByOrder (new List<string> (){"abbr","short",LocalizationManager.CurrentLanguageCode,"en"}, match.matchData.away_team.name);
		match_score.text = match.matchData.home_score.ToString () + "-" + match.matchData.away_score.ToString ();
		for (int i=0; i <= match.matchData.state; i++) {
			if (match.matchData.timeline [i].events.Count > 0) {
				PopulateEvents (i, match.matchData.timeline [i].events);
			}
       
		}

		StartCoroutine (resetTimeline ());

		PopulateCards (match.playedCards);
		openLiveAction ();

		//TODOadd extra half/penalties
       
		Coach.coachMessage ( LocalizationManager.GetTermTranslation( "tip3", true, 30, true), 27f, 4f, 2.5f, true, "tip3");
	}
    
	private void PopulateEvents (int pos, List<GameEvent> events)
	{
		GameObject lastCardGo = null;

		foreach (GameEvent gevent in events) {
			GameObject eventGO;

//			Debug.Log("We should disable this check when everything is ok");
			if (string.IsNullOrEmpty (LocalizationManager.GetTermTranslation (gevent.type)))
				Debug.Log (gevent.type + " | " + LocalizationManager.GetTermTranslation (gevent.type));
			//Animation for events here
			if (gevent.type.Contains ("End") || gevent.type.Contains ("Start")) {
				eventGO = NGUITools.AddChild (match_table.gameObject, EventPrefab);
				NGUITools.SetActive (eventGO, true);
				UILabel[] labels = eventGO.GetComponentsInChildren<UILabel> ();
				labels [0].text = LocalizationManager.GetTermTranslation (gevent.type, true);
				eventGO.transform.FindChild ("icon").FindChild ("EventIcon").GetComponent<UISprite> ().spriteName = getEventSprite ("ref");
			} else {
				eventGO = NGUITools.AddChild (match_table.gameObject, EventPrefab);
				NGUITools.SetActive (eventGO, true);
				UILabel[] labels = eventGO.GetComponentsInChildren<UILabel> ();
				labels [0].text = LocalizationManager.GetTermTranslation (gevent.type, true);
				if (gevent.players != null && gevent.players.Count > 0) {
					labels [1].text = gevent.time.ToString () + "'" + ", " + LocalizationManager.FixRTL_IfNeeded( gevent.players [0].name [LocalizationManager.CurrentLanguageCode]);
					if (gevent.type == "Goal") {
//						Debug.Log (gevent.team);
						NGUITools.SetActive (lastGoalObj, true);
						last_goal = LocalizationManager.FixRTL_IfNeeded( gevent.players [0].name [LocalizationManager.CurrentLanguageCode]) + ", " + ReturnName (gevent.team) + " " + gevent.time.ToString () + "'";
						last_goal_label.text = last_goal;
					}
				} else {
					labels [1].text = gevent.time.ToString () + "'";
				}
				eventGO.transform.FindChild ("icon").FindChild ("EventIcon").GetComponent<UISprite> ().spriteName = getEventSprite (gevent.type);
				BedBugTools.loadImageOnUITextureAndResize (eventGO.transform.GetComponentInChildren<UITexture> (), ReturnTextureUrl (gevent.team), 64, 64, FadeInOnLoad, 0);

			}
			eventGO.name = GetObjectName (gevent.created);
			eventGO.GetComponent<eventMatchCtrl> ().id = gevent._id;
			lastCardGo = eventGO;
			obj_count++;
		}


		match_table.Reposition ();
		if (lastCardGo != null)
			StartCoroutine (ScrollToEvent (lastCardGo, .1f));
	}

	public IEnumerator ScrollToEvent (GameObject go, float wait = 0.1f)
	{
		yield return new WaitForSeconds (wait);

		UICenterOnChild center = NGUITools.FindInParents<UICenterOnChild> (go);
		UIPanel panel = NGUITools.FindInParents<UIPanel> (go);
			
		if (center != null) {
			if (center.enabled)
				center.CenterOn (go.transform);
		} else if (panel != null && panel.clipping != UIDrawCall.Clipping.None) {
			UIScrollView sv = panel.GetComponent<UIScrollView> ();
			Vector3 offset = -panel.cachedTransform.InverseTransformPoint (go.transform.position);
			if (!sv.canMoveHorizontally)
				offset.x = panel.cachedTransform.localPosition.x;
			if (!sv.canMoveVertically) {
				offset.y = panel.cachedTransform.localPosition.y + 100L;
			}
			offset.y += 100;
			SpringPanel.Begin (panel.cachedGameObject, offset, 6f);
		}
	}
    
	IEnumerator resetTimeline ()
	{
		yield return new WaitForSeconds (0.1f);
		Timeline_scrollview.ResetPosition ();
	}

	private void PopulateCards (List<PlayCard> cards)
	{
//		Debug.Log ("Populating cards");


		foreach (PlayCard card in cards) {
			GameObject Card;
			if (card.cardType == "Instant" || card.cardType == "PresetInstant") {
				if (card.status == 0) {
					continue;
				}else
					Card = NGUITools.AddChild (match_table.gameObject, cardPrefab);
			} else {
				Card = NGUITools.AddChild (match_table.gameObject, overallcardPrefab);

			}

			NGUITools.SetActive (Card, true);

			// Play start Animation
			Card.GetComponent<Animation> ().Play ("cardshow1");

			// CHECK IF CARD IS PRESET ELSE
			if (card.cardType == "PresetInstant" && card.status == 0)
				Card.name = card.minute.ToString();
			else
				Card.name = GetObjectName (DateTime.Parse (card.activationTime));

			
			cardMatchCtrl ctrl = Card.GetComponent<cardMatchCtrl> ();
			ctrl.home_logo = match.matchData.home_team.logo;
			ctrl.away_logo = match.matchData.away_team.logo;

//			if(match.matchData.home_team.stats != null)
//				ctrl.home_stat = (StatsHelper(match.matchData.home_team.stats,card.primaryStatistic)/(int)match.matchData.home_team.stats["gamesPlayed"]).ToString();
//			if(match.matchData.away_team.stats!=null)
//				ctrl.away_stat = (StatsHelper(match.matchData.away_team.stats, card.primaryStatistic)/(int)match.matchData.away_team.stats["gamesPlayed"]).ToString();

			
			ctrl.card = card;
			ctrl.Init ();


		}



		match_table.Reposition ();            
		Loader.Visible (false);


	}
    
	public void OnCardPlay (PlayCard card)
	{
		GameObject Card;
		if (card.cardType == "Instant" || card.cardType == "PresetInstant") {
			if (card.cardType == "PresetInstant" && card.status == 0) {
				match.playedCards.Add (card); // Add the new card so it can be shown in tHe PlayedCards Tab before any update from the service
				cardsLeft--;
				print ("Cards Left: "+cardsLeft);
				cards_count.text = cardsLeft.ToString ();
				return;
			}else
				Card = NGUITools.AddChild (match_table.gameObject, cardPrefab);
		} else {
			Card = NGUITools.AddChild (match_table.gameObject, overallcardPrefab);

		}

		if (card.cardType == "PresetInstant" && card.status == 0)
			Card.name = card.minute.ToString();
		else
			Card.name = GetObjectName (DateTime.Parse (card.activationTime));

		
		NGUITools.SetActive (Card, true);
		cardMatchCtrl ctrl = Card.GetComponent<cardMatchCtrl> ();
		ctrl.card = card;
		ctrl.option_selected = int.Parse (card.optionId);
		ctrl.home_logo = match.matchData.home_team.logo;
		ctrl.away_logo = match.matchData.away_team.logo;

//		if(match.matchData.home_team.stats!=null)
//				ctrl.home_stat=(StatsHelper(match.matchData.home_team.stats,card.primaryStatistic)/(int)match.matchData.home_team.stats["gamesPlayed"]).ToString();
//		if(match.matchData.away_team.stats!=null)
//				ctrl.away_stat=(StatsHelper(match.matchData.away_team.stats,card.primaryStatistic)/(int)match.matchData.away_team.stats["gamesPlayed"]).ToString();

		ctrl.Init ();
		match_table.Reposition ();
		match.playedCards.Add (card);

		if (card.cardType == "Instant") {
			instants--;
		} else if (card.cardType == "Overall") {
			overalls--;
		}
		// Substract one from the cards counrter
		cardsLeft--;

		cards_count.text = cardsLeft.ToString ();
		StartCoroutine (ScrollToEvent (Card));
		
		//	GameObject Card_played;
		CountPlayedCards ();
		tabSelect (tabsLines [0].transform.parent.gameObject);

		Coach.coachMessage ( LocalizationManager.GetTermTranslation( "tip5", true, 30, true), 27f, 4f, 1.5f, false, "tip5");

		if (card.cardType == "Instant") {
			Coach.coachMessage ( LocalizationManager.GetTermTranslation( "tip8", true, 30, true), 27f, 4f, 7.0f, false, "tip8");
		}

		// if (card.cardType == "Instant") {
		// 	Card_played = NGUITools.AddChild (cards_table.gameObject, cardviewPrefab);
		// } else {
		// 	Card_played = NGUITools.AddChild (cards_table.gameObject, overallcardviewPrefab);
		// }


		// NGUITools.SetActive (Card_played, true);
		// Card_played.name = GetObjectName (DateTime.Parse (card.creationTime));
		// cardMatchCtrl ctrl_played = Card_played.GetComponent<cardMatchCtrl> ();
		// ctrl_played.card = card;
		// ctrl_played.Init ();
		// //cards_table.Reposition();
		// StartCoroutine(ScrollToEvent(Card_played));
		
	}
    
	private void PopulateEvent (GameEvent gevent)
	{
		if (!gevent.timeline_event)
			return;

		GameObject eventGO = new GameObject ();

		if (gevent.type.Contains ("End") || gevent.type.Contains ("Starts")) {

			eventGO = NGUITools.AddChild (match_table.gameObject, EventPrefab);
			NGUITools.SetActive (eventGO, true);
			UILabel[] labels = eventGO.GetComponentsInChildren<UILabel> (false);
			labels [0].text = LocalizationManager.GetTermTranslation (gevent.type, true);
			eventGO.transform.FindChild ("icon").FindChild ("EventIcon").GetComponent<UISprite> ().spriteName = getEventSprite ("ref");
			Debug.Log ("Here is the segment with label: " + LocalizationManager.GetTermTranslation (gevent.type));
		} else {
			eventGO = NGUITools.AddChild (match_table.gameObject, EventPrefab);
//			matchScrollCenter.CenterOn (eventGO.transform);

			NGUITools.SetActive (eventGO, true);
			BedBugTools.loadImageOnUITextureAndResize (eventGO.transform.GetComponentInChildren<UITexture> (), ReturnTextureUrl (gevent.team), 64, 64, FadeInOnLoad, 0);

			UILabel[] labels = eventGO.GetComponentsInChildren<UILabel> ();
			labels [0].text = LocalizationManager.GetTermTranslation (gevent.type, true);
			;
			eventGO.transform.FindChild ("icon").FindChild ("EventIcon").GetComponent<UISprite> ().spriteName = getEventSprite (gevent.type);// gevent.type.ToLower ();
			if (gevent.players != null && gevent.players.Count > 0) {
				labels [1].text = gevent.time.ToString () + "'" + ", " + LocalizationManager.FixRTL_IfNeeded( gevent.players [0].name [LocalizationManager.CurrentLanguageCode]);
			} else {
				labels [1].text = gevent.time.ToString () + "'";
			}

			 // If the event is winable // Chamge this to coroutine with random delay between 5-7 sec Float
				print("Checking for Rank Change!!!!!!!!");
				StartCoroutine( checkLeaderChange()) ; // Change this to coroutine with random delay from 6-8 float mls

			

		}

		eventGO.name = GetObjectName (gevent.created);

		eventGO.GetComponent<eventMatchCtrl> ().id = gevent._id;
		match_table.Reposition ();

		StartCoroutine (ScrollToEvent (eventGO));

		if (gevent.type == "Goal") {
			if (gevent.team == "home_team") {
				match.matchData.home_score++;
			} else {
				match.matchData.away_score++;
			}
             
			match_score.text = match.matchData.home_score.ToString () + "-" + match.matchData.away_score.ToString ();
			Debug.Log (match_score.text);
		} else {
			return;
		}


	}

	string getEventSprite (string type)
	{
		if (SpriteNames.ContainsKey (type))
			return SpriteNames [type];
		else
			return "sportimoicon";
	}

	StatsMod total_stats;
	StatsMod home_stats;
	StatsMod away_stats;

	public int StatsHelper (JsonData StatsList, String Stat)
	{
		if (StatsList != null)
		if (StatsList.Keys.Contains (Stat))
			return (int)StatsList [Stat];

		return 0;
	}

	public void PopulateStats (List<JsonData> Stats)
	{

		if (Stats == null)
			return;

		match.matchData.stats = Stats;
		//List<JsonData> Stats = match.matchData.stats;

		JsonData matchStatsData = null;
		JsonData homeStatsData = null;
		JsonData awayStatsData = null;


		foreach (JsonData js in Stats) {
			if (js.Keys.Contains ("name") && js ["name"].IsString) {
				if ((string)js ["name"] == "match")
					matchStatsData = js;
				if ((string)js ["name"] == "home_team")
					homeStatsData = js;
				if ((string)js ["name"] == "away_team")
					awayStatsData = js;
			}
		}
			
//		if (match.matchData.recentForm != null) {
//			for (int i=0; i<match.matchData.recentForm.Count(); i++) {
//				if (match.matchData.recentForm [i] == "W") {
//					matchstats.hthsprites [i].color = matchstats.Win;
//				} else if (match.matchData.recentForm [i] == "D") {
//					matchstats.hthsprites [i].color = matchstats.Draw;
//				} else {
//					matchstats.hthsprites [i].color = matchstats.Loss;
//				}
//				matchstats.hthlabels [i].text = match.matchData.recentForm [i];
//			}
//		}

		// recent Form
		if (match.matchData.headtohead != null) {
			for (int h = 0; h < 5; h++) {
				// Change the color depending the result
				if (!string.IsNullOrEmpty (match.matchData.headtohead [h])) {
					hthsprites [h].color = colors [match.matchData.headtohead [h]];
					// Change the label
					hthlabels [h].text = match.matchData.headtohead [h];
				}
			}
		} else {
			NGUITools.SetActive (headToheadObj, false);
		}
		
		// Possession
		int home_percent = match.matchData.GetStatByName ("home_team", "possession");
		matchstats.possessionStat.SetActive (home_percent > -1);
		matchstats.statsTable.Reposition ();

		matchstats.team1_possesion.text = home_percent + "%";
		matchstats.team2_possesion.text = match.matchData.GetStatByName ("away_team", "possession") + "%";
		matchstats.team1_possesion_fill.fillAmount = (float)home_percent * 0.01f;



		matchstats.team1_corner.text = StatsHelper (homeStatsData, "Corner").ToString ();
		matchstats.team2_corner.text = StatsHelper (awayStatsData, "Corner").ToString ();

		if (StatsHelper (matchStatsData, "Corner") > 0) {
			matchstats.team1_corner_fill.fillAmount = (float)(StatsHelper (homeStatsData, "Corner") * 100 / StatsHelper (matchStatsData, "Corner")) / 100;
			matchstats.team2_corner_fill.fillAmount = (float)(StatsHelper (awayStatsData, "Corner") * 100 / StatsHelper (matchStatsData, "Corner")) / 100;
		} else {
			matchstats.team1_corner_fill.fillAmount = 0;
			matchstats.team2_corner_fill.fillAmount = 0;
		}

		matchstats.team1_target.text = StatsHelper (homeStatsData, "Shot_on_Goal").ToString ();
		matchstats.team2_target.text = StatsHelper (awayStatsData, "Shot_on_Goal").ToString ();

		if ((int)StatsHelper (matchStatsData, "Shot_on_Goal") > 0) {
			matchstats.team1_target_fill.fillAmount = (float)(StatsHelper (homeStatsData, "Shot_on_Goal") * 100 / StatsHelper (matchStatsData, "Shot_on_Goal")) / 100;
			matchstats.team2_target_fill.fillAmount = (float)(StatsHelper (awayStatsData, "Shot_on_Goal") * 100 / StatsHelper (matchStatsData, "Shot_on_Goal")) / 100;
		} else {
			matchstats.team2_target_fill.fillAmount = 0;
			matchstats.team1_target_fill.fillAmount = 0;

		}

		matchstats.team1_offside.text = StatsHelper (homeStatsData, "Offside").ToString ();
		matchstats.team2_offside.text = StatsHelper (awayStatsData, "Offside").ToString ();
		
		if ((int)StatsHelper (matchStatsData, "Offside") > 0) {
			matchstats.team1_offside_fill.fillAmount = (float)(StatsHelper (homeStatsData, "Offside") * 100 / StatsHelper (matchStatsData, "Offside")) / 100;
			matchstats.team2_offside_fill.fillAmount = (float)(StatsHelper (awayStatsData, "Offside") * 100 / StatsHelper (matchStatsData, "Offside")) / 100;
		} else {
			matchstats.team2_offside_fill.fillAmount = 0;
			matchstats.team1_offside_fill.fillAmount = 0;
			
		}
			

		matchstats.team1_free.text = StatsHelper (homeStatsData, "Foul").ToString ();
		matchstats.team2_free.text = StatsHelper (awayStatsData, "Foul").ToString ();
		
		if ((int)StatsHelper (matchStatsData, "Foul") > 0) {
			matchstats.team1_free_fill.fillAmount = (float)(StatsHelper (homeStatsData, "Foul") * 100 / StatsHelper (matchStatsData, "Foul")) / 100;
			matchstats.team2_free_fill.fillAmount = (float)(StatsHelper (awayStatsData, "Foul") * 100 / StatsHelper (matchStatsData, "Foul")) / 100;
		} else {
			matchstats.team2_free_fill.fillAmount = 0;
			matchstats.team1_free_fill.fillAmount = 0;
			
		}

		matchstats.team1_yellow.text = StatsHelper (homeStatsData, "Yellow").ToString ();
		matchstats.team2_yellow.text = StatsHelper (awayStatsData, "Yellow").ToString ();
		
		if ((int)StatsHelper (matchStatsData, "Yellow") > 0) {
			matchstats.team1_yellow_fill.fillAmount = (float)(StatsHelper (homeStatsData, "Yellow") * 100 / StatsHelper (matchStatsData, "Yellow")) / 100;
			matchstats.team2_yellow_fill.fillAmount = (float)(StatsHelper (awayStatsData, "Yellow") * 100 / StatsHelper (matchStatsData, "Yellow")) / 100;
		} else {
			matchstats.team2_yellow_fill.fillAmount = 0;
			matchstats.team1_yellow_fill.fillAmount = 0;
			
		}

		matchstats.team1_red.text = StatsHelper (homeStatsData, "Red").ToString ();
		matchstats.team2_red.text = StatsHelper (awayStatsData, "Red").ToString ();
		
		if ((int)StatsHelper (matchStatsData, "Red") > 0) {
			matchstats.team1_red_fill.fillAmount = (float)(StatsHelper (homeStatsData, "Red") * 100 / StatsHelper (matchStatsData, "Red")) / 100;
			matchstats.team2_red_fill.fillAmount = (float)(StatsHelper (awayStatsData, "Red") * 100 / StatsHelper (matchStatsData, "Red")) / 100;
		} else {
			matchstats.team1_red_fill.fillAmount = 0;
			matchstats.team2_red_fill.fillAmount = 0;
			
		}
			
		
	}


	
    
	//Live Event Methods
	/// <summary>
	/// 
	/// Plays the banner ad animation
	/// bannerAnimator.bannerPlay(string texUrl)
	/// 
	/// Plays the event banner animation. 
	/// bannerAnimator.eventPlay (eventName); ex. "goal", "yellow"
	/// 
	/// Plays the change game state event animation. 
	/// bannerAnimator.eventStatePlay( string eventTitle); ex. "halftime"
	/// 
	/// Plays the substitution animation
	/// bannerAnimator.eventSubPlay (string subInName, string subOutName) ex. "Orfeas", "Themis"
	/// </summary>
	private void OnGameEventReceived (string json)
	{


		JsonData data = JsonMapper.ToObject (json);
		GameEvent gevent = JsonMapper.ToObject<GameEvent> (data ["data"].ToJson ());

		PopulateEvent (gevent);
       

		// Check if socket events animations should be ignored.
		// Usualy because the app has just returned to focus.
		if (pauseAnimations)
			return;
		
		print("gevent.type: "+ gevent.type);
		//Animation for events (top panel?)here
		if (gevent.type == "Yellow" || gevent.type == "Red" || gevent.type == "Goal") {

			bannerAnimator.eventPlay ("banner" + gevent.type);
		}

		if (gevent.type.Contains ("End") || gevent.type.Contains ("Starts")) {

			//TODO needs localization
			string eventTitle = gevent.type.Replace ("_", " ");
			bannerAnimator.eventStatePlay (eventTitle);
			
		} 
	}

	private IEnumerator checkLeaderChange() {
		// Do the call
		// API CALL:
		// leaderpay/v1/leaderboards/:uid/:country/match/:matchid/mini
		//"https://sportimo.mod.bz/leaderpay/v1"
		float waitTime = UnityEngine.Random.Range(5.0f, 7.0f);
		yield return new WaitForSeconds(waitTime);
		API.GetAll (Main.Settings.apis ["leaderpay"] +"/Leaderboards/"+ Main.AppUser._id + "/" + Main.AppUser.country+"/match/"+ match_id+"/mini", OnGetMiniLead);
	}

	private void OnGetMiniLead(BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res) {
		Debug.Log ("Mini Leaderboard: " + res.DataAsText);
	
		leaderMini.leaderMiniCheck (res.DataAsText, matchid);

	}

	private void endMatch() {
		// Do the call
		// API CALL:
		// leaderpay/v1/leaderboards/:uid/:country/match/:matchid/mini
		//"https://sportimo.mod.bz/leaderpay/v1"
		API.GetAll (Main.Settings.apis ["leaderpay"] +"/Leaderboards/"+ Main.AppUser._id + "/" + Main.AppUser.country+"/match/"+ match_id+"/mini", OnGetMatchEnd);
	}

	private void OnGetMatchEnd(BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res) {
		Debug.Log ("Match End Mini Leaderboard: " + res.DataAsText);

		endMatchCtrl.endMatchHandler (res.DataAsText);

	}

	private void OnSegmentEventReceived (string json)
	{
		JsonData data = JsonMapper.ToObject (json);

		if ((bool)data ["data"] ["segment"] ["timed"]) {
			NGUITools.SetActive (clock, true);
			segment_label.text = "";
			timelabel.text = CalculateTimeFromEvent (data ["data"] ["segment"]);
			timecounting = true;
		} else {
			CancelInvoke ("TimeCount");
			CancelInvoke ("TimeCountFromSegment");
			PauseInstantCards();
			segment_label.text = LocalizationManager.FixRTL_IfNeeded( (string)data ["data"] ["segment"] ["text"] [LocalizationManager.CurrentLanguageCode]);
			timecounting = false;
			NGUITools.SetActive (clock, false);
			timelabel.text = "";
		}

		match.matchData.state++;
      
	}
    
	private void OnUpdatedEventReceived (string json)
	{
		JsonData data = JsonMapper.ToObject (json);
		GameEvent gevent = JsonMapper.ToObject<GameEvent> (data ["data"].ToJson ());
		string name_lookup = GetObjectName (gevent.created);

		foreach (Transform go in match_table.transform) {
			if (go.name.StartsWith (name_lookup)) {
				if (go.GetComponent<eventMatchCtrl> ().id == gevent._id) {
					UILabel[] labels = go.GetComponentsInChildren<UILabel> ();
					labels [1].text += "," +LocalizationManager.FixRTL_IfNeeded(  gevent.players [0].name [LocalizationManager.CurrentLanguageCode]);
					if (gevent.type == "Goal") {
						NGUITools.SetActive (lastGoalObj, true);
						last_goal = LocalizationManager.FixRTL_IfNeeded( gevent.players [0].name [LocalizationManager.CurrentLanguageCode]) + ", " + ReturnName (gevent.team) + " " + gevent.time.ToString () + "'";
						last_goal_label.text = last_goal;
					}
				}
			}
		}
	}
    
	private void OnMessageEventReceived (string json)
	{
		JsonData data = JsonMapper.ToObject (json);
		
		BedBugTools.loadImageOnUITextureAndResize (banner, data ["data"] ["url"].ToString (), 600, 150, playBannerAnim, 1);
		
        
        
	}
    

	private void OnCardResumedReceived (string json)
	{
		JsonData data = JsonMapper.ToObject (json);
		PlayCard card = JsonMapper.ToObject<PlayCard> (data ["data"].ToJson ());
		foreach (Transform trans in match_table.transform) {
			cardMatchCtrl ctrl = trans.GetComponent<cardMatchCtrl> ();
			if (ctrl != null) {
				if (ctrl._id == card.id) {
					ctrl.card = card;
					ctrl.Init();
				}
			}
		}
	}
	
	private void OnCardLostReceived (string json)
	{
		JsonData data = JsonMapper.ToObject (json);
		PlayCard card = JsonMapper.ToObject<PlayCard> (data ["data"].ToJson ());
		foreach (Transform trans in match_table.transform) {
			cardMatchCtrl ctrl = trans.GetComponent<cardMatchCtrl> ();
			if (ctrl != null) {
				if (ctrl._id == card.id) {
					// Check CARD if ctrl.card.specials.DoublePoints.status == 1
					if (ctrl.card.specials.DoublePoints.status == 1) {
						// Cansel invoke
						ctrl.cancelDoublePointsCounter();

						// Add to match specials
						specials++;
						specialsLeftLabel.text = specials.ToString();
						// Fire event to change the specials label
						FirePowerEvent(ctrl.card);
					}

					bannerAnimator.eventPlay ("cardLost");
					ctrl.FinalizeCard (card.pointsAwarded);
					StartCoroutine (ScrollToEvent (trans.gameObject));


				}
			}
		}
		for (int i = 0; i<match.playedCards.Count(); i++) {
			if (match.playedCards [i].id == card.id) {
				match.playedCards [i] = card;
			}
		}

		CountPlayedCards ();

		if (played_populated) {
			foreach (Transform trans in cards_table.transform) {
				cardMatchCtrl ctrl = trans.GetComponent<cardMatchCtrl> ();
				if (ctrl != null) {
					if (ctrl._id == card.id) {
						if (ctrl.card.specials.DoublePoints.status == 1) {
							// Cansel invoke
							ctrl.cancelDoublePointsCounter();
							// Add to match specials
							//							specials++;
							//							// Fire event to change the specials label
							//							FirePowerEvent(ctrl.card);
						}

						//bannerAnimator.eventPlay ("cardWon");
						ctrl.FinalizeCard (card.pointsAwarded);
						StartCoroutine (ScrollToEvent (trans.gameObject));
						//user_score.text = (int.Parse (user_score.text) + card.pointsAwarded).ToString ();
						// Check CARD if ctrl.card.specials.DoublePoints.status == 1

					}
				}
			}
		}
	}

	private PlayCard lastwoncard;
    
	private void OnCardWonReceived (string json)
	{
		
		JsonData data = JsonMapper.ToObject (json);
		PlayCard card = JsonMapper.ToObject<PlayCard> (data ["data"].ToJson ());
		lastwoncard = card;

		foreach (Transform trans in match_table.transform) {
			cardMatchCtrl ctrl = trans.GetComponent<cardMatchCtrl> ();
			if (ctrl != null) {
				if (ctrl._id == card.id) {
					print("Same Card Found");
					print("DoublePoints.status"+ctrl.card.specials.DoublePoints.status);
					// Check CARD if ctrl.card.specials.DoublePoints.status == 1
					if (ctrl.card.specials.DoublePoints.status == 1) {
						print("Card in Timeline has active DoublePoints");
						// Cansel invoke
						ctrl.cancelDoublePointsCounter();
						// Add to match specials
						specials++;
						specialsLeftLabel.text = specials.ToString();
						// Fire event to change the specials label
						FirePowerEvent(ctrl.card);
					}

					bannerAnimator.eventPlay ("cardWon");
					ctrl.FinalizeCard (card.pointsAwarded);
					StartCoroutine (ScrollToEvent (trans.gameObject));
					user_score.text = (int.Parse (user_score.text) + card.pointsAwarded).ToString ();


				}
			}
		}

		for (int i=0; i<match.playedCards.Count(); i++) {
			if (match.playedCards [i].id == card.id) {
				match.playedCards [i] = card;
			}
		}

		CountPlayedCards ();


		// CARDS PLAYED ------------------------------------------------------
		if (played_populated) {
			foreach (Transform trans in cards_table.transform) {
			
				cardMatchCtrl ctrl = trans.GetComponent<cardMatchCtrl> ();
				if (ctrl != null) {
					if (ctrl._id == card.id) {
						// Check CARD if ctrl.card.specials.DoublePoints.status == 1
						print("Same Card Found");
						print("DoublePoints.status"+ctrl.card.specials.DoublePoints.status);
						if (ctrl.card.specials.DoublePoints.status == 1) {
							print("Card in Playcards has active DoublePoints");
							// Cansel invoke
							ctrl.cancelDoublePointsCounter();
							// Add to match specials
							//							specials++;
							//							// Fire event to change the specials label
							//							FirePowerEvent(ctrl.card);
						}

						//bannerAnimator.eventPlay ("cardWon");
						Debug.Log ("card found, finalizing");
						ctrl.FinalizeCard (card.pointsAwarded);
						StartCoroutine (ScrollToEvent (trans.gameObject));
						//user_score.text = (int.Parse (user_score.text) + card.pointsAwarded).ToString ();
						// Check CARD if ctrl.card.specials.DoublePoints.status == 1

					}
				}
			}
		}

		for (int i = 0; i < match.playedCards.Count; i++) {

			print ("playCards "+i+" " + match.playedCards [i].id);
			if (match.playedCards [i].id == card._id || match.playedCards [i]._id == card._id) {
				// Update card in match.playcards
				match.playedCards [i] = card;

			}
		}

		if(!string.IsNullOrEmpty(Main.AppUser.social_id)){
            Debug.Log("Share");
			share = "card";
			FBModal.Instance.PopModal(shareWall, "You just won a "+lastwoncard.title["en"]+" card, share it with the world?");
		}
	}



	private void OnPresetActivatedReceived (string json)
	{
		JsonData data = JsonMapper.ToObject (json);
		PlayCard card = JsonMapper.ToObject<PlayCard> (data ["data"].ToJson ());
		print("Activate Preset Recieved, Playingpla card!!!!!!!!!!!!!!!!!!!!!!!!");
		print (json);
		foreach (PlayCard pcard in match.playedCards.ToList()) {
			print (pcard.id);
			print (card.id);
			if (pcard.id == card.id){
				print ("Card with the same id: "+card.primaryStatistic);
				print ("Card to be removed: "+pcard.primaryStatistic);
				match.playedCards.Remove (pcard); // Remove the old card so the new updated can take its place
			}
		}
		OnCardPlay (card);
	}
	
	private void OnStatsUpdateReceived (string json)
	{
		JsonData data = JsonMapper.ToObject (json);
		List<JsonData> stats = JsonMapper.ToObject<List<JsonData>> (data ["data"].ToJson ());
		PopulateStats (stats);
	}

	string share;

	private void OnFinalizeReceived(){
		share = "points";

//		if(!string.IsNullOrEmpty(Main.AppUser.social_id))
//		FBModal.Instance.PopModal(PopFBModal,"You won "+user_score.text+" points from this match, share it with the world?");

		// Show the EndMatch Animation
		endMatch();
	}


	private void PauseInstantCards(){
		foreach (Transform trans in match_table.transform) {
			cardMatchCtrl ctrl = trans.GetComponent<cardMatchCtrl> ();
			if (ctrl != null) {
				
				ctrl.cancelTimer();
			
			}
		}

		if (played_populated) {
			foreach (Transform trans in cards_table.transform) {

				cardMatchCtrl ctrl = trans.GetComponent<cardMatchCtrl> ();
				if (ctrl != null && ctrl.gameObject.activeSelf) {
					ctrl.cancelTimer();


				}
			}
			//cards_table.Reposition();
		}
	}
	
	private void PowerUpPlayed (PlayCard card)
	{
		Debug.Log(card._id);
		for (int i = 0; i < match.playedCards.Count(); i++) {
			if (match.playedCards [i].id == card._id) {
				match.playedCards [i] = card;
			}
		
		}
		foreach (Transform trans in match_table.transform) {
			cardMatchCtrl ctrl = trans.GetComponent<cardMatchCtrl> ();
			if (ctrl != null && ctrl.gameObject.activeSelf) {
//				Debug.Log ("got ctrl, updating specials count");
				ctrl.UpdateSpecials ();
				if (ctrl._id == card._id) {
					ctrl.card = card;
					ctrl.Init ();
					
				}
			}
		}
	
		if (played_populated) {
			foreach (Transform trans in cards_table.transform) {
			
				cardMatchCtrl ctrl = trans.GetComponent<cardMatchCtrl> ();
				if (ctrl != null && ctrl.gameObject.activeSelf) {
					ctrl.UpdateSpecials ();

					if (ctrl._id == card._id) {
						ctrl.card = card;
						ctrl.Init ();
					
					}
				}
			}
			//cards_table.Reposition();
		}

		// Update Specials Label
		specialsLeftLabel.text = specials.ToString();
	}
    

	private void DoublePointsActive (PlayCard card)
	{
//		Debug.Log("this runs");

		card.startPoints = card.startPoints * 2;
		//card.endPoints = card.endPoints * 2;

//		Debug.Log(card._id);

		for (int i = 0; i < match.playedCards.Count(); i++) {
			if (match.playedCards [i].id == card._id) {
				match.playedCards [i] = card;
			}
		
		}
		foreach (Transform trans in match_table.transform) {
			cardMatchCtrl ctrl = trans.GetComponent<cardMatchCtrl> ();
			if (ctrl != null && ctrl.gameObject.activeSelf) {
//				Debug.Log ("got ctrl, updating specials count");
				ctrl.UpdateSpecials ();
				if (ctrl._id == card._id) {
					ctrl.card = card;
					ctrl.Init ();
					
				}
			}
		}
	
		if (played_populated) {
			foreach (Transform trans in cards_table.transform) {
			
				cardMatchCtrl ctrl = trans.GetComponent<cardMatchCtrl> ();
				if (ctrl != null && ctrl.gameObject.activeSelf) {
					ctrl.UpdateSpecials ();

					if (ctrl._id == card._id) {
						ctrl.card = card;
						ctrl.Init ();
					
					}
				}
			}
			//cards_table.Reposition();
		}
		// Update Specials Label
		specialsLeftLabel.text = specials.ToString();
	}
	
	public GameObject[] tabsLines = new GameObject[4];

	public void tabSelect (GameObject lineParent)
	{
		if (populating)
			return;
		


		// CLOSE ALL LINE AND OPEN THE PRESSED ONE
		for (int i = 0; i<tabsLines.Length; i++) {
			NGUITools.SetActive (tabsLines [i], false);
		}

		NGUITools.SetActive (lineParent.transform.FindChild ("line").gameObject, true);
		StartCoroutine (PopulateCards (lineParent.name));

		// REDRAW THE SCROLLVIEW ACCORDING TO LINEPARENT
	}

	bool populating = false;
    
	private IEnumerator PopulateCards (string selection)
	{
		if (populating)
			yield break;
		else
			populating = true;

		//print ("populating: "+ populating);

		foreach (Transform trans in cards_table.GetChildList()) {
			if (trans.gameObject.activeSelf) {
				NGUITools.Destroy (trans);
			}
		}

		foreach (PlayCard testCard in match.playedCards) {
			print (testCard._id + " card has " + testCard.specials.DoublePoints.status + " Double Points STatus " + testCard.specials.DoublePoints.status);
		}

		//yield return new WaitForSeconds (1.0f);
		float placeHeight = 0;

		if (selection == "Won") {
			foreach (PlayCard card in match.playedCards.Where(card => card.status== 2 && card.pointsAwarded > 0).ToList()) {
				GameObject Card;
				if (card.cardType == "Instant" || card.cardType == "PresetInstant") {
					Card = NGUITools.AddChild (cards_table.gameObject, cardviewPrefab);
				} else {
					Card = NGUITools.AddChild (cards_table.gameObject, overallcardviewPrefab);

				}


				NGUITools.SetActive (Card, true);

				print ("card Status: " + card.status);
				if (card.status > 0) {
					//if(timecounting || !timecounting && card.cardType == "Instant")
					Card.GetComponent<Animation> ().Play ("cardshow1");
				}

				// Placement Animation
				Card.transform.localPosition = new Vector3 (0, placeHeight, 0);
				float CardY = Card.transform.localPosition.y;
				cardsScroll.ResetPosition ();
				CardY -= 62;
				placeHeight -= 64;
				iTween.MoveFrom (Card, iTween.Hash ("y", CardY, "time", 0.3f, "easetype", iTween.EaseType.easeOutCirc, "islocal", true, "onCompleteTarget", gameObject, "onComplete", "resetCardScroll")); 
				yield return new WaitForSeconds (0.1f);

				if (Card != null) {
					if (card.cardType == "PresetInstant" && card.status == 0)
						Card.name = card.minute.ToString();
					else
					Card.name = GetObjectName (DateTime.Parse (card.creationTime));
					
					cardMatchCtrl ctrl = Card.GetComponent<cardMatchCtrl> ();
					ctrl.card = card;
					ctrl.home_logo = match.matchData.home_team.logo;
					ctrl.away_logo = match.matchData.away_team.logo;
					ctrl.Init ();
				}
			}
		} else if (selection == "Pending") {
			foreach (PlayCard card in match.playedCards.Where(card=>card.status<2 || card.status==3).ToList()) {
				GameObject Card;
				if (card.cardType == "Instant" || card.cardType == "PresetInstant") {
					Card = NGUITools.AddChild (cards_table.gameObject, cardviewPrefab);
				} else {
					Card = NGUITools.AddChild (cards_table.gameObject, overallcardviewPrefab);

				}
				NGUITools.SetActive (Card, true);

				if (card.status > 0) {
					//if(timecounting || !timecounting && card.cardType == "Instant")
					Card.GetComponent<Animation> ().Play ("cardshow1");
				}
				// Placement Animation
				Card.transform.localPosition = new Vector3 (0, placeHeight, 0);
				float CardY = Card.transform.localPosition.y;

				CardY -= 62;
				placeHeight -= 64;
				iTween.MoveFrom (Card, iTween.Hash ("y", CardY, "time", 0.3f, "easetype", iTween.EaseType.easeOutCirc, "islocal", true, "onCompleteTarget", gameObject, "onComplete", "resetCardScroll")); 
				yield return new WaitForSeconds (0.1f);

				if (Card != null) {
					if (card.cardType == "PresetInstant" && card.status == 0)
						Card.name = card.minute.ToString();
					else
						Card.name = GetObjectName (DateTime.Parse (card.creationTime));
					
					cardMatchCtrl ctrl = Card.GetComponent<cardMatchCtrl> ();
					ctrl.card = card;
					ctrl.home_logo = match.matchData.home_team.logo;
					ctrl.away_logo = match.matchData.away_team.logo;
					ctrl.Init ();
				}
				cardsScroll.ResetPosition ();
			}
		} else if (selection == "Lost") {
			foreach (PlayCard card in match.playedCards.Where(card=>card.status==2 && card.pointsAwarded==0).ToList()) {	
				GameObject Card;
				if (card.cardType == "Instant"  || card.cardType == "PresetInstant") {
					Card = NGUITools.AddChild (cards_table.gameObject, cardviewPrefab);
				} else {
					Card = NGUITools.AddChild (cards_table.gameObject, overallcardviewPrefab);

				}
				NGUITools.SetActive (Card, true);

				print ("card Status: " + card.status);
				if (card.status > 0) {
					//if(timecounting || !timecounting && card.cardType == "Instant")
					Card.GetComponent<Animation> ().Play ("cardshow1");
				}
				// Placement Animation
				Card.transform.localPosition = new Vector3 (0, placeHeight, 0);
				float CardY = Card.transform.localPosition.y;
				cardsScroll.ResetPosition ();
				CardY -= 62;
				placeHeight -= 64;
				iTween.MoveFrom (Card, iTween.Hash ("y", CardY, "time", 0.3f, "easetype", iTween.EaseType.easeOutCirc, "islocal", true, "onCompleteTarget", gameObject, "onComplete", "resetCardScroll")); 
				yield return new WaitForSeconds (0.1f);

				if (Card != null) {
					if (card.cardType == "PresetInstant" && card.status == 0)
						Card.name = card.minute.ToString();
					else
						Card.name = GetObjectName (DateTime.Parse (card.creationTime));
					
					cardMatchCtrl ctrl = Card.GetComponent<cardMatchCtrl> ();
					ctrl.card = card;
					ctrl.home_logo = match.matchData.home_team.logo;
					ctrl.away_logo = match.matchData.away_team.logo;
					ctrl.Init ();
				}
			}
		} else {
			yield return new WaitForSeconds (1.0f); // Wait a litle so the old cards are removed before running the code 

			foreach (PlayCard card in match.playedCards) { // Error when 2 Presets are played at the same min
				GameObject Card;
				if (card.cardType == "Instant"  || card.cardType == "PresetInstant") {
					Card = NGUITools.AddChild (cards_table.gameObject, cardviewPrefab);
				} else {
					Card = NGUITools.AddChild (cards_table.gameObject, overallcardviewPrefab);

				}

				NGUITools.SetActive (Card, true);

//				print ("card Status: "+card.status);
//				print (" "+timecounting);
//				print (" "+card.cardType);
				if (card.status > 0) {
//					if(timecounting || !timecounting && card.cardType == "Instant")
					Card.GetComponent<Animation> ().Play ("cardshow1");
				}
//				} else if(card.status == 0 && card.cardType != "Instant")
//					Card.GetComponent<Animation>().Play ("cardshow1");

				// Placement Animation
				Card.transform.localPosition = new Vector3 (0, placeHeight, 0);
				float CardY = Card.transform.localPosition.y;
				cardsScroll.ResetPosition ();
				CardY -= 62;
				placeHeight -= 64;
				iTween.MoveFrom (Card, iTween.Hash ("y", CardY, "time", 0.3f, "easetype", iTween.EaseType.easeOutCirc, "islocal", true, "onCompleteTarget", gameObject, "onComplete", "resetCardScroll")); 
				yield return new WaitForSeconds (0.1f);

				if (Card != null) {
					if (card.cardType == "PresetInstant" && card.status == 0)
						Card.name = card.minute.ToString();
					else
						Card.name = GetObjectName (DateTime.Parse (card.creationTime));
					
					cardMatchCtrl ctrl = Card.GetComponent<cardMatchCtrl> ();
					ctrl.card = card;
					ctrl.home_logo = match.matchData.home_team.logo;
					ctrl.away_logo = match.matchData.away_team.logo;
					ctrl.Init ();
				}
			}
		}
			
		//cards_table.Reposition ();
		cardsScroll.ResetPosition ();
		populating = false;
		//cards_table.transform.parent.gameObject.GetComponent<UIScrollView> ().ResetPosition ();
	}

	void resetCardScroll ()
	{
		cardsScroll.ResetPosition ();
	}
  
    
	//Utility/UI Methods
    
    
	public static string GetObjectName (DateTime created)
	{

		return created.ToString ("yyyyMMddHHmmss");
		

//
//		string name;
//		string minute_s;
//		if (obj_count < 10) {
//			name = created.ToString ("ddHHmmss") + "00" + obj_count.ToString ();
//			;
//		
//		} else if (obj_count < 100) {
//			name = created.ToString ("ddHHmmss") + "0" + obj_count.ToString ();
//
//		} else {
//			name = created.ToString ("ddHHmmss") + obj_count.ToString ();
//
//		}
//
//		obj_count++;
//		return name;
	}

	private void CalculateTime (int state)
	{

		if (match.matchData.timeline [state].timed) {
			NGUITools.SetActive (clock, true);
			segment_label.text = "";
			DateTime start = match.matchData.timeline [state].start;
			start = start.ToUniversalTime ();
            

			if ((DateTime.UtcNow - start).Hours > 0) {
				minutes = (DateTime.UtcNow - start).Hours * 60;
			}
			minutes = minutes + (DateTime.UtcNow - start).Minutes;
			seconds = (DateTime.UtcNow - start).Seconds;
			minutes = minutes + match.matchData.timeline [state].sport_start_time;
			timecounting = true;
			InvokeRepeating ("TimeCount", 1, 1);
			timelabel.text = minutes.ToString ();// + ":" + seconds.ToString ();
		} else {
			timecounting = false;
			if (state != 0) {
				timelabel.text = "";
				NGUITools.SetActive (clock, false);
				segment_label.text = LocalizationManager.FixRTL_IfNeeded( match.matchData.timeline [state].text [getUsedLang(match.matchData.timeline [state].text)]);
			} else {
				segment_label.text = LocalizationManager.FixRTL_IfNeeded( match.matchData.timeline [state].text [getUsedLang(match.matchData.timeline [state].text)]);
				NGUITools.SetActive (clock, false);
				timelabel.text = "";
			}
		}
	}

	DateTime start;
	int start_time;
	private static int _match_minute = 1;

	public static int match_minute {
		get{ return _match_minute;}
		set {
//			Debug.Log("Match time set to: "+ value);
			if (value == 0)
				_match_minute = 1;
			else
				_match_minute = value;
		}
	}

	private string CalculateTimeFromEvent (JsonData data)
	{
		if ((bool)data ["timed"]) {
			// DateTime start;
			// DateTime.TryParse((string)data["start"],out start);
			start = DateTime.Parse (data ["start"].ToString ());
			start_time = int.Parse (data ["sport_start_time"].ToString ());
			InvokeRepeating ("TimeCountFromSegment", 1, 1);
			return minutes.ToString () + ":" + seconds.ToString ();
		} else {
			return (string)data ["name"] [LocalizationManager.CurrentLanguageCode];
		}
	}

	public int minutes;
	int seconds;
	int hours = 0;

	private void TimeCount ()
	{
		if ((DateTime.UtcNow - start).Hours > 0) {
			hours = (DateTime.UtcNow - match.matchData.timeline [match.matchData.state].start.ToUniversalTime ()).Hours * 60;
		}
		seconds = (DateTime.UtcNow - match.matchData.timeline [match.matchData.state].start.ToUniversalTime ()).Seconds;
		minutes = hours + (DateTime.UtcNow - match.matchData.timeline [match.matchData.state].start.ToUniversalTime ()).Minutes + match.matchData.timeline [match.matchData.state].sport_start_time;
//		if (minutes < 10 && seconds < 10) {
//			timelabel.text = "0" + minutes.ToString () + ":" + "0" + seconds.ToString ();
//		} else if (minutes < 10) {
//			timelabel.text = "0" + minutes.ToString () + ":" + seconds.ToString ();
//		} else if (seconds < 10) {
//			timelabel.text = minutes.ToString () + ":" + "0" + seconds.ToString ();
//		} else {
//			timelabel.text = minutes.ToString () + ":" + seconds.ToString (); 
//		}
        
		if (minutes < 10 && seconds < 10) {
			timelabel.text = (minutes + 1).ToString () + "'";
		} else if (minutes < 10) {
			timelabel.text = (minutes + 1).ToString () + "'";
		} else if (seconds < 10) {
			timelabel.text = (minutes + 1).ToString () + "'";
		} else {
			timelabel.text = (minutes + 1).ToString () + "'"; 
		}
    
       
	}
	
	private void TimeCountFromSegment ()
	{
		seconds = (DateTime.UtcNow - start.ToUniversalTime ()).Seconds;
		minutes = (DateTime.UtcNow - start.ToUniversalTime ()).Minutes + start_time;

//	
//		if (minutes < 10 && seconds < 10) {
//			timelabel.text = "0" + minutes.ToString () + ":" + "0" + seconds.ToString ();
//		} else if (minutes < 10) {
//			timelabel.text = "0" + minutes.ToString () + ":" + seconds.ToString ();
//		} else if (seconds < 10) {
//			timelabel.text = minutes.ToString () + ":" + "0" + seconds.ToString ();
//		} else {
//			timelabel.text = minutes.ToString () + ":" + seconds.ToString (); 
//		}

		match_minute = minutes;

		if (minutes < 10 && seconds < 10) {
			timelabel.text = (minutes + 1).ToString () + "'";
		} else if (minutes < 10) {
			timelabel.text = (minutes + 1).ToString () + "'";
		} else if (seconds < 10) {
			timelabel.text = (minutes + 1).ToString () + "'";
		} else {
			timelabel.text = (minutes + 1).ToString () + "'"; 
		}
        
        
    
       
	}
    
	public void openMatchStats ()
	{
		NGUITools.SetActive (matchStats, true);
		NGUITools.SetActive (livegame, false);
		NGUITools.SetActive (playedCards, false);
	}
	
	public void openLiveMatch ()
	{
		NGUITools.SetActive (matchStats, false);
		NGUITools.SetActive (livegame, true);
	}

	public void openLiveAction ()
	{
		TweenAlpha.Begin (livegame, 0.3f, 1);
		TweenAlpha.Begin (playedCards, 0.3f, 0);
		//RepopulateLiveCards();
		//NGUITools.SetActive (livegame, true);
		//NGUITools.SetActive (playedCards, false);
	}

	public void playedCardsAction ()
	{
		played_populated = true;
		CountPlayedCards ();
		tabSelect (tabsLines [0].transform.parent.gameObject);
		TweenAlpha.Begin (livegame, 0.5f, 0);
		TweenAlpha.Begin (playedCards, 0.5f, 1);
		
		//NGUITools.SetActive (livegame, false);
		//SetActive (playedCards, true);
	}
	
	public void CountPlayedCards ()
	{
//		Debug.Log("counting");
		if (match.playedCards.Count == 0) {
			for (int i = 0; i < 2; i++) {
				NGUITools.SetActive (cardcounters [i], false);
			}
		}
		
		if (match.playedCards.Where (card => card.status == 2 && card.pointsAwarded > 0).ToList ().Count == 0) {
			NGUITools.SetActive (cardcounters [0], false);

		} else {
			NGUITools.SetActive (cardcounters [0], true);
			cardcounters [0].GetComponentInChildren<UILabel> ().text = match.playedCards.Where (card => card.status == 2 && card.pointsAwarded > 0).ToList ().Count.ToString ();
		}
		
		if (match.playedCards.Where (card => card.status == 2 && card.pointsAwarded == 0).ToList ().Count == 0) {
			NGUITools.SetActive (cardcounters [1], false);

		} else {
			NGUITools.SetActive (cardcounters [1], true);

			cardcounters [1].GetComponentInChildren<UILabel> ().text = match.playedCards.Where (card => card.status == 2 && card.pointsAwarded == 0).ToList ().Count.ToString ();
		}
		if (match.playedCards.Where (card => card.status == 1 || card.status == 0).ToList ().Count == 0) {
			NGUITools.SetActive (cardcounters [2], false);

		} else {
			NGUITools.SetActive (cardcounters [2], true);
			cardcounters [2].GetComponentInChildren<UILabel> ().text = match.playedCards.Where (card => card.status == 1 || card.status == 0).ToList ().Count.ToString ();
		}
		//card
	}
    
	public void closeCardPlay ()
	{
		cardPlay.GetComponent<PlayCardCtrl> ().goBack ();
		
	}

	public void openOption (GameObject Go)
	{	
//		print (Go.name);
//		print (optionOpened [Go.name]);
		if (!optionOpened [Go.name] || Go == optionGOs [1]) {
			for (int i = 0; i < optionGOs.Length; i++) {
				changeScrollviewAlpha (optionGOs [i], "srollview", 0.0f, true);
				//NGUITools.SetActive(optionGOs[i], false );
				TweenAlpha.Begin (optionGOs [i], 0.3f, 0);
				//changeScrollviewAlpha(optionGOs[i], "srollview", 0.0f, true);
			}

			//NGUITools.SetActive (Go, true);
			iTween.MoveFrom (Go, iTween.Hash ("y", -460, "time", 0.5f, "easetype", iTween.EaseType.easeOutCirc, "islocal", true)); 
			TweenAlpha.Begin (Go, 0.4f, 1);
			changeScrollviewAlpha (Go, "srollview", 1.0f, false);

			foreach (GameObject go in optionGOs) {
				optionOpened [go.name] = false;
			}
			optionOpened [Go.name] = true;

		} else {
			// Close the Go if we know its open
			optionOpened [Go.name] = false;
			// Open the livegame
			openOption (optionGOs [1]);
		}
	}
	
	public void OpenTeamInfo (GameObject go)
	{
		if (go.name == "Team1") {
			Main.Instance.openView ("teaminfo", match.matchData.home_team.id);
		} else {
			Main.Instance.openView ("teaminfo", match.matchData.away_team.id);
		}
	}

	public void changeScrollviewAlpha (GameObject parent, string tag, float alpha, bool instant)
	{
		Transform tr = parent.transform;
		
		foreach (Transform child in tr) {
			if (child.gameObject.tag == "scrollview") {
				//child.GetComponent<UIPanel>().alpha = alpha;
				if (!instant)
					TweenAlpha.Begin (child.gameObject, 1.0f, alpha);
				else
					child.GetComponent<UIPanel> ().alpha = 0;
			}
		}
	}
	
	public string ReturnTextureUrl (string team)
	{
          
        
		if (team == "home_team") {
			return match.matchData.home_team.logo;
		} else {
			return match.matchData.away_team.logo;
		}
        
       
	}
    
	private string ReturnName (string team)
	{
		if (team == "home_team" || team == "home_team_name") {
			return home_team.GetComponentInChildren<UILabel> ().text;
		} else {
			return away_team.GetComponentInChildren<UILabel> ().text;
		}
        
	}
    
	public static string stReturnName (string team)
	{
		if (team == "[[home_team_name]]") {
			return home_name;
		} else {
			return away_name;
		}
        
	}
  
	void playBannerAnim (UITexture texture)
	{
		print ("playung banner animation");
		BannerAnim.Play ("banner");
	}
    
	private void FadeInOnLoad (UITexture texture)
	{
		if (texture) {
			UITweener tween = texture.GetComponent<UITweener> ();

			if (tween)
				tween.PlayForward ();
			else
				texture.alpha = 1;
		}
	}
	
	public static void FirePowerEvent (PlayCard card)
	{
		OnPowerUp (card);
		
	}

	public static void FireDoublePointsEvent(PlayCard card){
		onDoublePointsActivated(card);
	}

	public void openCardInfo (UILabel cardName)
	{
		print ("This is a " + cardName + " Card");
		cardInfoAnim.Play ("cardInfo");
		NGUITools.SetActive (cardInfoAnim.gameObject, true);
	}

	public void closeCardInfo ()
	{
		NGUITools.SetActive (cardInfoAnim.gameObject, false);
	}
    	public void PopFBModal(string answer){
		if(answer=="yes"){
			Loader.Visible(true);
			FacebookConnect();
		}
		else{
			return;
		}
    }

    public void shareWall(string answer)
    {
        if (answer == "yes")
        {
            Debug.Log("ANSWER YES");
            if (FB.IsLoggedIn)
            {
                Debug.Log("LOGGED IN");
                if (share == "card")
                    FB.FeedShare(null, new Uri("http://www.sportimo.com"), "Sportimo MBC", "Sportimo MBC", "I just won a " + lastwoncard.title["en"] + " card on Sportimo MBC", new Uri(lastwoncard.image["url"].ToString()),"",shareCallback);
                else if (share == "points")
                    FB.FeedShare(null, new Uri("http://www.sportimo.com"), "Sportimo MBC", "Sportimo MBC", "I just won " + user_score.text + " points on Sportimo MBC on " + match.matchData.home_team.name["en"] + " - " + match.matchData.away_team.name["en"] + ".",null,"", shareCallback);
            }
            else
                FacebookConnect();
        }
      
    }


public void FacebookConnect(){
        Debug.Log("YES");
        if (!FB.IsInitialized)
			FB.Init(FBLogin);
		else{
			FBLogin();
		}
	}

	public void FBLogin(){
        Debug.Log("YES");
        if (FB.IsLoggedIn){
            Debug.Log("YES");
            if (!string.IsNullOrEmpty(share)){
                Debug.Log("YES");
                //MessagePanel.Instance.animMessage("Facebook account already connected.");
                if (share=="card")
                    FB.FeedShare(null, new Uri("http://www.sportimo.com"), "Sportimo MBC", "Sportimo MBC", "I just won a " + lastwoncard.title["en"] + " card on Sportimo MBC", new Uri(lastwoncard.image["url"].ToString()), "", shareCallback);
                //FB.FeedShare(null, new Uri("http://www.sportimo.com"), "Sportimo MBC", "Sportimo MBC", "I just won a " + "test" + " card on Sportimo MBC", null, "", shareCallback);
                else if(share=="points"){
				FB.FeedShare(null,new Uri("http://www.sportimo.com"),"Sportimo MBC","Sportimo MBC","I just won "+user_score.text+" points on Sportimo MBC on "+match.matchData.home_team.name["en"]+" - "+match.matchData.away_team.name["en"]+".",null,"", shareCallback);
                    share = null;
				}
			}
			
			FB.API ("me?fields=id,name,email", HttpMethod.GET, OnGetData);
			}
		else{
			FB.LogInWithReadPermissions(new List<string>(){"email,user_friends"},OnFBLogin);
		}
	}

    private void shareCallback(IShareResult result)
    {
        Debug.Log(result);
    }

    public void OnFBLogin(ILoginResult fbresult){
        if (fbresult.Error != null)
        {
            Debug.Log(fbresult.Error);
            Loader.Visible(false);
        }
        else {
            //Debug.Log (fbresult.RawResult);
            JsonData data = JsonMapper.ToObject(fbresult.RawResult);

            PlayerPrefs.SetString("social_id", AccessToken.CurrentAccessToken.UserId);
            Hashtable fbtable = new Hashtable();
            fbtable.Add("social_id", data["user_id"].ToString());
            API.PutOne(Main.Settings.apis["users"] + "/" + Main.AppUser._id, fbtable, OnConnectCallback, Main.AppUser.token);

            if (!string.IsNullOrEmpty(share))
            {
                Debug.Log("Time to share something!");
                //MessagePanel.Instance.animMessage("Facebook account already connected.");
                if (share == "card")
                    FB.FeedShare(null, new Uri("http://www.sportimo.com"), "Sportimo MBC", "Sportimo MBC", "I just won a " + lastwoncard.title["en"] + " card on Sportimo MBC", new Uri(lastwoncard.image["url"].ToString()), "", shareCallback);
                //FB.FeedShare(null, new Uri("http://www.sportimo.com"), "Sportimo MBC", "Sportimo MBC", "I just won a " + "test" + " card on Sportimo MBC",null, "", shareCallback);
                else if (share == "points")
                {
                    FB.FeedShare(null, new Uri("http://www.sportimo.com"), "Sportimo MBC", "Sportimo MBC", "I just won " + user_score.text + " points on Sportimo MBC on " + match.matchData.home_team.name["en"] + " - " + match.matchData.away_team.name["en"] + ".", null, "", shareCallback);
                    share = null;
                }
            }
        }
	}
		public void OnGetData(IGraphResult result){
		if (result.Error != null) {
			Debug.Log (result.Error);
			Loader.Visible(false);
		} else {
			JsonData data = JsonMapper.ToObject (result.RawResult);
			Hashtable fbtable=new Hashtable();
			fbtable.Add ("social_id", data ["id"].ToString ());
			API.PutOne(Main.Settings.apis["users"]+"/"+Main.AppUser._id,fbtable,OnConnectCallback,Main.AppUser.token);
		}
	}

	public void OnConnectCallback(BestHTTP.HTTPRequest req,BestHTTP.HTTPResponse res){
	Debug.Log(res.DataAsText);
		if(res.StatusCode==200){
			MessagePanel.Instance.animMessage("Facebook connected successfully");
			PlayerPrefs.SetString ("social_id", AccessToken.CurrentAccessToken.UserId);
			Main.AppUser.social_id = AccessToken.CurrentAccessToken.UserId;
			//FB.FeedShare(null,new Uri("http://www.sportimo.com"),"Sportimo MBC","Sportimo MBC","I just won a card on Sportimo MBC");

		}
		else{
			FB.LogOut();
			MessagePanel.Instance.animMessage("Facebook id already connected to other account");
		}

	}

	string getUsedLang(Dictionary<string,string> dictionary) {
		string usedLang;

		if (dictionary.ContainsKey (LocalizationManager.CurrentLanguageCode))
			usedLang = LocalizationManager.CurrentLanguageCode;
		else
			usedLang = "en";

		return usedLang;
	}

 
}
