﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BedbugRest;
using BestHTTP;
using System;
using LitJson;
using I2.Loc;
using Facebook.Unity;
public class LeadersNewCtrl: View
{

	public GameObject leader_user;
	public GameObject user_light;
	public GameObject user_dark;

	public Animation bannerAdAnim;
	public UITexture bannerPic;
	public bool bannerOpened = false;
	public bool sponsoredOpened = false;
	public bool fbBannerOpened = false;

	GameObject leader;
	public GameObject user_score;
	public UIGrid leadersgrid;
	public bool populated = false;
	private readonly string endpoint = "https://sportimo.mod.bz/leaderpay/v1/leaderboards/";
	private static string leaderboards_endpoint = "/leaderboards/";
	public UIScrollView LeaderboardScrollview;
	private Leaderboard SponsoredLeaders;
	private Leaderboard SeasonLeaders;
	private Leaderboard FriendsLeaders;
	private bool isPopulating = false;

	private bool friends=false;

	public GameObject InvButton;

	public UITexture userPicTex;
	public LeadersNewCtrl ()
	{
//		Debug.Log("Create a Leaders View");
	}
    
     

	//These methods are overrides and therefore
	//can override any virtual methods in the parent
	//class.
 

  
	private void onTransitionInStart (viewData _viewdata = null)
	{
		Loader.Visible (true);
		// TODO Important CHANGE GR TO CURRENT COUNTRY
		//API.GetAll (Main.Settings.apis ["leaderpay"] + leaderboards_endpoint + Main.AppUser._id + "/GR" + "/allplayers", OnGetSeasonLeaders); 
		API.GetAll (Main.Settings.apis ["leaderpay"] + "pools/for/country/"+"gr", OnGetAllLeaderboards); 

    	if (bannerOpened) {
			bannerOpened = false;
			playBannerAnim (0.5f, -1);
		}

	}

	private void OnGetAllLeaderboards (HTTPRequest req, HTTPResponse res) {
		print ("All leaderboards: "+res.DataAsText);
	}

	public void OpenSponsoredLeaders ()
	{
		// TODO Important CHANGE GR TO CURRENT COUNTRY
		API.GetAll (Main.Settings.apis ["leaderpay"] + leaderboards_endpoint + Main.AppUser._id + "/GR" + "/allplayers", OnGetSponsoredLeaders); 
	}

	public void OpenFriendsLeaders ()
	{
		foreach (Transform transform in leadersgrid.GetChildList()) {
			if (transform.gameObject.activeSelf)
				NGUITools.Destroy (transform);
		}

		leadersgrid.Reposition ();
		LeaderboardScrollview.ResetPosition ();


		if(FB.IsLoggedIn){	
			Loader.Visible (true);
			FB.API ("me?fields=friends", HttpMethod.GET, OnGetFriends);
		}else if(string.IsNullOrEmpty(PlayerPrefs.GetString("social_id")) && string.IsNullOrEmpty(Main.AppUser.social_id)){
			FBModal.Instance.PopModal(PopFBModal);
		}
		else{
			BlockInput.set (true);
			FacebookConnect();
		}
	}


	public void PopFBModal(string answer){
		if(answer=="no"){
			return;
		}
		else{
			Loader.Visible(true);
			FacebookConnect();
		}
	}

	public void FacebookConnect(){
		if(!FB.IsInitialized)
			FB.Init(FBLogin);
		else{
			FBLogin();
		}
	}

	public void FBLogin(){
		if(FB.IsLoggedIn){

			Debug.Log (FB.IsLoggedIn);
			if(!string.IsNullOrEmpty(Main.AppUser.social_id)){
				FB.API ("me?fields=id,name,email", HttpMethod.GET, OnGetData);
				return;
			}
			
			}
		else{
			Debug.Log (FB.IsLoggedIn);
			FB.LogInWithReadPermissions(new List<string>(){"email,user_friends"},OnFBLogin);
		}
	}

	public void OnFBLogin(ILoginResult fbresult){

		BlockInput.set (false);

		if (fbresult.Error != null) {
			Debug.Log (fbresult.Error);
			Loader.Visible(false);
		} else {
			
//			Debug.Log (fbresult.RawResult);
			JsonData data = JsonMapper.ToObject (fbresult.RawResult);
			if (data.Keys.Contains ("user_id")) {
				Hashtable fbtable = new Hashtable ();
				fbtable.Add ("social_id", data ["user_id"].ToString ());
				API.PutOne (Main.Settings.apis ["users"] + "/" + Main.AppUser._id, fbtable, OnConnectCallback, Main.AppUser.token);
			} else {
				Loader.Visible(false);
			}
	}
	}
	public void OnGetData(IGraphResult result){
		if (result.Error != null) {
			Debug.Log (result.Error);
			Loader.Visible(false);
		} else {
			JsonData data = JsonMapper.ToObject (result.RawResult);
			Hashtable fbtable=new Hashtable();
			fbtable.Add ("social_id", data ["id"].ToString ());
			API.PutOne(Main.Settings.apis["users"]+"/"+Main.AppUser._id,fbtable,OnConnectCallback,Main.AppUser.token);
		}
	}

	public void OnConnectCallback(BestHTTP.HTTPRequest req,BestHTTP.HTTPResponse res){
		Debug.Log(res.DataAsText);
		if(res.IsSuccess){
			PlayerPrefs.SetString ("social_id", AccessToken.CurrentAccessToken.UserId);
			FB.API ("me?fields=friends", HttpMethod.GET, OnGetFriends);
			Main.AppUser.social_id = AccessToken.CurrentAccessToken.UserId;

		}
		else{
			FB.LogOut();
			MessagePanel.Instance.animMessage (res.DataAsText);
		}

	}

	public void InviteFriends(){
		FB.Mobile.AppInvite(new Uri("https://fb.me/1762114710698244"),null,OnInviteFinished);
	}
	
	public void OnInviteFinished(IAppInviteResult result){
		Debug.Log(result.RawResult);
	}
	private void OnGetFriends (IGraphResult result)
	{
		List<string> friends = new List<string> ();
		Debug.Log ("FB - Received friends");
		JsonData data = JsonMapper.ToObject (result.RawResult);
		if (data ["friends"] ["data"][0].Count > 0) {
			Debug.Log(data ["friends"] ["data"].ToJson());
			List<Friend> fbdata=JsonMapper.ToObject<List<Friend>>(data ["friends"] ["data"].ToJson());
			foreach (Friend friend in fbdata) {
				friends.Add (friend.id);
			}
		}
		friends.Add (Main.AppUser.social_id);
		Hashtable table = new Hashtable ();
		table.Add ("friends", friends);
		API.PostOne (Main.Settings.apis ["leaderpay"] + "/leaderboards/" + Main.AppUser._id + "/friends", table, OnGetFriendsLeaders);
	
	}

	private void OnGetFriendsLeaders (HTTPRequest req, HTTPResponse res)
	{
		Loader.Visible(false);
		Debug.Log (res.DataAsText);
		FriendsLeaders = JsonMapper.ToObject<Leaderboard> (res.DataAsText);
		StartCoroutine (PopulateLB (FriendsLeaders));
		friends=true;

	}
    
    
//    private void OnGetWeekLeaders(HTTPRequest req,HTTPResponse res){
//		if(res.StatusCode != 200 && res.StatusCode != 304){
//			MessagePanel.Instance.animMessage(res.DataAsText);
//			return;
//		}
//		//Loader.Visible (true);
//       WeekLeaders = JsonMapper.ToObject<Leaderboard>(res.DataAsText);
//    }
    
	private void OnGetSeasonLeaders (HTTPRequest req, HTTPResponse res)
	{
		Loader.Visible (false);

		if (res.StatusCode != 200 && res.StatusCode != 304) {
			MessagePanel.Instance.animMessage (res.DataAsText);
			return;
		}
		Debug.Log (res.DataAsText);
//		Debug.Log (req.Uri);
		//Loader.Visible (true);
		SeasonLeaders = JsonMapper.ToObject<Leaderboard> (res.DataAsText);

		if (bannerOpened) {
			bannerOpened = false;
			playBannerAnim (0.5f, 0);
		}
		friends=false;
//       if(!populated){
		StartCoroutine (PopulateLB (SeasonLeaders));
//       populated = true;
//       
	}

	private void OnGetSponsoredLeaders (HTTPRequest req, HTTPResponse res)
	{
		Loader.Visible (false);
		
		if (res.StatusCode != 200 && res.StatusCode != 304) {
			MessagePanel.Instance.animMessage (res.DataAsText);
			return;
		}
		//		Debug.Log (res.DataAsText);
		//		Debug.Log (req.Uri);
		//Loader.Visible (true);
		SponsoredLeaders = JsonMapper.ToObject<Leaderboard> (res.DataAsText);
		//       if(!populated){
			friends=false;
		StartCoroutine (PopulateLB (SponsoredLeaders));
		//       populated = true;
		//       }
	}
    
	// private void OnGetCustomLeaders(HTTPRequest req,HTTPResponse res){
	// 	if(res.StatusCode!=200 && res.StatusCode!=304){
	// 		MessagePanel.Instance.animMessage(res.DataAsText);
	// 		return;
	// 	}
	// 	//Loader.Visible (true);
	//   CustomLeaders = JsonMapper.ToObject<Leaderboard>(res.DataAsText);
	// }

	public override void selectionChanged (string selection)
	{
		base.selectionChanged (selection);
		//Loader.Visible (true);
		print (selection + " This was fired from the Leaders view");
		// StartCoroutine( PopulateLB(selection));

	}
    
  


	// TAB METHODS
	public GameObject[] tabsLines = new GameObject[3];

	public void tabSelect (GameObject lineParent)
	{

		// Dont change if populating
		if (isPopulating)
			return;

		if (lineParent.name == "all") {
			onTransitionInStart (null);
		} else if (lineParent.name == "friends") {
			
				OpenFriendsLeaders ();
			

			
		} else if (lineParent.name == "custom") {
			OpenSponsoredLeaders ();
		}

		// CLOSE ALL LINE AND OPEN THE PRESSED ONE
		for (int i = 0; i<tabsLines.Length; i++) {
			NGUITools.SetActive (tabsLines [i], false);
		}

		NGUITools.SetActive (lineParent.transform.FindChild ("line").gameObject, true);
		selectionChanged (lineParent.name);

		
		if (lineParent.name != "custom")
			closeAdBanner ();
		else
			openAdBanner ();

		if (lineParent.name != "friends")
			closeFBBanner ();
		else
			openFBBanner ();
		// REDRAW THE SCROLLVIEW ACCORDING TO LINEPARENT
	}

	

	void openAdBanner ()
	{
		sponsoredOpened = true;
		bannerAdAnim ["bannerAnimOpen"].time = 0;
		bannerAdAnim ["bannerAnimOpen"].speed = 1;
		bannerAdAnim.Play ("bannerAnimOpen");
	}

	void closeAdBanner ()
	{
		if (sponsoredOpened) {
			bannerAdAnim ["bannerAnimOpen"].time = 0.5f;
			bannerAdAnim ["bannerAnimOpen"].speed = -1;
			bannerAdAnim.Play ("bannerAnimOpen");
			sponsoredOpened = false;
		}
	}

	void openFBBanner ()
	{
		fbBannerOpened = true;
		bannerAdAnim ["bannerAnimOpen2"].time = 0;
		bannerAdAnim ["bannerAnimOpen2"].speed = 1;
		bannerAdAnim.Play ("bannerAnimOpen2");
	}

	void closeFBBanner ()
	{
		if (fbBannerOpened) {
			bannerAdAnim ["bannerAnimOpen2"].time = 0.5f;
			bannerAdAnim ["bannerAnimOpen2"].speed = -1;
			bannerAdAnim.Play ("bannerAnimOpen2");
			fbBannerOpened = false;
		}
	}
    
	//TODO populate user if score.user_id=User.id
	public IEnumerator PopulateLB (Leaderboard board)
	{
		// Close the buttton while a list is being created
		if (isPopulating)
			yield break;
		else
			isPopulating = true;

		foreach (Transform transform in leadersgrid.GetChildList()) {
			if (transform.gameObject.activeSelf)
				NGUITools.Destroy (transform);
		}

		leadersgrid.Reposition ();
		LeaderboardScrollview.ResetPosition ();

		if (board.sponsor != null) {
			BedBugTools.loadImageOnUITextureAndResize (bannerPic, board.sponsor.banner,320,75, openBanner, 1);
		}

		float placeHeight = 80;

		if (board.user != null) {


			if (!string.IsNullOrEmpty( board.user.pic) ) {

				BedBugTools.loadImageOnUITextureAndResize (userPicTex, board.user.pic, 50, 50, FadeInOnLoad);
			} else {
				string baseUrl = "https://s3-eu-west-1.amazonaws.com/sportimo-media/avatars/playerProfile.png";
				BedBugTools.loadImageOnUITextureAndResize (userPicTex, baseUrl, 50,50 ,FadeInOnLoad, 0);
			}
			if((int)board.user.rank > 100)
				user_score.transform.FindChild ("index").GetComponentInChildren<UILabel> ().text = LocalizationManager.GetTermTranslation("_leaderboard_you",true);
			else
				user_score.transform.FindChild ("index").GetComponentInChildren<UILabel> ().text = board.user.rank.ToString ();

			user_score.transform.FindChild ("points").GetComponentInChildren<UILabel> ().text = board.user.score.ToString ();
			user_score.transform.FindChild ("name").GetComponentInChildren<UILabel> ().text = board.user.name;
		} else {
			MessagePanel.Instance.animMessage ("You have to participate in a match before you can be ranked on the leaderboards");
		}

		for (int i=0; i<board.leaderboad.Count; i++) {

			if(i>99) continue;

			// This is important
			LeaderboardScrollview.ResetPosition ();
              
			if (i == 0) {
				leader = NGUITools.AddChild (leadersgrid.gameObject, leader_user);
			} else if (i % 2 == 0) {
				leader = NGUITools.AddChild (leadersgrid.gameObject, user_light);
			} else {
				leader = NGUITools.AddChild (leadersgrid.gameObject, user_dark);
			}
			leader.name = board.leaderboad [i]._id;
//			Debug.Log(board.leaderboad [i].name);
			NGUITools.SetActive (leader, true);
			if (board.leaderboad [i].pic != null)
				BedBugTools.loadImageOnUITextureAndResize (leader.transform.GetComponentInChildren<UITexture> (), board.leaderboad [i].pic, 64, 64);

			leader.transform.FindChild ("block").GetComponentInChildren<UISprite> ().alpha = Main.AppUser.blockedusers.Contains(board.leaderboad [i]._id)?1:0;

			leader.transform.FindChild ("index").GetComponentInChildren<UILabel> ().text = (i + 1).ToString ();
			leader.transform.FindChild ("points").GetComponentInChildren<UILabel> ().text = board.leaderboad [i].score.ToString ();
			leader.transform.FindChild ("name").GetComponentInChildren<UILabel> ().text = board.leaderboad [i].name;
           
          
			// Placement Animation
			leader.transform.localPosition = new Vector3 (0, placeHeight, 0);
			float topY = leader.transform.localPosition.y;
			topY -= 30;
			placeHeight -= 35;
			if(i<12){
			iTween.MoveFrom (leader, iTween.Hash ("y", topY, "time", 0.3f, "easetype",iTween.EaseType.easeOutCirc, "islocal", true)); 
			//iTween.RotateFrom(leader, iTween.Hash("z", -10, "time",0.3f,  "islocal", true)); 
			yield return new WaitForSeconds (0.1f);
			}


		}


		if(friends)
		NGUITools.SetActive(InvButton,true);
		else{
			NGUITools.SetActive(InvButton,false);
		}
		Coach.coachMessage  (LocalizationManager.GetTermTranslation( "tip10", true, 30, true), 0f, 5f, 1.5f, true, "tip10");
		Loader.Visible (false);
		isPopulating = false;
//		StartCoroutine (reposition ());
		//leadersgrid.Reposition();

	}

	void openBanner (UITexture obj)
	{
		//bannerAdAnim.Play ("bannerAnimOpen");

		playBannerAnim (0, 1);
		bannerOpened = true;
		StartCoroutine (repoGrid ());
	}

	void playBannerAnim(float time, float speed) {
		bannerAdAnim ["bannerAnimOpen"].time = time;
		bannerAdAnim ["bannerAnimOpen"].speed = speed;
		bannerAdAnim.Play ("bannerAnimOpen");
	}

	private void FadeInOnLoad(UITexture texture){
		
		TweenAlpha.Begin (texture.gameObject, 0.5f, 1.0f);
	}
	IEnumerator repoGrid ()
	{
		yield return new WaitForSeconds (0.5f);
		//leadersgrid.Reposition ();
		LeaderboardScrollview.ResetPosition ();
	}


	IEnumerator reposition ()
	{
		yield return new WaitForSeconds (2f);
		leadersgrid.Reposition ();
		LeaderboardScrollview.ResetPosition ();
	}
 
	public void onTransitionOutStart ()
	{
		foreach (Transform transform in leadersgrid.GetChildList()) {
			if (transform.gameObject.activeSelf)
				NGUITools.Destroy (transform);
		}

//		leadersgrid.Reposition();
//		LeaderboardScrollview.ResetPosition ();
	}
    
	private string CalculateWeek ()
	{
		DateTime startOfWeek = DateTime.Today.AddDays (-1 * (int)(DateTime.Today.DayOfWeek));
		DateTime endOfWeek = startOfWeek.AddDays (7);
		string nonpool = "{\"conditions\": {\"country\":" + Main.AppUser.country + ",\"created\": {\"$gte\": " + startOfWeek.ToString ("dd/MM/yyyy") + ",\"$lte\": " + endOfWeek.ToString ("dd/MM/yyyy") + "}}";
		return nonpool;
	}
	
	public void OpenProfile (GameObject go)
	{
		//TweenAlpha.Begin (scrollViewUiPanel.gameObject, 0.0f, 0.0f);
//		Main.Instance.openView ("userview", go.name);
		UserModalController.ViewUser (go.name);
		UserModalController.OnModalClose += handleUserModalClose;
	}

	void handleUserModalClose (string obj)
	{
		UserModalController.OnModalClose -= handleUserModalClose;
		leadersgrid.transform.FindChild(obj).transform.FindChild ("block").GetComponentInChildren<UISprite> ().alpha = Main.AppUser.blockedusers.Contains(obj)?1:0;
	}

	private class Friend{
		public string id;
		public string name;
	}
}
