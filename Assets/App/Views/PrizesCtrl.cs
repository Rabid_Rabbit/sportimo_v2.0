﻿using UnityEngine;
using System.Collections.Generic;
using BedbugRest;
using I2.Loc;
using LitJson;
using BestHTTP;

public class PrizesCtrl : View
{

    public GameObject prize;
    public UIScrollView prizes_scroll;
    public UIGrid prizes_table;

    // UNI SELECT
    private List<Pool> allPools;
    List<string> leaderboardTypes = new List<string>();

    Pool selectedPool;

    [Space(10)]
    public UICenterOnChild center;
    public UIGrid selectTable;
    public UIScrollView selectScroll;

    [Space(10)]   
    public UniDropDown _UniDropDown;

    private void onTransitionInStart(viewData _viewdata = null)
    {
        Loader.Visible(true);
        if (string.IsNullOrEmpty(Main.AppUser.country))
        {
            MessagePanel.Instance.animMessage("Please setup your country in your profile.");
            return;
        }

        API.GetAll(Main.Settings.apis["leaderpay"] + "/pools/for/country/" + Main.AppUser.country + "/true", OnGetAllPools);

        _UniDropDown.OnSelect += PopulatePools;

    }

    private void OnGetAllPools(HTTPRequest req, HTTPResponse res)
    {

        Loader.Visible(false);

        if (!res.IsSuccess)
        {
            MessagePanel.Instance.animMessage(res.DataAsText);
            return;
        }
        //List<Friend> fbdata=JsonMapper.ToObject<List<Friend>>(data ["friends"] ["data"].ToJson());
        allPools = JsonMapper.ToObject<List<Pool>>(res.DataAsText);

        leaderboardTypes.Clear();

        foreach (Pool leaderboard in allPools)
        {
            leaderboardTypes.Add(BedBugTools.GetStringByLocPrefs(leaderboard.title));
            //			print (leaderboard.roomtype);
        }

        // Set Initial Values of the Header Drop Down Selector
        _UniDropDown.UpdateOptions(leaderboardTypes);
        if (leaderboardTypes[0] != null)
        {
            _UniDropDown.setSelected(leaderboardTypes[0]);
            PopulatePools(leaderboardTypes[0]);
        }
    }




    private void PopulatePools(string selection)
    {

        foreach (Transform transform in prizes_table.GetChildList())
        {
            if (transform.gameObject.activeSelf)
                NGUITools.Destroy(transform);
        }

        foreach (Pool pool in allPools)
        {
            string usedLang;
            if (pool.title.ContainsKey(LocalizationManager.CurrentLanguageCode))
                usedLang = LocalizationManager.CurrentLanguageCode;
            else
                usedLang = "en";

            if (pool.title[usedLang] == selection)
            {

                for (int i = 0; i < pool.prizes.Count; i++)
                {
                    GameObject _prize = NGUITools.AddChild(prizes_table.gameObject, prize);
                    NGUITools.SetActive(_prize, true);
                    BedBugTools.loadImageOnUITextureAndResize(_prize.transform.FindChild("image").GetComponent<UITexture>(), pool.prizes[i].picture, 256, 256);
                    _prize.name = i.ToString();
                    Dictionary<string, int> pos = pool.prizes[i].positions;

                    if (pos["from"] != pos["to"])
                    {
                        _prize.transform.FindChild("BgTop").GetComponentInChildren<UILabel>().text = pos["from"].ToString() + Pool.Prize.returnEnding(pos["from"]) + " - " + pos["to"].ToString() + Pool.Prize.returnEnding(pos["to"]) + " place";
                    }
                    else {
                        _prize.transform.FindChild("BgTop").GetComponentInChildren<UILabel>().text = pos["from"].ToString() + Pool.Prize.returnEnding(pos["from"]) + " place";
                    }
                    _prize.transform.FindChild("descriptionLabel").GetComponent<UILabel>().text = LocalizationManager.FixRTL_IfNeeded( BedBugTools.GetStringByLocPrefs(pool.prizes[i].text),25, true);
                }

            }
        }

        prizes_table.Reposition();
        prizes_scroll.ResetPosition();
        
        Loader.Visible(false);
    }


    public void onTransitionOutStart()
    {
        foreach (Transform transform in prizes_table.GetChildList())
        {
            if (transform.gameObject.activeSelf)
                NGUITools.Destroy(transform);
        }
    }
    public override void selectionChanged(string selection)
    {
        base.selectionChanged(selection);
        print(selection + " This was fired from the prizes view");
        PopulatePools(selection);

    }



    // TAB METHODS
    public GameObject[] tabsLines = new GameObject[3];

    public void tabSelect(GameObject lineParent)
    {

        // CLOSE ALL LINE AND OPEN THE PRESSED ONE
        for (int i = 0; i < tabsLines.Length; i++)
        {
            NGUITools.SetActive(tabsLines[i], false);
        }

        NGUITools.SetActive(lineParent.transform.FindChild("line").gameObject, true);
        selectionChanged(lineParent.name);
        // REDRAW THE SCROLLVIEW ACCORDING TO LINEPARENT
    }

} 