﻿using UnityEngine;
using System.Collections;
using BedbugRest;
using LitJson;
using System.Collections.Generic;
using I2.Loc;
using Facebook.Unity;


public class userDataCtrl: View {

	public UITexture avatarSprite;
	public UIScrollView dataScroll;

	public GameObject birthSelector;
	public UILabel birthLabel;

	public UILabel CountryLabel;
	public UILabel CountryCodeLabel;
	public UILabel msisdnLabel;
	public UIPopupList CountryPop;
	public UIToggle GenderTogle;

	public fbavatarloader fbavatarbtn;

    public GameObject locked_avatars;

	public userDataCtrl() {
//		Debug.Log("Create a Menu View");
	}

	void Start() {

	}

	//These methods are overrides and therefore
	//can override any virtual methods in the parent
	//class.
	
	public void onTransitionInStart(viewData _viewData = null){

		// Get avatar
		avatarSprite.mainTexture = null;

		if (Main.AppUser.picture != null) {
			BedBugTools.loadImageOnUITextureAndResize (avatarSprite, Main.AppUser.picture, 256,256, FadeInOnLoad, 0);
		} else {
			string baseUrl = "https://s3-eu-west-1.amazonaws.com/sportimo-media/avatars/playerProfile.png";
			BedBugTools.loadImageOnUITextureAndResize (avatarSprite, baseUrl, 256,256, FadeInOnLoad, 0);
		}

        if (Main.isPaidCustomer(null, true))
            locked_avatars.SetActive(false);
        else
            locked_avatars.SetActive(true);


        dataScroll.ResetPosition ();

		// Assign birth date
		if (!string.IsNullOrEmpty (Main.AppUser.birth))
			birthLabel.text = Main.AppUser.birth;
		else
			birthLabel.text = "----";

		// Assign ountry Code
		//if (!string.IsNullOrEmpty (Main.AppUser.countrycode)) {
		//	print ("countrycode " + Main.AppUser.countrycode);
		//	CountryCodeLabel.text = Main.AppUser.countrycode;
		//} else {
		//	CountryCodeLabel.text = "";
		//}

		// Assign msisdn
		if (!string.IsNullOrEmpty (Main.AppUser.msisdn)) {
			print ("MSISDN " + Main.AppUser.msisdn);
			msisdnLabel.text = Main.AppUser.msisdn;
		} else {
			msisdnLabel.text = "";
		}

		// Assign country
		if (!string.IsNullOrEmpty (Main.AppUser.country)) {
			CountryLabel.text = Main.AppUser.country;
			CountryPop.value = Main.AppUser.country;
		}

		// Assign gender
		if (!string.IsNullOrEmpty (Main.AppUser.gender)) {
			if(Main.AppUser.gender == "male")
				GenderTogle.value = true;
			else if(Main.AppUser.gender == "female")
				GenderTogle.value = false;
			print("Gender "+Main.AppUser.gender);
		} else {
			GenderTogle.value = true;
		}
			
	}

	public void onTransitionOutStart(){
		Main.AppUser.saveProfileData ();
	}



	private void FadeInOnLoad(UITexture texture){
		
		TweenAlpha.Begin (texture.gameObject, 0.5f, 1.0f);
	}



	public void OnGetUnread(BestHTTP.HTTPRequest req,BestHTTP.HTTPResponse res){
		if(res.StatusCode!=200 && res.StatusCode!=304){
			MessagePanel.Instance.animMessage(res.DataAsText);
			return;
		}
		JsonData data = JsonMapper.ToObject(res.DataAsText);
//		if(data.Keys.Contains("unread") && data["unread"].ToString()!="0"){
//			//NGUITools.SetActive(messages.transform.parent.gameObject,true);
//			//messages.text=data["unread"].ToString();
//		}
//		else{
//			//NGUITools.SetActive(messages.transform.parent.gameObject,false);
//		}
		
	}
	public override void Init ()
	{
		base.Init();
//		Debug.Log("The function called by the Menu View.");     
	}

//	public override void selectionChanged(string selection) {
//		base.selectionChanged (selection);
//		print (selection +" This was fired from the menu view");
//
//	}

	public void popValueChanged() {
		

		print("Current value Change: "+UIPopupList.current.value);
		Main.AppUser.country = UIPopupList.current.value;
		//		print (transform.parent);
	}

	public 
	string baseUrl = "https://s3-eu-west-1.amazonaws.com/sportimo-media/avatars/";

	public void changeAvatar(GameObject avatar) {
		//if(!Main.Instance.Paywall("Changing avatars")){
		//	return;
		//}
		string avatarName = avatar.name;
		if(avatar.name == "facebookPic"){
			if(FB.IsLoggedIn){
				Loader.Visible (true);
				FB.API ("me?fields=picture.height(250)", HttpMethod.GET, OnGetPicture);
			}else if(string.IsNullOrEmpty(PlayerPrefs.GetString("social_id")) && string.IsNullOrEmpty(Main.AppUser.social_id)){
				FBModal.Instance.PopModal(PopFBModal);
			}
			else{
				FacebookConnect();
			}	
			return;
		}
		print ("New Avatar Name: "+avatar.name);
		PlayerPrefs.SetString ("avatar", avatarName);
		//avatarSprite.mai = avatarName;

		User.updatePic(baseUrl+avatarName+".png");
		Main.AppUser.picture = baseUrl + avatarName + ".png";

		// Update UserData Pic
		BedBugTools.loadImageOnUITextureAndResize (avatarSprite, Main.AppUser.picture, 256,256, FadeInOnLoad, 0);
	}

	public void PopFBModal(string answer){
		if(answer=="no"){
			return;
		}
		else{
			Loader.Visible(true);
			FacebookConnect();
		}
	}

	public void FacebookConnect(){
		if(!FB.IsInitialized)
			FB.Init(FBLogin);
		else{
			FBLogin();
		}
	}

	public void FBLogin(){
		if(FB.IsLoggedIn){
			if(!string.IsNullOrEmpty(Main.AppUser.social_id)){
				FB.API ("me/picture.height(250)", HttpMethod.GET, OnGetPicture);
				return;
			}
			
			}
		else{
			FB.LogInWithReadPermissions(new List<string>(){"email,user_friends"},OnFBLogin);
		}
	}
	private string social_id;
	public void OnFBLogin(ILoginResult fbresult){
		if (fbresult.Error != null) {
			Debug.Log (fbresult.Error);
			Loader.Visible(false);
		} else {
			Debug.Log (fbresult.RawResult);
			JsonData data = JsonMapper.ToObject (fbresult.RawResult);
			Debug.Log (data ["user_id"].ToString ());
			Hashtable fbtable=new Hashtable();
			fbtable.Add ("social_id", data ["user_id"].ToString ());
			API.PutOne(Main.Settings.apis["users"]+"/"+Main.AppUser._id,fbtable,OnConnectCallback);
	}}

	public void OnConnectCallback(BestHTTP.HTTPRequest req,BestHTTP.HTTPResponse res){
		if(res.StatusCode == 200){
			MessagePanel.Instance.animMessage("Facebook connected successfully");
			PlayerPrefs.SetString ("social_id", AccessToken.CurrentAccessToken.UserId);
			Main.AppUser.social_id = AccessToken.CurrentAccessToken.UserId;

				Dictionary<string,string> fbdata=new Dictionary<string,string>();
		fbdata.Add("type","large");
		FB.API ("me?fields=picture.height(250)", HttpMethod.GET, OnGetPicture,fbdata);

		}
		else{
			FB.LogOut();
			MessagePanel.Instance.animMessage("Facebook id already connected to other account");

		}
	
		

	}

	public void OnGetPicture(IGraphResult result){

		Loader.Visible (false);

		Debug.Log(result.RawResult);

		if(result.Error != null)
		Debug.Log(result.Error);
		JsonData data = JsonMapper.ToObject(result.RawResult);
//		User.updatePic(data["picture"]["data"]["url"].ToString());
		Main.AppUser.picture = data["picture"]["data"]["url"].ToString();

		fbavatarbtn.SetTextureToFBPic (Main.AppUser.picture);

		// Update UserData Pic
		BedBugTools.loadImageOnUITextureAndResize (avatarSprite, Main.AppUser.picture, 256,256, FadeInOnLoad, 0);
	}
	public void togleValueGet() {
		if(UIToggle.current.value)
			Main.AppUser.gender = "male";
		else if(!UIToggle.current.value)
			Main.AppUser.gender = "female";

	}

	public void openBirthSelect() {
		NGUITools.SetActive (birthSelector, true);
	}

	public void getDate(GameObject go) {
		birthLabel.text = go.name;
		NGUITools.SetActive (birthSelector, false);
		Main.AppUser.birth = go.name;

	}

}
