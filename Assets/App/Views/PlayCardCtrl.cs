﻿using UnityEngine;
using System;
using System.Collections.Generic;
using BedbugRest;
using BestHTTP;
using I2.Loc;


public class PlayCardCtrl : MonoBehaviour
{



    private List<PlayCard> instantcards = new List<PlayCard>();
    private List<PlayCard> overallcards = new List<PlayCard>();
    private List<PlayCard> presetcards = new List<PlayCard>();

    public bool CardsOpenCheck = false;
    private DateTime lastCardCreated;

    public bool rollerFullyOPened = true;

    public MatchCtrl matchInfo;

    public PlayCardCtrl()
    {
        //		Debug.Log("Create a news View");
    }

    void Start()
    {


        // Init State
        rollerLeft.closeRoller();
        rollerRight.closeRoller();

        //NGUITools.SetActive (centerRoller, false);
        centerRoller.GetComponent<CenterCardsCtrl>().closeCenterCards();
        centerRoller.GetComponent<CenterCardsCtrl>().isActive = false;
        iTween.ScaleTo(centerRoller, iTween.Hash("scale", new Vector3(0.001f, 0.001f, 0.001f), "time", 0.1f));
    }

    public void goBack()
    {
        Main.unregisterOverlay("cardPlayOverlay");
        TweenAlpha.Begin(this.gameObject, 0.5f, 0);
        CardsOpenCheck = false;
        centerRoller.GetComponent<CenterCardsCtrl>().closeCenterCards();
        centerRoller.GetComponent<CenterCardsCtrl>().isActive = false;

    }

    //These methods are overrides and therefore
    //can override any virtual methods in the parent
    //class.


    public RollerCardCtrl rollerLeft;
    public RollerCardCtrl rollerRight;
    public RollerCardCtrl rollerPreset;
    public GameObject centerRoller;

    public void Initialize(object matchid)
    {
        Debug.Log("GET CARDS DATA CALL:" + Main.Settings.apis["gamecards"] + "/" + matchid.ToString() + "/user/" + Main.AppUser._id);
        Loader.Visible(true);
        API.GetAll(Main.Settings.apis["gamecards"] + "/" + matchid.ToString() + "/user/" + Main.AppUser._id, OnGetCardsData);

        rollerRight.closeRoller();
        rollerLeft.closeRoller();
        if (rollerPreset != null)
            rollerPreset.closeRoller();
        rollerLeft.locked = true;
        rollerRight.locked = true;

        // Close and Slow open All Card Rollers
        TweenAlpha.Begin(rollerLeft.gameObject, 0.0f, 0);
        TweenAlpha.Begin(rollerRight.gameObject, 0.0f, 0);
        TweenAlpha.Begin(rollerPreset.gameObject, 0.5f, 0);

        TweenAlpha.Begin(rollerLeft.gameObject, 0.5f, 1);
        TweenAlpha.Begin(rollerRight.gameObject, 0.5f, 1);
        TweenAlpha.Begin(rollerLeft.gameObject, 0.5f, 1);

        //NGUITools.SetActive (centerRoller, false);
        centerRoller.GetComponent<CenterCardsCtrl>().isActive = false;
        iTween.ScaleTo(centerRoller, iTween.Hash("scale", new Vector3(0.001f, 0.001f, 0.001f), "time", 0.1f));

        // If game is at pre-game and player is not a free player open the PresetCards
        print("Game State Is: " + matchInfo.liveMatch.matchData.state);
        if (matchInfo.liveMatch.matchData.state == 0)
        { //TODO Check if its a free player or not
            TweenAlpha.Begin(rollerLeft.gameObject, 0.0f, 0);
            rollerLeft.gameObject.GetComponent<Collider>().enabled = false;
            TweenAlpha.Begin(rollerPreset.gameObject, 0.3f, 1);
            rollerPreset.gameObject.GetComponent<Collider>().enabled = true;
        }
        else {
            TweenAlpha.Begin(rollerLeft.gameObject, 0.3f, 1);
            rollerLeft.gameObject.GetComponent<Collider>().enabled = true;
            TweenAlpha.Begin(rollerPreset.gameObject, 0.0f, 0);
            rollerPreset.gameObject.GetComponent<Collider>().enabled = false;
        }
    }

    public void onTransitionOutStart()
    {

        if (centerRoller != null)
        {
            iTween.ScaleTo(centerRoller, iTween.Hash("scale", new Vector3(0.001f, 0.001f, 0.001f), "time", 1f));
            rollerRight.closeRoller();
            rollerLeft.closeRoller();
            rollerPreset.closeRoller();

            rollerLeft.locked = true;
            rollerRight.locked = true;
        }

        if (instantcards != null)
            instantcards.Clear();
        if (instantcards != null)
            overallcards.Clear();
        if (instantcards != null)
            presetcards.Clear();
    }


    private void OnGetCardsData(HTTPRequest req, HTTPResponse res)
    {

        if (res.StatusCode != 200 && res.StatusCode != 304)
        {
            MessagePanel.Instance.animMessage(res.DataAsText);
            return;
        }

        Debug.Log(res.DataAsText);

        string json = res.DataAsText.Replace("[[home_team_name]]", MatchCtrl.stReturnName("[[home_team_name]]"));

        json = json.Replace("[[away_team_name]]", MatchCtrl.stReturnName("[[away_team_name]]"));

        LitJson.JsonData data = LitJson.JsonMapper.ToObject(json);
        Debug.Log("CARDS DATA: " + res.DataAsText);
        //		print ("CARDS DATA: " + res.DataAsText);
        List<PlayCard> playcards = LitJson.JsonMapper.ToObject<List<PlayCard>>(data["data"].ToJson());

        int i = 0;
        int j = 0;
        int p = 0;

        // Clear Lists
        instantcards.Clear();
        overallcards.Clear();
        presetcards.Clear();

        // Get lastCardCreated from playerperfs
        if (PlayerPrefs.HasKey("lastCardCreated"))
            lastCardCreated = Convert.ToDateTime(PlayerPrefs.GetString("lastCardCreated")); //DateTime.Parse(PlayerPrefs.GetString("lastCardCreated"));
        else
            lastCardCreated = DateTime.UtcNow;

        foreach (PlayCard card in playcards)
        {

            // Check for new cards
            DateTime createdtime = DateTime.Parse(card.creationTime);
            //If card create date > lastcardcreateddate playcard.newCard = true
            if (lastCardCreated != null)
            {
                if (createdtime > lastCardCreated)
                {
                    //					print ("CREATED:"+createdtime.ToString());
                    //					print ("" +"LAST CREATED:"+lastCardCreated.ToString());
                    //					print (card.title["en"]+" is a NEW Card");
                    card.newCard = true;
                }
                else {
                    //					print (card.title["en"]+" is an old Card");
                    card.newCard = false;
                }
            }

            // Check for sprite
            card.Icon = card.image.ContainsKey("sprite") ? card.image["sprite"] : "sportimoicon";

            if (card.cardType == "Instant")
            {
                card.int_id = i;
                //	print ("creation Time: "+card.creationTime);
                //	print ("creation Time: "+card.creationTime);
                instantcards.Add(card);
                i++;
            }
            else if (card.cardType == "Overall")
            {
                card.int_id = j;
                overallcards.Add(card);
                print("Card " + j + " has " + card.guruAction);
                j++;
            }
            else if (card.cardType == "PresetInstant")
            {
                card.int_id = p;
                presetcards.Add(card);
                p++;
            }


        }

        // Now get the lastCradCreated time for future ref
        foreach (PlayCard card in playcards)
        {
            // Check for new cards
            DateTime createdtime = DateTime.Parse(card.creationTime);

            if (createdtime > lastCardCreated || lastCardCreated == null)
                lastCardCreated = createdtime.AddSeconds(1);

        }


        // Save the lastCardCreated date
        //PlayerPrefs.SetString ("lastCardCreated",DateTime.UtcNow.ToString());
        PlayerPrefs.SetString("lastCardCreated", lastCardCreated.ToString(("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'")));



        //print ("Last card Created: " + lastCardCreated);

        // Add Lists to the rollerCard Object
        // INSTANT CARDS
        RollerCardCtrl leftRollerCtrl = rollerLeft.GetComponent<RollerCardCtrl>();
        leftRollerCtrl.playcards = instantcards;

        if (instantcards.Count < 6 && instantcards.Count != 0)
        {

            print(instantcards.Count + " Count < 6 + Count != 0");
            leftRollerCtrl.noCards = false;
            leftRollerCtrl.lessThanSix = true;

        }
        else if (instantcards.Count == 0)
        {

            print(instantcards.Count + " Count = 0");
            leftRollerCtrl.noCards = true;

        }
        else {

            //print (instantcards.Count+" Count > 6");
            leftRollerCtrl.lessThanSix = false;
            leftRollerCtrl.noCards = false;
        }

        leftRollerCtrl.init();

        // PRESET CARDS
        RollerCardCtrl PresetRollerCtrl = rollerPreset.GetComponent<RollerCardCtrl>();
        PresetRollerCtrl.playcards = presetcards;

        if (presetcards.Count < 6 && presetcards.Count != 0)
        {

            print(presetcards.Count + " Count < 6 + Count != 0");
            PresetRollerCtrl.noCards = false;
            PresetRollerCtrl.lessThanSix = true;

        }
        else if (presetcards.Count == 0)
        {

            print(presetcards.Count + " Count = 0");
            PresetRollerCtrl.noCards = true;

        }
        else {

            //print (instantcards.Count+" Count > 6");
            PresetRollerCtrl.lessThanSix = false;
            PresetRollerCtrl.noCards = false;
        }

        PresetRollerCtrl.init();

        // OVERALL CARDS
        RollerCardCtrl rightRollerCtrl = rollerRight.GetComponent<RollerCardCtrl>();

        // Check The length of the Overall and if is shorter than 6 add locked cards
        if (overallcards.Count < 6 && overallcards.Count != 0)
        {

            List<PlayCard> tempoverallcards = new List<PlayCard>();

            ///
            /// This Is for extra Populate the same Cards
            /// 
            //			while( tempoverallcards.Count < 6 ) {
            //				//print ("tempoverallcards Count: "+tempoverallcards.Count);
            //				for (int k = 0; k < overallcards.Count; k++) {
            //					tempoverallcards.Add(overallcards[k]);
            //				}
            //			}

            ///
            /// This is for closeing the holders
            /// 



            rightRollerCtrl.noCards = false;
            //rightRollerCtrl.playcards = tempoverallcards;
            rightRollerCtrl.lessThanSix = true;
            rightRollerCtrl.playcards = overallcards;

        }
        else if (overallcards.Count == 0)
        {
            // Roller CAN open is false
            rightRollerCtrl.noCards = true;

        }
        else {

            rightRollerCtrl.playcards = overallcards;
            rightRollerCtrl.lessThanSix = false;
            rightRollerCtrl.noCards = false;
        }

        rightRollerCtrl.init();

        rollerLeft.locked = false;
        rollerRight.locked = false;

        //		print ("MatchCtrl.timecounting "+MatchCtrl.timecounting);
        // Check if Match is in a playing state or half
        if (!MatchCtrl.timecounting)
        {
            print("Open Right Roller");
            rollerLeft.closeRoller();
            rollerPreset.closeRoller();
            // Check if overall cards count is > 0 so it doesn't auto-open 
            if (overallcards.Count > 0)
            {
                rollerRight.openRoller();
                //if(matchInfo.liveMatch.matchData.state == 0)
                //Coach.coachMessage (LocalizationManager.GetTermTranslation( "tip6", true), 27f, 5f, 1.5f, true, "tip6");
            }

        }
        else {
            print("Open Left Roller");

            if (instantcards.Count > 0)
            {
                rollerLeft.openRoller();
                //Coach.coachMessage (LocalizationManager.GetTermTranslation( "tip7", true ), 27f, 5f, 1.5f, true, "tip7");
            }
            rollerRight.closeRoller();
        }

        Loader.Visible(false);
    }


}
