﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TopMenuCtrl : MonoBehaviour {

	View parentView;

	public Dictionary<string, string> languageStrings = new Dictionary<string, string>();
	public List<string> languages = new List<string> ();
	public UISprite flag;
	private int flagCount;

	void Start() {
		languageStrings.Add("en", "englishFlag");
		languageStrings.Add("uae", "arabiaFlag");

		languages.Add ("en");
		languages.Add ("uae");
	}

	public void goBack() {
		Main.Instance.goBack ();
	}

	public void openSettings () {
		//Call open Stettings method in Main
	}
	
	// Fire string event so the view can catch it and change its content
	public delegate void popSelect (string selection);
	public static event popSelect pickedSelection;
	
	public void selectionChanged() {

//		if (pickedSelection != null)
//			pickedSelection(UIPopupList.current.value);
		if (parentView == null)
			parentView = transform.parent.gameObject.GetComponent<View> ();

		parentView.selectionChanged (UIPopupList.current.value);
//		print (transform.parent);
	}

	// When the view starts its Transitions this will do its own transition
	void onTransitionInStart(viewData _viewData = null) {
		//print ("Do something when the transition In Starts");
		//iTween.FadeFrom (this.gameObject, 0.0f, 5);

		//TweenAlpha.Begin (this.gameObject,0.3f, 1);

	}
	
	void onTransitionOutStart() {
		//print ("Do something when the transition Out Starts");
		//TweenAlpha.Begin (this.gameObject,0.0f,0);

	}

	public void changeFlag() {
		flagCount++;

		if (flagCount >= languages.Count)
			flagCount = 0;

		flag.spriteName = languageStrings [languages [flagCount]];
	}
}
