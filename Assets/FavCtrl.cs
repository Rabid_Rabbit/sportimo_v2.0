﻿using UnityEngine;
using System.Collections.Generic;
using BedbugRest;
using BestHTTP;
using LitJson;
using I2.Loc;
using System.Collections;


public class FavCtrl : View {

    // Use this for initialization
    List<string> FavoriteTeamsList = new List<string>();

	public UIPopupList favsPopList;
	public UILabel topLabel;
	public GameObject arrowIcon;

	private List<Team> teams = new List<Team>();
	Team selectedTeam;

	[Space(10)]
	public GameObject uniPopSelect;
	public bool useTwoColors = false;
	private bool useDarc = false;
	private bool uniPopOpen = false;
	public UICenterOnChild center;
	public UIGrid selectTable;
	public UIScrollView selectScroll;
	public UIWrapContent wrap;
	public GameObject lightPrefab;
	public GameObject darkPrefab;
	public GameObject topPrefab;
	public GameObject bottomPrefab;
	GameObject uniTeam;
	[Space(10)]

 	public UILabel index;
    public UILabel name;
    public UILabel played;
    public UILabel won;
    public UILabel draw;
    public UILabel lost;

    public UILabel goals;

    public UILabel points;

    public UIGrid grid;

	public UISprite[] hthsprites;
	public UILabel[] hthlabels;
	
	public Color32 WinColor = new Color32(49,171,100,255);
	public Color32 DrawColor = new Color32(72,142,224,255);
	public Color32 LoseColor = new Color32(207,1,27,255);
	public Dictionary<string,Color32> colors = new Dictionary<string, Color32> ();

	public UITexture next_home;
	public UITexture previous_home;

	public UITexture next_away;
	public UITexture previous_away;

	public UILabel next_homename;
	public UILabel next_awayname;
	public UILabel previous_homename;
	public UILabel previous_awayname;

	public UILabel next_date;
	public UILabel previous_dates;
	public GameObject topscorer;

	public UILabel scorerName;
	public UILabel scorerGoals;
	public UITexture scorerPic;

	public UILabel assisterName;
	public UILabel assisterAssists;
	public UITexture assisterPic;

	public UILabel scoreLastLabel;

	public GameObject[] stats = new GameObject[4];
	public Animation favAnim;

	public UIScrollView stasScroll;
	public GameObject scrollHeader;
	public GameObject noFavsChoose; 

	int intCount = 0; // Count to find the last team in teams
	int dbCount = 1; // Count to find the team in double count teams

    [Space(10)]
    public UniDropDown _UniDropDown;

    public void onTransitionInStart(viewData _viewData = null)
    {
        print("Favs Trans In Start");
        playFavAnim(0.1f, 0f);
        stasScroll.ResetPosition();
        NGUITools.SetActive(noFavsChoose, false);

        //Close uniPopSelect
        TweenAlpha.Begin(uniPopSelect, 0.0f, 0);
        //		NGUITools.SetActive (uniPopSelect, false);
        //uniPopOpen = false;

        Loader.Visible(true);
        getFavTeams();

        _UniDropDown.OnSelect += PopulateFavoriteBySelection;
    }


    void Start() {
		colors.Add ("W", WinColor);
		colors.Add ("D", DrawColor);
		colors.Add ("T", DrawColor);
		colors.Add ("L", LoseColor);
	}

	

	void getFavTeams ()
	{
		//print ("Emptying Holders");

		intCount = 0;
		dbCount = 1;
		index.text = "-";
		played.text = "-";
		won.text = "-";
		draw.text = "-";
		lost.text = "-";
		goals.text = "-";
		points.text = "-";
		name.text = "";

		// Top label
		topLabel.text = "Favorite Teams";

		// recent Form
		for (int h = 0; h < 5; h++) {
			// Change the color depending the result
			hthsprites[h].color = new Color32(255,255,255,1);
			// Change the label
			hthlabels[h].text = "";
		}
		
		// Top Scorer
		scorerName.text = "";
		scorerGoals.text = "";
		scorerPic.mainTexture = null;
		//BedBugTools.loadImageOnUITexture (scorerPic, team.topscorer.pic, 64, 64);

		// Top Assister
		assisterName.text = "";
		assisterAssists.text = "";
		assisterPic.mainTexture = null;

		teams.Clear();
		//favsPopList.Clear ();
        FavoriteTeamsList.Clear();
        _UniDropDown.Clear();

        // Clear the UniPopSelect
        foreach (Transform transform in selectTable.GetChildList()) {
			if (transform.gameObject.activeSelf)
				NGUITools.Destroy (transform);
		}

		if (Main.AppUser.favoriteteams.Count > 0) {
			// Open Team Info
			foreach (string id in Main.AppUser.favoriteteams) {				
				API.GetAll (Main.Settings.apis ["data"] + "/teams/" + id + "/favorite", OnFavouriteCallback);
			}

			//print (teams.Count);
			//populateUniSelect();
			// close header
			NGUITools.SetActive(scrollHeader, true);


			// Open Heart
			NGUITools.SetActive (heartIcon, true);

			if (Main.AppUser.favoriteteams.Count > 1) {
				// Open Arrow
				NGUITools.SetActive (arrowIcon, true);
			}


		} else {
			MessagePanel.Instance.animMessage(LocalizationManager.GetTermTranslation("mess_favTeams1", true, 40, true));
			Loader.Visible(false);
			NGUITools.SetActive (noFavsChoose, true);
			// Close Heart
			NGUITools.SetActive (heartIcon, false);
			// close header
			NGUITools.SetActive(scrollHeader, false);

			// Close Arrow
			if (Main.AppUser.favoriteteams.Count < 2) {
				// Open Arrow
				NGUITools.SetActive (arrowIcon, false);
			}
		}
		//favsPopList.value = favsPopList[0]  .name[LocalizationManager.CurrentLanguageCode];
	}




    private void OnFavouriteCallback(HTTPRequest req,HTTPResponse res){
        intCount++;
        if (res.IsSuccess)
        {
           

            Team team = JsonMapper.ToObject<Team>(res.DataAsText);
            teams.Add(team);

            if (team._id == null)
                return;
            print("favorite: " + team.name["en"]);



            FavoriteTeamsList.Add(BedBugTools.GetStringByLocPrefs(team.name));

            //favsPopList.AddItem(team.name[LocalizationManager.CurrentLanguageCode]);

           
        }

        if (intCount == Main.AppUser.favoriteteams.Count)
        {
            // Set Initial Values of the Header Drop Down Selector
            _UniDropDown.UpdateOptions(FavoriteTeamsList);

            if (FavoriteTeamsList[0] != null)
            {
                //_UniDropDown.setSelected(FavoriteTeamsList[0]);
                PopulateFavoriteBySelection(FavoriteTeamsList[0]);
            }

            // favsPopList.value = favsPopList.items[0];

            //print (teams.Count);
            //print (Main.AppUser.favoriteteams.Count);

            //populateUniSelect ();
            //PopulateFavorite(teams[0]);

        }
    }

	void populateUniSelect ()
	{

		foreach (Transform transform in selectTable.GetChildList()) {
			if (transform.gameObject.activeSelf)
				NGUITools.Destroy (transform);
		}


		foreach (Team team in teams) {
			print (team.name [LocalizationManager.CurrentLanguageCode]);
			// Add Team to uniSelect

			if(Main.AppUser.favoriteteams.Count != 2) {
				//wrap.itemSize = 39;
				if (((teams.Count & 1) == 0) || useTwoColors) { // check if teams count is odd useTwoColors || 
					if (useDarc) {
						uniTeam = NGUITools.AddChild (selectTable.gameObject, darkPrefab);
						useDarc = false;
					} else {
						uniTeam = NGUITools.AddChild (selectTable.gameObject, lightPrefab);
						useDarc = true;
					}
				
				} else {
					uniTeam = NGUITools.AddChild (selectTable.gameObject, lightPrefab);
				}

				selectTable.Reposition();
				wrap.SortBasedOnScrollMovement ();
			} else {
				//wrap.itemSize = 80;
				if(dbCount == 1) {
					uniTeam = NGUITools.AddChild (selectTable.gameObject, topPrefab);
					dbCount = 2;
				} else if (dbCount == 2) {
					uniTeam = NGUITools.AddChild (selectTable.gameObject, bottomPrefab);
					dbCount = 0;
				}
				wrap.SortBasedOnScrollMovement ();
				selectTable.Reposition();
			}
			NGUITools.SetActive(uniTeam, true);
			uniTeam.name = LocalizationManager.FixRTL_IfNeeded( team.name[LocalizationManager.CurrentLanguageCode]);
			uniTeam.transform.FindChild("label").gameObject.GetComponent<UILabel>().text = LocalizationManager.FixRTL_IfNeeded( team.name[LocalizationManager.CurrentLanguageCode]);


		}



	}

	public override void selectionChanged(string selection) {
		base.selectionChanged (selection);

		Loader.Visible (true);
		playFavAnim (0.5f, -3.0f);
		PopulateFavoriteBySelection(selection);
		
	}

	public void selectThis(UILabel teamName){
		//Close uniPopSelect
		TweenAlpha.Begin (uniPopSelect, 0.0f, 0);
//		NGUITools.SetActive (uniPopSelect, false);
		uniPopOpen = false;
		// Update Top Label
		topLabel.text = teamName.text;
		// Populate with selection
		PopulateFavoriteBySelection (teamName.text);
	}

	public void openUniPop() {
		// Check if Count = 0 or 1
		if (Main.AppUser.favoriteteams.Count < 2)
			return;

		if (!uniPopOpen) {
			//Open uniPopSelect
			TweenAlpha.Begin (uniPopSelect, 0.3f, 1);
			//NGUITools.SetActive (uniPopSelect, true);
			uniPopOpen = true;
			selectTable.Reposition ();
			wrap.SortBasedOnScrollMovement ();

			if(selectedTeam != null)
			center.CenterOn(selectTable.transform.FindChild(selectedTeam.name[LocalizationManager.CurrentLanguageCode]).transform);
		} else {
			//Open uniPopSelect
			TweenAlpha.Begin (uniPopSelect, 0.2f, 0);
//			NGUITools.SetActive (uniPopSelect, false);
			uniPopOpen = false;
			selectTable.Reposition ();
			wrap.SortBasedOnScrollMovement ();
		}
	}

	//Favorite team population by selection
	void PopulateFavoriteBySelection(string selection){
		foreach (Team team  in teams) {
            //print("fav: " + selection + " | " + team.name[LocalizationManager.CurrentLanguageCode]);

			if(LocalizationManager.FixRTL_IfNeeded(selection) ==  LocalizationManager.FixRTL_IfNeeded(BedBugTools.GetStringByLocPrefs(team.name))){

				for (int h = 0; h < 5; h++) {
					// Change the color depending the result
					hthsprites[h].color = new Color32(255,255,255,1);
					// Change the label
					hthlabels[h].text = "";
				}

				selectedTeam = team;
				isFav = true;
				NGUITools.SetActive (heartIcon, true);


				if (team.standing != null)
				{
					name.text = LocalizationManager.FixRTL_IfNeeded( team.name[LocalizationManager.CurrentLanguageCode]);
					index.text = team.standing.rank.ToString();
					played.text = team.standing.gamesPlayed.ToString();
					won.text = team.standing.wins.ToString();
					draw.text = team.standing.ties.ToString();
					lost.text = team.standing.losses.ToString();
					goals.text = (team.standing.goalsFor - team.standing.goalsAgainst).ToString();
					points.text = team.standing.points.ToString();
					
					// recent Form
					if(team.recentform.Count > 0) {
						for (int h = 0; h < team.recentform.Count; h++) {
							// Change the color depending the result
							hthsprites[h].color = colors[team.recentform[h]];
							// Change the label
							hthlabels[h].text = team.recentform[h];
						}
					}
					
					// Top Scorer
					if(team.topscorer != null) {
						scorerName.text = LocalizationManager.FixRTL_IfNeeded( team.topscorer.name[LocalizationManager.CurrentLanguageCode]);
						if(team.topscorer.stats.ContainsKey("season"))
							scorerGoals.text = team.topscorer.stats["season"].goalsTotal.ToString();
						BedBugTools.loadImageOnUITextureAndResize (scorerPic, team.topscorer.pic, 128,128);
					} else {
						scorerName.text = "";
						scorerGoals.text = "";
						scorerPic.mainTexture = null; 
						//BedBugTools.loadImageOnUITexture (scorerPic, team.topscorer.pic, 64, 64);
					}

					// Top Assister
					if(team.topassister != null) {
						assisterName.text = LocalizationManager.FixRTL_IfNeeded( team.topassister.name[LocalizationManager.CurrentLanguageCode]);
						if(team.topassister.stats.ContainsKey("season"))
							assisterAssists.text = team.topassister.stats["season"].assistsTotal.ToString();
						BedBugTools.loadImageOnUITextureAndResize (scorerPic, team.topassister.pic, 128,128);
					} else {
						assisterName.text = "";
						assisterAssists.text = "";
						assisterPic.mainTexture = null; 
						//BedBugTools.loadImageOnUITexture (scorerPic, team.topscorer.pic, 64, 64);
					}

					// last match
					if(team.lastmatch != null) {
						print (team.lastmatch.eventdate.ToString());
						previous_dates.text = team.lastmatch.eventdate.ToString ("dd/MM HH:mm");
						previous_homename.text = LocalizationManager.FixRTL_IfNeeded( team.lastmatch.home.name[LocalizationManager.CurrentLanguageCode]);
						BedBugTools.loadImageOnUITextureAndResize (previous_home, team.lastmatch.home.logo, 128, 128);
						
						previous_awayname.text = LocalizationManager.FixRTL_IfNeeded( team.lastmatch.away.name[LocalizationManager.CurrentLanguageCode]);
						BedBugTools.loadImageOnUITextureAndResize (previous_away, team.lastmatch.away.logo, 128,128);
						
						scoreLastLabel.text = team.lastmatch.homescore+"-"+team.lastmatch.awayscore;

					} else {
						previous_dates.text = "N/A"; //team.lastmatch.eventdate.ToString ("dd/MM HH:mm");
						previous_homename.text = "N/A"; //team.lastmatch.home.name[LocalizationManager.CurrentLanguageCode];
						previous_awayname.text = "N/A";
						previous_home.mainTexture = null;
						previous_away.mainTexture = null;
						//print (previous_home.mainTexture);
						//BedBugTools.loadImageOnUITextureAndResize (previous_home, team.lastmatch.home.logo, 128, 128);
						
						//previous_awayname.text = team.lastmatch.away.name[LocalizationManager.CurrentLanguageCode];
						//BedBugTools.loadImageOnUITextureAndResize (previous_away, team.lastmatch.away.logo, 128,128);
						
						scoreLastLabel.text = "0-0";
					
					}


					// Next match
					if(team.nextmatch != null) {
						next_date.text = team.nextmatch.eventdate.ToString ("dd/MM HH:mm");
						//print (team.nextmatch.home);
						if( team.nextmatch.home != null) {
							next_homename.text = LocalizationManager.FixRTL_IfNeeded( team.nextmatch.home.name[LocalizationManager.CurrentLanguageCode]);
							BedBugTools.loadImageOnUITextureAndResize (next_home, team.nextmatch.home.logo, 128, 128);
						}
						
						if( team.nextmatch.away != null) {
							next_awayname.text = LocalizationManager.FixRTL_IfNeeded( team.nextmatch.away.name[LocalizationManager.CurrentLanguageCode]);
							BedBugTools.loadImageOnUITextureAndResize (next_away, team.nextmatch.away.logo, 128, 128);
						}
					}

					
				}else{
					name.text="No data available";
				}

			}
		}
		stasScroll.ResetPosition ();
		playFavAnim (0.0f, 1.5f);
		Loader.Visible (false);
	}

	void playFavAnim(float time, float speed) {

		favAnim ["favAnim"].time = time;
		favAnim ["favAnim"].speed = speed;
		favAnim.Play ("favAnim");
	}

	private void PopulateFavorite(Team team){
		selectedTeam = team;
		stasScroll.ResetPosition ();
        if (team.standing != null)
        {
            name.text = LocalizationManager.FixRTL_IfNeeded(BedBugTools.GetStringByLocPrefs(team.name));
            index.text = team.standing.rank.ToString();
            played.text = team.standing.gamesPlayed.ToString();
            won.text = team.standing.wins.ToString();
            draw.text = team.standing.ties.ToString();
            lost.text = team.standing.losses.ToString();
            goals.text = (team.standing.goalsFor - team.standing.goalsAgainst).ToString();
            points.text = team.standing.points.ToString();

            // recent Form
            if (team.recentform.Count > 0)
            {
                for (int h = 0; h < team.recentform.Count; h++)
                {
                    // Change the color depending the result
                    hthsprites[h].color = colors[team.recentform[h]];
                    // Change the label
                    hthlabels[h].text = team.recentform[h];
                }
            }

            // Top Scorer
            if (team.topscorer != null)
            {
                scorerName.text = LocalizationManager.FixRTL_IfNeeded(team.topscorer.name[LocalizationManager.CurrentLanguageCode]);
                scorerGoals.text = team.topscorer.stats["season"].goalsTotal.ToString();
                BedBugTools.loadImageOnUITextureAndResize(scorerPic, team.topscorer.pic, 128, 28);
            }

            // Top Assister
            if (team.topassister != null)
            {
                assisterName.text = LocalizationManager.FixRTL_IfNeeded(team.topassister.name[LocalizationManager.CurrentLanguageCode]);
                if (team.topassister.stats.ContainsKey("season"))
                    assisterAssists.text = team.topassister.stats["season"].assistsTotal.ToString();
                BedBugTools.loadImageOnUITextureAndResize(scorerPic, team.topassister.pic, 128, 128);
            }

            // last match
            if (team.lastmatch != null)
            {
                print(team.lastmatch.eventdate.ToString());
                previous_dates.text = team.lastmatch.eventdate.ToString("dd/MM HH:mm");
                previous_homename.text = LocalizationManager.FixRTL_IfNeeded(team.lastmatch.home.name[LocalizationManager.CurrentLanguageCode]);
                BedBugTools.loadImageOnUITextureAndResize(previous_home, team.lastmatch.home.logo, 128, 128);

                previous_awayname.text = LocalizationManager.FixRTL_IfNeeded(team.lastmatch.away.name[LocalizationManager.CurrentLanguageCode]);
                BedBugTools.loadImageOnUITextureAndResize(previous_away, team.lastmatch.away.logo, 128, 128);

                scoreLastLabel.text = team.lastmatch.homescore + "-" + team.lastmatch.awayscore;
            }
            else {
                previous_dates.text = "N/A"; //team.lastmatch.eventdate.ToString ("dd/MM HH:mm");
                previous_homename.text = "N/A"; //team.lastmatch.home.name[LocalizationManager.CurrentLanguageCode];
                previous_awayname.text = "N/A";
                previous_home.mainTexture = null;
                previous_away.mainTexture = null;
                //BedBugTools.loadImageOnUITextureAndResize (previous_home, team.lastmatch.home.logo, 128, 128);

                //previous_awayname.text = team.lastmatch.away.name[LocalizationManager.CurrentLanguageCode];
                //BedBugTools.loadImageOnUITextureAndResize (previous_away, team.lastmatch.away.logo, 128,128);

                scoreLastLabel.text = "0-0";

            }
            // Next match
            next_date.text = team.nextmatch.eventdate.ToString("dd/MM HH:mm");
            print(team.nextmatch.home);
            if (team.nextmatch.home != null)
            {
                next_homename.text = LocalizationManager.FixRTL_IfNeeded(team.nextmatch.home.name[LocalizationManager.CurrentLanguageCode]);
                BedBugTools.loadImageOnUITextureAndResize(next_home, team.nextmatch.home.logo, 128, 128);
            }

            if (team.nextmatch.away != null)
            {
                next_awayname.text = LocalizationManager.FixRTL_IfNeeded(team.nextmatch.away.name[LocalizationManager.CurrentLanguageCode]);
                BedBugTools.loadImageOnUITextureAndResize(next_away, team.nextmatch.away.logo, 128, 128);
            }

        }
        else {
            name.text = "No data available";
        }

		stasScroll.ResetPosition ();
		playFavAnim (0.0f, 1.5f);
		Loader.Visible (false);
	}

	// Fav Team Change
	// Favorite team
	public GameObject heartIcon;
	public bool isFav = false;
	public void toggleFav()
	{
		//Close uniPopSelect
		TweenAlpha.Begin (uniPopSelect, 0.0f, 0);
//		NGUITools.SetActive (uniPopSelect, false);
		uniPopOpen = false;

		
		//if (isFav) {
			NGUITools.SetActive (heartIcon, false);
			isFav = false;
			// Message that team is removed from Favorites
			//foreach(string id in Main.AppUser.favoriteteams) {
			//if(id == team._id)
			Main.AppUser.favoriteteams.RemoveAll (x => x == selectedTeam._id);
			//}
			Hashtable table = new Hashtable ();
			table.Add ("favoriteteams", Main.AppUser.favoriteteams);
			API.PutOne (Main.Settings.apis ["users"] + "/" + Main.AppUser._id, table, OnFavoritesUpdate);
			// Remove from favorite teams list
			StartCoroutine(resetFav());
	//	} 
//		else {
//			NGUITools.SetActive (heartIcon, true);
//			isFav = true;
//			
//			Main.AppUser.favoriteteams.Add (selectedTeam._id);
//			Hashtable table = new Hashtable ();
//			table.Add ("favoriteteams", Main.AppUser.favoriteteams);
//			API.PutOne (Main.Settings.apis ["users"] + "/" + Main.AppUser._id, table, OnFavoritesUpdate);
//			// Add to favorite teams list
//		}
	}

	IEnumerator resetFav ()
	{
		playFavAnim (0.5f, -1.0f);
		yield return new WaitForSeconds (0.5f);
		getFavTeams ();
	}

	public void OnFavoritesUpdate(HTTPRequest req, HTTPResponse res)
	{
		Debug.Log(res.DataAsText);
		MessagePanel.Instance.animMessage(LocalizationManager.GetTermTranslation("mess_favTeams3", true, 40, true));
	}

	public void addTeams() {
		Main.Instance.openView("standings");
	}
}
