﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(UIWidget))]
public class AnimatedFill : MonoBehaviour
{
	[Range(0f, 1f)]
	public float fillAmount = 1f;
	
	UISprite mWidget;
	
	void Awake () { mWidget = GetComponent<UISprite>(); }
	void Update () {
		mWidget.fillAmount = fillAmount;
	}
}