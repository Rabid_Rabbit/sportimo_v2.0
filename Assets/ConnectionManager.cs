﻿using UnityEngine;
using System.Collections;
using BestHTTP.WebSocket;
using System;
using LitJson;
using Prime31;
using I2.Loc;

using System.Collections.Generic;

public class ConnectionManager : MonoBehaviour
{
  
	private static ConnectionManager instance = null;

	public static ConnectionManager Instance {
		get {
			if (instance == null)
				instance = FindObjectOfType (typeof(ConnectionManager)) as ConnectionManager;
			
			return instance;
		}
	}

	void Awake ()
	{
		instance = this;


//		WebSocketStart ();
	}



	void OnDisable ()
	{
		removeHooks (ActiveWebSocket);

	}

	public delegate void GameEvent (string json);

	public delegate void SegmentEvent (string json);

	public delegate void StatsEvent (string json);

	public delegate void UpdatedEvent (string json);

	public delegate void MessageEvent (string json);

	public delegate void CardResumedEvent (string json);

	public delegate void CardLostEvent (string json);

	public delegate void CardWonEvent (string json);

	public delegate void DuplicateConnection (string json);

	public delegate void TauntEvent (string json);

	public delegate void Card_PresetInstant_activated (string json);

	public delegate void ReconnectEvent(string id);

	public delegate void FinalizeEvent();
    
	public static event DuplicateConnection OnDuplicateConnection;
	public static event GameEvent OnGameEvent;
	public static event SegmentEvent OnSegmentEvent;
	public static event StatsEvent OnStatsEvent;
	public static event UpdatedEvent OnUpdatedEvent;

	public static event CardResumedEvent OnCardResumedEvent;
	public static event CardLostEvent OnCardLostEvent;
	public static event CardWonEvent OnCardWonEvent;
	public static event MessageEvent OnMessageReceived;
	public static event Card_PresetInstant_activated OnPresetActivated;


	public static event TauntEvent OnTauntReceived;

	public static event ReconnectEvent OnReconnected;

	public static event FinalizeEvent OnFinalized;
    
	public static WebSocket ActiveWebSocket;
	public static List<WebSocket> Sockets = new List<WebSocket>();

    public GameObject SocketConnectionBlip;
 
	public static void Connect ()
	{
		Debug.Log (Main.Settings.apis ["socket"]);
		var ws = new WebSocket (new Uri (Main.Settings.apis ["socket"]));
		Instance.addHooks (ws);
		ws.Open ();
	}

	public static void Reconnect(){
		reconnecting = 1;
        instance.SocketConnectionBlip.SetActive(true);

        Debug.Log ("RetryConnection");

		if(ActiveWebSocket != null)
			ActiveWebSocket.Close ();

		Connect ();
	}

	void addHooks (WebSocket ws)
	{
		ws.OnOpen += OnWebSocketOpen;
		ws.OnClosed += OnWebSocketClose;
		ws.OnError += OnWebSocketError;
		ws.OnMessage += OnWebSocketMessage;
	}
	
	void removeHooks(WebSocket ws){
		ws.OnOpen -= OnWebSocketOpen;
		ws.OnClosed -= OnWebSocketClose;
		ws.OnError -= OnWebSocketError;
		ws.OnMessage -= OnWebSocketMessage;
	}

	private string registration = "";
	private string lastSubscription = "";

	public void Subscribe (string match_id)
	{
		// RegisterUser();
		string message = "{\"subscribe\":{\"room\":\"" + match_id + "\"}}";
		lastSubscription = message;
		//Debug.Log (message);
		ActiveWebSocket.Send (message);
	}
    
	public void Unsubscribe (string match_id)
	{
		string message = "{\"unsubscribe\":{\"room\":\"" + match_id + "\"}}";
		lastSubscription = "";
		//Debug.Log (message);
		ActiveWebSocket.Send (message);

	}
    

	public void DisconnectUser(){
		ActiveWebSocket.Close();
	}
	public void RegisterUser ()
	{
		string message = "{\"register\":{\"uid\":\"" + Main.AppUser._id + "\",\"uname\":\"" + Main.AppUser.username + "\"}}";
		registration = message;
		//Debug.Log (message);
        if (ActiveWebSocket != null)
        {
            if (ActiveWebSocket.IsOpen)
                ActiveWebSocket.Send(message);
            else
                RetryConnection();
        }
        else
            RetryConnection();
    }
    
	public void WebSocketStart ()
	{
		if (!ActiveWebSocket.IsOpen) {
			ActiveWebSocket.Open ();
		}
	}
    
	void OnWebSocketOpen (WebSocket ws)
	{
        Debug.Log("WebSocket connection successfull");
        Sockets.Add (ws);
		ActiveWebSocket = ws;

		if (reconnecting == 1) {
			CancelInvoke ("RetryConnection");
			if(!string.IsNullOrEmpty(registration))
				ActiveWebSocket.Send(registration);
			if(!string.IsNullOrEmpty(lastSubscription))
				ActiveWebSocket.Send(lastSubscription);
			reconnecting = 0;
            instance.SocketConnectionBlip.SetActive(false);

            //MessagePanel.Instance.animMessage ("Reconnected successfully.", "Connection established", "sportimo-Logo", 3.0f);
             
            
			
			if(!string.IsNullOrEmpty(lastSubscription)){
				if(OnReconnected!=null){
					OnReconnected(lastSubscription);
				}
			}
			Debug.Log ("Reconnection Attempt Successfull");
		}
	}
    
	void OnWebSocketClose (WebSocket webSocket, UInt16 code, string message)
	{
		Debug.Log ("WebSocket closed");
		//Handle unexpected web socket closure
	}
    
	void OnWebSocketMessage (WebSocket ws, string message)
	{
		if (message.IndexOf ("instance") > -1)
			return;

		Debug.Log (message);

        
		JsonData eventdata = JsonMapper.ToObject (message);
		if (eventdata.Keys.Contains ("type")) {
			if ((string)eventdata ["type"] == "Taunt") {
				if (OnTauntReceived != null)
					OnTauntReceived (eventdata ["data"].ToJson());
			}
			else if ((string)eventdata ["type"] == "Event_added") {
				if (OnGameEvent != null)
					OnGameEvent (message);
			} else if ((string)eventdata ["type"] == "disconnect_user") {
				if (OnDuplicateConnection != null)
					OnDuplicateConnection (message);
				MessagePanel.Instance.animMessage (LocalizationManager.FixRTL_IfNeeded( eventdata ["data"] ["message"] [LocalizationManager.CurrentLanguageCode].ToString ()));
					Main.forcedLogout = true;
					Main.Instance.openView ("login2");
			} else if ((string)eventdata ["type"] == "Stats_changed") {
				if (OnStatsEvent != null)
					OnStatsEvent (message);
			} else if ((string)eventdata ["type"] == "Advance_Segment") {
				if (OnSegmentEvent != null)
					OnSegmentEvent (message);
			} else if ((string)eventdata ["type"] == "Event_updated") {
				if (OnUpdatedEvent != null) {
					OnUpdatedEvent (message);
				}
			} else if ((string)eventdata ["type"] == "Banner") {
				if (OnMessageReceived != null) {
					OnMessageReceived (message);
				}
			} else if ((string)eventdata ["type"] == "Card_resumed") {
				if (OnCardResumedEvent != null) {
					OnCardResumedEvent (message);
				}
			} else if ((string)eventdata ["type"] == "Card_lost") {
				if (OnCardLostEvent != null) {
					OnCardLostEvent (message);
				}
			} else if ((string)eventdata ["type"] == "Card_won") {
				if (OnCardWonEvent != null) {
					OnCardWonEvent (message);
				}
			} else if ((string)eventdata ["type"] == "Card_PresetInstant_activated") {
				if (OnPresetActivated != null) {
					OnPresetActivated (message);
				}
			} 
			else if ((string)eventdata ["type"] == "Message") {
				// No need to localize it here. The Says method will localize it by itself.
				Dictionary <string,string> headline = new Dictionary<string, string>(){{"en",""}};
				if(eventdata ["data"].Keys.Contains("title"))
					headline = JsonMapper.ToObject<Dictionary<string,string>>(eventdata ["data"] ["title"].ToJson());

				Dictionary <string,string> words = new Dictionary<string, string>(){{"en",""}};
				if(eventdata ["data"].Keys.Contains("message"))
					words = JsonMapper.ToObject<Dictionary<string,string>>(eventdata ["data"] ["message"].ToJson());

				Coach.Says(headline, words, true);
			} 
			else if (eventdata ["type"].ToString () == "Video") {
				//Application.OpenURL (eventdata ["data"] ["url"].ToString ());
				OpenVideo(eventdata ["data"] ["url"].ToString ());
			}
			else if (eventdata ["type"].ToString () == "VideoAd") {
				AdsManager.Instance.ShowAd();
			}
			else if (eventdata ["type"].ToString () == "Match_full_time") {
				if(OnFinalized!=null){
					OnFinalized();
				}
			}
		}
        
		//Differentiate event types
		//fire correct event/action for app listeners
	}
  	
	private static int reconnecting = 0;

	void OnWebSocketError (WebSocket ws, Exception ex)
	{
		string errorMsg = string .Empty;
		if (ws.InternalRequest.Response != null)
			errorMsg = string.Format ("Status Code from Server: {0} and Message: {1}",

        ws.InternalRequest.Response.StatusCode,
        ws.InternalRequest.Response.Message);

		Debug.Log (errorMsg);
        //MessagePanel.Instance.animMessage("Lost Socket Connection", "Trying to reconnect in 10", "sportimo-Logo", 3.0f);
        Debug.Log ("An error occured: " + (ex != null ? ex.Message : "Unknown: " + errorMsg));
        instance.SocketConnectionBlip.SetActive(true);
        removeHooks (ws);
		Invoke("RetryConnection", 10);

	}

	void RetryConnection ()
	{
		reconnecting = 1;
        

        Debug.Log ("RetryConnection");
		if(ActiveWebSocket != null)
			ActiveWebSocket.Close ();
		Connect ();
	}


	#if  UNITY_ANDROID
	UniWebView _webView;

	bool OnWebViewShouldClose (UniWebView webView)
	{
		if (webView == _webView) {
			_webView.OnLoadComplete -= OnLoadComplete;
			_webView.OnWebViewShouldClose -= OnWebViewShouldClose;
			_webView = null;
			return true;
		}
		return false;
	}

	// The listening method of OnLoadComplete method.
	void OnLoadComplete (UniWebView webView, bool success, string errorMessage)
	{
		if (success) {
			// Great, everything goes well. Show the web view now.
			Debug.Log ("WEB VIEW LAODED!");
			webView.Show ();
		} else {
			// Oops, something wrong.
			Debug.LogError ("Something wrong in web view loading: " + errorMessage);
		}
	}
#endif

	public GameObject UniWebPrefab;

	public void OpenVideo (string url)
	{

		#if UNITY_EDITOR
			Application.OpenURL (url);
		#elif UNITY_ANDROID
			 _webView = (Instantiate (UniWebPrefab) as GameObject).GetComponent<UniWebView> ();//new GameObject().AddComponent<UniWebView>();
			 _webView.backButtonEnable=true;
			_webView.OnWebViewShouldClose += OnWebViewShouldClose;
			_webView.OnLoadComplete += OnLoadComplete;
			//_webView.Show();
			_webView.url = url;
			_webView.Load ();
		#elif UNITY_IOS
			EtceteraBinding.showWebPage(url ,true);
		#endif



	}
}
