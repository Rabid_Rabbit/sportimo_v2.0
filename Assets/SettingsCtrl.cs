﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BedbugRest;
using I2.Loc;
using BestHTTP;
using LitJson;
using Facebook.Unity;
public class SettingsCtrl : View {

	// Use this for initialization

	
	public UIGrid grid;
	public UIScrollView infoScroll;

	//private string endpoint = "/players";

	public List<UIToggle> notToggles = new List<UIToggle> ();
	public GameObject  nots;
	public UIToggle allNot;

	public void onTransitionInStart(viewData _viewData = null){
		//Debug.Log ("[Player] "+args.ToString());
		iTween.MoveTo (nots, iTween.Hash ("y", -460, "time", 0.01f, "islocal", true, "easetype", iTween.EaseType.easeOutCirc));

		infoScroll.ResetPosition ();
		//Get Not settings
		if (Main.AppUser.pushSettings != null) {
			// All Notification setting
			allNot.value = (bool)Main.AppUser.pushSettings[allNot.gameObject.name];
			// Rest Notifications
			foreach( UIToggle notToggle in notToggles) {

				notToggle.value = (bool)Main.AppUser.pushSettings[notToggle.gameObject.name];
			
			}
		}

		toggleStringLang (LocalizationManager.CurrentLanguageCode);
	}

	public GameObject[] lines = new GameObject[3];

	public GameObject nameAsk;
	public GameObject passAsk;

	public void Start(){
			if(PlayerPrefs.HasKey("sleep")){
			if(PlayerPrefs.GetString("sleep")=="never"){
				Screen.sleepTimeout = SleepTimeout.NeverSleep;
				NGUITools.SetActive(stop, true);
			}
			else{
				Screen.sleepTimeout = SleepTimeout.SystemSetting;
				NGUITools.SetActive(stop, false);
			}
		}
		else{
			Screen.sleepTimeout=SleepTimeout.SystemSetting;
		}
	}
	public void OnGetPlayerCallback(BestHTTP.HTTPRequest req,BestHTTP.HTTPResponse res){

		if(res.StatusCode != 200 && res.StatusCode != 304){
			MessagePanel.Instance.animMessage(res.DataAsText);
			return;
		}

		LitJson.JsonData data=LitJson.JsonMapper.ToObject(res.DataAsText);
//		Debug.Log(data["player"].ToJson());

	}

	public void toggleLang(GameObject lang) {
		switch (lang.name) {
		case "en":
				openLine (0);
				LocalizationManager.CurrentLanguageCode = "en"; 
			break;
			case "ar":
				openLine(1);
				LocalizationManager.CurrentLanguageCode = "ar"; 
			break;
			case "fr":
				openLine(2);
				LocalizationManager.CurrentLanguageCode = "fr"; 
			break;
		}

		print ("Current language is: " + LocalizationManager.CurrentLanguage);
		print ("and code is: " + LocalizationManager.CurrentLanguageCode);
		//LocalizationManager.CurrentLanguageCode = lang.name;
	}

	public void toggleStringLang(string lang) {
		switch (lang) {
		case "en":
			openLine (0);
			//LocalizationManager.CurrentLanguageCode = "en"; 
			break;
		case "ar":
			openLine(1);
			//LocalizationManager.CurrentLanguageCode = "ar"; 
			break;
		case "fr":
			openLine(2);
			//LocalizationManager.CurrentLanguageCode = "fr"; 
			break;
		}

		print ("Current language is: " + LocalizationManager.CurrentLanguage);
		print ("and code is: " + LocalizationManager.CurrentLanguageCode);
		//LocalizationManager.CurrentLanguageCode = lang.name;
	}

	void openLine( int line) {
		for (int i = 0; i< lines.Length; i++) {
			NGUITools.SetActive(lines[i], false);
		}
		NGUITools.SetActive (lines [line], true);
	}
		
	public void openTut() {
		//Application.LoadLevel ("tutorial");
		Main.Instance.openView("tutorial");
	}

	public void changePas() {
		print("Change Password");
		NGUITools.SetActive (passAsk, true);
	}

	public void updatePass(string newPAss) {
		Hashtable table=new Hashtable();
		table.Add("password",newPAss);
		API.PutOne(Main.Settings.apis["users"]+"/"+Main.AppUser._id,table,OnUpdatePassword);
		print ("Ths is the new pass: " + newPAss);	
	}

	private void OnUpdatePassword(HTTPRequest req, HTTPResponse res){
		MessagePanel.Instance.animMessage("Your password has been successfuly updated");
		JsonData request = JsonMapper.ToObject (System.Text.Encoding.ASCII.GetString (req.RawData));

		PlayerPrefs.SetString("password", request ["password"].ToString ());
		closeAll ();

	}

	public void changeName() {
		print("Change Name");
		NGUITools.SetActive (nameAsk, true);
	}

	public void updateName(string newName) {
		Hashtable table = new Hashtable();
		table.Add("name",newName);
		API.PutOne(Main.Settings.apis["users"]+"/"+Main.AppUser._id,table,OnUpdateName);
		print ("Ths is the new pass: " + newName);	
		closeAll ();
	}

	private void OnUpdateName(HTTPRequest req, HTTPResponse res){
		MessagePanel.Instance.animMessage("Your name has been successfuly updated");
		//JsonData request = JsonMapper.ToObject (System.Text.Encoding.ASCII.GetString (req.RawData));

		//PlayerPrefs.SetString("password", request ["password"].ToString ());
		closeAll ();

	}
	public GameObject stop;
	public void PreventSleep(){

		if(PlayerPrefs.HasKey("sleep")){
			if(PlayerPrefs.GetString("sleep") == "never"){
				NGUITools.SetActive(stop, false);
				PlayerPrefs.SetString("sleep","system");
				Screen.sleepTimeout = SleepTimeout.SystemSetting;
			}
			else{
				NGUITools.SetActive(stop, true);
				PlayerPrefs.SetString("sleep","never");
				Screen.sleepTimeout = SleepTimeout.NeverSleep;
				
			}
		}
		else{
			PlayerPrefs.SetString("sleep","never");
			Screen.sleepTimeout=SleepTimeout.NeverSleep;
		}
	}

	public void closeAll() {
		NGUITools.SetActive (nameAsk, false);
		NGUITools.SetActive (passAsk, false);
	}

	public void muteSound() {
		print("Mute Sound");
	}



	public void editNots() {

		iTween.MoveTo (nots, iTween.Hash ("y", -33, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeOutCirc));
		print ("Edit Notifications");
	}

	public void closeNots() {
		iTween.MoveTo (nots, iTween.Hash ("y", -460, "time", 0.3f, "islocal", true, "easetype", iTween.EaseType.easeOutCirc));
		User.updatePushSetts (Main.AppUser.pushSettings);
		Loader.Visible (true);
	}

	public bool jsonDataHelper (JsonData notsList, string not)
	{
		if (notsList != null)
			if (notsList.Keys.Contains (not))
				return (bool)notsList [not];
		
		return false;
	}

	public void openPol() {
		print("Privacy Policy");
	}

	public void openEula() {
		print("Open EULA");
	}

	public void openTerms() {
		print("Open Terms");
	}

	public void unsubscribe() {
		print("Unsbscribe");
	}

	public void logout() {
		Main.forcedLogout = false;

		PlayerPrefs.DeleteKey("password");
		PlayerPrefs.DeleteKey("username");
		PlayerPrefs.DeleteKey("token");
		PlayerPrefs.DeleteKey("social_id");
		PlayerPrefs.DeleteKey("avatar");

		if(FB.IsLoggedIn)
			FB.LogOut();

		Main.Instance.ClearViewStack();
		ConnectionManager.Instance.DisconnectUser();
		Main.AppUser = new User ();
		Main.Instance.openView ("login2");
	}

	public void onValueChange (UIToggle notChanged) {

		print(notChanged.gameObject.name+" :"+notChanged.value);

		if (Main.AppUser.pushSettings != null) {
			Main.AppUser.pushSettings [notChanged.gameObject.name] = (bool)notChanged.value;
			print (Main.AppUser.pushSettings [notChanged.gameObject.name]);

			// If all is changed
			if(notChanged.gameObject.name == "all") {
				foreach( UIToggle notToggle in notToggles) {
					notToggle.value = notChanged.value;
					Main.AppUser.pushSettings [notToggle.gameObject.name] = (bool)notChanged.value;
				}
			}
		}
	}

	public void FacebookConnect(){
		if(!FB.IsInitialized)
			FB.Init(FBLogin);
		else{
			FBLogin();
		}
	}

	public void FBLogin(){
		if(FB.IsLoggedIn){
			if(!string.IsNullOrEmpty(Main.AppUser.social_id)){
				MessagePanel.Instance.animMessage("Facebook account already connected.");
				return;
			}
			
			FB.API ("me?fields=id,name,email", HttpMethod.GET, OnGetData);
			}
		else{
			FB.LogInWithReadPermissions(new List<string>(){"email,user_friends"},OnFBLogin);
		}
	}

	private string social_id;
	public void OnFBLogin(ILoginResult fbresult){
		if (fbresult.Error != null) {
			Debug.Log (fbresult.Error);
		} else {
            
			JsonData data = JsonMapper.ToObject (fbresult.RawResult);
            if (data.Keys.Contains("user_id"))
            {
                social_id = data["user_id"].ToString();
                Hashtable fbtable = new Hashtable();
                fbtable.Add("social_id", data["user_id"].ToString());
                API.PutOne(Main.Settings.apis["users"] + "/" + Main.AppUser._id, fbtable, OnConnectCallback);
                Debug.Log(fbresult.RawResult);
            }
		}
	}
	public void OnGetData(IGraphResult result){
			JsonData data = JsonMapper.ToObject (result.RawResult);
			Hashtable fbtable=new Hashtable();
			fbtable.Add ("social_id", data ["id"].ToString ());
			API.PutOne(Main.Settings.apis["users"]+"/"+Main.AppUser._id,fbtable,OnConnectCallback);

	}

	public void OnConnectCallback(BestHTTP.HTTPRequest req,BestHTTP.HTTPResponse res){
		if(res.StatusCode==200){
			MessagePanel.Instance.animMessage("Facebook connected successfully");
			Main.AppUser.social_id = social_id;
			PlayerPrefs.SetString ("social_id", AccessToken.CurrentAccessToken.UserId);
		}
		else{
			FB.LogOut();
			MessagePanel.Instance.animMessage("Facebook id already connected to other account");
			
		}
	}
}
