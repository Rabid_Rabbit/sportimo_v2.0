﻿using UnityEngine;
using System.Collections;

/// <summary>
/// UI Controller script
/// </summary>
[RequireComponent(typeof(Collider))]
[AddComponentMenu("MainUI/Presentation/InputChild")]
public class InputChild : MonoBehaviour
{
	/// <summary>
	/// determines l/r selection order
	/// </summary>
	public int Col;
	
	/// <summary>
	/// determines up/down selection order
	/// </summary>
	public int Row;
	
	/// <summary>
	/// Determines Tab order
	/// </summary>
	public int TabIndex = 0;

	private InputController controller;

	public void Awake()
	{
		controller = NGUITools.FindInParents <InputController>(gameObject);
	}
	
	void OnEnable()
	{
		//   if(controller)
		controller.Add(this);
	}
	
	void OnDisable()
	{
		// if (controller)
		controller.Remove(this);
		
		if (UICamera.selectedObject == gameObject)
		{
			UICamera.selectedObject = null;
		}
	}
	
	void OnHover(bool isHover)
	{
		if (enabled && isHover)
		{
			UICamera.selectedObject = gameObject;
		}
		else if (UICamera.selectedObject == gameObject)
		{
			UICamera.selectedObject = null;
		}
	}
	
	void OnKey(KeyCode key)
	{
		if (controller.Paused)
			return;
		
		if (enabled && NGUITools.GetActive(gameObject))
		{
			switch (key)
			{
			case KeyCode.LeftArrow:
				controller.GoLeft(this);
				break;
			case KeyCode.RightArrow:
				controller.GoRight(this);
				break;
			case KeyCode.UpArrow:
				controller.GoUp(this);
				break;
			case KeyCode.DownArrow:
				controller.GoDown(this);
				break;
			case KeyCode.Space:
			case KeyCode.Return:
				controller.GoInvoke(this);
				break;
			case KeyCode.Tab:
				if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
				{
					controller.GoBack(this);
				}
				else
				{
					controller.GoNext(this);
				}
				break;
			}
		}
	}
}