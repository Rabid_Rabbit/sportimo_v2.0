﻿using UnityEngine;
using UnityEditor;
using System.Text;
using System.Xml;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class UnicornManager : EditorWindow
{
	private Vector2 scroll;
	public List<View> SceneViews;

   

    



    Dictionary<string,bool> defaultStates = new Dictionary<string, bool>(){
		{"stats",true},
		{"achievs",true},
		{"tutorial",true},
		{"playerInfo",true},
		{"prizes",true},
		{"standings",true},
		{"leaders",true},
		{"teaminfo",true},
		{"login2",true},
		{"login",true},
		{"userview",true},
		{"mail",true},
		{"settings",true},
		{"menu",true},
		{"profile",true},
		{"news",true},
		{"match",true},
		{"matches",true},
		{"favteam", true},
		{"userData", true}
	};

	View selectedview {
		get;
		set;
	}
	
	void Update ()
	{
	}


	// Add menu named "My Window" to the Window menu
	[MenuItem ("Unicorn/Manager")]
	static void Init ()
	{
		// Get existing open window or if none, make a new one:
		UnicornManager window = (UnicornManager)EditorWindow.GetWindow (typeof(UnicornManager));
		window.autoRepaintOnSceneChange = true;
		window.Show ();
	}

	void Awake ()
	{
		findViews ();
            EditorApplication.playmodeStateChanged += OnPlayModeChanged;
      
    }

    private void OnPlayModeChanged()
        {
        Debug.Log("On change");
        if (!EditorApplication.isPlaying && !EditorApplication.isPlayingOrWillChangePlaymode) ;
            findViews();
        }

        void OnEnable ()
	{
		findViews ();

	}

	void findViews ()
	{
		SceneViews = GameObject.FindObjectsOfType<View> ().OrderBy (o => o.name).ToList ();

//		SceneViews = SceneViews;
		//SceneViews = Resources.FindObjectsOfTypeAll<View> ();
	}
	
	void OnDisable ()
	{
		SceneViews = null;
	}
	
	void YourPlaymodeStateChangedHandler ()
	{
		//		if (EditorApplication.isPlaying) {
		//			levelPack = userData.levelPackToLoad;
		//			levelID = userData.levelToLoad;
		//			LoadLevel ();
		//		}
	}

	//private string mDelName = "";
	void OnSelectionChange ()
	{
		//mDelName = null;
		Repaint ();
	}
	
	void Start ()
	{
		
	}
	
	//	void Update()
	//	{
	//		Debug.Log("List:"+actorsList.Count);
	//	}
	private Color uicolor;
	Vector2 windowScrollPos;

	void setAllToHiddenExcept (View _view)
	{
		foreach (View _v in SceneViews) {
			if(_v == _view){
				Debug.Log(_v.name);
				_v.GetComponent<UIPanel>().alpha = 1;
                //_v.gameObject.SetActive(true);
            }
            else{
				_v.GetComponent<UIPanel>().alpha = 0;
                //_v.gameObject.SetActive(false);
            }
		}
        EditorUtility.SetDirty(this);
    }

	void setAllVisibilityTo(bool state){
		foreach (View _v in SceneViews) {
			if(state)
				_v.GetComponent<UIPanel>().alpha = 1;
			else
				_v.GetComponent<UIPanel>().alpha = 0;
            //_v.gameObject.SetActive(state);

        }

        //allViewsVisibility = state;
    }

	void setAllVisibilityToDefault ()
	{
		foreach (View _v in SceneViews) {
			//Debug.Log(defaultStates[_v.name]);
			//if(defaultStates.ContainsKey(_v.name)){
			//	_v.gameObject.SetActive(defaultStates[_v.name]);
			//}else{
			//	_v.gameObject.SetActive(false);
			//}

			_v.GetComponent<UIPanel>().alpha = 0;
		}
	}
	
	void OnGUI ()
	{
		if (UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene().name.IndexOf ("Main") == -1) {
			GUILayout.Space (6f);
			EditorGUILayout.HelpBox ("This editor window only works in " +
			                         "the Main scene. Please switch to the " +
			                         "Hub scene or the Playground scene if you want to use it. ", MessageType.Info);
			return;
		}
		
//		windowScrollPos = EditorGUILayout.BeginScrollView (windowScrollPos,
//		                                                   false,
//		                                                   false);
		//vertical space
		GUILayout.Space (20);
		
//		GUILayout.BeginVertical ();
		

		
		GUILayout.Label ("Level Creator", EditorStyles.boldLabel);
		
		
		
		if (GUI.changed) {			
			EditorUtility.SetDirty (this);
		}
		
		int count = SceneViews.Count;
		
		GUI.backgroundColor = Color.white;

		NGUIEditorTools.DrawSeparator ();

		int levelIndex = 0;

		if (NGUIEditorTools.DrawHeader ("Views: " + count, true)) {
			{
                if (GUILayout.Button("Gather all views")) findViews();
                if (GUILayout.Button("Toggle all to default state")) setAllVisibilityToDefault();
//				EditorGUILayout.BeginVertical (GUILayout.MinHeight (148.0f));
				GUILayout.Space (7f);
				
				
				scroll = GUILayout.BeginScrollView (scroll);
				
				foreach (View _view in SceneViews) {
					if(_view == null) break;
					++levelIndex;
					
					GUILayout.Space(-1f);
					bool highlight = (UIAtlasInspector.instance != null) && (selectedview == _view);
					GUI.backgroundColor = highlight ? Color.white : new Color(0.8f, 0.8f, 0.8f);
					GUILayout.BeginHorizontal("AS TextArea", GUILayout.MinHeight(20f));
					GUI.backgroundColor = Color.white;
					GUILayout.Label(levelIndex.ToString(), GUILayout.Width(24f));
					
					if (GUILayout.Button(_view.name, "OL TextField", GUILayout.Height(20f)))
						selectedview = _view;
					
					if (_view.GetComponent<UIPanel>().alpha == 1)
					{
						GUI.color = Color.green;
						GUILayout.Label("Visible", GUILayout.Width(50f));
						GUI.color = Color.white;
					}
					else 
					{
						GUI.color = Color.cyan;
						GUILayout.Label("Hidden", GUILayout.Width(50f));
						GUI.color = Color.white;
					}

							// If we have not yet selected a sprite for deletion, show a small "X" button

					if (GUILayout.Button("toggle", GUILayout.Width(50f))) _view.gameObject.SetActive(!_view.gameObject.activeSelf);
					if (GUILayout.Button("Solo", GUILayout.Width(50f))) setAllToHiddenExcept(_view);

					GUILayout.EndHorizontal();

					}
					

					
				
				GUILayout.EndScrollView ();

			}
		}
		
//		EditorGUILayout.EndVertical ();
	}

	//private bool allViewsVisibility = true;

}
