﻿//#define FOCUS_GROUP 

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using BestHTTP;
using Facebook.Unity;

using I2.Loc;



public class historyItem
{
	public View view;
	public string name;
	public viewData data;
}

public class historyOverlayItem
{
    public string viewName;
    public string overlayName;    
}

public class Main : MonoBehaviour
{
    public static string app_version = "2.0.2.4";
    // Current Build Version
    public static int ios_local_build_version = 1;
    public static int android_local_build_version = 1;

    public static User AppUser = new User ();

	// This should be loaded on, on bootstrap.
	// It contains the production paths for the APIs
	public static AppSettings Settings = new AppSettings ();

	public View[] views;
	public Dictionary<string, View> viewsDict = new Dictionary<string, View> ();
	public View viewSelected;
	private bool isAnimating = false;
	private bool bottomMenuOpen = false;
	public GameObject bottomMenu;
	public GameObject bottomMatchMenu;
	private btmMenuCtrl menuCtrl;

	public GameObject ConnectionError;

	/// <summary>
    /// Action that informs registered components that the view changed. 
    /// Used mostly to clean leftovers in the view or close static modules.
    /// </summary>
	public static Action onViewChange;

    /// <summary>
    /// Action that inform that a specific view overlay should close.
    /// It is send by pressing the back button on the header bar while a
    /// registered overlay is open.
    /// </summary>
	public static Action<historyOverlayItem> onOverlayItemClosed;


    public static string InitialView = "login";

	// Flag to inform controllers that the user's last logout was forced. 
	public static bool forcedLogout {
		get;
		set;
	}
	
	// Logo
	public LogoAnimCtrl LogoCtrl;
	public bool logoStartPlayed = false;

	public delegate void LateInit ();
	
	public static event LateInit lateInit;
	// Static singleton property
	public static Main Instance { get; private set; }


	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)) { 

			if (viewSelected.name == "menu"){
				openCloser();
				print ("OPEN CLOSE POP UP");
			}else{
				goBack();
			}
		
		}
	}

	public void closeApp() {
		Application.Quit(); 
	}

	public GameObject closer;
	public void closeCloser() {
		NGUITools.SetActive (closer, false);
	}

	public void openCloser() {
		NGUITools.SetActive (closer, true);
	}

	public void StartLate ()
	{
		lateInit ();
	}

	void Awake ()
	{
		//PlayerPrefs.DeleteAll();
		// First we check if there are any other instances conflicting
		if (Instance != null && Instance != this) {
			// If that is the case, we destroy other instances
			Destroy (gameObject);
		}
		
		// Here we save our singleton instance
		Instance = this;
		
		// Furthermore we make sure that we don't destroy between scenes (this is optional)
		//DontDestroyOnLoad (gameObject);

//		Debug.Log ("Does Main start before anything else?");
		// Use local data. /* ONLY FOR DEBUGGING */
		if (false) {
			Main.Settings.apis = new Dictionary<string, string> (){
				{"gamecards","http://localhost:3030/v1/gamecards"},
				{"data","http://localhost:3030/v1/data"},
				{"users", "http://localhost:3030/v1/users"},
				{"questions","http://localhost:3030/v1/questions"},
				{"leaderpay","http://localhost:3030/leaderpay/v1"},
				//{"socket","ws://localhost:8080"}
				{"socket","ws://socketserverv2-56658.onmodulus.net/"}
            };
		}



		// Connect the sockets
		ConnectionManager.Connect ();

//		Debug.Log("---- Main -----: "+ FB.IsInitialized);
		// Facebook Init
		if (!FB.IsInitialized) {
			// Initialize the Facebook SDK
			FB.Init(InitCallback, OnHideUnity);
		} else {
			// Already initialized, signal an app activation App Event
			FB.ActivateApp();
		}
	}

	private void InitCallback ()
	{
//		Debug.Log ("FB.IsLoggedIn: "+ FB.IsLoggedIn);

		if (FB.IsInitialized) {
			// Signal an app activation App Event
			FB.ActivateApp();
		} else {
			Debug.Log("Failed to Initialize the Facebook SDK");
		}
	}
	
	private void OnHideUnity (bool isGameShown)
	{
		if (!isGameShown) {
			// Pause the game - we will need to hide
			Time.timeScale = 0;
		} else {
			// Resume the game - we're getting focus again
			Time.timeScale = 1;
		}
	}

    public static bool isPaidCustomer(string _term_text, bool silent = false)
    {
		// Just Testing
		//AppUser.customerType = "free";
        if (AppUser.customerType == "free")
        {
            if(!silent)
            CoachModalController.Confirm(_term_text, null, (option) => {
                if (option == "Btn1")
                {
                    DummySubscribe.show();
                    //#if !FOCUS_GROUP
                    //#if UNITY_ANDROID
                    //                        ConfirmationModalController.Confirm(I2.Loc.LocalizationManager.GetTermTranslation("_subscribe_by_text", true, 23), null, I2.Loc.LocalizationManager.GetTermTranslation("_subscribe_mobile", true, 23), I2.Loc.LocalizationManager.GetTermTranslation("_subscribe_google", true, 23), (userSelection) =>
                    //                        {
                    //                            if (userSelection == "Btn1")
                    //                            {
                    //                                Debug.Log("mobile");
                    //                                UnityEngine.SceneManagement.SceneManager.LoadScene("subscription");
                    //                            }
                    //                            else if (userSelection == "Btn2")
                    //                            {
                    //                                Debug.Log("GooglePlay Purchase");
                    //                                StoreListener.Instance.InitiatePurchase("com.gup.basicweekly");
                    //                            }
                    //                        }, true);
                    //#elif UNITY_IPHONE
                    //                       StoreListener.Instance.InitiatePurchase("com.gup.basicweekly");
                    //#elif UNITY_EDITOR
                    //                        confirm("purchase");
                    //#endif
                    //#else
                    //                    //DummySubscribe.show();
                    //#endif
                }
            });

            //ConfirmationModalController.Confirm(LocalizationManager.GetTermTranslation("_subscription_required_header"), LocalizationManager.GetTermTranslation("_subscription_required_text"), null, null, (option) =>
            //{
            //    if (option == "Btn1")
            //    {
            //        Debug.Log("go to subscription");
            //    }
            //});
            return false;
        }
        return true;
    }


    

    IEnumerator Start ()
	{
		// Populate the views[] List with all the views
		views = FindObjectsOfType (typeof(View)) as View[];

		// Wait for the width to be initialized
		yield return new WaitForSeconds (0.1f);

		foreach (View view in views) {
			//Add to disctionary
			viewsDict.Add (view.name, view);

			// Close all the views
			view.GetComponent<UIPanel> ().alpha = 0;
		}

		//yield return new WaitForSeconds (0.5f);
		// make the menu view the Selected and turn it On
		if(PlayerPrefs.HasKey("username"))
			openView ("login2");
		else
			openView (InitialView);
//		viewSelected = viewsDict ["login2"];
		//NGUITools.SetActive(viewSelected.gameObject, true);
		viewSelected.GetComponent<UIPanel> ().alpha = 1;

		menuCtrl = bottomMenu.GetComponent<btmMenuCtrl> ();
		InvokeRepeating("TestConnectivity",10f,10f);
	}

	public viewData historyDataObjectBefore;
	public viewData historyDataObjectAfter;

    /// <summary>
    /// Used to store the history of opened Vies
    /// </summary>
	public Stack<historyItem> viewStack = new Stack<historyItem> ();

    /// <summary>
    /// Registered overlay that will hijack the call to go back in View History;
    /// </summary>
    public static historyOverlayItem registeredOverlay;


    /// <summary>
    /// Used to register an overlay that will hijack history.
    /// The module is responsible to hook on the onOverlayItemClosed action
    /// and handle the following methods that close the overlay. The action
    /// is fired when the user presses the Back button or Esc.
    /// </summary>
    public static void registerOverlayHistory(string viewName, string overlayName)
    {
        historyOverlayItem newOveraly = new historyOverlayItem();
        newOveraly.viewName = viewName;
        newOveraly.overlayName = overlayName;
        registeredOverlay = newOveraly;
      
    }

    public static void unregisterOverlay(string overlayName)
    {
        if (registeredOverlay != null && registeredOverlay.overlayName == overlayName)
            registeredOverlay = null;
    }

	public void openView (string viewName, object attrObj = null, bool useCachedHistory = false)
	{

		if (onViewChange != null)
			onViewChange ();

        // De-allocate leftover refernces
        onOverlayItemClosed = null;

		if (viewName == "menu" && !logoStartPlayed || viewName == "login" && !logoStartPlayed) {
//			print (logoStartPlayed);
			string hello = LocalizationManager.GetTermTranslation ("mes_title_hello", true);
			hello = hello.Replace ("[user_name]", AppUser.username);
			MessagePanel.Instance.animMessage (LocalizationManager.GetTermTranslation ("mes_body_welcome", true), hello, "sportimo-Logo", 3.0f, 8.0f);

			logoStartPlayed = true;
//			print ("Playing AnimEnd");
			if (PlayerPrefs.HasKey ("playedOnce"))
				LogoCtrl.playAnimation ("logoAnimationEnd");

			PlayerPrefs.SetString ("playedOnce", "playedOnce");
		} else if (viewName == "menu") {
			if (!PlayerPrefs.HasKey ("playedOnce"))
				PlayerPrefs.SetString ("playedOnce", "playedOnce");
		}
		// Check if already open
		if (viewName != viewSelected.name && !isAnimating) {

			StartCoroutine (openViewEnum (viewName, attrObj, useCachedHistory));
		}
	}
//
	private IEnumerator openViewEnum (string viewName, object attrObj, bool useCachedHistory = false)
	{
		if (onViewChange != null)
			onViewChange ();

		//iTween.Stop ();
		//yield return new WaitForSeconds (0.3f);
		// Close the loader if opened
		Loader.Visible (false);
		// store the data object
		viewData newViewData = new viewData(attrObj, false);
		historyDataObjectAfter = newViewData;

		if(historyDataObjectBefore!=null)
		historyDataObjectBefore.useCache = useCachedHistory;

		// Get the view
		View newView;
		if (viewsDict.ContainsKey (viewName))
			newView = viewsDict [viewName];
		else{
			Debug.Log(viewName);
			yield break;
		}

		// get the Panels for the depth change
		UIPanel newViewPanel = newView.GetComponent<UIPanel> ();
		UIPanel oldViewPanel = viewSelected.GetComponent<UIPanel> ();

		// Open the New View Gameobject
		TweenAlpha.Begin (newView.gameObject, 0.0f, 1);
		TweenAlpha.Begin (oldViewPanel.gameObject, 0.0f, 1);
		// Change its depth so it is onTop
		newViewPanel.depth = 5;
		// Find Scrollview in children
		if (newView.scrollViewUiPanel != null) {
			newView.scrollViewUiPanel.depth = 6;
			TweenAlpha.Begin (newView.scrollViewUiPanel.gameObject, 0.3f, 1);
		}
		// Change old View's depth to back
		oldViewPanel.depth = 0;

		if (viewSelected.scrollViewUiPanel != null) {
			//viewSelected.scrollViewUiPanel.depth = 0;
			TweenAlpha.Begin (viewSelected.scrollViewUiPanel.gameObject, 0.1f, 0);
		}

		// Check if view has bottom menu and open it if not already open
		if (newView.bottomMenuType != 0) {
			if (newView.bottomMenuType == 1) {
				// close match menu
				NGUITools.SetActive (bottomMatchMenu, false);
				// Open the line in the menu
//				print ("Open Line:" + newView.menuSelectNum);
				menuCtrl.openLine (newView.menuSelectNum);

				if (!bottomMenuOpen)
					NGUITools.SetActive (bottomMenu, true);

			}
			if (newView.bottomMenuType == 2) {

				// Open match menu (no need to close Bottom Menu, higher depth)
				NGUITools.SetActive (bottomMatchMenu, true);
				
			}
		} else { 
			// Close both Bottom and BottomMatch menus
			NGUITools.SetActive (bottomMenu, false);
			NGUITools.SetActive (bottomMatchMenu, false);
		}

		// Slide the new View
		isAnimating = true; // is still animating the views

		iTween.MoveFrom (newView.gameObject, iTween.Hash ("x", 0, "time", 0.2f, "easetype", iTween.EaseType.easeOutQuad, "islocal", true)); 
		newView.BroadcastMessage ("onTransitionInStart", newViewData, SendMessageOptions.DontRequireReceiver);

		// Open And Close View Objects
		if (!newView.cancelObjOpen) {
			foreach (GameObject obj in newView.objToClose) {
				TweenAlpha.Begin (obj, 0.5f, 1);
			}
		}
		foreach (GameObject obj in viewSelected.objToClose) {
			TweenAlpha.Begin (obj, 0.0f, 0);
		}


		// Slide the old View
		iTween.MoveTo (viewSelected.gameObject, iTween.Hash ("x", 0, "time", 0.2, "easetype", iTween.EaseType.easeOutQuad, "islocal", true, "onCompleteTarget", gameObject, "onComplete", "openViewEnded", "oncompleteparams", newView)); 
	
		viewSelected.BroadcastMessage ("onTransitionOutStart", SendMessageOptions.DontRequireReceiver);

		historyItem _history = new historyItem ();
		_history.name = newView.name;
		_history.view = newView;
		_history.data = newViewData;

		// Add the old View in the top of the Stack for the back button to work
		viewStack.Push (_history);

		//		Debug.Log ("Added to Stack: "+_history.name);
		logStack (viewStack);

		yield return new WaitForSeconds (0.0f);
		// Close the old View
		oldViewPanel.alpha = 0;


		



//		print ("VIEW SELECTED: " + viewSelected.name);
	}

	// When The transition ends
	void openViewEnded (View newView)
	{
		historyDataObjectBefore = historyDataObjectAfter;
		//Reset Position of oldView
		viewSelected.gameObject.transform.localPosition = Vector3.zero;
		// Change what view is curently selected
		viewSelected = newView;
		// Stoped Animating the view
		isAnimating = false;
	}



	private void logStack(Stack<historyItem> stack){
//		int count = 0;
//		foreach (historyItem i in stack)
//		{
//			Debug.Log(count+": "+i.name);
//			count++;
//		}
	}
	public void ClearViewStack(){
		viewStack.Clear();
		{
			
		}
	}
//
	public void goBack ()
	{

		if (!isAnimating)
			StartCoroutine (goBackEnum ());
	}

    IEnumerator goBackEnum ()
	{

        if (registeredOverlay != null)
        {
            if (onOverlayItemClosed != null)
                onOverlayItemClosed(registeredOverlay);

            registeredOverlay = null;

           yield break;
        }

        if (onViewChange != null)
			onViewChange ();

		if (viewStack.Count > 1) {
			viewStack.Pop ();
		
			historyItem backView = viewStack.Peek ();
			logStack (viewStack);
			
			// get the Panels for the depth change
			UIPanel newViewPanel = backView.view.gameObject.GetComponent<UIPanel> ();
			UIPanel oldViewPanel = viewSelected.gameObject.GetComponent<UIPanel> ();
			
			// Open the New View Gameobjct
			//NGUITools.SetActive (backView, true);
			newViewPanel.alpha = 1;
			
			//Open All Objects set to Close
			if (!backView.view.cancelObjOpen) {
				foreach (GameObject obj in backView.view.objToClose) {
					TweenAlpha.Begin (obj, 0.5f, 1);
				}
			}
			// Change its depth so it is onTop
			//newViewPanel.depth = 5;
			// Change old View's depth to back
			//oldViewPanel.depth = 0;
			// Slide the new View
			isAnimating = true; // is still animating the views
			TweenAlpha.Begin (viewSelected.gameObject, 0.1f, 0);
			iTween.MoveTo (viewSelected.gameObject, iTween.Hash ("x", 0, "time", 0.2, "easetype", iTween.EaseType.easeOutQuad, "islocal", true, "onCompleteTarget", gameObject, "onComplete", "goBackEnded", "oncompleteparams", viewSelected));
			iTween.MoveTo (backView.view.gameObject, iTween.Hash ("x", 0, "time", 0.2, "easetype", iTween.EaseType.easeOutQuad, "islocal", true));
			
			backView.view.BroadcastMessage ("onTransitionInStart", backView.data, SendMessageOptions.DontRequireReceiver);
			
			viewSelected.BroadcastMessage ("onTransitionOutStart", SendMessageOptions.DontRequireReceiver);
			// This must be the same time as the animation so the views dont switch
			
			// Check if view has bottom menu and open it if not already open
			if (backView.view.bottomMenuType != 0) {
				if (backView.view.bottomMenuType == 1) {
					// close match menu
					NGUITools.SetActive (bottomMatchMenu, false);
					// Open the line in the menu
					menuCtrl.openLine (backView.view.menuSelectNum);
					
					if (!bottomMenuOpen)
						NGUITools.SetActive (bottomMenu, true);
					
				}
				if (backView.view.bottomMenuType == 2) {
					
					// Open match menu (no need to close Bottom Menu, higher depth)
					NGUITools.SetActive (bottomMatchMenu, true);
					
				}
			} else { 
				// Close both Bottom and BottomMatch menus
				NGUITools.SetActive (bottomMenu, false);
				NGUITools.SetActive (bottomMatchMenu, false);
			}
			
			yield return new WaitForSeconds (0.2f);
			
			// Close the old View
			//NGUITools.SetActive (viewSelected.gameObject, false);
			//oldViewPanel.alpha = 0;
			// Find Scrollview in children
			if (backView.view.scrollViewUiPanel != null) {
				backView.view.scrollViewUiPanel.depth = 6;
				TweenAlpha.Begin (backView.view.scrollViewUiPanel.gameObject, 0.3f, 1);
			}
			
			//viewSelected.transform.position = Vector3.zero;
			newViewPanel.depth = 5;
			
			// Change what view is curently selected
			oldViewPanel.depth = 0;
			
			
			viewSelected = backView.view;
		
		} else {

		}


	}


	public GameObject submenu;
	public void openSubmenu(){
		NGUITools.SetActive(submenu,true);

	}

	public void closeSubmenu(){
		NGUITools.SetActive(submenu,false);
	}

	public void ValidateAgain(){
		closeSubmenu();
		StoreListener.Instance.ValidateReceipt();
	}

	public void ReturnToSub(){
		Application.LoadLevel("subscription");
	}
	
	void goBackEnded (View view)
	{
		//Reset Position of oldView
		view.gameObject.transform.localPosition = Vector3.zero;

		isAnimating = false;
	}

	public void openHome ()
	{
		openView ("menu");
	}

	public void openSettings ()
	{
		openView ("settings");
	}

	public void testMethodForMenu ()
	{
		openView ("profile");
	}

	public void openUserData ()
	{
		openView ("userData");
	}

	public bool Paywall(string feature){
		if(AppUser.subscription.status=="free"){
			ConfirmationModalController.Confirm("This feature is only available to pro users",feature+" is only accessible to pro users. If you would like to subscribe now, click on the \"Subscribe now\" button below. Otherwise, you may continue as a free user.","Subscribe Now","Continue as free user",(option)=>{
				if(option=="Btn1"){
        		UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex -1);
				}
				
			});
			return false;
		}
		else{
			return true;
		}
		
	}
    public void TestConnectivity()
    {

        HTTPRequest request = new HTTPRequest(new Uri("http://www.google.com"),TestCallback);
     
        // Very little time, for testing purposes:
        //request.ConnectTimeout = TimeSpan.FromMilliseconds(2);
        request.Timeout = TimeSpan.FromSeconds(10);
        request.DisableCache = true;
        request.Send();



    }

	public void TestCallback(HTTPRequest req,HTTPResponse resp){
 {
          switch (req.State)
          {
          // The request finished without any problem.
          case HTTPRequestStates.Finished:
                  if(resp.StatusCode==200){
					  NGUITools.SetActive(ConnectionError,false);
                  }
                  break;

          // The request finished with an unexpected error.
          // The request's Exception property may contain more information about the error.
          case HTTPRequestStates.Error:
                  Debug.LogError("Request Finished with Error! " +
                  (req.Exception != null ?
                  (req.Exception.Message + "\n" + req.Exception.StackTrace) :
                 "No Exception"));
					  NGUITools.SetActive(ConnectionError,true);

                  break;

          // The request aborted, initiated by the user.
          case HTTPRequestStates.Aborted:
                  Debug.LogWarning("Request Aborted!");
                  break;

          // Ceonnecting to the server timed out.
          case HTTPRequestStates.ConnectionTimedOut:
                  Debug.LogError("Connection Timed Out!");
					  NGUITools.SetActive(ConnectionError,true);
                  break;

          // The request didn't finished in the given time.
          case HTTPRequestStates.TimedOut:
					  NGUITools.SetActive(ConnectionError,true);
                  Debug.LogError("Processing the request Timed Out!");
                  break;
          }

      }

	}

}
