﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AppSettings {

    public  string UseCDN = "http://c9oeltrn.cloudimg.io/s/cdn/x/";
	public  string CropCDN ="http://c9oeltrn.cloudimg.io/s/crop/";

	// Versions
	public int ios_current_build_version = 1;
	public int android_current_build_version = 1;
	public bool update_required = false;
	public bool prizes = true;
    public List <string> GRS = new List<string>()
    {
        "https://sportimo.mod.bz",
        "https://sportimo_backup.mod.bz"
    };

    //private int current_grs = 0;
    //public bool hasLooped = false;

    //public void advanceCurrentGRS()
    //{
    //    current_grs++;
    //    if (current_grs > GRS.Count - 1)
    //    {
    //        current_grs = 0;
    //        hasLooped = true;
    //    }
        
    //}

	// APIs
	public Dictionary<string,string> apis = new Dictionary<string, string>(){
		{"gamecards", "https://sportimo.mod.bz/v1/gamecards"},
		{"data","https://sportimo.mod.bz/v1/data"},
		{"users", "https://sportimo.mod.bz/v1/users"},
		{"questions", "https://sportimo.mod.bz/v1/questions"},
		{"polls", "https://sportimo.mod.bz/v1/polls"},
		{"leaderpay", "https://sportimo.mod.bz/leaderpay/v1"},
		{"socket", "wss://socketserverv2-56658.onmodulus.net/"}
	};
	// Twitter
	public string hashtag = "#sportimo";
}
