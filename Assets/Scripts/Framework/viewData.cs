﻿
using System.Collections;

public class viewData {

	public object data;
	public bool useCache;

	public viewData(object data, bool cache = false){
		this.data = data;
		this.useCache = cache;
	}
}
