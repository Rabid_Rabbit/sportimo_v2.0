﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class View : MonoBehaviour {

	public int id;
	//public string name;
	public UIPanel scrollViewUiPanel;
	public int bottomMenuType; //(0:none, 1:default, 2:match)
	public int menuSelectNum; // For Menu line selection
	public bool cancelObjOpen = false;

	public List<GameObject> objToClose = new List<GameObject>();

	public View (){

	}

	void Start() {
		Init ();
	}
	
	public View(int viewId, string viewName)
	{
		id = viewId;

	}

	//These methods are virtual and thus can be overriden
	//in child classes
	public virtual void Init ()
	{
//		Debug.Log("The View base function.");  

	}
	
	public virtual void LateInit(){
		
	}

	// Register to the changeSelection Event
	void OnEnable()
	{
		TopMenuCtrl.pickedSelection += selectionChanged;
		
	}
	
	
	void OnDisable()
	{
		TopMenuCtrl.pickedSelection -= selectionChanged;

	}

	// When the view starts its Transitions this will do its own transition
//	void onTransitionInStart() {
//		print ("Do something when the transition In Starts");
//	}

	public virtual void selectionChanged (string selection)
	{
		// print the string passed by the event
		// filter the list and populate the scrollview with the new information
		//print (selection);
	}




}
