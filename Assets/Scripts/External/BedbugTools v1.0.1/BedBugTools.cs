﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Texture2DExtensions;
using I2.Loc;

//using Prime31;

public class BedBugTools : MonoBehaviour
{

	public static string UseCDN = Main.Settings.UseCDN;
	public static string CropCDN = Main.Settings.CropCDN;

	public static BedBugTools _instance;

	public static BedBugTools Instance {
		get {
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType<BedBugTools> ();
				if (_instance == null) {
					_instance = new GameObject ().AddComponent<BedBugTools> ();
					_instance.name = "Bedbug Tools";
				}
			}

			return _instance;
		}
	}


	//----------------------------------------------
	//				Utility Methods
	//----------------------------------------------

	/**
	 * Ordered Key
	*/
	public static string GetStringByOrder (List<string> keys, Dictionary<string,string> textPairs)
	{
		foreach (string key in keys) {
			if (textPairs.ContainsKey (key)){
				return textPairs [key];
			}
		}
		return null;
	}

	public static string GetStringByLocPrefs (Dictionary<string, string> words)
	{
		if(words == null)
			return "";

		List<string> keys = new List<string> (){LocalizationManager.CurrentLanguageCode,"en"};
       
		foreach (string key in keys) {
			if (words.ContainsKey (key)){
				return words [key];
			}
		}
		return null;
	}

	public static event Action<UITexture> OnImageLoaded;
	public static event Action<string> OnImageLoadedString;
	//----------------------------------------------
	//				IMAGE LOADING
	//----------------------------------------------
//	public static void loadImageOnUITexture (UITexture texture, string imageURI, GameObject loader = null, Action<UITexture> LoadedCallback = null, float alpha = 1)
//	{
//		if(string.IsNullOrEmpty(imageURI))
//		   {
//			Debug.Log("Not a valid image url");
//			return;
//		}
//		Instance.StartCoroutine (loadImageOnUITex (texture, imageURI, loader, LoadedCallback, alpha));
//	}

//	public static IEnumerator loadImageOnUITex (UITexture texture, string imageURI, GameObject loader = null, Action<UITexture> LoadedCallback = null, float alpha = 1)
//	{
////		Debug.Log(UseCDN + imageURI);
////			print(filename+" DOES NOT exist in cache. Loading it from:" + imageURI);
//		WWW imageLoader;
//		if (!string.IsNullOrEmpty (Main.Settings.UseCDN)) {
//			imageLoader = new WWW (Main.Settings.UseCDN + imageURI);
//		} else
//			imageLoader = new WWW (imageURI); 
//
//		Debug.Log ("Main.Settings.UseCDN image: " + (Main.Settings.UseCDN + imageURI));
//
//		yield return imageLoader;
////		Debug.Log ("Image url:" + imageURI + " | Loaded");
//		if (imageLoader.error != null) {
//			//Debug.Log (imageURI + " | " + imageLoader.error);
//			yield break;
//		} else {
////				#if UNITY_ANDROID || UNITY_IPHONE
////				System.IO.Directory.CreateDirectory(Application.persistentDataPath +"/cache/images");
////				byte[] tex = imageLoader.texture.EncodeToPNG();
////				System.IO.File.WriteAllBytes(Application.persistentDataPath + "/cache/images/"+ filename, tex);
////				#endif
//				
//			texture.mainTexture = imageLoader.texture;
////			Debug.Log(imageURI+" | "+ imageLoader.error);
//
//			texture.alpha = alpha;
//
//			if (loader != null) {
//				loader.SetActive (false);
//			}
//
//			yield return new WaitForSeconds (.2f);
//
//			if (OnImageLoaded != null)
//				OnImageLoaded (texture);
//			if (LoadedCallback != null)
//				LoadedCallback (texture);
//
//		}
//	}

	public static IEnumerator loadImageOn2DTexture (Texture2D texture, string imageURI, Action<string> LoadedCallback = null)
	{
		WWW imageLoader;

		if (imageURI.StartsWith ("http:", System.StringComparison.InvariantCultureIgnoreCase) || imageURI.StartsWith ("https:", System.StringComparison.InvariantCultureIgnoreCase))
			imageLoader = new WWW (imageURI);
		else
			imageLoader = new WWW (@"file://" + imageURI);
		
//		WWW imageLoader = new WWW( imageURI );
		yield return imageLoader;
		
		if (imageLoader.error != null) {
			Debug.Log (imageURI + " | " + imageLoader.error);
			yield break;
		} else {
			//				#if UNITY_ANDROID || UNITY_IPHONE
			//				System.IO.Directory.CreateDirectory(Application.persistentDataPath +"/cache/images");
			//				byte[] tex = imageLoader.texture.EncodeToPNG();
			//				System.IO.File.WriteAllBytes(Application.persistentDataPath + "/cache/images/"+ filename, tex);
			//				#endif
			
			imageLoader.LoadImageIntoTexture (texture);
//		//	texture.alpha = alpha;
//			if(loader != null) {
//				loader.SetActive(false);
//			}
			
			yield return new WaitForSeconds (.2f);
			
			if (OnImageLoaded != null)
				OnImageLoadedString (imageURI);
			if (LoadedCallback != null)
				LoadedCallback (imageURI);
			
		}
	}

	public static void loadImageOnUITextureAndResize (UITexture texture, string imageURI, int width, int height, Action<UITexture> LoadedCallback = null, float alpha = 1)
	{
		Instance.StartCoroutine (loadImageOnUITextureAndRes (texture, imageURI, width, height, LoadedCallback, alpha));
	}

	public static IEnumerator loadImageOnUITextureAndRes (UITexture texture, string imageURI, int width, int height, Action<UITexture> LoadedCallback = null, float alpha = 1)
	{

		WWW imageLoader;

		if (!string.IsNullOrEmpty (Main.Settings.CropCDN)) {
			imageLoader = new WWW (Main.Settings.CropCDN + width + "x" + height + "/" + imageURI);
		} else
			imageLoader = new WWW (imageURI);

//		Debug.Log ("Main.Settings.CropCDN image: " + (Main.Settings.CropCDN + width + "x" + height + "/" + imageURI));
		yield return imageLoader;
		
		if (imageLoader.error != null) {
			Debug.Log (imageLoader.error);
			yield break;
		} else {

			texture.alpha = alpha;

			if(!string.IsNullOrEmpty(imageURI))
				texture.mainTexture = imageLoader.texture;
			else
				texture.mainTexture = null;

			yield return new WaitForSeconds (.2f);
			
			if (OnImageLoaded != null)
				OnImageLoaded (texture);
			if (LoadedCallback != null)
				LoadedCallback (texture);
			
		}
	}

	public static void loadImageOnUITextureAndCrop (UITexture texture, string imageUri, int width, int height, Action<UITexture> LoadedCallback = null)
	{
		Instance.StartCoroutine (loadImageOnUITexAndCrop (texture, imageUri, width, height, LoadedCallback));
	}

	public static IEnumerator loadImageOnUITexAndCrop (UITexture texture, string imageUri, int width, int height, Action<UITexture> LoadedCallback = null)
	{
		WWW imageLoader;

//		Debug.Log(imageUri);

		if (!string.IsNullOrEmpty (CropCDN))
			imageLoader = new WWW (CropCDN + width + "x" + height + "/" + imageUri);
		else
			imageLoader = new WWW (imageUri);

		Debug.Log ("CropCDN image: " + (CropCDN + width + "x" + height + "/" + imageUri));
//		Debug.Log (CropCDN +width+"x"+height+"/"+ imageUri);
		yield return imageLoader;
		
		if (imageLoader.error != null) {
			//Debug.Log(imageUri+" | "+imageLoader.error);
			yield break;
		} else {

			texture.mainTexture = CropTexture (imageLoader.texture, width, height);
	
			yield return new WaitForSeconds (.2f);
			
			if (OnImageLoaded != null)
				OnImageLoaded (texture);
			if (LoadedCallback != null)
				LoadedCallback (texture);
			
		}
	}

	public static void loadFromAlbum (Action<string> success, Action cancel, string filename = "")
	{
		#if UNITY_EDITOR
//		success("/Users/MasterBug/Desktop/profilepic.png");
		#endif
		#if UNITY_ANDROID
//		EtceteraAndroidManager.albumChooserSucceededEvent += success;
//		EtceteraAndroidManager.albumChooserCancelledEvent += cancel;
//		EtceteraAndroid.promptForPictureFromAlbum(filename);
		#endif
		#if UNITY_IPHONE
//		if(EtceteraManager!=null){
//		EtceteraManager.imagePickerChoseImageEvent += success;
//		EtceteraManager.imagePickerCancelledEvent += cancel;
//		EtceteraBinding.promptForPhoto( 0.30f, PhotoPromptType.CameraAndAlbum );
//		}
		#endif
	}

	public static string DecimalPlaceNoRounding (double d, int decimalPlaces = 2)
	{
		d = d * Math.Pow (10, decimalPlaces);
		d = Math.Truncate (d);
		d = d / Math.Pow (10, decimalPlaces);
		return string.Format ("{0:N" + Math.Abs (decimalPlaces) + "}", d);
	}


	/* 
	 * 
	 * Image Manipulation
	 * 
	*/

	public static float Ratio (Texture2D originalTexture)
	{
		return (float)originalTexture.width / (float)originalTexture.height;
	}

	public static float Ratio (float width, float height)
	{
		return (float)width / (float)height;
	}

	public static Texture2D ResizeTexture (Texture2D originalTexture, float width = 0, float height = 0)
	{
		float originalRatio = Ratio (originalTexture);

		if (originalTexture.width > width && width > 0) { // we will scale it down based on width
			originalTexture.Scale ((int)width, Mathf.RoundToInt (width / originalRatio));
		}

		if (originalTexture.height > height && height > 0) {	
			originalTexture.Scale ((int)(height * originalRatio), (int)height);
		}

		return originalTexture;

	}

	public static Texture2D CropTexture (Texture2D originalTexture, float width, float height)
	{

		// NoNeed check
		if (originalTexture.width == width && originalTexture.height == height) {
//			Debug.Log("No need for cropping");
			return originalTexture;
		}

		float originalRatio = Ratio (originalTexture);
		float wantedRatio = Ratio (width, height);

		if (originalRatio < wantedRatio) { // We have to crop height
			float wantedHeight = Mathf.RoundToInt (originalTexture.width * (height / width));
			originalTexture.Crop (new Rect (0f, ((originalTexture.height - wantedHeight) / 2), originalTexture.width, wantedHeight));
		} else {
			float wantedWidth = originalTexture.height * (width / height);
			originalTexture.Crop (new Rect (((originalTexture.width - wantedWidth) / 2), 0f, wantedWidth, originalTexture.height));
		}

		if (originalTexture.width > width || originalTexture.height > height) { // we need to scale down the image also

//			Texture2D modifiedTexture = ResizeTexture(originalTexture, width, height);
			return originalTexture;
		} else
			return originalTexture;


	}

















}
