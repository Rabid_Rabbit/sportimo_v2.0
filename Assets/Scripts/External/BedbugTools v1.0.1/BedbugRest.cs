﻿/* ***********************************************
 * ** 											**
 * **		UNICORN API CONNECTION				**
 * **											**
 * **	Bedbug Studio							**
 * **	First Project Use: Cityfab				**
 * **											**
 * **********************************************/
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using BestHTTP;
using LitJson;

using System.Configuration;

namespace BedbugRest
{
	public class API : MonoBehaviour
	{
		public static bool ShouldLog = true;

		public static Action<string> OnResponseReturnedList;
		public static Action<string> OnResponseReturnedOne;
		public static Action<HTTPRequest,HTTPResponse> OnHttpCallback;

		private static string BaseURL = "";
		private static Boolean	_useBaseURL = false;
		public static Boolean DoNotUseBaseUrl {
			set {
				if (value == true) {
					BaseURL = "";
					_useBaseURL = false;
				} else {
					BaseURL = "http://sportimo.mod.bz/v1/";
					_useBaseURL = true;
				}
			}
		}


		static string leaderpay = "http://sportimo.mod.bz/leaderpay/v1/";

// 	public static void SetBaseAPIUrl(string URL){
// //		Debug.Log("Setting the API Base URL to: "+URL);
// 		BaseURL = URL;
// 	}
	

	
		public static void setLogStatus(bool status){
			ShouldLog = status;
		}
		public static void Log (string apiResponse)
		{
			if (ShouldLog)
				Debug.Log (apiResponse);
		}

	
		/* **************************
	 * *						*
	 * *	GET COMMANDS		*
	 * *						*
	 * **************************/

//	public static void callbackTest(Action<string> Callback)
//	{
//		if(Callback !=null)
//			Callback("hello");
//	}
//	
//		public static void GetAll(string Endpoint, Action<HTTPRequest,HTTPResponse> Callback)
//		{
//			Log (BaseURL + Endpoint);
//
//			HTTPRequest request = new HTTPRequest(new Uri(BaseURL + Endpoint), Callback); 
//			request.Send();
//		}
//		
//		public static void GetOneByID(string Endpoint, string id, Action<HTTPRequest,HTTPResponse> Callback )
//		{
//			Log (BaseURL + Endpoint + "/" + id);
//			HTTPRequest request = new HTTPRequest(new Uri(BaseURL + Endpoint +"/"+ id), Callback); 
//			request.Send();
//		}
//		
//		public static void GetAllByID(string Endpoint, string id, Action<HTTPRequest,HTTPResponse> Callback)
//		{
//			Log(BaseURL + Endpoint +"/"+ id);
//			HTTPRequest request = new HTTPRequest(new Uri(BaseURL + Endpoint +"/"+ id), Callback); 
//			request.Send();
//		}

		// BEST HTTP 1.9.0
		public static void GetAll (string Endpoint, OnRequestFinishedDelegate Callback)
		{
			Log(BaseURL + Endpoint);
			HTTPRequest request = new HTTPRequest (new Uri (BaseURL + Endpoint), Callback); 
			//request.AddHeader("X-Access-Token","tempstring");
			request.Send ();
		}

		public static void GetOne (string Endpoint, OnRequestFinishedDelegate Callback)
		{
			Debug.Log (BaseURL + Endpoint);
			HTTPRequest request = new HTTPRequest (new Uri (BaseURL + Endpoint), Callback); 
			request.Send ();
		}

		public static void GetOneByID (string Endpoint, string id, OnRequestFinishedDelegate Callback)
		{
			Log (BaseURL + Endpoint + "/" + id);
			HTTPRequest request = new HTTPRequest (new Uri (BaseURL + Endpoint + "/" + id), Callback); 
			request.Send ();
		}

		public static void GetAllByID (string Endpoint, string id, OnRequestFinishedDelegate Callback)
		{
			Log (BaseURL + Endpoint + "/" + id);
			HTTPRequest request = new HTTPRequest (new Uri (BaseURL + Endpoint + "/" + id), Callback); 
			request.Send ();
		}

		/* **************************
	 * *						*
	 * *	POST COMMANDS		*
	 * *						*
	 * **************************/
		public static void PostOne (string Endpoint, object data)
		{
			Debug.Log ("[API CALL]: " + BaseURL + Endpoint + "/\n" + JsonMapper.ToJson (data));
			HTTPRequest request = new HTTPRequest (new Uri (BaseURL + Endpoint), HTTPMethods.Post, HttpCallback);
//			request.UseAlternateSSL = true;

			request.SetHeader ("Content-Type", "application/json; charset=UTF-8");
			request.RawData = System.Text.Encoding.ASCII.GetBytes (JsonMapper.ToJson (data));
			request.Send ();
		}

//	public static void PostOne(string Endpoint, object data, Action<HTTPRequest,HTTPResponse> Callback)
//	{
//		Debug.Log("[API CALL]: "+ BaseURL + Endpoint +"/\n"+JsonMapper.ToJson(data));
//		HTTPRequest request = new HTTPRequest(new Uri(BaseURL + Endpoint ), HTTPMethods.Post, Callback);
//		request.SetHeader("Content-Type","application/json; charset=UTF-8");
//		request.RawData = System.Text.Encoding.ASCII.GetBytes(JsonMapper.ToJson(data));
//		request.Send();
//	}
		public static void PostOne (string Endpoint, object data, OnRequestFinishedDelegate Callback)
		{
			Log ("[API CALL]: " + BaseURL + Endpoint + "/\n" + JsonMapper.ToJson (data));
			HTTPRequest request = new HTTPRequest (new Uri (BaseURL + Endpoint), HTTPMethods.Post, Callback);
//			request.UseAlternateSSL = true;
//			Debug.Log("No alternate also");
			request.SetHeader ("Content-Type", "application/json; charset=UTF-8");
			request.RawData = System.Text.Encoding.ASCII.GetBytes (JsonMapper.ToJson (data));
			request.Send ();
		}
	
		public static void PostOneCard (string Endpoint, object data, OnRequestFinishedDelegate Callback)
		{
			Log ("[API CALL]: " + BaseURL + Endpoint + "/\n" + JsonMapper.ToJson (data));
			HTTPRequest request = new HTTPRequest (new Uri (BaseURL + Endpoint), HTTPMethods.Post, Callback);
			request.SetHeader ("Content-Type", "application/json");
			request.RawData = System.Text.Encoding.ASCII.GetBytes (JsonMapper.ToJson (data));
			request.Send ();
		}
	
		public static void PostOne (string Endpoint, OnRequestFinishedDelegate Callback)
		{
			Log ("[API CALL]: " + BaseURL + Endpoint + "/\n");
			HTTPRequest request = new HTTPRequest (new Uri (BaseURL + Endpoint), HTTPMethods.Post, Callback);
			request.SetHeader ("Content-Type", "application/json; charset=UTF-8");
			request.Send ();
		}
//	public static void PostOne(string Endpoint, Cityfab_Intro.PushObject data, OnRequestFinishedDelegate Callback)
//	{
//		Log("[API CALL]: "+ BaseURL + Endpoint +"/\n"+JsonMapper.ToJson(data));
//		HTTPRequest request = new HTTPRequest(new Uri(BaseURL + Endpoint ), HTTPMethods.Post, Callback);
//		request.SetHeader("Content-Type","application/json; charset=UTF-8");
//		request.RawData = System.Text.Encoding.ASCII.GetBytes(JsonMapper.ToJson(data));
//		request.Send();
//	}
//	public static void PostOne(string Endpoint, userLogin.PushObject data, OnRequestFinishedDelegate Callback)
//	{
//		Log("[API CALL]: "+ BaseURL + Endpoint +"/\n"+JsonMapper.ToJson(data));
//		HTTPRequest request = new HTTPRequest(new Uri(BaseURL + Endpoint ), HTTPMethods.Post, Callback);
//		request.SetHeader("Content-Type","application/json; charset=UTF-8");
//		request.RawData = System.Text.Encoding.ASCII.GetBytes(JsonMapper.ToJson(data));
//		request.Send();
//	}
//	public static void PostOne(string Endpoint, RateObject data, OnRequestFinishedDelegate Callback)
//	{
//		Log("[API CALL]: "+ BaseURL + Endpoint +"/\n"+JsonMapper.ToJson(data));
//		HTTPRequest request = new HTTPRequest(new Uri(BaseURL + Endpoint ), HTTPMethods.Post, Callback);
//		request.SetHeader("Content-Type","application/json; charset=UTF-8");
//		request.RawData = System.Text.Encoding.ASCII.GetBytes(JsonMapper.ToJson(data));
//		request.Send();
//	}
//	public static void PostOne(string Endpoint, appointment data, OnRequestFinishedDelegate Callback)
//	{
//		Log("[API CALL]: "+ BaseURL + Endpoint +"/\n"+JsonMapper.ToJson(data));
//		HTTPRequest request = new HTTPRequest(new Uri(BaseURL + Endpoint ), HTTPMethods.Post, Callback);
//		request.SetHeader("Content-Type","application/json; charset=UTF-8");
//		request.RawData = System.Text.Encoding.ASCII.GetBytes(JsonMapper.ToJson(data));
//		request.Send();
//	}

		/* **************************
	 * *						*
	 * *	PUT COMMANDS		*
	 * *						*
	 * **************************/
		public static void PutOne (string Endpoint, object data, OnRequestFinishedDelegate Callback)
		{
			Log (BaseURL + Endpoint);
			HTTPRequest request = new HTTPRequest (new Uri (BaseURL + Endpoint), HTTPMethods.Put, Callback);
			request.SetHeader ("Content-Type", "application/json; charset=UTF-8");
			request.RawData = System.Text.Encoding.ASCII.GetBytes (JsonMapper.ToJson (data));
			request.Send ();
		}


			public static void PutOne (string Endpoint, object data, OnRequestFinishedDelegate Callback,string token)
		{
			Log (BaseURL + Endpoint);
			HTTPRequest request = new HTTPRequest (new Uri (BaseURL + Endpoint), HTTPMethods.Put, Callback);
			request.SetHeader ("Content-Type", "application/json; charset=UTF-8");
			request.SetHeader("X-Access-Token",token);
			request.RawData = System.Text.Encoding.ASCII.GetBytes (JsonMapper.ToJson (data));
			Debug.Log((BaseURL + Endpoint));
			request.Send ();
		}



//BestHTTP 1.9.0
		public static void UpdateByID (string Endpoint, string id, object data, OnRequestFinishedDelegate Callback)
		{
			HTTPRequest request = new HTTPRequest (new Uri (BaseURL + Endpoint + "/" + id), HTTPMethods.Put, Callback);
			request.SetHeader ("Content-Type", "application/json; charset=UTF-8");
			request.RawData = System.Text.Encoding.ASCII.GetBytes (JsonMapper.ToJson (data));
			request.Send ();
			
		}


		
	/* **************************
	 * *						*
	 * *	DELETE COMMANDS		*
	 * *						*
	 * **************************/
		public static void Delete (string Endpoint, OnRequestFinishedDelegate Callback)
		{
			Log (BaseURL + Endpoint);
			HTTPRequest request = new HTTPRequest (new Uri (BaseURL + Endpoint), HTTPMethods.Delete, Callback);
			request.SetHeader ("Content-Type", "application/json; charset=UTF-8");
			request.Send ();
		}


//	public static void UpdateByID(string Endpoint, string id, appointment data,OnRequestFinishedDelegate Callback)
//	{
//		Log("[API CALL]: "+ BaseURL + Endpoint +" PUT");
//		HTTPRequest request = new HTTPRequest(new Uri(BaseURL + Endpoint +"/"+ id), HTTPMethods.Put, Callback);
//		request.SetHeader("Content-Type","application/json; charset=UTF-8");
//		request.RawData = System.Text.Encoding.ASCII.GetBytes(JsonMapper.ToJson(data));
//		request.Send();
//	}
//	public static void UpdateByID(string Endpoint, string id, specialist data, OnRequestFinishedDelegate Callback)
//	{
//		Log("[API CALL]: "+ BaseURL + Endpoint +" PUT");
//		HTTPRequest request = new HTTPRequest(new Uri(BaseURL + Endpoint +"/"+ id), HTTPMethods.Put, Callback);
//		request.SetHeader("Content-Type","application/json; charset=UTF-8");
//		request.RawData = System.Text.Encoding.ASCII.GetBytes(JsonMapper.ToJson(data));
//		request.Send();
//	}
//	public static void UpdateByID(string Endpoint, string id, workdate data, OnRequestFinishedDelegate Callback)
//	{
//		Log("[API CALL]: "+ BaseURL + Endpoint +" PUT");
//		HTTPRequest request = new HTTPRequest(new Uri(BaseURL + Endpoint +"/"+ id), HTTPMethods.Put, Callback);
//		request.SetHeader("Content-Type","application/json; charset=UTF-8");
//		request.RawData = System.Text.Encoding.ASCII.GetBytes(JsonMapper.ToJson(data));
//		request.Send();
//	}


		/* **********************
	 * *	Callbacks		*
	 * **********************/


		static void HttpCallback (HTTPRequest request, HTTPResponse response)
		{

			if (OnHttpCallback != null)
				OnHttpCallback (request, response);
		
		}

		static void onRequestFinishedList (HTTPRequest request, HTTPResponse response)
		{
//		Debug.Log("received Response");
			if (OnResponseReturnedList != null)
				OnResponseReturnedList (response.DataAsText);

		}

		static void onRequestFinishedOne (HTTPRequest request, HTTPResponse response)
		{
			if (OnResponseReturnedOne != null)
				OnResponseReturnedOne (response.DataAsText);
		}
	}

}
