﻿using UnityEngine;
using System.Collections.Generic;
using System;
using LitJson;
public class StatsMod {

	public string name;
	
	public string id;
	
	public int Offside;
	public int Penalty;
	public int Yellow;
	public int Shot_on_Goal;
	public int Goal;
	public int Corner;
	public int Shot;
	public int Free_Kick;
	public int Cross;
	public int Foul;
	public int Throw_In;
	
	public int Red;
	
	public static List<StatsMod> getStats(string json){
		if (string.IsNullOrEmpty (json))
			return null;

		List<StatsMod> stats=new List<StatsMod>();
		JsonData data = JsonMapper.ToObject(json);
		for(int i=0;i<data.Count;i++){
			StatsMod mod = new StatsMod();
			mod.name=data[i]["name"].ToString();
			if(data[i].Keys.Contains("id")){
				mod.id=data[i]["id"].ToString();
			}
			else{
				mod.id="";
			}
			if(data[i].Keys.Contains("Offside")){
				mod.Offside=(int)data[i]["Offside"];
			}
			else{
				mod.Offside=0;
			}
			if(data[i].Keys.Contains("Penalty")){
				mod.Penalty=(int)data[i]["Penalty"];
			}
			else{
				mod.Penalty=0;
			}
			if(data[i].Keys.Contains("Yellow")){
				mod.Yellow=(int)data[i]["Yellow"];
			}
			else{
				mod.Yellow=0;
			}
			if(data[i].Keys.Contains("Shot_on_Goal")){
				mod.Shot_on_Goal=(int)data[i]["Shot_on_Goal"];
			}
			else{
				mod.Shot_on_Goal=0;
			}
			if(data[i].Keys.Contains("Goal")){
				mod.Goal=(int)data[i]["Goal"];
			}
			else{
				mod.Goal=0;
			}
			if(data[i].Keys.Contains("Corner")){
				mod.Corner=(int)data[i]["Corner"];
			}
			else{
				mod.Corner=0;
			}
			if(data[i].Keys.Contains("Shot")){
				mod.Shot=(int)data[i]["Shot"];
			}
			else{
				mod.Shot=0;
			}
			if(data[i].Keys.Contains("Free_Kick")){
				mod.Free_Kick=(int)data[i]["Free_Kick"];
			}
			else{
				mod.Free_Kick=0;
			}
			if(data[i].Keys.Contains("Cross")){
				mod.Cross=(int)data[i]["Cross"];
			}
			else{
				mod.Cross=0;
			}
			if(data[i].Keys.Contains("Foul")){
				mod.Foul=(int)data[i]["Foul"];
			}
			else{
				mod.Foul=0;
			}
			if(data[i].Keys.Contains("Throw_In")){
				mod.Throw_In=(int)data[i]["Throw_In"];
			}
			else{
				mod.Throw_In=0;
			}
			if(data[i].Keys.Contains("Red")){
				mod.Red=(int)data[i]["Red"];
			}
			else{
				mod.Red=0;
			}
			stats.Add(mod);
		}
		return stats;
	}
}
