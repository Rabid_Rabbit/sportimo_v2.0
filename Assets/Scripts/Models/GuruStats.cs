﻿using UnityEngine;
using System.Collections;

public class GuruStats{

	public GuruStat Shot_On_Goal;
	public GuruStat Goal;
	public GuruStat Yellow;
	public GuruStat Corner;

	
	public class GuruStat{
		public int[] total;
		public int[] awayTeam;
		public int[] homeTeam;
	}
}
