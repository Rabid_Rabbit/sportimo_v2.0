﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class LeaderboardObject {

	public string _id;
	public Dictionary<string,string> title = new Dictionary<string, string>();
	public string roomtype;
	public DateTime starts;
	public DateTime ends;
//	public string sponsor;
	public bool isdefault;
	public int bestscores;
	public DateTime created;
	public Dictionary<string,string> info;
	public string leadInfo = "This is a test info for this leaderboard!";
	public List<string> friends = new List<string> ();
	//public Dictionary<int,string> country = new Dictionary<int, string>();
	public List<string> country = new List<string> ();

	public List<Player> players = new List<Player> ();

	public string status;
}
