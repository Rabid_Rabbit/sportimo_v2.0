﻿using UnityEngine;
using System.Collections;

public class Country  {

	public string text;
	public string operatorcode;
	public string shorttext;

	public string prefix;

public Country(string text,string operatorcode,string shorttext,string prefix){
	this.text=text;
	this.operatorcode=operatorcode;
	this.shorttext=shorttext;
	this.prefix=prefix;
}

}
