﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Wildcard : MonoBehaviour {

	public string userid;
	public string gameid;
	public string text;
	public int minute;
	public int segment;
	public int activates_in;
	public int duration;
	public int timer;
	public Dictionary <string, string> appear_conditions;
	public Dictionary <string, string> wind_conditions;
	public int points;
	public int points_step;
	public int minpoints;
	public int maxpoints;
	public DateTime created;
	public DateTime activated;
	public DateTime ended;
	public DateTime won;
	public int status;
	public string linked_event;
	
	
}
