﻿using UnityEngine;
using System.Collections.Generic;

public class ProfileStats  {

	public User user;
	public int pointsPerGame;
	public int[] lastmatches;
	public AllStats all;

}

public class AllStats{
	public int cardsPlayed;
	public int cardsWon;
	public double successPercent;

	public int overallCardsPlayed;
	public int overallCardsWon;
	public double overallSuccessPercent;

	public int instantCardsPlayed;
	public int instantCardsWon;
	public double instantSuccessPercent;
}


