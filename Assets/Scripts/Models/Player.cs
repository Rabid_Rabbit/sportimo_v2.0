﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player {

	public string _id;
	public Dictionary <string,string> name;
	public string team_id;
	public string pic;
	
	public Dictionary<string,PlayerStats> stats;
	public string uniformNumber;
	public string team;
	
	public PersonalData personalData;
	
	public string position;
	
	
	public Player(){}
	
	public class PersonalData{
		public Height height;
	
	public Weight weight;
	
	public Birth birth;
	
	public Nationality nationality;
	}
	public class Height{
		public int centimeters;
		
		public int inches;
	}
	
	public class Weight{
		public int kilograms;
		public int pounds;
	}
	
	public class Birth{
		public Dictionary<string,object> birthDate;
		
		public string city;
		
		public Dictionary<string,object> country;
	}
	
	public class Nationality{
		public int countryId;
		
		public string name;
		
		public string abbreviation;
	}

	public class season{
		public int kickedGoals;
		public int headedGoals;
		public int ownGoalds;
		public int gameWinningGoals;
		public int goals;

	}
}
