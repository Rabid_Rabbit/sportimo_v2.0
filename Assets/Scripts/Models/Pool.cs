﻿using System.Collections.Generic;
using System;
using LitJson;
public class Pool {
    public string id;
    public Dictionary<string,string> title;
    public string roomtype;
    
    
    public string starts;
    public string ends;
    //public Dictionary<string,string> sponsor;
    
    public bool isdefault;
    public string status;
    public string[] country;
    public List<Prize> prizes=new List<Prize>();

    public class Prize{
    public Dictionary<string,string> name;
    public Dictionary<string,string> text;
    public string picture;
    public Dictionary<string,int> positions;
    
    public static List<Prize> jsonMapper(string json){
        List<Prize> prizes=new List<Prize>();
        JsonData data=JsonMapper.ToObject(json);
        for(int i=0;i<data.Count;i++){
            Prize prize=new Prize();
            prize.name=JsonMapper.ToObject<Dictionary<string,string>>(data[i]["name"].ToJson());
            prize.text=JsonMapper.ToObject<Dictionary<string,string>>(data[i]["text"].ToJson());
            prize.picture=(string)data[i]["picture"];
            prize.positions=JsonMapper.ToObject<Dictionary<string,int>>(data[i]["positions"].ToJson());
            prizes.Add(prize);
        }
        return prizes;
    }
    
    public static string returnEnding(int i){
        if(i==1){
            return "st";
        }
        else if(i==2){
            return "nd";
        }
        else if(i==3){
            return "rd";
        }
        return "th";
    }
    
}
public class LBEntry{
    public string username;
    public int score;
    public string pic_url;
    
    public string country_code;
}
}
