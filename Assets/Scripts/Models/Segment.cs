﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Segment  {

	public string _id;
	public List<GameEvent> events;
	public DateTime start;
	public DateTime end;
	public Dictionary <string,string> text;
	public int sport_start_time;
	public bool timed;
}
