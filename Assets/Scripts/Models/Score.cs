﻿using UnityEngine;
using System.Collections.Generic;
using LitJson;
//The class for user score for leaderboards
public class Score  {

	public string _id;
	public string pic;
	public string name;
	public int score;
	public string country;
    private float _level;
	public double level {
        get {
            return _level;
        }
        set {            
            _level = (float)value;
        }
    }
	
	public int rank;
	
	
		
}
