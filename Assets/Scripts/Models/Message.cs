﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Message {
	
	public string _id;
	
	public Dictionary<string,string> title;
	public Dictionary<string,string> msg;

	public string link;
	public string img;
	public DateTime created;
	
	//public Dictionary<string,string> data;
}
