﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

//The league and it's standings class
public class League {

	public Dictionary<string,string> name;
	public string identity;
	public string _id;
	public int season;
	public DateTime created;
	public DateTime lastupdate;
	public string competitionid;
	public List <Standing> teams;
	
	
//	public static List<League> jsonMappper(string json){
//		List<League> Leagues=new List<League>();
//        JsonData data=JsonMapper.ToObject(json);
//        for(int i=0;i<data.Count;i++){
//        	League league=new League();
//			league.name=JsonMapper.ToObject<Dictionary<string,string>>(data[i]["name"].ToJson());
//			league.identity=data[i]["identity"].ToString();
//			league.season=(string)data[i]["season"].ToString();
//			DateTime.TryParse(data[i]["created"].ToString(),out league.created);
//			league.standings=Standing.jsonMappper(data[i]["teams"].ToJson());
//			Leagues.Add(league);
//		}
//		return Leagues;
//	
//	}
}

public class Standing {

	public Dictionary <string,string> teamName;
	public string teamId;
	public int gamesPlayed;
	public int wins;
	public int ties;
	public int losses;
	public int goalsFor;
	public int goalsAgainst;
	public int points;
	public string pointsPerGame;
	public int rank;
	public int penaltyPoints;
	
	
//	public static List<Standing> jsonMappper(string json){
//
//		List<Standing> Standings=new List<Standing>();		
//		JsonData data = JsonMapper.ToObject(json);
//		for(int i=0;i<data.Count;i++){
//			Standing standing=new Standing();
//			standing.teamName=JsonMapper.ToObject<Dictionary<string,string>>(data[i]["teamName"].ToJson());
//			standing.teamId=(string)data[i]["teamId"].ToString();
//			standing.gamesPlayed=(int)data[i]["gamesPlayed"];
//			standing.wins=(int)data[i]["wins"];
//			standing.ties=(int)data[i]["ties"];
//			standing.losses=(int)data[i]["losses"];
//			standing.goalsFor=(int)data[i]["goalsFor"];
//			standing.goalsAgainst=(int)data[i]["goalsAgainst"];
//			standing.points=(int)data[i]["points"];
//			standing.penaltyPoints=(int)data[i]["penaltyPoints"];
//			standing.rank=(int)data[i]["rank"];
//			float.TryParse(data[i]["pointsPerGame"].ToString(),out standing.ppg);
//			               
//			Standings.Add(standing);
//				
//		}
//		return Standings;
//		
//	}
}

