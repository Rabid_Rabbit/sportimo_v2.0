﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameEvent  {

	public string _id;

	public List<Player>  players;

	public string team;
	
	public int time;
	
	public string type;
	
	public bool timeline_event;
	public int state;
	public DateTime created;
}
