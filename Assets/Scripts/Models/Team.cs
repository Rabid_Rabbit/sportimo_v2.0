﻿using System.Collections.Generic;
using LitJson;

//The class for teams in scheduled matches
public class Team {

	public string id{
		get {return _id;}
	}
	
	public Dictionary <string, string> name;
	public string _id;
    public string logo;
	
	public string league;
	
	public List<Player> players;
	
	public Standing standing;
	
	public ScheduledMatch lastmatch;
	
	public ScheduledMatch nextmatch;
	
	public Player topscorer;
	
	public Player topassister;

	public List <string> recentform;

	public JsonData stats;
	//public Dictionary<string, int> stats = new Dictionary<string, int>();
	//public List<string> recentform;
    
	public Team(){}
}

//{
//	"_id": {
//		"$oid": "56e81b7c30345c282c01b2d1"
//	},
//	"parserids": {
//		"Stats": 7127
//	},
//	"league": "epl",
//	"logo": null,
//	"name": {
//		"short": "Leicester",
//		"ru": "Leicester City",
//		"ar": "ليستر سيتي",
//		"en": "Leicester City"
//	},
//	"name_en": "Leicester City",
//	"created": {
//		"$date": "2016-03-15T14:26:04.471Z"
//	},
//	"players": [],
//	"__v": 0,
//	"competitionid": "56f4800fe4b02f2226646297"
//}