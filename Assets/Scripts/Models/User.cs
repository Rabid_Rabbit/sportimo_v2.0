﻿using UnityEngine;
using System.Collections;
using System;
using BedbugRest;
using BestHTTP;
using LitJson;
using System.Collections.Generic;
using Facebook.Unity;

public class CBK
{
	public bool success;
	public bool failure;
	public string message;
	public User userData;
}

public class Subscription
{
	public string status;
	public string userid;
	public string provider;
	public string receiptid;
	public DateTime start;
	public DateTime end;
	public string state;
}

public class RankingStats
{
	public int bestRank;
	public int bestScore;
	public ScheduledMatch bestRankMatch;
	public ScheduledMatch bestScoreMatch;
}

//The current user class
public class User
{
	public string _id;
	public string name;
	public string username;
	public string email;
	public string password;
	public string token;
	public string pushToken = "userIsNotLoadedYet";
	public string tempPushToken = "tempPushToken";
	//public string countrycode;
	public string country = "SA";
    public string subscriptionContractId;
    public string customerType = "free"; // [free / paid]
    public bool isOnline = true;
    public Subscription subscription = new Subscription();
	public LeaderCheckObj leaderCheckObj = new LeaderCheckObj();

	public string birth {
		get;
		set;
	}

	public string gender {
		get;
		set;
	}

	public string msisdn {
		get;
		set;
	}

	public string pincode {
		get;
		set;
	}

	public string picture;
	public string social_id;
	public double level;
	public List<Achievement> achievements = new List<Achievement>();
	public UserStats stats;
	public List<string> blockedusers = new List<string>();
	public List<string> favoriteteams = new List<string>();
	public int unread;
	public RankingStats rankingStats;
	public JsonData pushSettings;
	// In order to have the authenticate method in the class and to have the ability to use callbacks, we use
	// this operation to clone our class without the unsafe properties and use it as a request object.
	private User cloneSafe ()
	{
		User safeUser = new User ();
		safeUser._id = _id;
		safeUser.name = name;
		safeUser.username = username;
		safeUser.email = email;
		safeUser.password = password;
		safeUser.token = token;
		safeUser.pushToken = pushToken;
		safeUser.country = country;
		safeUser.birth = birth;
		safeUser.gender = gender;
		safeUser.msisdn = msisdn;
		safeUser.pincode = pincode;
		safeUser.picture = picture;
		safeUser.subscription=subscription;
		return safeUser;
	}

	public void saveProfileData ()
	{
		Hashtable saveData = new Hashtable ();

		if (Main.AppUser.picture != null)
			saveData.Add ("picture", Main.AppUser.picture);

		if (Main.AppUser.country != null)
			saveData.Add ("country", Main.AppUser.country);

		if (Main.AppUser.birth != null)
			saveData.Add ("birth", Main.AppUser.birth);

		if (Main.AppUser.gender != null)
			saveData.Add ("gender", Main.AppUser.gender);

		if (Main.AppUser.msisdn != null)
			saveData.Add ("msisdn", Main.AppUser.msisdn);

		if (Main.AppUser.pincode != null)
			saveData.Add ("pincode", Main.AppUser.pincode);

		API.PutOne (Main.Settings.apis ["users"] + "/" + this._id, saveData, (HTTPRequest req, HTTPResponse res) => {
			Debug.Log (res.DataAsText);
		});
	}

	public Action<CBK> RequestCallback;
	public Action<CBK> VerifyCallback;

	public void Authenticate (Action<CBK> cbk)
	{
		RequestCallback = cbk;
		API.PostOne (Main.Settings.apis ["users"] + "/authenticate", this.cloneSafe (), AuthenticateCallback);
	}
	
	public void SocialAuthenticate (Action<CBK> cbk)
	{
		RequestCallback = cbk;
		Hashtable data = new Hashtable ();
		data.Add ("social_id", Main.AppUser.social_id);
		API.PostOne (Main.Settings.apis ["users"] + "/authenticate/social", data, AuthenticateCallback);

	}

	public void AuthenticateCallback (HTTPRequest req, HTTPResponse response)
	{
		BedbugRest.API.Log (response.DataAsText);

		JsonData resp = JsonMapper.ToObject<JsonData> (response.DataAsText);

		if ((bool)resp ["success"] == false) {
			MessagePanel.Instance.animMessage (resp ["message"].ToString ());		
		}

		if (response.StatusCode != 200 || response.DataAsText.Contains ("\"success\":false")) {
			if (RequestCallback != null) {
				CBK cbk = new CBK ();
				cbk.failure = true;
				cbk.message = response.Message;
				RequestCallback (cbk);
				RequestCallback = null;
			}
		} else {

			if (RequestCallback != null) {
				CBK cbk = new CBK ();
				// Assign the response to this user
				cbk.userData = JsonMapper.ToObject<User> (response.DataAsText);
				cbk.success = true;
				RequestCallback (cbk);
				RequestCallback = null;
			}
		}
	}

	public void Register (Action<CBK> cbk = null)
	{
		if (cbk != null)
			RequestCallback = cbk;

		Hashtable table = new Hashtable ();
		table.Add ("username", Main.AppUser.username);
		table.Add ("email", Main.AppUser.email);
		table.Add ("password", Main.AppUser.password);
		table.Add ("country", Main.AppUser.country);
		API.PostOne (Main.Settings.apis ["users"], table, RegCallback);
 
	}
    
	public void AutoAuthenticate (Action<CBK> cbk)
	{

		RequestCallback = cbk;
		Hashtable table = new Hashtable ();
		table.Add ("username", PlayerPrefs.GetString ("username"));
		table.Add ("password", PlayerPrefs.GetString ("password"));

//		Debug.Log (Main.Settings.apis ["users"] + "/authenticate");
		API.PostOne (Main.Settings.apis ["users"] + "/authenticate", table, AuthenticateCallback);
	}
	
	public void SocialAutoAuthenticate (Action<CBK> cbk)
	{
//		Debug.Log ("Authenticate call count");
		RequestCallback = cbk;
		Hashtable table = new Hashtable ();
		table.Add ("social_id", PlayerPrefs.GetString ("social_id"));

		API.PostOne (Main.Settings.apis ["users"] + "/authenticate/social", table, AuthenticateCallback);
	}
    
	public void RegCallback (HTTPRequest req, HTTPResponse response)
	{
		Debug.Log (response.DataAsText);
	
		
		Loader.Visible (false);
		JsonData resp = JsonMapper.ToObject<JsonData> (response.DataAsText);
		
		if (response.StatusCode == 500) {
			if (RequestCallback != null)
			{
				CBK cbk = new CBK();
				// Assign the response to this user
				cbk.userData = null;
				cbk.success = false;
				cbk.failure = true;
				RequestCallback(cbk);
				RequestCallback = null;
			}
			if (resp ["code"] != null) {
				Debug.Log ("We have a duplicate key somewhere");
				string errmsg = resp ["errmsg"].ToString ();
				if (errmsg.IndexOf ("username") > -1) { // Duplicate username
					MessagePanel.Instance.animMessage (String.Format (I2.Loc.LocalizationManager.GetTermTranslation ("duplicate_exists", true), resp ["op"] ["username"].ToString ()));	
				} else if (errmsg.IndexOf ("email") > -1) { // Duplicate username
					MessagePanel.Instance.animMessage (String.Format (I2.Loc.LocalizationManager.GetTermTranslation ("duplicate_exists", true), resp ["op"] ["email"].ToString ()));	
				}
			} else {
				
				MessagePanel.Instance.animMessage (I2.Loc.LocalizationManager.GetTermTranslation ("something_wrong", true));
			}

			
		} else if (response.StatusCode != 200) {
			MessagePanel.Instance.animMessage (response.DataAsText);		
		} else {
			Debug.Log ("success register");
			JsonData data = JsonMapper.ToObject (response.DataAsText);
			Main.AppUser._id = data ["id"].ToString ();

			Main.AppUser.subscription.userid = Main.AppUser._id;

			if (data.Keys.Contains ("name"))
			Main.AppUser.name = data ["name"].ToString ();
			if(data.Keys.Contains("social_id")){
			Main.AppUser.social_id=data["social_id"].ToString();
			PlayerPrefs.SetString("social_id",data["social_id"].ToString());
			}
			Main.AppUser.username = data ["username"].ToString ();
			Main.AppUser.token = data ["token"].ToString ();
			
			PlayerPrefs.SetString ("token", Main.AppUser.token);
			PlayerPrefs.SetString ("username", Main.AppUser.username);
			PlayerPrefs.SetInt ("tutorial_complete", 1);
			JsonData request = JsonMapper.ToObject (System.Text.Encoding.ASCII.GetString (req.RawData));
			PlayerPrefs.SetString ("password", request ["password"].ToString ());
			if (FB.IsLoggedIn) {
				PlayerPrefs.SetString ("social_id", AccessToken.CurrentAccessToken.UserId);
			}
			if (RequestCallback != null) {
				CBK cbk = new CBK ();
				// Assign the response to this user
				cbk.userData = JsonMapper.ToObject<User> (response.DataAsText);
				cbk.success = true;
				RequestCallback (cbk);
				RequestCallback = null;
			}
			
			// Main.AppUser.updatePushToken ();
			// // And register our ID to the socket server
			// //ConnectionManager.Instance.RegisterUser ();
			// //UpdateSubInfo(); Enable for subscription verification on register
			// ConnectionManager.Instance.RegisterUser ();
			
			// EndLoginFlow ();
			
		}
		
	}

	// STATIC INTERNAL CALLBACK! WHY????
	// public static void RegisterCallback (HTTPRequest req, HTTPResponse response)
	// {
	// 	Loader.Visible (false);
	// 	Debug.Log (response.DataAsText);
	// 	JsonData resp = JsonMapper.ToObject<JsonData> (response.DataAsText);
		
	// 	if (response.StatusCode == 500) {
	// 		if (resp ["code"] != null) {
	// 			Debug.Log ("We have a duplicate key somewhere");
	// 			string errmsg = resp ["errmsg"].ToString ();
	// 			if (errmsg.IndexOf ("username") > -1) { // Duplicate username
	// 				MessagePanel.Instance.animMessage (String.Format (I2.Loc.LocalizationManager.GetTermTranslation ("duplicate_exists"), resp ["op"] ["username"].ToString ()));	
	// 			} else if (errmsg.IndexOf ("email") > -1) { // Duplicate username
	// 				MessagePanel.Instance.animMessage (String.Format (I2.Loc.LocalizationManager.GetTermTranslation ("duplicate_exists"), resp ["op"] ["email"].ToString ()));	
	// 			}
	// 		} else {
	// 			MessagePanel.Instance.animMessage (I2.Loc.LocalizationManager.GetTermTranslation ("something_wrong"));
	// 		}


	// 	} else if (response.StatusCode != 200) {
	// 		MessagePanel.Instance.animMessage (response.DataAsText);		
	// 	} else {
	// 		Debug.Log ("success register");
	// 		JsonData data = JsonMapper.ToObject (response.DataAsText);
	// 		Main.AppUser._id = data ["_id"].ToString ();
	// 		if (data.Keys.Contains ("name"))
	// 			Main.AppUser.name = data ["name"].ToString ();
	// 		Main.AppUser.username = data ["username"].ToString ();
	// 		Main.AppUser.token = data ["token"].ToString ();

	// 		PlayerPrefs.SetString ("token", Main.AppUser.token);
	// 		PlayerPrefs.SetString ("username", Main.AppUser.username);
	// 		PlayerPrefs.SetInt ("tutorial_complete", 1);
	// 		JsonData request = JsonMapper.ToObject (System.Text.Encoding.ASCII.GetString (req.RawData));
	// 		PlayerPrefs.SetString ("password", request ["password"].ToString ());
	// 		if (FB.IsLoggedIn) {
	// 			PlayerPrefs.SetString ("social_id", AccessToken.CurrentAccessToken.UserId);
	// 		}


	// 		Main.AppUser.updatePushToken ();
	// 		// And register our ID to the socket server
	// 		ConnectionManager.Instance.RegisterUser ();

	// 		EndLoginFlow ();
	// 	}

	// }
    
	public void UpdateSubInfo(){
		if(Main.AppUser.subscription.provider=="iap"){
			API.PostOne (Main.Settings.apis ["users"] + "/"+Main.AppUser._id+"/subscription", Main.AppUser.subscription, SubInfoCallback);
			
			
		}
		else if(Main.AppUser.subscription.provider=="tpay"){
			API.PostOne (Main.Settings.apis ["users"] + "/"+Main.AppUser._id+"/subscription", Main.AppUser.subscription, SubInfoCallback);
			
		}
			
	}

	public void ValidateTpay(Action<CBK> callback=null){
		if(callback!=null)
		VerifyCallback=callback;
		HTTPRequest request = new HTTPRequest(new Uri("http://staging.tpay.me/api/TPaySubscription.svc/json/GetSubscriptionContracts)"),HTTPMethods.Post,OnGetContracts);
		Dictionary<object,object> table = new Dictionary<object,object>();
		request.AddHeader("Content-Type","application/json");
		//table.Add("signature",signature);
		string datet=DateTime.UtcNow.AddDays(7).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
		table.Add("customerAccountNumber",Main.AppUser.subscription.receiptid);
		table.Add("msisdn",Main.AppUser.msisdn);
		string sign=StoreListener.Instance.CalculateSignature(table);
		Dictionary<object,object> sendtable=new Dictionary<object,object>();
		sendtable.Add("signature",sign);
		foreach(KeyValuePair<object,object> kv in table){
			sendtable.Add(kv.Key,kv.Value);
		}
		request.RawData = System.Text.Encoding.ASCII.GetBytes (JsonMapper.ToJson (sendtable));
		request.Send();
	}

	private void OnGetContracts(HTTPRequest req, HTTPResponse res){ //for tpay contracts
		Debug.Log(res.DataAsText);
		JsonData data=JsonMapper.ToObject(res.DataAsText);

		if((int)data["status"]==2){
			//Main.AppUser.subscription.start=result[0].purchaseDate;
			//Main.AppUser.subscription.end=result[0].purchaseDate.AddDays(7);
			Main.AppUser.subscription.provider="tpay";
			//TODO need to map precise return list
				CBK cbk = new CBK ();
				cbk.success = true;
				cbk.message =res.Message;
				VerifyCallback (cbk);
				VerifyCallback = null;
		}
		else{
			CBK cbk = new CBK ();
				cbk.success = false;
				cbk.message =res.Message;
				VerifyCallback (cbk);
				VerifyCallback = null;
		}
	}
	
	private void SubInfoCallback(HTTPRequest req, HTTPResponse res){
		Debug.Log(res.DataAsText);
		
		}
	
	public static void EndLoginFlow ()
	{
		
		Main.Instance.openView ("menu");
	}

	public void updatePushToken ()
	{
//		Debug.Log("------ Checking Push Tokens -------");
		if (!string.IsNullOrEmpty (tempPushToken) && tempPushToken != pushToken && tempPushToken != "tempPushToken" && pushToken != "userIsNotLoadedYet") {
			pushToken = tempPushToken;
//			Debug.Log ("Updating user pushtoken  :"+pushToken);
			Hashtable tokenUpdate = new Hashtable ();
			tokenUpdate.Add ("pushToken", pushToken);
			API.PutOne (Main.Settings.apis ["users"] + "/" + Main.AppUser._id, tokenUpdate, null);
		}
	}

	public static void updatePic (string picUrl)
	{
		//Debug.Log("------ Checking Push Tokens -------");
		//if (!string.IsNullOrEmpty(tempPushToken) && tempPushToken != pushToken && tempPushToken != "tempPushToken" && pushToken != "userIsNotLoadedYet") {
		///pushToken = tempPushToken;
//			Debug.Log ("Updating user Pic  :"+picUrl);
		Hashtable picUpdate = new Hashtable ();
		picUpdate.Add ("picture", picUrl);
		API.PutOne (Main.Settings.apis ["users"] + "/" + Main.AppUser._id, picUpdate, null);
		//}
	}

	public static void updatePushSetts (JsonData pushSets)
	{
		Debug.Log ("Updating Push Settings!");

		//Debug.Log (pushSets.ToJson ());
		Hashtable pushUpdate = new Hashtable ();
		pushUpdate ["pushSettings"] = JsonMapper.ToObject<Dictionary<string,bool>> (pushSets.ToJson ()) as Dictionary<string,bool>;
//		pushUpdate.Add ("pushsetting", pushSets);

		API.PutOne (Main.Settings.apis ["users"] + "/" + Main.AppUser._id, pushUpdate, upadteNotsCallback);
		//}
	}

	static void upadteNotsCallback (HTTPRequest originalRequest, HTTPResponse response)
	{
		Loader.Visible (false);
		Debug.Log (response.DataAsText);
	}
}
















