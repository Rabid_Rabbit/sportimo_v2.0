using System.Collections.Generic;
using System;

class PrizeEligibilityQuestion
{
	public Dictionary<string,string> 		text;
	public List<PrizeEligibilityAnswer> 	answers;
	public string 							correct;
	public DateTime 						created;
}

