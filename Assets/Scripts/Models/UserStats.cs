﻿using UnityEngine;
using System.Collections;

public class UserStats{
	public int cardsWon;
	public int cardsPlayed;
	public int instantCardsWon;
	public int instantCardsPlayed;
	public int overallCardsWon;
	public int overallCardsPlayed;
	public int matchesPlayed;
	public int matchesVisited;
	public int pointsPerGame;
}