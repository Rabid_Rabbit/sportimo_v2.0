﻿using UnityEngine;
using System.Collections.Generic;

public class Achievement {


	public bool completed;
	public int total;
	public int has;
	public Dictionary<string,string> text;

	public Dictionary<string,string> title;
	public string icon;
	public string uniqueid;

	public string _id;

}
