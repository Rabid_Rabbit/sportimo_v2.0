﻿using UnityEngine;
using System.Collections.Generic;
using BedbugRest;

public class Pools
{
	private List<Pool> TimedPools = new List<Pool> ();
	public static Pool SeasonPool = new Pool ();
	public static Pool WeeklyPool = new Pool ();
	public static List<Pool> CustomPools = new List<Pool> ();
	private readonly string poolsendpoint = "/pools/timed/" + Main.AppUser.country;
        
    
	public delegate void PoolsRefreshed ();

	public static event PoolsRefreshed PoolsUpdate;
    
	//TODO call after login
	public Pools ()
	{
		InitPools ();
	}
	// Use this for initialization
	private void InitPools ()
	{
		RequestUserPools ();
	}

	public void RequestUserPools ()
	{
		API.GetAll (Main.Settings.apis ["leaderpay"] + poolsendpoint, RequestUserPoolsCallback);
	}
    
	public void RequestUserPoolsCallback (BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
	{
		if(res.StatusCode!=200 && res.StatusCode!=304){
			MessagePanel.Instance.animMessage(res.DataAsText);
			return;
		}
		Debug.Log (res.DataAsText);
		TimedPools = LitJson.JsonMapper.ToObject<List<Pool>> (res.DataAsText);
		SortPools ();
	}
    
	private void SortPools ()
	{
		Dictionary<string,List<Pool>> tempdict = new Dictionary<string,List<Pool>> ();
		foreach (Pool pool in TimedPools) {
			if (pool.roomtype == "Week") {
				WeeklyPool = pool;
			} else if (pool.roomtype == "Season") {
				SeasonPool = pool;
			} else if (pool.roomtype == "Custom") {
				CustomPools.Add (pool);
			} else {
				Debug.Log ("Unexpected room type in pool.");
			}
		}
     if(PoolsUpdate!=null)
		PoolsUpdate ();
        
	}
    
   
}
