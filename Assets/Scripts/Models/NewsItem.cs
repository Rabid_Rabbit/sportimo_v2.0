﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

//The news item class
public class NewsItem {

	// Use this for initialization
	public string id;
	public string photo_url;
	public Dictionary<string,string> title;
	public string html_url="http://www.google.com"; //test_url
    
    public DateTime publishDate;


    public static List<NewsItem> jsonMapper(string json){
        List<NewsItem> News=new List<NewsItem>();
        JsonData data=JsonMapper.ToObject(json);
        for(int i=0;i<data.Count;i++){
            NewsItem item=new NewsItem();
            item.id=(string)data[i]["_id"];
            item.photo_url=(string)data[i]["photo"];
			item.title=JsonMapper.ToObject<Dictionary<string,string>>(data[i]["publication"]["title"].ToJson());
            DateTime.TryParse((string)data[i]["publishDate"],out item.publishDate);
            News.Add(item);
        }
        return News;
    }



}
