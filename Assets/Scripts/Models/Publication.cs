﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Publication {
	
	public string _id;
	public string type;
	public string photo;
//	public List<Tag> tags;
	public publicationTexts publication;
    public DateTime publishDate;
	public DateTime created;
	
}

public class publicationTexts{
	public Dictionary<string,string> title;
//	public Dictionary<string,string> text;
}
