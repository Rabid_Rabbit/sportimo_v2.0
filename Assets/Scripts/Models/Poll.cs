﻿using System;
using System.Collections;
using System.Collections.Generic;

public class Poll{
	public string _id;
	public List<Tag> tags;
	public string img;
	public Dictionary <string,string> text;
	public Sponsor sponsor;
	public DateTime created;
	public int total_votes;
	public List<Answer> answers;
	public string hasAnswered;
	public int hasAlreadyVoted;

	public Poll(){}
}

public class Tag {
	public string _id;
	public Dictionary<string,string> name;
	
	public Tag(){}
}

public class Answer
{
	public string _id;
	public Dictionary<string,string> text;
	public string img;
	public int votes;
	public List<string> voters;
	public int percent;

	public Answer(){}
}

public class Sponsor{
	public string banner;
	public string name;

	public Sponsor(){}
}