﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {
	
	public int gamesPlayed;
	public int  gamesStarted;
	public int minutesPlayed;
	public int goalsTotal;
	public int goalsGameWinning;
	public int goalsOwn;
	public int goalsHeaded;
	public int goalsKicked;
	public int assistsTotal;
	public int assistsGameWinning;
	public int shots;
	public int shotsOnGoal;
	public int crosses;
	public int penaltyKicksShots;
	public int penaltyKicksGoals;
	public int foulsCommitted;
	public int foulsSuffered;
	public int yellowCards;
	public int redCards;
	public int offsides;
	public int cornerKicks;
	public int clears;
	public int goalMouthBlocks;
	public int touchesTotal;
	public int touchesPasses;
	public int touchesInterceptions;
	public int touchesBlocks;
	//public int? tackles;
	public int overtimeShots;
	public int overtimeGoals;
	public int overtimeAssists;
	public int kickedGoals;
	public int headedGoals;
	
	public int ownGoalds;
	
	public int gameWinningGOals;
	
	public int goals;
}
