﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

//The match class
public class ScheduledMatch
{

	public string _id;
	public string sport;
	public Team home_team;
	public Team away_team;
	public Team home;
	public Team away;
	public DateTime start;

	public GuruStats guruStats;
	public DateTime eventdate;
	public string color;
	public Competion competition;
	public int home_score;
	public int away_score;
	public int homescore;
	public int awayscore;
	public bool completed;
	public int time;
	public int state;
//	public JsonData stats;
	public List<JsonData> stats;
	public List<Segment> timeline;
	public bool isTimeCounting;
	public string[] recentForm;
	public Settings settings;
	public List <string> headtohead;

	public int GetStat (string id, string stat)
	{
		foreach (JsonData jd in stats) {
			if (jd.Keys.Contains ("id"))
			if (jd ["id"].ToString () == id) {
				if (jd.Keys.Contains(stat))
					return (int)jd [stat];
				break;
			}
		}

		return 0;
	}

	public int GetStatByName (string name, string stat)
	{
		foreach (JsonData jd in stats) {
			if (jd.Keys.Contains ("name"))
			if (jd ["name"].ToString () == name) {
				if (jd.Keys.Contains (stat))
					return int.Parse (jd [stat].ToString ());
				break;
			}
		}
		
		return -1;
	}

}
