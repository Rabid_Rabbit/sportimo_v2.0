﻿using UnityEngine;

using System.Collections.Generic;
using System.Collections;

public class MainTwitter : MonoBehaviour
{

	public GameObject tweetGrid;
	public GameObject tweetGridPrefab;
	public GameObject tweetScrollView;
	public string hashtag = "#";
	public UILabel hashtaglabel;
	public static bool _promoActive;
	public bool promoActive;
	//{
//		get{ return _promoActive;}
//		set{_promoActive = value;}
//	}
	public string promoText;
	public string PromoCode;
	public string PromoURL;
	public string AdTarget;
	public string AdImage;
	public string AdTitle;
	public string AdText;
	public string BannerTitle;
	public string BannerText;
	public string BannerTarget;
	public string BannerImage;
	public string BannerButton;
	public string BannerTitleRu;
	public string BannerTextRu;
	public string BannerButtonRu;
	UIScrollView mScrollView;
	UIPanel tweetUIPanel;
	bool loadingTweets = false;
	bool showingLoader = false;

	void OnEnable ()
	{
	
		mScrollView = tweetScrollView.GetComponent<UIScrollView> ();
		tweetUIPanel = NGUITools.FindInParents<UIPanel> (tweetScrollView);

		mScrollView.onDragFinished += reloadTweets;
		mScrollView.onDragStarted += OnDrag;
	}

	void OnDisable ()
	{
		mScrollView.onDragFinished -= reloadTweets;
	}

	void OnDestroy ()
	{
		mScrollView.onDragFinished -= reloadTweets;
	}

	bool dragging = false;

	void OnDrag ()
	{
		dragging = true;
	}

	void Update ()
	{
		if (dragging) {
			float pos = tweetUIPanel.CalculateConstrainOffset (mScrollView.bounds.min, mScrollView.bounds.max).y;
			if (pos > 0) {
				if (!showingLoader)
					showingLoader = true;
				
//				Loader.showTwittertLoader ();
			//	Loader.scrubTo ("loaderAnim", pos / 250);

			}
		}
	}

	public void reloadTweets ()
	{
		dragging = false;

		if (!loadingTweets && tweetUIPanel.CalculateConstrainOffset (mScrollView.bounds.min, mScrollView.bounds.max).y > 250) {
			loadingTweets = true;
			getTweets ();

		}
	
	}

	// Use this for initialization
	public void getTweets ()
	{
		foreach (Transform child in tweetGrid.transform) {
			if (child.gameObject.activeSelf)
				Destroy (child.gameObject);
		}
		//TwitterAPI.instance.SearchTwitter(hashtag, ResultsCallBack);
		TwitterAPI.instance.SearchTwitter ("@fclm_official", ResultsCallBack);
		hashtaglabel.text = hashtag;

	}

	void ResultsCallBack (List<TwitterData> tweetList)
	{
		int limit;
		if (tweetList.Count > 20) {
			limit = 20;
		} else {
			limit = tweetList.Count;
		}


		for (int i = 0; i < limit; i++) {

			TwitterData twitterData = tweetList [i] as TwitterData;


			GameObject item = NGUITools.AddChild (tweetGrid, tweetGridPrefab);
			item.name = twitterData.screenName;
			BedBugTools.loadImageOnUITextureAndResize (item.transform.Find ("pic").GetComponent<UITexture> (), twitterData.profileImageUrl, 128, 128);

			string tweetText = twitterData.tweetText;


			//item.GetComponent<tweetUrlBtn> ().artUrl = twitterData.articleUrl;

			item.transform.Find ("tweet").GetComponent<UILabel> ().text = tweetText;
			item.transform.Find ("tweetname").GetComponent<UILabel> ().text = twitterData.screenName;

			NGUITools.SetActive (item, true);
			tweetGrid.GetComponent<UIGrid> ().Reposition ();

		}

		tweetGrid.GetComponent<UIGrid> ().Reposition ();
		tweetScrollView.GetComponent<UIScrollView> ().ResetPosition ();

		//Loader.stopAndHideTwitterLoader ();
		showingLoader = false;
		loadingTweets = false;

	}

	bool clickedad;

	
		

}
