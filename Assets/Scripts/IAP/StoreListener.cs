﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using System.Security.Cryptography;
using System.Text;
using LitJson;
using System.Linq;
using System;

public class StoreListener :MonoBehaviour,IStoreListener {

	private IStoreController controller;
    private IExtensionProvider extensions;
	protected string publickey="pT8jvUmRGt1uWqoYemi0";

	protected string privatekey="8cwFbVFGZx7CGcgSTBII";
	private CrossPlatformValidator validator;
	private static StoreListener instance = null;

	public static StoreListener Instance {
		get {
			if (instance == null)
				instance = FindObjectOfType (typeof(StoreListener)) as StoreListener;
		
			if (instance == null) {
				instance = new GameObject ().AddComponent<StoreListener> ();
				instance.name = "StoreListener";
			}


			
			return instance;
		}
	}

	void Awake ()
	{
		instance = this;
		DontDestroyOnLoad(this.gameObject);
        InitializeStore();
    }

    public void InitializeStore () {
     if(controller==null){
		    var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.AddProduct("com.gup.basicweekly", ProductType.Subscription);
		//builder.AddProduct("com.gup.eliteweekly", ProductType.Subscription);
		validator=new CrossPlatformValidator(GooglePlayTangle.Data(),AppleTangle.Data(),Application.bundleIdentifier);
        UnityPurchasing.Initialize (this, builder);
	 }
	 else{
		 return;
	 }
    }

    /// <summary>
    /// Called when Unity IAP is ready to make purchases.
    /// </summary>
    public void OnInitialized (IStoreController controller, IExtensionProvider extensions)
    {
		Debug.Log("initialized IAP");
        this.controller = controller;
        this.extensions = extensions;
	
    }

    /// <summary>
    /// Called when Unity IAP encounters an unrecoverable initialization error.
    ///
    /// Note that this will not be called if Internet is unavailable; Unity IAP
    /// will attempt initialization until it becomes available.
    /// </summary>
    public void OnInitializeFailed (InitializationFailureReason error)
    {
		Debug.Log(error.ToString());
    }

	public bool IsInitialized()
    {
            // Only say we are initialized if both the Purchasing references are set.
    	return controller != null && extensions != null;
    }
	public void InitiatePurchase(string productId){
		if(IsInitialized())
		controller.InitiatePurchase(productId);
		else{
			InitializeStore();
		}
	}

	public bool ValidateReceipt(){
		//#if UNITY_EDITOR
		//return false;
		//#endif
		Debug.Log("running validation");

		if(controller.products.WithStoreSpecificID("com.gup.basicweekly").hasReceipt){
			try{
			IPurchaseReceipt[] result = validator.Validate(controller.products.WithStoreSpecificID("com.gup.basicweekly").receipt);
			  foreach (IPurchaseReceipt productReceipt in result) {
           		 Debug.Log(productReceipt.productID);
           		 Debug.Log(productReceipt.purchaseDate);
            	 Debug.Log(productReceipt.transactionID);
        	}
			result.OrderByDescending(d=>d.purchaseDate);
			if(result[0].productID=="com.gup.basicweekly"){
				Main.AppUser.subscription.start=result[0].purchaseDate;
				Main.AppUser.subscription.end=result[0].purchaseDate.AddDays(7);
				Main.AppUser.subscription.provider="iap";
				#if UNITY_ANDROID
				GooglePlayReceipt google = result[0] as GooglePlayReceipt;

                    if (google!=null)
				Main.AppUser.subscription.receiptid=google.purchaseToken;
				#elif UNITY_IOS
				AppleInAppPurchaseReceipt apple = result[0] as AppleInAppPurchaseReceipt;
				if(apple!=null)
				Main.AppUser.subscription.receiptid=apple.originalTransactionIdentifier;
				#endif

				if(Main.AppUser.subscription.end>DateTime.UtcNow){
					Main.AppUser.subscription.status="basic";
					Main.AppUser.subscription.state="active";

					return true;
				}
				else{
					Main.AppUser.subscription.status="free";
					Main.AppUser.subscription.state="inactive";
					return false;
				}
			}
			//webcall for current date
			//if active bypass this scene
			}
			catch(IAPSecurityException){
				Debug.Log("invalid receipt");
				return false;
			}
			return false;		
		}
        else {
            return false;
        }
    }
    /// <summary>
    /// Called when a purchase completes.
    ///
    /// May be called at any time after OnInitialized().
    /// </summary>
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (String.Equals(args.purchasedProduct.definition.id, "com.gup.basicweekly", StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

            try
            {
                IPurchaseReceipt[] result = validator.Validate(controller.products.WithStoreSpecificID("com.gup.basicweekly").receipt);
                foreach (IPurchaseReceipt productReceipt in result)
                {
                    Debug.Log(productReceipt.productID);
                    Debug.Log(productReceipt.purchaseDate);
                    Debug.Log(productReceipt.transactionID);
                }
                result.OrderByDescending(d => d.purchaseDate);

                if (result[0].productID == "com.gup.basicweekly")
                {
                    Main.AppUser.subscription.start = result[0].purchaseDate;
                    Main.AppUser.subscription.end = result[0].purchaseDate.AddDays(7);
                    Main.AppUser.subscription.provider = "IAP";
                    Main.AppUser.customerType = "paid";

#if UNITY_ANDROID && !UNITY_EDITOR
                    GooglePlayReceipt google = result[0] as GooglePlayReceipt;
                    if (google != null)
                        Main.AppUser.subscriptionContractId = google.purchaseToken;
#elif UNITY_IOS
				        AppleInAppPurchaseReceipt apple = result[0] as AppleInAppPurchaseReceipt;
				        if(apple!=null)
				        Main.AppUser.subscriptionContractId = apple.originalTransactionIdentifier;
#endif
                    UpdateCustomerType(Main.AppUser.customerType, Main.AppUser.subscriptionContractId, (BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res) =>
                     {
                         if (res.IsSuccess)
                         {
                             Debug.Log("success");
                         }
                     });
                }

            }
            catch (IAPSecurityException)
            {
                Debug.Log("invalid receipt");
#if UNITY_EDITOR
                Main.AppUser.subscriptionContractId = "editor_test";
                Main.AppUser.customerType = "paid";
                UpdateCustomerType(Main.AppUser.customerType, Main.AppUser.subscriptionContractId, (BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res) =>
                {
                    if (res.IsSuccess)
                    {
                        confirmationReload();
                    }
                });
#endif
            }
        }
        // Or ... an unknown product has been purchased by this user. Fill in additional products here....
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }

        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }

    private static void confirmationReload()
    {
        ConfirmationModalController.Confirm(I2.Loc.LocalizationManager.GetTermTranslation("_subscribed_reload",true,23), null,null,null, (userSelection) =>
        {
            if (userSelection == "Btn1")
            {
                
                UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
            }           
        });
    }


    public void UpdateCustomerType(string customerType, string purchaseid, BestHTTP.OnRequestFinishedDelegate onFinish)
{
    Debug.Log("Updating Customer!");

    //Debug.Log (pushSets.ToJson ());
    Hashtable pushUpdate = new Hashtable();
    pushUpdate["customerType"] = customerType;
    pushUpdate["id"] = purchaseid;

    //		pushUpdate.Add ("pushsetting", pushSets);

    BedbugRest.API.PutOne(Main.Settings.apis["users"] + "/" + Main.AppUser._id, pushUpdate, onFinish);
    //}
}
/// <summary>
/// Called when a purchase fails.
/// </summary>
public void OnPurchaseFailed (Product i, PurchaseFailureReason p)
    {
		Debug.Log(i.metadata.localizedTitle);
		Debug.Log(p.ToString());
    }


	public string CalculateSignature(Dictionary<object,object> table){
		string signature=publickey+":";
		string concatstring="";
		foreach(object obj in table.Values){
			if(obj.ToString()=="True" || obj.ToString()=="False"){
			concatstring+=obj.ToString().ToLower();
			}
			else{
			concatstring+=obj.ToString();
			}
		}
		Debug.Log(concatstring);
		HMACSHA256 hmac=new HMACSHA256(new UTF8Encoding().GetBytes(privatekey));
		signature+=BitConverter.ToString(hmac.ComputeHash(new UTF8Encoding().GetBytes(concatstring))).Replace("-","").ToLower();
		Debug.Log(signature);
		return signature;
	}


	public string CalculateSignature(string table){
		string signature=publickey+":";
		string concatstring=table;	
		Debug.Log(table);
		HMACSHA256 hmac=new HMACSHA256(new UTF8Encoding().GetBytes(privatekey));
		signature+=BitConverter.ToString(hmac.ComputeHash(new UTF8Encoding().GetBytes(concatstring))).Replace("-","").ToLower();
		Debug.Log(signature);
		return signature;
	}
}
