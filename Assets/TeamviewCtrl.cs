﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BedbugRest;
using LitJson;
using I2.Loc;
using System.Linq;
using BestHTTP;
using System;

public class TeamviewCtrl : View
{

    // Use this for initialization
    public UILabel index;
    public UILabel name;
    public UILabel played;
    public UILabel won;
    public UILabel draw;
    public UILabel lost;

    public UILabel goals;

    public UILabel points;

    public UIGrid grid;

	public GameObject playerSortTabs;
    public UIScrollView playersScroll;
	public UIScrollView teamStats;
	public GameObject playerLine;
	public GameObject statsLine;

    public GameObject playerprefab;
    private string teams_endpoint = "/teams";

    private Team team;

	// Stats
	public UILabel gamesPlayed;
	public UILabel totalGoals;
	public UILabel shotsOnGoal;
	public UILabel crosses;
	public UILabel corners;
	public UILabel offsides;
	public UILabel penalties;
	public UILabel fouls;
	public UILabel yellows;
	public UILabel reds;

	// UNI Select
	[Space(10)]
	public GameObject uniPopSelect;
	public UILabel selectLabel;
	public bool useTwoColors = false;
	private bool useDarc = false;
	private bool uniPopOpen = false;
	public UICenterOnChild center;
	public UIGrid selectTable;
	public UIScrollView selectScroll;
	public UIWrapContent wrap;
	public GameObject lightPrefab;
	public GameObject darkPrefab;
	public GameObject topPrefab;
	public GameObject bottomPrefab;
	GameObject uniTeam;


	public void onTransitionInStart(viewData _viewData)
    {
//		Debug.Log("[Team] " + _viewData.data.ToString());

		if (!_viewData.useCache) {
			ResetView();
			openPlayers ();
			Loader.Visible (true);

			Coach.coachMessage ( LocalizationManager.GetTermTranslation( "tip2", true, 28, true), 27f, 5f, 1.5f, true, "tip2");

			// CLOSE ALL LINE AND OPEN THE PRESSED ONE
			for (int i = 0; i < tabsLines.Length; i++) {
				NGUITools.SetActive (tabsLines [i], false);
			}

			API.GetOneByID (Main.Settings.apis ["data"] + teams_endpoint, _viewData.data.ToString () + "/full", OnGetTeamInfo);
		}
    }

    public void onTransitionOutStart()
    {
       
    }

	public void ResetView(){
		foreach (Transform trans in grid.GetChildList())
		{
			if (trans.gameObject.activeSelf)
			{
				NGUITools.Destroy(trans.gameObject);
			}
		}
		index.text = "-";
		played.text = "-";
		won.text = "-";
		draw.text = "-";
		lost.text = "-";
		goals.text = "-";
		points.text = "-";
		name.text = "";

	}

    private void OnGetTeamInfo(BestHTTP.HTTPRequest req, BestHTTP.HTTPResponse res)
    {




        if (res.StatusCode != 200 && res.StatusCode != 304)
        {
            MessagePanel.Instance.animMessage(res.DataAsText);
            return;
        }

        if (string.IsNullOrEmpty(res.DataAsText))
        {
            Debug.Log("something went wrong with our request to the server");
            Debug.Log(req.Uri.ToString());
        }


        team = JsonMapper.ToObject<Team>(res.DataAsText);



		if (Main.AppUser.favoriteteams != null && Main.AppUser.favoriteteams.Contains(team.id))
        {
            NGUITools.SetActive(heartIcon, true);
			isFav = true;
        }
        else
        {
            NGUITools.SetActive(heartIcon, false);
			isFav = false;
        }

		name.text = LocalizationManager.FixRTL_IfNeeded( team.name[LocalizationManager.CurrentLanguageCode]);

        if (team.standing != null)
        {

            index.text = team.standing.rank.ToString();
            played.text = team.standing.gamesPlayed.ToString();
            won.text = team.standing.wins.ToString();
            draw.text = team.standing.ties.ToString();
            lost.text = team.standing.losses.ToString();
            goals.text = (team.standing.goalsFor - team.standing.goalsAgainst).ToString();
            points.text = team.standing.points.ToString();
        }
        else{
            name.text="No data available";
        }

		// Players Filter as All

		tabSelect (tabsLines [0].transform.parent.gameObject);

        PopulatePlayers("All");

		// STATS
		if (team.stats != null) {

			// games played
			gamesPlayed.text = StatsHelper (team.stats, "gamesPlayed").ToString();
			// Total Goals
			totalGoals.text = StatsHelper (team.stats, "Goal").ToString();
			// Shots on Goal
			shotsOnGoal.text = StatsHelper (team.stats, "Shot_On_Goal").ToString();
			// Crosses
			crosses.text = StatsHelper (team.stats, "Crosses").ToString();
			// Corners
			corners.text = StatsHelper (team.stats, "Corner").ToString();
			// Offsides
			offsides.text = StatsHelper (team.stats, "Offside").ToString();
			// Penalties
			penalties.text = StatsHelper (team.stats, "Penalty").ToString();
			// Fouls
			fouls.text = StatsHelper (team.stats, "Foul").ToString();
			// Yellows
			yellows.text = StatsHelper (team.stats, "Yellow").ToString();
			// Reds
			reds.text = StatsHelper (team.stats, "Red").ToString(); 


		}

    }

	public int StatsHelper (JsonData StatsList, String Stat)
	{
		if (StatsList != null)
			if (StatsList.Keys.Contains (Stat))
				return (int)StatsList [Stat];
		
		return 0;
	}

    private void PopulatePlayers(string selection)
    {
        //		Debug.Log(selection);
        foreach (Transform trans in grid.GetChildList())
        {
            if (trans.gameObject.activeSelf)
            {
                NGUITools.Destroy(trans.gameObject);
            }
        }

		List<Player> sortedPlayers = team.players;//.OrderBy (p => p.position).ToList ();

        if (team.players.Count > 0)
        {
            if (selection == "All")
            {

				foreach (Player player in sortedPlayers )
                {

                    GameObject playa = NGUITools.AddChild(grid.gameObject, playerprefab);
                    NGUITools.SetActive(playa, true);

                    playersScroll.ResetPosition();

                    playa.name = player._id;
                    playerctrl ctrl = playa.GetComponent<playerctrl>();
					ctrl.name.text = LocalizationManager.FixRTL_IfNeeded( player.name[LocalizationManager.CurrentLanguageCode]);
                     if (player.position == "Defender")
                    {
                        ctrl.name.text += " " + "(DF)";
                    }
                    else if (player.position == "Forward")
                    {
                        ctrl.name.text += " " + "(FW)";
                    }
                    else if (player.position == "Goalkeeper")
                    {
                        ctrl.name.text += " " + "(GK)";
                    }
                    else if (player.position == "Midfielder")
                    {
                        ctrl.name.text += " " + "(MF)";
                    }
                    if (player.personalData != null)
                    {
                        if (player.personalData.height != null)
                            ctrl.height.text = player.personalData.height.centimeters.ToString();
                        if (player.personalData.weight != null)
                        {
                            ctrl.weight.text = player.personalData.weight.kilograms.ToString();
                            if (player.personalData.birth != null)
                            {
                                ctrl.born.text = player.personalData.birth.birthDate["full"].ToString();
                            }
                        }
                    }
                }
            }
            else if (selection == "DEF")
            {
               foreach (Player player in team.players.Where(player => player.position == "Defender").ToList())
                {
                    GameObject playa = NGUITools.AddChild(grid.gameObject, playerprefab);
                    NGUITools.SetActive(playa, true);

                    playersScroll.ResetPosition();

                    playa.name = player._id;
                    playerctrl ctrl = playa.GetComponent<playerctrl>();
					ctrl.name.text = LocalizationManager.FixRTL_IfNeeded( player.name[LocalizationManager.CurrentLanguageCode]);
                    ctrl.name.text += " " + "(DF)";

                    if (player.personalData != null)
                    {
                        if (player.personalData.height != null)
                            ctrl.height.text = player.personalData.height.centimeters.ToString();
                        if (player.personalData.weight != null)
                        {
                            ctrl.weight.text = player.personalData.weight.kilograms.ToString();
                            if (player.personalData.birth != null)
                            {
                                ctrl.born.text = player.personalData.birth.birthDate["full"].ToString();
                            }
                        }
                    }
                }
            }
            else if (selection == "FW")
            {
                foreach (Player player in team.players.Where(player => player.position == "Forward"))
                {
                    GameObject playa = NGUITools.AddChild(grid.gameObject, playerprefab);
                    NGUITools.SetActive(playa, true);

                    playersScroll.ResetPosition();

                    playa.name = player._id;
                    playerctrl ctrl = playa.GetComponent<playerctrl>();
					ctrl.name.text = LocalizationManager.FixRTL_IfNeeded( player.name[LocalizationManager.CurrentLanguageCode]);
                 
                    ctrl.name.text += " " + "(FW)";
                
                    if (player.personalData != null)
                    {
                        if (player.personalData.height != null)
                            ctrl.height.text = player.personalData.height.centimeters.ToString();
                        if (player.personalData.weight != null)
                        {
                            ctrl.weight.text = player.personalData.weight.kilograms.ToString();
                            if (player.personalData.birth != null)
                            {
                                ctrl.born.text = player.personalData.birth.birthDate["full"].ToString();
                            }
                        }
                    }
                }
            }
            else if (selection == "GK")
            {
                foreach (Player player in team.players.Where(player => player.position == "Goalkeeper").ToList())
                {
                    GameObject playa = NGUITools.AddChild(grid.gameObject, playerprefab);
                    NGUITools.SetActive(playa, true);

                    playersScroll.ResetPosition();

                    playa.name = player._id;
                    playerctrl ctrl = playa.GetComponent<playerctrl>();
					ctrl.name.text = LocalizationManager.FixRTL_IfNeeded( player.name[LocalizationManager.CurrentLanguageCode]);
                        ctrl.name.text += " " + "(GK)";
                  
                    if (player.personalData != null)
                    {
                        if (player.personalData.height != null)
                            ctrl.height.text = player.personalData.height.centimeters.ToString();
                        if (player.personalData.weight != null)
                        {
                            ctrl.weight.text = player.personalData.weight.kilograms.ToString();
                            if (player.personalData.birth != null)
                            {
                                ctrl.born.text = player.personalData.birth.birthDate["full"].ToString();
                            }
                        }
                    }
                }
            }
            else if (selection == "MID")
            {
                foreach (Player player in team.players.Where(player => player.position == "Midfielder").ToList())
                {
                    GameObject playa = NGUITools.AddChild(grid.gameObject, playerprefab);
                    NGUITools.SetActive(playa, true);

                    playersScroll.ResetPosition();

                    playa.name = player._id;
                    playerctrl ctrl = playa.GetComponent<playerctrl>();
					ctrl.name.text = LocalizationManager.FixRTL_IfNeeded( player.name[LocalizationManager.CurrentLanguageCode]);
                    ctrl.name.text += " " + "(MF)";
                    
                    if (player.personalData != null)
                    {
                        if (player.personalData.height != null)
                            ctrl.height.text = player.personalData.height.centimeters.ToString();
                        if (player.personalData.weight != null)
                        {
                            ctrl.weight.text = player.personalData.weight.kilograms.ToString();
                            if (player.personalData.birth != null)
                            {
                                ctrl.born.text = player.personalData.birth.birthDate["full"].ToString();
                            }
                        }
                    }
                }
            }
        }
        grid.Reposition();
        // scroll.transform.localPosition=new Vector2(0,-62);
        Loader.Visible(false);


    }


    public void OnPlayerClick(GameObject go)
    {
        Main.Instance.openView("playerInfo", go.name, true);

    }
    // Update is called once per frame

    public GameObject[] tabsLines = new GameObject[3];

    public void tabSelect(GameObject lineParent)
    {

        // CLOSE ALL LINE AND OPEN THE PRESSED ONE
        for (int i = 0; i < tabsLines.Length; i++)
        {
            NGUITools.SetActive(tabsLines[i], false);
        }

        NGUITools.SetActive(lineParent.transform.FindChild("line").gameObject, true);
        PopulatePlayers(lineParent.name);
        // REDRAW THE SCROLLVIEW ACCORDING TO LINEPARENT
    }

    // Favorite team
    public GameObject heartIcon;
    public bool isFav = false;
    public void toggleFav()
    {
        if (isFav)
        {
            NGUITools.SetActive(heartIcon, false);
            isFav = false;
            // Message that team is removed from Favorites
			//foreach(string id in Main.AppUser.favoriteteams) {
				//if(id == team._id)
	            Main.AppUser.favoriteteams.RemoveAll(x => x == team._id);
			//}
            Hashtable table = new Hashtable();
            table.Add("favoriteteams", Main.AppUser.favoriteteams);
            API.PutOne(Main.Settings.apis["users"] + "/" + Main.AppUser._id, table, OnFavoritesUpdate);
            // Remove from favorite teams list
        }
        else
        {
            NGUITools.SetActive(heartIcon, true);
            isFav = true;
            
            Main.AppUser.favoriteteams.Add(team._id);
            Hashtable table = new Hashtable();
            table.Add("favoriteteams", Main.AppUser.favoriteteams);
            API.PutOne(Main.Settings.apis["users"] + "/" + Main.AppUser._id, table, OnFavoritesUpdate);
            // Add to favorite teams list
        }

		//foreach (string id in Main.AppUser.favoriteteams)
			//print (id);

    }

    public void OnFavoritesUpdate(HTTPRequest req, HTTPResponse res)
    {
        Debug.Log(res.DataAsText);
		MessagePanel.Instance.animMessage(LocalizationManager.GetTermTranslation("mess_favTeams2", true, 40, true));
    }

	// UniSelect
	public void toggleUniSelect() {
		if (uniPopOpen) {
			uniPopOpen = false;
			TweenAlpha.Begin (uniPopSelect, 0.2f, 0);
		} else {
			uniPopOpen = true;
			TweenAlpha.Begin (uniPopSelect, 0.2f, 1);
		}
	}



	// STATS
	public override void selectionChanged(string selection) {
		base.selectionChanged (selection);

	}

	public void openPlayers() {
		// Close Stats
		TweenAlpha.Begin(statsLine, 0.1f, 0.0f);
		TweenAlpha.Begin(teamStats.gameObject, 0.3f, 0.0f);
		// Open Players
		TweenAlpha.Begin(playerLine, 0.1f, 1.0f);
		TweenAlpha.Begin(playerSortTabs, 0.3f, 1.0f);
		TweenAlpha.Begin(playersScroll.gameObject, 0.5f, 1.0f);
	}

	public void openStats() {
		// Open Stats
		TweenAlpha.Begin(statsLine, 0.1f, 1.0f);
		TweenAlpha.Begin(teamStats.gameObject, 0.5f, 1.0f);
		// Close Players
		TweenAlpha.Begin(playerLine, 0.1f, 0.0f);
		TweenAlpha.Begin(playerSortTabs, 0.5f, 0.0f);
		TweenAlpha.Begin(playersScroll.gameObject, 0.3f, 0.0f);
	}
}
