﻿using UnityEngine;
using System;
using System.Collections;
using BedbugRest;

public class UserModalController : MonoBehaviour
{
    public UIWidget mainWidget;
    public GameObject loader;
    public string testId;
    [Space(10)]

    public UITexture
        userPicture;
    public UILabel userName;
    public GameObject backupPicture;
    [Space(10)]
    public UIWidget
        profileWidget;
    public UIWidget interactionsWidget;
    public UIWidget timelineWidget;
    public ButtonStates timelineButtonState;
    public ButtonStates statsButtonState;

    [Space(10)]
    public UIScrollView timelineScroll;
    public UITable timelineTable;
    public GameObject cardviewPrefab;
    public GameObject overallcardviewPrefab;
    
    [Space(10)]
    public GameObject
        BlockButton;
    public GameObject UnBlockButton;
    public static UserModalController _instance;

    public UILabel status_label;
    public UISprite status_sprite;

    public static Action<string> OnModalClose;
    private MatchCtrl MatchInfo;
	public UIWidget modalBg;

    void Awake()
    {
        _instance = this;

        if (!string.IsNullOrEmpty(testId))
            ViewUser(testId, false);
    }

    private ProfileStats _profile;
    private bool interactionsOpen = false;
    private bool timelineOpen = false;
    private string matchid = "";

    

    public static void ViewUser(string uid, bool useViewHooks = true)
    {
        if (useViewHooks)
            Main.onViewChange += _instance.closeModalOnViewChange;

        _instance.resetModal();

        if (Main.AppUser.customerType == "free")
        {
            Debug.Log(Main.AppUser.customerType == "free");
            _instance.timelineButtonState.setState(1); // locked
            _instance.statsButtonState.setState(1);
        }
        else
        {
            _instance.timelineButtonState.setState(2);
            _instance.statsButtonState.setState(0);
        }

        API.GetOne(Main.Settings.apis["users"] + "/" + uid + "/stats", handleUserObject);

        _instance.mainWidget.alpha = 1;
    }

    public static void ViewUserInMatch(string uid, string mid, bool useViewHooks = true)
    {
        if (useViewHooks)
            Main.onViewChange += _instance.closeModalOnViewChange;

        _instance.resetModal();

        if (Main.AppUser.customerType == "free")
        {
            _instance.timelineButtonState.setState(1); // locked
            _instance.statsButtonState.setState(1);
        }
        else
        {
            _instance.timelineButtonState.setState(0); // locked
            _instance.statsButtonState.setState(0);
        }

        _instance.matchid = mid;

        API.GetOne(Main.Settings.apis["users"] + "/" + uid + "/stats", handleUserObject);

        _instance.mainWidget.alpha = 1;
    }

    void resetModal()
    {
        loader.SetActive(true);
        userPicture.mainTexture = null;
        backupPicture.GetComponent<UISprite>().alpha = 0;
        userName.text = "";
        BlockButton.SetActive(true);
        UnBlockButton.SetActive(false);
        profileWidget.alpha = 0;
        interactionsWidget.alpha = 0;

		TweenHeight.Begin (modalBg, 0.0f, 422 ); 
    }

    public void openInteractions()
    {
        interactionsOpen = true;
        profileWidget.gameObject.SetActive(false);
        interactionsWidget.alpha = 1;
		TweenHeight.Begin (modalBg, .1f, 300 );
    }

    public void openTimeline()
    {
        foreach (Transform tr in timelineTable.GetChildList())
            Destroy(tr.gameObject);

        timelineScroll.ResetPosition();
        var temptext = I2.Loc.LocalizationManager.GetTermTranslation("_conversion_timeline", false);
        temptext = string.Format(temptext, _profile.user.username);


        if (Main.isPaidCustomer(I2.Loc.LocalizationManager.FixRTL_IfNeeded(temptext, 25, true)))
        {
            if (string.IsNullOrEmpty(matchid)) return;

            timelineOpen = true;
            profileWidget.gameObject.SetActive(false);
            loader.SetActive(true);
            API.GetOne(Main.Settings.apis["data"] + "/match/" + matchid + "/user/" + _profile.user._id + "/gamecards", handeGameCardsResponse);
        }
    }

    void handeGameCardsResponse(BestHTTP.HTTPRequest originalRequest, BestHTTP.HTTPResponse response)
    {
        if (MatchInfo == null)
            MatchInfo = GameObject.FindObjectOfType<MatchCtrl>();

        loader.SetActive(false);

        if (response.IsSuccess)
        {
            
            LiveMatch UserTimeline = LitJson.JsonMapper.ToObject<LiveMatch>(response.DataAsText);
            Debug.Log(response.DataAsText);
            
            foreach (PlayCard card in UserTimeline.playedCards)
            {
                GameObject Card;
                if (card.cardType == "Instant" || card.cardType == "PresetInstant")
                    Card = NGUITools.AddChild(timelineTable.gameObject, cardviewPrefab);
                else 
                    Card = NGUITools.AddChild(timelineTable.gameObject, overallcardviewPrefab);
                
                NGUITools.SetActive(Card, true);

                if (card.status > 0)
                    Card.GetComponent<Animation>().Play("cardshow1");

                if (Card != null)
                {
                    if (card.cardType == "PresetInstant" && card.status == 0)
                        Card.name = card.minute.ToString();
                    else
                        Card.name = MatchCtrl.GetObjectName(card.minute.ToString(),DateTime.Parse(card.creationTime));

                    cardMatchCtrl ctrl = Card.GetComponent<cardMatchCtrl>();
                    UIDragScrollView drag = Card.GetComponent<UIDragScrollView>();

                    drag.scrollView = timelineScroll;

                    ctrl.card = card;
					ctrl.home_logo = MatchInfo.liveMatch.matchData.home_team.logo;
					ctrl.away_logo = MatchInfo.liveMatch.matchData.away_team.logo;
                    ctrl.Init();
                }

                timelineTable.repositionNow = true;
              
            }
            StartCoroutine(resetTimelinePositions());
           
        }
        else 
            MessagePanel.Instance.animMessage(response.DataAsText);
        

        TweenAlpha.Begin(timelineWidget.gameObject, 0.5f, 1.0f);
    }

    private IEnumerator resetTimelinePositions()
    {
        yield return new WaitForEndOfFrame();
        
        timelineTable.Reposition();
        timelineScroll.ResetPosition();
    }

    public void openStats()
    {
        if (Main.isPaidCustomer(I2.Loc.LocalizationManager.GetTermTranslation("_conversion_stats", true, 20)))
            Main.Instance.openView("userview", _profile.user._id);
    }

    public void blockUser()
    {
        loader.SetActive(true);
        profileWidget.alpha = 0;
        API.GetOne(Main.Settings.apis["users"] + "/" + Main.AppUser._id + "/block/" + _profile.user._id + "/true", handeBlockResponse);
    }

    public void unblockUser()
    {
        loader.SetActive(true);
        profileWidget.alpha = 0;
        API.GetOne(Main.Settings.apis["users"] + "/" + Main.AppUser._id + "/block/" + _profile.user._id + "/false", handeBlockResponse);
    }

    void handeBlockResponse(BestHTTP.HTTPRequest originalRequest, BestHTTP.HTTPResponse response)
    {
        loader.SetActive(false);

        if (response.IsSuccess)
        {
            LitJson.JsonData res = LitJson.JsonMapper.ToObject<LitJson.JsonData>(response.DataAsText);
            string blockState = (string)res["blocked"];
            if (blockState == "true")
            {
                BlockButton.SetActive(false);
                UnBlockButton.SetActive(true);
                Main.AppUser.blockedusers.Add(_profile.user._id);
            }
            else {
                BlockButton.SetActive(true);
                UnBlockButton.SetActive(false);
                Main.AppUser.blockedusers.Remove(_profile.user._id);
            }
        }
        else {
            MessagePanel.Instance.animMessage(response.DataAsText);
        }

        TweenAlpha.Begin(profileWidget.gameObject, 0.5f, 1.0f);
    }

    public void closeModal()
    {
		TweenHeight.Begin (modalBg, .0f, 422 );

        if (interactionsOpen)
        {
            interactionsWidget.alpha = 0;
            profileWidget.gameObject.SetActive(true);
            interactionsOpen = false;
            return;
        }

        if (timelineOpen)
        {
            timelineWidget.alpha = 0;
            profileWidget.gameObject.SetActive(true);
            timelineOpen = false;
            return;
        }

        mainWidget.alpha = 0;
        resetModal();

        if (OnModalClose != null && _profile != null)
            OnModalClose(_profile.user._id);


    }

    void consumeProfile()
    {
        Debug.Log(_profile.user.isOnline);
        if (_profile.user.isOnline)
        {
            status_label.text = I2.Loc.LocalizationManager.GetTermTranslation("_online", true);
            status_sprite.color = new Color32(0x00, 0x9E, 0x0E, 0xFF);
        }
        else
        {
            status_label.text = I2.Loc.LocalizationManager.GetTermTranslation("_offline", true);
            status_sprite.color = new Color32(0x9E, 0x00, 0x00, 0xFF);
        }
        // Set the image
        if (!string.IsNullOrEmpty(_profile.user.picture))
            BedBugTools.loadImageOnUITextureAndResize(userPicture, _profile.user.picture, 128, 128, FadeInOnLoad, 0);
        else
            TweenAlpha.Begin(backupPicture, 0.5f, 1.0f);

        // The username
        userName.text = _profile.user.username;

        // Set block status
        if (Main.AppUser.blockedusers.Contains(_profile.user._id))
        {
            BlockButton.SetActive(false);
            UnBlockButton.SetActive(true);
        }
        else {
            BlockButton.SetActive(true);
            UnBlockButton.SetActive(false);
        }

        TweenAlpha.Begin(profileWidget.gameObject, 0.5f, 1.0f);
    }

    private void FadeInOnLoad(UITexture texture)
    {

        TweenAlpha.Begin(texture.gameObject, 0.5f, 1.0f);
    }

    static void handleUserObject(BestHTTP.HTTPRequest originalRequest, BestHTTP.HTTPResponse response)
    {
        _instance.loader.SetActive(false);
        Debug.Log(response.DataAsText);
        if (response.IsSuccess)
        {
            _instance._profile = LitJson.JsonMapper.ToObject<ProfileStats>(response.DataAsText);
            _instance.consumeProfile();
        }
        else {
            MessagePanel.Instance.animMessage(response.DataAsText);
            _instance.closeModal();
        }
    }

    private void closeModalOnViewChange()
    {

        Main.onViewChange -= closeModalOnViewChange;

        interactionsWidget.alpha = 0;
        profileWidget.gameObject.SetActive(true);
        interactionsOpen = false;

        mainWidget.alpha = 0;
        resetModal();

        if (OnModalClose != null)
            OnModalClose(_profile.user._id);
    }

    public void responseSelected(string responseID)
    {
        Debug.Log(responseID);

        simpleUser sender = new simpleUser();
        sender._id = Main.AppUser._id;
        sender.picture = Main.AppUser.picture;
        sender.username = Main.AppUser.username;

        simpleUser recipient = new simpleUser();
        recipient._id = _profile.user._id;
        recipient.picture = _profile.user.picture;
        recipient.username = _profile.user.username;

        interactionMessageController.TermTaunt(sender, recipient, responseID);

        closeModal();

    }
}
