﻿using UnityEngine;
using System.Collections;

public class Loader : MonoBehaviour {
	public UITweener FadeInOut;
	public UITweener FadeOut;

	private static Loader _instance;
	public static Loader Instance{
		get{
			if(_instance == null)
				_instance = GameObject.FindObjectOfType(typeof(Loader)) as Loader;

			return _instance;
		}
	}

	public static void Visible(bool state){

		if (state) {
			//Instance.FadeInOut.ResetToBeginning ();
			//Instance.FadeInOut.PlayForward ();
			TweenAlpha.Begin (Instance.gameObject, 0.5f, 1);
		} else  {
			//Instance.FadeInOut.ResetToBeginning ();
			//Instance.FadeInOut.PlayReverse ();
			TweenAlpha.Begin (Instance.gameObject, 0.5f, 0);
		}
	}
}
